<?php

/*
 * This file contain set of core php functions commonly used throughout the application.
 */

/*
 * Common function used to return response on failure.
 */

function failureResponse($errorMsg = "Error Occured!!") {
    $responseData['message'] = $errorMsg;
    $response = ['responseCode' => FAILURE_RESPONSE_CODE, 'responseData' => $responseData];
    header("Content-Type: application/json");
    echo json_encode($response);
    exit;
}

/*
 * Common function used to return response on success.
 */

function successResponse($responseData = []) {
    $response = ['responseCode' => SUCCESS_RESPONSE_CODE, 'responseData' => $responseData];
    header("Content-Type: application/json");
    echo json_encode($response);
    exit;
}

/*
 * Common function used to return response on both success & failure.
 */

function sendResponse($responseCode = FAILURE_RESPONSE_CODE, $responseData = []) {
    $response = ['responseCode' => $responseCode, 'responseData' => $responseData];
    header("Content-Type: application/json");
    echo json_encode($response);
    exit;
}

function encryptor($action, $string) {

    $output = false;
    $encrypt_method = "AES-256-CBC";
    //pls set your unique hashing key
    $secret_key = 'gmoverez';
    $secret_iv = 'uk';
    // hash
    $key = hash('sha256', $secret_key);
    // iv - encrypt method AES-256-CBC expects 16 bytes - else you will get a warning
    $iv = substr(hash('sha256', $secret_iv), 0, 16);
    //do the encyption given text/string/number
    if ($action == 'encrypt') {
        $output = openssl_encrypt($string, $encrypt_method, $key, 0, $iv);
        $output = base64_encode($output);
    } else if ($action == 'decrypt') {
        //decrypt the given text/string/number
        $output = openssl_decrypt(base64_decode($string), $encrypt_method, $key, 0, $iv);
    }
    return $output;
}

// Encode & Return id
function encodeID($id) {
    if (function_exists('encryptor')) {
        return encryptor('encrypt', $id);
    }
    return false;
}

/*
 * Decode & Return id
 */

function decodeID($encodedID) {
    if (function_exists('encryptor')) {
        return encryptor('decrypt', $encodedID);
    }
    return false;
}

/*
 * Customer function to encrypt data if Hashids not found
 */

function getFirstErrorXXX($errors) {
    $errMsg = "Error Occured!!";
    try {
        $errors = array_shift($errors);
        $errMsg = array_shift($errors);
    } catch (\Exception $ex) {
        
    }
    return $errMsg;
}

function getFirstError($errors) {
    if (is_array($errors)) {
        return getFirstError(array_shift($errors));
    } else {
        return $errors;
    }
    return false;
}

function makeSeoUrl($url) {
    if ($url) {
        $url = trim($url);
        $value = preg_replace("![^a-z0-9]+!i", "-", $url);
        $value = trim($value, "-");
        return strtolower($value);
    }
}

function scheduleDateTime($datetime) {
    if (!empty($datetime) && $datetime != "" && $datetime != "0000-00-00 00:00:00") {
        $formatted = date("d-M-Y \a\\t H:i\H\\r ", strtotime($datetime));
    } else {
        return false;
    }
    return $formatted;
}

function scheduleTime($datetime) {
    if (!empty($datetime) && $datetime != "" && $datetime != "0000-00-00 00:00:00") {
        $formatted = date("H:i\H\\r", strtotime($datetime));
    } else {
        return false;
    }
    return $formatted;
}

function scheduleDate($datetime) {

    if (!empty($datetime) && $datetime != "" && $datetime != "0000-00-00 00:00:00") {
        $formatted = date("d-M-Y", strtotime($datetime));
    } else {
        return false;
    }
    return $formatted;
}

function getGeoCode($postCode) {
    $result = FALSE;
    try {
        if (!empty($postCode)) {
            $url = "http://api.postcodes.io/postcodes/$postCode";
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
            $response = curl_exec($ch);
            curl_close($ch);
            $response_a = json_decode($response);
            if (isset($response_a->status) && $response_a->status == 200) {
                $result = $response_a->result;
            }
        }
    } catch (\Exception $ex) {
        
    }
    return $result;
}

/*
 * Function used to format chat time.
 */

function formatChatTime($dateTime) {
    if (is_int($dateTime)) {
        $formatedDate = date("d M Y \a\\t H:i\H\\r ", $dateTime);
    } else {
        $formatedDate = date("d M Y \a\\t H:i\H\\r ", strtotime($dateTime));
    }
    return $formatedDate;
}

function getDatesBetween($startDate, $endDate, $weekDayNum = [], $oneoffDates = []) {
    $weekdays = ['mon', 'tue', 'wed', 'thu', 'fri', 'sat', 'sun'];
    $timeZone = new DateTimeZone('UTC');

    $startDate = (new DateTime($startDate))->setTimezone($timeZone);
    $endDate = (new DateTime($endDate))->setTimezone($timeZone)->setTime(0, 0, 1);

    $unavailableDates = [];

    if (!empty($weekDayNum)) {
        foreach ($weekDayNum as $day) {
            if (isset($weekdays[$day])) {
                $includeStartDate = ( $startDate->format('N') - 1 != $day );
                $interval = \DateInterval::createFromDateString('next ' . $weekdays[$day]);
                $allDates = new \DatePeriod($startDate, $interval, $endDate, $includeStartDate);
                $unavailableDates = array_merge($unavailableDates, iterator_to_array($allDates));
            }
        }
    } else if (!empty($oneoffDates)) {
        foreach ($oneoffDates as $oneoffDate) {
            $date = (new DateTime($oneoffDate))->setTimezone($timeZone);
            if ($date >= $startDate && $date <= $endDate) {
                $unavailableDates = array_merge($unavailableDates, [$date]);
            }
        }
    } else {
        $interval = new \DateInterval('P1D');
        $allDates = new \DatePeriod($startDate, $interval, $endDate);
        $unavailableDates = array_merge($unavailableDates, iterator_to_array($allDates));
    }

    return $unavailableDates;
}

function getTimesBetween($startTime, $endTime) {
    $weekdays = ['mon', 'tue', 'wed', 'thu', 'fri', 'sat', 'sun'];
    $timeZone = new DateTimeZone('UTC');

    $startTime = (new DateTime($startTime))->setTimezone($timeZone);
    $endTime = (new DateTime($endTime))->setTimezone($timeZone);

    $unavailableDates = [];

    $interval = new \DateInterval('PT30M');
    $timePeriod = new \DatePeriod($startTime, $interval, $endTime);
    $times = iterator_to_array($timePeriod);

    $timeRange = array_map(function($time) {
        return $time->format('H:i');
    }, $times);

    return $timeRange;
}

//

function yesno_TXT($cYesNo) {
    if (function_exists("logg") && defined("LOWLEVEL"))
        logg(basename(__FILE__) . "[" . __FUNCTION__ . "]", LOWLEVEL);

    switch ($cYesNo) {
        case YES : $sYesNo = "Yes";
            break;
        case NO : $sYesNo = "No";
            break;

        default : $sYesNo = "";
    }

    return $sYesNo;
}

function mask_cc_no($sData) {
    if (function_exists("logg") && defined("LOWLEVEL"))
        logg(basename(__FILE__) . "[" . __FUNCTION__ . "]", LOWLEVEL);

    if (strlen($sData) <= 4) {
        return "****";
    }

    $sLast4 = substr($sData, strlen($sData) - 4);

    return str_pad($sLast4, strlen($sData) - 4, "*", STR_PAD_LEFT);
}

function red($sData) {
    if (function_exists("logg") && defined("LOWLEVEL"))
        logg(basename(__FILE__) . "[" . __FUNCTION__ . "]", LOWLEVEL);

    return "<span style=\"color: red;\">" . $sData . "</span>";
}

function blue($sData) {
    if (function_exists("logg") && defined("LOWLEVEL"))
        logg(basename(__FILE__) . "[" . __FUNCTION__ . "]", LOWLEVEL);

    return "<span style=\"color: blue;\">" . $sData . "</span>";
}

function purple($sData) {
    if (function_exists("logg") && defined("LOWLEVEL"))
        logg(basename(__FILE__) . "[" . __FUNCTION__ . "]", LOWLEVEL);

    return "<span style=\"color: purple;\">" . $sData . "</span>";
}

function green($sData) {
    if (function_exists("logg") && defined("LOWLEVEL"))
        logg(basename(__FILE__) . "[" . __FUNCTION__ . "]", LOWLEVEL);

    return "<span style=\"color: green;\">" . $sData . "</span>";
}

function lt_green($sData) {
    if (function_exists("logg") && defined("LOWLEVEL"))
        logg(basename(__FILE__) . "[" . __FUNCTION__ . "]", LOWLEVEL);

    return "<span style=\"color: #00ff66;\">" . $sData . "</span>";
}

function bold($sData) {
    if (function_exists("logg") && defined("LOWLEVEL"))
        logg(basename(__FILE__) . "[" . __FUNCTION__ . "]", LOWLEVEL);

    return "<span style=\"font-weight: bold;\">" . $sData . "</span>";
}

function green_highlight($sData) {
    if (function_exists("logg") && defined("LOWLEVEL"))
        logg(basename(__FILE__) . "[" . __FUNCTION__ . "]", LOWLEVEL);

    return "<span class=\"green-highlight\">" . $sData . "</span>";
}

function red_highlight($sData) {
    if (function_exists("logg") && defined("LOWLEVEL"))
        logg(basename(__FILE__) . "[" . __FUNCTION__ . "]", LOWLEVEL);

    return "<span class=\"red-highlight\">" . $sData . "</span>";
}

function wrap_word($sString, $iWidth = 20) {
    if (function_exists("logg") && defined("LOWLEVEL"))
        logg(basename(__FILE__) . "[" . __FUNCTION__ . "]", LOWLEVEL);

    $sNewString = wordwrap($sString, $iWidth, "<br/>", 1);

    return $sNewString;
}

function get_order_status_TXT($cStatus) {
//    logg(basename(__FILE__) . "[" . __FUNCTION__ . "]", TRACE);

    $sStatus = "";

    switch ($cStatus) {
        case ORDSTATUS_AWAITING_PAYMENT : $sStatus = "Awaiting Payment";
            break;
        case ORDSTATUS_PENDING : $sStatus = "Pending";
            break;
//        case ORDSTATUS_APPROVED : $sStatus = green("Approved");
//            break;
        case ORDSTATUS_PICKING : $sStatus = green("Being Picked");
            break;
        case ORDSTATUS_PICKED : $sStatus = green("Picked");
            break;
        case ORDSTATUS_DISPATCHED : $sStatus = green("Dispatched");
            break;
//        case ORDSTATUS_PART_DISPATCHED : $sStatus = green("Part Dispatched");
//            break;
        case ORDSTATUS_DELIVERED : $sStatus = green("Delivered");
            break;
        case ORDSTATUS_SUSPENDED : $sStatus = red("Suspended");
            break;
        case ORDSTATUS_SUSPECT : $sStatus = red("SUSPECT");
            break;
        case ORDSTATUS_TEMP : $sStatus = "Order in Progress";
            break;
        case ORDSTATUS_BANK_REVIEW : $sStatus = blue("In Review");
            break;
        case ORDSTATUS_COMPLETED : $sStatus = green("COMPLETED");
            break;

        case ORDSTATUS_INVALID : $sStatus = red("Invalid, possibly spoofed");
            break;
        case ORDSTATUS_FAILED : $sStatus = red("Failed");
            break;
        case ORDSTATUS_UNKNOWN : $sStatus = red("Unknow, possible serious error");
            break;

        default: $sStatus = red("UNKNOWN");
    }

    return $sStatus;
}

function get_payment_method_TXT($iMethod) {
//    logg(basename(__FILE__) . "[" . __FUNCTION__ . "]", TRACE);

    $sMethod = "";

    switch ($iMethod) {
        case PAYMETHOD_NONE : $sMethod = "NONE";
            break;
        case PAYMETHOD_CC_ONLINE : $sMethod = "Credit Card (On-line)";
            break;
        case PAYMETHOD_CC_TEL : $sMethod = "Credit Card (Telephone)";
            break;
        case PAYMETHOD_CC_FAX : $sMethod = "Credit Card (Fax)";
            break;
        case PAYMETHOD_CC_EMAIL : $sMethod = "Credit Card (Email)";
            break;
        case PAYMETHOD_CHEQUE : $sMethod = "Cheque / Postal Order";
            break;
        case PAYMETHOD_BANK_DRAFT : $sMethod = "Bankers Draft";
            break;
        case PAYMETHOD_INT_MONEY_ORD : $sMethod = "International Money Order";
            break;
        case PAYMETHOD_BANK_TRANSFER : $sMethod = "Bank Transfer";
            break;
        case PAYMETHOD_INVOICE : $sMethod = "Invoice";
            break;
        case PAYMETHOD_PAYPAL : $sMethod = "Paypal";
            break;
        case PAYMETHOD_PAYPAL_STD : $sMethod = "Paypal Std";
            break;

        default: $sMethod = "UNKNOWN";
    }

    return $sMethod;
}

function getVariationData($iVariationId, $iFieldNo = 0) {
//        logg(basename(__FILE__) . "[" . __FUNCTION__ . "]", TRACE);

    if (!$this->isVariationIdValid($iVariationId)) {
        return "";
    }
}

function secpay_generate_hash($aBits) {
    logg(basename(__FILE__) . "[" . __FUNCTION__ . "]", TRACE);

    $sPreHash = HASHKEY .
            number_format($aBits['grandtotal'] / 100, 2, '.', '') .
            number_format(($aBits['subtotal'] - $aBits['delcharge']) / 100, 2, '.', '') .
            number_format($aBits['delcharge'] / 100, 2, '.', '') .
            number_format($aBits['subtotal'] / 100, 2, '.', '') .
            number_format($aBits['vat'] / 100, 2, '.', '');

    $sSVPHash = md5($sPreHash);

    return $sSVPHash;
}

function clean_data($sData) {
//    if (function_exists("logg") && defined("LOWLEVEL"))
//        logg(basename(__FILE__) . "[" . __FUNCTION__ . "]", LOWLEVEL);

    $sCleanedData = strip_tags($sData, "\n");
//	$sCleanedData	= addslashes($sCleanedData);
    $sCleanedData = htmlentities($sCleanedData);

    return $sCleanedData;
}

function clean_input($sData) {
//    if (function_exists("logg") && defined("LOWLEVEL"))
//        logg(basename(__FILE__) . "[" . __FUNCTION__ . "]", LOWLEVEL);

    return trim(clean_data($sData));
}

// Filters unwanted characters out of an input string.  Useful for tidying up FORM field inputs.
function cleanInput($sRawText, $sType) {

    if ($sType == "Number") {
        $sClean = "0123456789.";
        $bHighOrder = false;
    } else if ($sType == "VendorTxCode") {
        $sClean = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-_.";
        $bHighOrder = false;
    } else {
        $sClean = " ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789.,'/{}@():?-_&�$=%~<>*+\"";
        $bHighOrder = true;
    }

    $sCleanedText = "";
    $iCharPos = 0;

    do {
        // Only include valid characters
        $cThisChar = substr($sRawText, $iCharPos, 1);

        if (strspn($cThisChar, $sClean, 0, strlen($sClean)) > 0) {
            $sCleanedText = $sCleanedText . $cThisChar;
        } else if ($bHighOrder == true) {
            // Fix to allow accented characters and most high order bit chars which are harmless 
            if (bin2hex($cThisChar) >= 191) {
                $sCleanedText = $sCleanedText . $cThisChar;
            }
        }

        $iCharPos = $iCharPos + 1;
    } while ($iCharPos < strlen($sRawText));

    $sCleanInput = ltrim($sCleanedText);

    return $sCleanInput;
}

//function addPKCS5Padding($input) {
//    $blockSize = 16;
//    $padd = "";
//    $length = $blockSize - (strlen($input) % $blockSize);
//    for ($i = 1; $i <= $length; $i++) {
//        $padd .= chr($length);
//    }
//
//    return $input . $padd;
//}
//function encryptAes($string, $key) {
//    $string = addPKCS5Padding($string);
//    $crypt = mcrypt_encrypt(MCRYPT_RIJNDAEL_128, $key, $string, MCRYPT_MODE_CBC, $key);
//
//    return '@' . strtoupper(bin2hex($crypt));
//}
//
//function decryptAes($strIn, $password) {
//    $sStrIn = substr($strIn, 1);
//
//    $strInitVector = $password;
//    $sStrIn = pack('H*', $sStrIn);
//    $string = mcrypt_decrypt(MCRYPT_RIJNDAEL_128, $password, $sStrIn, MCRYPT_MODE_CBC, $strInitVector);
//
//    return removePKCS5Padding($string);
//}


function addPKCS5Padding($input) {
    $blockSize = 16;
    $padd = "";
    $length = $blockSize - (strlen($input) % $blockSize);
    for ($i = 1; $i <= $length; $i++) {
        $padd .= chr($length);
    }
    return $input . $padd;
}

function removePKCS5Padding($input) {
    $blockSize = 16;
    $padChar = ord($input[strlen($input) - 1]);
    $unpadded = substr($input, 0, (-1) * $padChar);
    return $unpadded;
}

function encryptAes($string, $key) {
    $string = addPKCS5Padding($string);
    $crypt = mcrypt_encrypt(MCRYPT_RIJNDAEL_128, $key, $string, MCRYPT_MODE_CBC, $key);
    return strtoupper(bin2hex($crypt));
}

function decryptAes($strIn, $myEncryptionPassword) {

#Sagepay specific - remove the '@'
    $strIn = substr($strIn, 1);

    $strInitVector = $myEncryptionPassword;
    $strIn = pack('H*', $hex);
    $string = mcrypt_decrypt(MCRYPT_RIJNDAEL_128, $myEncryptionPassword, $strIn, MCRYPT_MODE_CBC, $strInitVector);
    return removePKCS5Padding($string);
}

function getToken($sThisString) {

    // List the possible tokens
    $aTokens = array("Status",
        "StatusDetail",
        "VendorTxCode",
        "VPSTxId",
        "TxAuthNo",
        "Amount",
        "AVSCV2",
        "AddressResult",
        "PostCodeResult",
        "CV2Result",
        "GiftAid",
        "3DSecureStatus",
        "CAVV",
        "AddressStatus",
        "PayerStatus",
        "CardType",
        "Last4Digits",
        "FraudResponse",
        "Surcharge",
        "ExpiryDate",
        "BankAuthCode",
        "DeclineCode");

    $aOutPut = array();
    $aResults = array();

    // Get the next token in the sequence
    for ($i = count($aTokens) - 1; $i >= 0; $i--) {
        // Find the position in the string
        $vStart = strpos($sThisString, $aTokens[$i]);

        // If it's present
        if ($vStart !== false) {
            $aResults[$i] = new stdClass();

            // Record position and token name
            $aResults[$i]->start = $vStart;
            $aResults[$i]->token = $aTokens[$i];
        }
    }

    // Sort in order of position
    sort($aResults);

    // Go through the result array, getting the token values
    for ($i = 0; $i < count($aResults); $i++) {
        // Get the start point of the value
        $iValueStart = $aResults[$i]->start + strlen($aResults[$i]->token) + 1;

        // Get the length of the value
        if ($i == (count($aResults) - 1)) {
            $aOutPut[$aResults[$i]->token] = substr($sThisString, $iValueStart);
        } else {
            $iValueLength = $aResults[$i + 1]->start - $aResults[$i]->start - strlen($aResults[$i]->token) - 2;
            $aOutPut[$aResults[$i]->token] = substr($sThisString, $iValueStart, $iValueLength);
        }
    }

    return $aOutPut;
}
