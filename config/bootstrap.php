<?php

/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @since         0.10.8
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */
/*
 * Configure paths required to find CakePHP + general filepath constants
 */
require __DIR__ . '/paths.php';

/*
 * Bootstrap CakePHP.
 *
 * Does the various bits of setup that CakePHP needs to do.
 * This includes:
 *
 * - Registering the CakePHP autoloader.
 * - Setting the default application paths.
 */
require CORE_PATH . 'config' . DS . 'bootstrap.php';

use Cake\Cache\Cache;
use Cake\Console\ConsoleErrorHandler;
use Cake\Core\Configure;
use Cake\Core\Configure\Engine\PhpConfig;
use Cake\Core\Plugin;
use Cake\Database\Type;
use Cake\Datasource\ConnectionManager;
use Cake\Error\ErrorHandler;
use Cake\Http\ServerRequest;
use Cake\Log\Log;
use Cake\Mailer\Email;
use Cake\Utility\Inflector;
use Cake\Utility\Security;

/**
 * Uncomment block of code below if you want to use `.env` file during development.
 * You should copy `config/.env.default to `config/.env` and set/modify the
 * variables as required.
 *
 * It is HIGHLY discouraged to use a .env file in production, due to security risks
 * and decreased performance on each request. The purpose of the .env file is to emulate
 * the presence of the environment variables like they would be present in production.
 */
// if (!env('APP_NAME') && file_exists(CONFIG . '.env')) {
//     $dotenv = new \josegonzalez\Dotenv\Loader([CONFIG . '.env']);
//     $dotenv->parse()
//         ->putenv()
//         ->toEnv()
//         ->toServer();
// }

/*
 * Read configuration file and inject configuration into various
 * CakePHP classes.
 *
 * By default there is only one configuration file. It is often a good
 * idea to create multiple configuration files, and separate the configuration
 * that changes from configuration that does not. This makes deployment simpler.
 */
try {
    Configure::config('default', new PhpConfig());
    Configure::load('app', 'default', false);
} catch (\Exception $e) {
    exit($e->getMessage() . "\n");
}

/*
 * Load an environment local configuration file.
 * You can use a file like app_local.php to provide local overrides to your
 * shared configuration.
 */
//Configure::load('app_local', 'default');

/*
 * When debug = true the metadata cache should only last
 * for a short time.
 */
if (Configure::read('debug')) {
    Configure::write('Cache._cake_model_.duration', '+2 minutes');
    Configure::write('Cache._cake_core_.duration', '+2 minutes');
    // disable router cache during development
    Configure::write('Cache._cake_routes_.duration', '+2 seconds');
}

/*
 * Set the default server timezone. Using UTC makes time calculations / conversions easier.
 * Check http://php.net/manual/en/timezones.php for list of valid timezone strings.
 */
date_default_timezone_set(Configure::read('App.defaultTimezone'));

/*
 * Configure the mbstring extension to use the correct encoding.
 */
mb_internal_encoding(Configure::read('App.encoding'));

/*
 * Set the default locale. This controls how dates, number and currency is
 * formatted and sets the default language to use for translations.
 */
ini_set('intl.default_locale', Configure::read('App.defaultLocale'));

/*
 * Register application error and exception handlers.
 */
$isCli = PHP_SAPI === 'cli';
if ($isCli) {
    (new ConsoleErrorHandler(Configure::read('Error')))->register();
} else {
    (new ErrorHandler(Configure::read('Error')))->register();
}

/*
 * Include the CLI bootstrap overrides.
 */
if ($isCli) {
    require __DIR__ . '/bootstrap_cli.php';
}

/*
 * Set the full base URL.
 * This URL is used as the base of all absolute links.
 *
 * If you define fullBaseUrl in your config file you can remove this.
 */
if (!Configure::read('App.fullBaseUrl')) {
    $s = null;
    if (env('HTTPS')) {
        $s = 's';
    }

    $httpHost = env('HTTP_HOST');
    if (isset($httpHost)) {
        Configure::write('App.fullBaseUrl', 'http' . $s . '://' . $httpHost);
    }
    unset($httpHost, $s);
}

Cache::setConfig(Configure::consume('Cache'));
ConnectionManager::setConfig(Configure::consume('Datasources'));
Email::setConfigTransport(Configure::consume('EmailTransport'));
Email::setConfig(Configure::consume('Email'));
Log::setConfig(Configure::consume('Log'));
Security::setSalt(Configure::consume('Security.salt'));

/*
 * The default crypto extension in 3.0 is OpenSSL.
 * If you are migrating from 2.x uncomment this code to
 * use a more compatible Mcrypt based implementation
 */
//Security::engine(new \Cake\Utility\Crypto\Mcrypt());

/*
 * Setup detectors for mobile and tablet.
 */
ServerRequest::addDetector('mobile', function ($request) {
    $detector = new \Detection\MobileDetect();

    return $detector->isMobile();
});
ServerRequest::addDetector('tablet', function ($request) {
    $detector = new \Detection\MobileDetect();

    return $detector->isTablet();
});

/*
 * Enable immutable time objects in the ORM.
 *
 * You can enable default locale format parsing by adding calls
 * to `useLocaleParser()`. This enables the automatic conversion of
 * locale specific date formats. For details see
 * @link https://book.cakephp.org/3.0/en/core-libraries/internationalization-and-localization.html#parsing-localized-datetime-data
 */
Type::build('time')
        ->useImmutable();
Type::build('date')
        ->useImmutable();
Type::build('datetime')
        ->useImmutable();
Type::build('timestamp')
        ->useImmutable();

/*
 * Custom Inflector rules, can be set to correctly pluralize or singularize
 * table, model, controller names or whatever other string is passed to the
 * inflection functions.
 */
//Inflector::rules('plural', ['/^(inflect)or$/i' => '\1ables']);
//Inflector::rules('irregular', ['red' => 'redlings']);
//Inflector::rules('uninflected', ['dontinflectme']);
//Inflector::rules('transliteration', ['/å/' => 'aa']);

define('SITE_LIVE', false);

define('HTTP_ROOT', 'http://localhost/woodburningstoves/');
define("DEFAULT_VAT_RATE", 20.0);
define('SITE_NAME_TEXT', 'Woodburning Stoves');
define('SITE_NAME', 'Woodburning Stoves');
define('MAX_FILE_SIZE', 4 * 1024 * 1024);

define('BRAND_PIC', 'files/brands/');
define('HOME_PIC', 'files/homeImages/');

define('PRODUCT_PIC', 'files/products/');

define("IMG_HOME_WIDTH", 249);
define("IMG_HOME_HEIGHT", 171);

//google recaptcha
define('RECAPTCHA_SITEKEY', '6Lf7r18UAAAAALXds7suqRNxl6gedmtjNd7ULjal');
define('RECAPTCHA_SECRETKEY', '6Lf7r18UAAAAAB8niLR_PSbezSFbGE_jeqsnsUzA');

define("COMPLETE_ORDER", 10);
define("ORDER_PAID", 11);
define("ADD", 12);
define("EDIT_INFO", 14);
define("DISPATCH_ORDER", 15);
//-----------------------------------------------------------------------------
//	PRODUCT VARIATIONS
//-----------------------------------------------------------------------------
define("PRODUCT_VARIATION_COUNT", 3);
//-----------------------------------------------------------------------------
//	CMS
//-----------------------------------------------------------------------------
define("CMS_DELIVERY_INFO", 1);
define("CMS_RETURNS_INFO", 2);
define("CMS_HOME_INFO", 3);
define("CMS_REVIEWS", 3);
define("CMS_TERMS", 4);


//-----------------------------------------------------------------------------
//	TOP LEVEL GROUPS
//-----------------------------------------------------------------------------
define("GROUP_ONE", 1);
define("GROUP_TWO", 2);
define("GROUP_THREE", 3);
//-----------------------------------------------------------------------------
//	ORDER STATUSES
//-----------------------------------------------------------------------------
/**
 * Given to order that is still in shopping mode
 *
 */
define("ORD_UNIQID", 'ORDI');
/**
 * Approved by payment system, but waiting to be processed
 * by the site owner
 *
 */
define("ORDSTATUS_TEMP", 'A');
/**
 * Approved by payment system, but waiting to be processed
 * by the site owner
 *
 */
define("ORDSTATUS_PAYMENT", 'P');
/**
 * When payment system completed through user by COD or Any net Banking method, then ORDSTATUS_TEMP move to ORDSTATUS_PAYMENT .
 *
 */
define("ORDSTATUS_PENDING", 'B');
/**
 * Order approved by site owner
 *
 */
//define("ORDSTATUS_APPROVED", 'C');
//
///**
// * Order has been suspended by site owner
// *
// */
define("ORDSTATUS_CANCEL", 'C');

/**
 * Order has been canceled by USER. 
 *
 */
define("ORDSTATUS_SUSPENDED", 'D');

/**
 * Ordered products are currently being picked from
 * stock.
 *
 */
define("ORDSTATUS_PICKING", 'E');

/**
 * All products in order have been picked
 *
 */
define("ORDSTATUS_PICKED", 'F');

/**
 * Order has been dispatched to customer
 *
 */
define("ORDSTATUS_DISPATCHED", 'G');

/**
 * Confirmation that order has been delivered to  the customer,
 * then ORDSTATUS_PAYMENT move to ORDSTATUS_DISPATCHED .
 *
 */
define("ORDSTATUS_DELIVERED", 'H');

/**
 * Payment system has placed the order in review
 * this usually means one or more details are suspect
 *
 */
define("ORDSTATUS_BANK_REVIEW", 'I');

/**
 * The customer has cancelled the order during
 * the payment entry process
 *
 */
define("ORDSTATUS_USER_CANCELLED", 'J');

/**
 * The site owner has cancelled the order
 *
 */
define("ORDSTATUS_CANCELLED", 'K');

/**
 * The payment system has failed the order for
 * one reason or another.
 *
 */
define("ORDSTATUS_FAILED", 'L');
define("ORDSTATUS_SUSPECT", 'M');
define("ORDSTATUS_COMPLETED", 'N');
/**
 * Primarily used for paypal as there can be a big delay in payment going through
 *
 */
define("ORDSTATUS_AWAITING_PAYMENT", 'O');
//define("ORDSTATUS_INVALID", 'P');
define("ORDSTATUS_UNKNOWN", 'Q');
/**
 * Order has been part dispatched to customer
 *
 */
define("ORDSTATUS_RETURN", 'R');
/**
 * Order has been RETURN by USER after ORDSTATUS_DISPATCHED(delivery success) to ORDSTATUS_RETURN.
 *
 */
//define("ORDSTATUS_PART_DISPATCHED", 'R');
//-----------------------------------------------------------------------------
//	PAYMENT METHODS (ONLY ADD TO - NEVER REMOVE AN ENTRY)
//-----------------------------------------------------------------------------
define("PAYMETHOD_NONE", 0);
define("PAYMETHOD_CC_ONLINE", 1);
define("PAYMETHOD_CC_TEL", 2);
define("PAYMETHOD_CC_FAX", 3);
define("PAYMETHOD_CC_EMAIL", 4);
define("PAYMETHOD_CHEQUE", 5);
define("PAYMETHOD_BANK_DRAFT", 6);
define("PAYMETHOD_INT_MONEY_ORD", 7);
define("PAYMETHOD_BANK_TRANSFER", 8);
define("PAYMETHOD_PAYPAL", 10);
define("PAYMETHOD_CLICKBANK", 11);
define("PAYMETHOD_INVOICE", 12);
define("PAYMETHOD_PAYPAL_STD", 14);

//-----------------------------------------------------------------------------
//	PAYMENT SETTINGS
//-----------------------------------------------------------------------------
define("PAYMENT_OK", 'P');
define("PAYMENT_DECLINED", 'F');
define("PAYMENT_IN_TRANSIT", 'T');
define("PAYMENT_INVALID", 'I');
define("PAYMENT_UNKNOWN", 'U');
//-----------------------------------------------------------------------------
//	YES / NO
//-----------------------------------------------------------------------------
define("YES", 'Y');
define("NO", 'N');
//-----------------------------------------------------------------------------
//	RETAIL / TRADE
//-----------------------------------------------------------------------------
define("RETAIL", 'R');
define("TRADE", 'T');
define("SHADE_LIGHT", '#ffffff');
define("SHADE_DARK", '#efefef');
//-----------------------------------------------------------------------------
//	DATE / TIME
//-----------------------------------------------------------------------------
define("NO_DATETIME", '0000-00-00 00:00:00');
define("NO_DATE", '0000-00-00');

//-----------------------------------------------------------------------------
//	CUSTOMER SETTINGS
//-----------------------------------------------------------------------------
define("EMAIL_ENQUIRIES", "enquiries@woodburningstovesdirect.com");
define("EMAIL_INFO", "enquiries@woodburningstovesdirect.com");
define("EMAIL_SALES", "sales@saltfirestoves.com");
define("EMAIL_NOREPLY", "noreply@saltfirestoves.com");

// designed for testing and initial period after going live to make sure 
// things are flowing
define("EMAIL_BCC", "Bcc: harbour_design@yahoo.co.uk\r\n");
//Approve 
define("APPROVE_ORDER", 9);

//GOOGLE API KEY FOR AUTO COMPLETE
define('AUTOCOMPLETE_KEY', 'AIzaSyAI449VbCpz-FLDG7S0aEssjlC1TbELxKI');
//-----------------------------------------------------------------------------
//	LIVE / NOT LIVE (mainly connected with payment systems)
//-----------------------------------------------------------------------------
define("SAGEPAY_ON", true);
define("PROTX_SIMULATOR", false);
define("PROTX_TEST", false);
define("PROTX_LIVE", true);
define("SAGEPAY_SIMULATOR", false);
define("SAGEPAY_TEST", false);
define("SAGEPAY_LIVE", true);
define("PAYPAL_STD_TEST", false);
define("PAYPAL_STD_LIVE", true);

if (PAYPAL_STD_LIVE) {
    define("PAYPAL_STD_DOMAIN", "https://www.paypal.com/cgi-bin/webscr");
} else {
    define("PAYPAL_STD_DOMAIN", "https://www.sandbox.paypal.com/cgi-bin/webscr");
}

if (PROTX_LIVE) {
    define("PROTX_ENCRYPT_PWD", "7QvqQ3abExkSnBgV");
    define("PROTX_VENDOR", "saltfirestoves");
} elseif (PROTX_SIMULATOR) {
    define("PROTX_ENCRYPT_PWD", "3spu4UP9xhvTgzB2");
    define("PROTX_VENDOR", "harbourdesign");
} else {
    define("PROTX_ENCRYPT_PWD", "7QvqQ3abExkSnBgV");
    define("PROTX_VENDOR", "saltfirestoves");
}
define('UPLOAD_PATH', 'images/products/');
define('BANNER_PATH', 'files/banners/');
define('PDFS_PATH', 'pdfs/');

//paypal constants
define('LIVE', FALSE);

Configure::write('Paypal', [
    'payment_status' => [
        "New" => 1, "Pending" => 2, "Processed" => 3, "Completed" => 4, "Expired" => 5, "Failed" => 6, "Refunded" => 7, "Reversed" => 8, "Canceled_Reversal" => 9, "Denied" => 10, "Voided" => 11, "Created" => 13
    ]
]);
if (LIVE) {
    Configure::write('Paypal.PAYPAL_PAYMENT_URL', 'https://www.paypal.com/cgi-bin/webscr');
    Configure::write('Paypal.PAYPAL_MERCHANT_EMAIL', 'chinmay235-facilitor@gmail.com');
} else {
    Configure::write('Paypal.PAYPAL_PAYMENT_URL', 'https://www.sandbox.paypal.com/cgi-bin/webscr');
    Configure::write('Paypal.PAYPAL_MERCHANT_EMAIL', 'chinmay235-facilitor@gmail.com');
}

define('CURRENCY_NAME', "USD");
define('CURRENCY_SYMBOL', "£");

define('INVOICE',  'files/invoice/');



/* Common Core Functions */
require __DIR__ . '/site_config.php';




/* Common Core Functions */

require __DIR__ . '/functions.php';
