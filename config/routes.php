<?php

use Cake\Routing\RouteBuilder;
use Cake\Routing\Router;
use Cake\Routing\Route\DashedRoute;

Router::defaultRouteClass(DashedRoute::class);

Router::prefix('admin', function ($routes) {
    $routes->connect('/', ['controller' => 'Appadmins', 'action' => 'dashboard']);
    $routes->connect('/dashboard', ['controller' => 'Appadmins', 'action' => 'dashboard']);

    $routes->connect('/listbrands', ['controller' => 'Brands', 'action' => 'listbrands']);
    $routes->connect('/add-brand/*', ['controller' => 'Brands', 'action' => 'addBrand']);
    $routes->connect('/stock-list', ['controller' => 'Products', 'action' => 'stockList']);
    $routes->connect('/review-ratings', ['controller' => 'Products', 'action' => 'reviewRatings']);
    $routes->connect('/change-status/*', ['controller' => 'Brands', 'action' => 'changeStatus']);
    $routes->connect('/delete-brand/*', ['controller' => 'Brands', 'action' => 'deleteBrand']);

    $routes->connect('/list-regions', ['controller' => 'Delivery', 'action' => 'listRegions']);
    $routes->connect('/add-region/*', ['controller' => 'Delivery', 'action' => 'addRegion']);
    $routes->connect('/delete-region/*', ['controller' => 'Delivery', 'action' => 'deleteRegion']);
    $routes->connect('/list-deliveries', ['controller' => 'Delivery', 'action' => 'listDeliveries']);
    $routes->connect('/add-delivery/*', ['controller' => 'Delivery', 'action' => 'addDelivery']);
    $routes->connect('/delete-delivery/*', ['controller' => 'Delivery', 'action' => 'deleteDelivery']);

    $routes->connect('/list-administrator', ['controller' => 'Users', 'action' => 'listAdministrators']);
    $routes->connect('/add-administrator/*', ['controller' => 'Users', 'action' => 'addAdministrators']);

    $routes->connect('/home-page-images', ['controller' => 'Miscellaneous', 'action' => 'listHomeImages']);
    $routes->connect('/add-new-home-image', ['controller' => 'Miscellaneous', 'action' => 'addNewHomeImage']);
    $routes->connect('/home-image-status/*', ['controller' => 'Miscellaneous', 'action' => 'homeImageStatus']);
    $routes->connect('/delete-home-image/*', ['controller' => 'Miscellaneous', 'action' => 'deleteHomeImage']);
    $routes->connect('/list-faqs', ['controller' => 'Miscellaneous', 'action' => 'listFaqs']);
    $routes->connect('/add-new-faq/*', ['controller' => 'Miscellaneous', 'action' => 'addNewFaq']);
    $routes->connect('/delete-faq/*', ['controller' => 'Miscellaneous', 'action' => 'deleteFaq']);
    $routes->connect('/countries/*', ['controller' => 'Miscellaneous', 'action' => 'countries']);
    $routes->connect('/company', ['controller' => 'Miscellaneous', 'action' => 'company']);
    $routes->connect('/reviews', ['controller' => 'Miscellaneous', 'action' => 'reviews']);
    $routes->connect('/change-country-status', ['controller' => 'Miscellaneous', 'action' => 'changeCountryStatus']);

    $routes->connect('/add-cms/*', ['controller' => 'Cms', 'action' => 'addCms']);
    $routes->connect('/list-cms', ['controller' => 'Cms', 'action' => 'listCms']);
    $routes->connect('/new-page/*', ['controller' => 'Cms', 'action' => 'newPage']);
    $routes->connect('/list-page', ['controller' => 'Cms', 'action' => 'listPages']);
    $routes->connect('/home-page', ['controller' => 'Cms', 'action' => 'homePageContent']);

    $routes->fallbacks(DashedRoute::class);
});

Router::scope('/', function (RouteBuilder $routes) {

    // $routes->applyMiddleware('csrf');

    $routes->connect('/', ['controller' => 'Pages', 'action' => 'index']);

    $routes->connect('/activate-account/**', ['controller' => 'Users', 'action' => 'activateAccount']);
    $routes->connect('/brands', ['controller' => 'Pages', 'action' => 'listbrands']);
    $routes->connect('/returns', ['controller' => 'Pages', 'action' => 'returnsInfo']);
    $routes->connect('/delivery', ['controller' => 'Pages', 'action' => 'deliveryInfo']);
    $routes->connect('/reviews', ['controller' => 'Pages', 'action' => 'reviews']);
    $routes->connect('/review-submit', ['controller' => 'Pages', 'action' => 'reviewSubmit']);
    $routes->connect('/adept-ekol', ['controller' => 'Pages', 'action' => 'adeptEkol']);
    $routes->connect('/boiler-stove', ['controller' => 'Pages', 'action' => 'boilerStove']);
    $routes->connect('/building-regulations', ['controller' => 'Pages', 'action' => 'buildingRegulations']);
    $routes->connect('/ecodesign-2022', ['controller' => 'Pages', 'action' => 'ecoDesign']);
    $routes->connect('/flexiLiner', ['controller' => 'Pages', 'action' => 'flexiLiner']);
    $routes->connect('/twinWall', ['controller' => 'Pages', 'action' => 'twinWall']);
    $routes->connect('/gallery-installations', ['controller' => 'Pages', 'action' => 'galleryInstallations']);
    $routes->connect('/stove-glossary', ['controller' => 'Pages', 'action' => 'stoveGlossary']);
    $routes->connect('/installation-advice', ['controller' => 'Pages', 'action' => 'installationAdvice']);
    $routes->connect('/new-products', ['controller' => 'Pages', 'action' => 'newProducts']);
    $routes->connect('/peanut-stove', ['controller' => 'Pages', 'action' => 'peanutStove']);
    $routes->connect('/sia', ['controller' => 'Pages', 'action' => 'sia']);
    $routes->connect('/Showroom', ['controller' => 'Pages', 'action' => 'visitShowroom']);
    $routes->connect('/Installer', ['controller' => 'Pages', 'action' => 'installerBlog']);
    $routes->connect('/Video', ['controller' => 'Pages', 'action' => 'listVideo']);
    $routes->connect('/term-conditions', ['controller' => 'Pages', 'action' => 'termConditions']);
    $routes->connect('/privacy-policy', ['controller' => 'Pages', 'action' => 'privacyPolicy']);
    $routes->connect('/list-product', ['controller' => 'Products', 'action' => 'productsList']);
    $routes->connect('/contact-us', ['controller' => 'Pages', 'action' => 'contactUs']);

    $routes->connect('/login', ['controller' => 'Users', 'action' => 'login']);
    $routes->connect('/product-list', ['controller' => 'Products', 'action' => 'productsList']);
    $routes->fallbacks(DashedRoute::class);
});

