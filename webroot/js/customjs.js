//.....Main Nav section js.........
	$(document).ready(function () {
        $(window).resize(function () {
            var width = $(window).width();
            if (width < 991) {
                $(".mob-menu").click(function () {
                    $(".nav").slideToggle();
                });
                $(".nav-close").click(function () {
                    $(".nav").fadeOut();
                });
                $('html').click(function (nav) {
                    if ($(nav.target).closest('.mob-menu, .nav-close,.nav').length === 0) {
                        $(".nav").fadeOut();
                    }
                });
            }
        });

        $("li.sub-menu").click(function () {
            if (!$($(this).find(".dropdown-menu")).is(':visible')) {
                $(".dropdown-menu").fadeOut();
                $(this).find(".dropdown-menu").fadeIn();
            }
            else {
                //$(".dropdown-menu").fadeOut();
                $(this).find(".dropdown-menu").fadeOut();
            }

        });

        $('html').click(function (subNav) {
            if ($(subNav.target).closest('.sub-menu, .dropdown-menu').length === 0) {
                $(".dropdown-menu").fadeOut();
            }
        });
    });

