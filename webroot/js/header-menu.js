//.....Main Nav section js.........
	$(document).ready(function () {

        $(".mob-menu").click(function () {                                
            $(".nav").slideToggle();
        }); 

        $(".nav-close").click(function () {
            $(".nav").fadeOut();
        });

        $('html').click(function (navv) {                
            if ($(navv.target).closest('.mob-menu, .nav-close,.nav,.datepicker,.timepicker').length === 0) {
                $(".nav").fadeOut();
            }                   
        });

        $(window).resize(function () {
            var width = $(window).width();
            if (width < 991) {
                $('html').click(function (navv) {                
                    if ($(navv.target).closest('.mob-menu, .nav-close,.nav').length === 0) {
                        $(".nav").fadeOut();
                    }                   
                });
            } else {
                $('html').unbind('click');
                $(".nav").fadeIn();
            }
        });
            
       

        $("li.sub-menu").click(function () {
            if (!$($(this).find(".dropdown-menu")).is(':visible')) {
                $(".dropdown-menu").fadeOut();
                $(this).find(".dropdown-menu").fadeIn();
            }
            else {
                //$(".dropdown-menu").fadeOut();
                $(this).find(".dropdown-menu").fadeOut();
            }

        });

        $('html').click(function (subNav) {
            if ($(subNav.target).closest('.sub-menu, .dropdown-menu,.datepicker,.timepicker').length === 0) {
                $(".dropdown-menu").fadeOut();
            }
        });


       $(".user-page-mob-menu").click(function () {                                
            $(".user-nav").slideToggle();
        }); 

        $(".user-nav-close").click(function () {
            $(".user-nav").fadeOut();
        });
//        $('html').click(function (usernav) {                
//                    if ($(usernav.target).closest('.user-page-mob-menu, .user-nav-close,.user-nav').length === 0) {
//                        $(".user-nav").fadeOut();
//                    }                   
//                });
        $(window).resize(function () {
            var width = $(window).width();
            if (width < 991) {
                $('html').click(function (usernav) {                
                    if ($(usernav.target).closest('.user-page-mob-menu, .user-nav-close,.user-nav').length === 0) {
                        $(".user-nav").fadeOut();
                    }                   
                });
            } 
            else {
                $('html').unbind('click');
                $(".user-nav").fadeIn();
            }
        });

    });

