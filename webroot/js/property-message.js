var PropertyChat = {

    // Properties Start
    baseUrl: siteUrl,
    currentThreadId: null,
    senderId: '',
    senderName: '',
    maxCharCount: 1000,
    intervalAjax: null,
    fetchInterval: 4000,

    /*  
     * Fetch all the properties with whcih the user is associated & their related threads
     * @param {type} propertId
     */
    getPropertyThreads: function (propertId) {
        var propert_id = propertId > 0 ? propertId : '';
        $.ajax({
            type: 'GET',
            url: siteUrl + 'property-messages/get-threads',
            data: {'property_id': propert_id},
            beforeSend: function() {
                $('.property-thread-list').html('').addClass('button-loader');
            },
            success: function (responseHtml) {
                $('.property-thread-list').html(responseHtml);
                $('.chat-details-right').css({'height': '100%', 'display': 'flex', 'align-items': 'center'});
                $('.chat-details-right').html('<span style="font-size: 14px; text-align: center; line-height: 25px;">Please select any thread from the left to start the chat.</span>');
            }, 
            complete: function() {
                $('.property-thread-list').removeClass('button-loader');
            }
        });
    },

    /*
     * Update chat form with all previous messsages when a new thread is selected.
     */
    updateChatForm: function () {

        var threadId = $(this).data('thread-id');

        if (threadId > 0) {

            PropertyChat.currentThreadId = threadId;

            if(!$(this).hasClass('active')) {
                
                $('.client-chat').removeClass('active');
                $(this).addClass('active');

                $.ajax({
                    type: 'GET',
                    url: siteUrl + 'property-messages/update-chat-form',
                    data: {'thread_id': threadId},
                    beforeSend: function() {
                        $('.chat-details-right').children().css('visibility', 'hidden');
                        $('.chat-details-right').addClass('button-loader');
                        
                    },
                    success: function (formHtml) {
                        $("#chatFormContainer").html(formHtml);
                        PropertyChat.startFetchChatInterval();
                    }, 
                    complete: function() {
                        $('.chat-details-right').removeClass('button-loader');
                        
                    }
                });
                
            }
            
        }
    },

    /*
     * Send Message.
     */
    sendMessage: function () {

        var msgInput = $('#chatForm input#msgInput');
        var propertyThreadId = $('#chatForm input[name="thread_id"]').val();
        var msg = $.trim(msgInput.val());

        if (msg.length) {
            $.ajax({
                type: 'POST',
                url: siteUrl + 'property-messages/send',
                dataType: 'json',
                data: {
                    'property_thread_id': propertyThreadId,
                    'message': msg
                },
                success: function (response) {                    
                    if (response.status == 'success') {
                        PropertyChat.appendToChat(response.data);
                        PropertyChat.clearInput();
                        PropertyChat.scrollTop();
                        PropertyChat.updateCountdown();
                    }
                }
            });
        }
    },

    /*
     * This function will run after 5 sec interval & fetch updated chat messages.
     */
    fetchChatMessages: function () {
        // console.log('fetchChatMessages');
        if (PropertyChat.currentThreadId > 0) {
            var oldMsgCount = $('.msg-row').length;
            $.ajax({
                type: 'GET',
                url: siteUrl + 'property-messages/fetch-chat-messages',
                data: {'thread_id': PropertyChat.currentThreadId, 'old_msg_count': oldMsgCount},
                success: function (chatMessages) {
                    if (!chatMessages)
                        return;
                    var newMsgCount = $('<div>' + chatMessages + '</div>').find('.msg-row').length;
                    if (newMsgCount > oldMsgCount) {
                        $('#ChatThread-' + PropertyChat.currentThreadId + ' .msg_container_base').html(chatMessages);
                        PropertyChat.scrollTop();
                    }
                }
            });
        }
    },

    /*
     * Clear Interval.
     */
    startFetchChatInterval() {

        PropertyChat.stopInterval();

        PropertyChat.intervalAjax = setInterval(function () {
            PropertyChat.fetchChatMessages();
        }, PropertyChat.fetchInterval);
    },

    /*
     * Clear Interval.
     */
    stopInterval() {
        if (PropertyChat.intervalAjax) {
            clearInterval(PropertyChat.intervalAjax);
            PropertyChat.intervalAjax = null;
        }
    },

    /*
     * Append current chat message to dom.
     */
    appendToChat: function (message) {
        var newMsg = '<div class="msg-row">'
                + '<div class="u-msg"><strong>' + message.sender.title + ' ' + message.sender.first_name + ' ' + message.sender.last_name + '</strong><br/>' + message.message + '</div>'
                + '<div class="msd-data t-a-r" data-chat-timestamp="' + message.chat_time + '">' + PropertyChat.timeAgo(message.chat_time) + '</div>'
                + '</div>';
        $('.msg-row:last').after(newMsg);
    },

    /*
     * Append current chat message to dom.
     */
    clearInput: function () {
        $('#chatForm input#msgInput').val('');
    },

    /*
     * Scroll chat box to top.
     */
    scrollTop: function () {
        setTimeout(function () {
            $(".msg_container_base").stop().animate({scrollTop: $(".msg_container_base")[0].scrollHeight}, 1000);
        }, 100);
    },

    /*
     * Display no of characters a user has entered in the chat input box & show remaining chars
     */
    updateCountdown: function () {
        var remaining = PropertyChat.maxCharCount - $('#msgInput').val().length;
        $('#countdown').text(remaining + ' characters remaining.');
    },
    timeAgo: function (timestamp) {
        return moment(timestamp * 1000).fromNow();
    },
    updateChatTime: function (timestamp) {
        if ($('[data-chat-timestamp]').length > 0) {
            $('[data-chat-timestamp]').each(function () {
                var timestamp = $(this).attr('data-chat-timestamp');
                $(this).text(PropertyChat.timeAgo(timestamp));
            });
        }
    },
    /*
     * Get current date time
     */
    getCurrDateTime: function () {
        const monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
        var today = new Date();
        var date = today.getDate() + ' ' + monthNames[today.getMonth()] + ' ' + today.getFullYear();
        var time = today.getHours() + ":" + today.getMinutes() + " Hr";
        var dateTime = date + ' ' + time;
        return dateTime;
    }
};

$(document).ready(function (e) {

    // Initialize properties
    PropertyChat.senderId = $('#senderId').val();
    PropertyChat.senderName = $('#senderName').val();

    // Fetch all property threads
//    PropertyChat.getPropertyThreads();

    // Update Chat Time
    PropertyChat.updateChatTime();                
    setInterval(function () {
        PropertyChat.updateChatTime();
    }, 10000);

    // Update selected property threads
    $(document).on('change', 'select#property_id', function () {
        PropertyChat.getPropertyThreads($(this).val());
    });

    // Update chat Box when a new thread is selected
    $(document).on('click', '.client-chat', PropertyChat.updateChatForm);

    // Send Message
    $(document).on('click', '#chatForm button.chat-submit', PropertyChat.sendMessage);
    $(document).on('submit', '#chatForm', function (e) {
        e.preventDefault();
        PropertyChat.sendMessage();
    });

    // Display entered char count
    $(document).on('change, keyup', '#msgInput', PropertyChat.updateCountdown);

});


