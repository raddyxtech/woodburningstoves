
var AgentCalendar = {

    // Properties Start
    element: '.pick-weeks-calendar',
    calendar: {},
    calConfig: {
        today:new Date(),
        initialize: false,
        scheduleOptions: {
                colors: { other: '#5c6270',viewing: '#2fabb7', valuation: '#00bc1f'}
        },
        select: function(date, context) {
             var $this = $(this);
             var data = {date:date[0].format('YYYY-MM-DD')};
             var url = siteUrl + 'agents/agaxGetAppoinments';
             $('#ajax-appointments-details .loader-div').show();
             $.post(url,data,function(response){
                     document.getElementById('ajax-appointments-details').innerHTML = response;
                 }
             );
        }
    },
    
    init: function(options){
       var _this = this;
       _this.calConfig = $.extend( true, _this.calConfig, options );
       _this.calendar = $(_this.element).pignoseCalendar(_this.calConfig );
    }
};



