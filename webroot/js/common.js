(function() {
    var days = ['Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday'];

    var months = ['January','February','March','April','May','June','July','August','September','October','November','December'];

    Date.prototype.getMonthName = function() {
        return months[ this.getMonth() ];
    };
    Date.prototype.getDayName = function() {
        return days[ this.getDay() ];
    };
    Date.prototype.getSupDay = function() {
        var dt = this.getDate();
        var sup = 'th';
        if((dt === 1) || (dt === 21) || (dt === 31)) {
            sup = 'st';
        } else if((dt === 2) || (dt === 22)) {
            sup = 'nd';
        } else if((dt === 3) || (dt === 23)) {
            sup = 'rd';
        }
        return dt + sup.sup();
    };
    Date.prototype.formatDate = function() {
        var yyyy = this.getFullYear().toString();
        var mm = (this.getMonth()+1).toString(); // getMonth() is zero-based
        var dd  = this.getDate().toString();
        return yyyy + "/" + (mm[1]?mm:"0"+mm[0]) + "/" + (dd[1]?dd:"0"+dd[0]); // padding
    };
})();
function getCurrentMonth(date){
    var monthNames = ["January", "February", "March", "April", "May", "June",
        "July", "August", "September", "October", "November", "December"
    ];
    return monthNames[(new Date(date)).getMonth()];
}

$(document).on('click mousedown','.inactive,.button-loader',function(e){
    e.preventDefault();
});

function currencyNumbers(num, currencySymbol) {
    currencySymbol = typeof currencySymbol == 'undefined' ? '£' : currencySymbol;
    var price = ('' + num).replace(
            /(\d)(?=(?:\d{3})+(?:\.|$))|(\.\d\d?)\d*$/g,
            function (m, s1, s2) {
                return s2 || (s1 + ',');
            }
    );
    return currencySymbol + price;
}
function getFormdata(formObject){
    var formData = formObject.serializeArray();
    var urlEncoded ='';
    for(key in formData){
        if(formData[key].value != ''){
            urlEncoded += encodeURIComponent(formData[key].name) +'='+ encodeURIComponent(formData[key].value) + '&';
        }
    }
    return urlEncoded;
}

function validateMime(mime,type){
    return mime.split("/")[0].toLowerCase() == type.toLowerCase();
}

function viewingDateFormat(date){
    var formattedDate = '';
    try {
       var dateObj = new Date(date);
       formattedDate = dateObj.getDayName()+ ', '+dateObj.getSupDay() + ' '+ dateObj.getMonthName()+' '+ dateObj.getFullYear();
    } catch(e){
        formattedDate = e.message;
    }
   return  formattedDate;
}
function valuationDateTime(date,format){
    if(typeof date == 'undefined' || !date){
        return;
    }
    format = format || 'yyyy-mm-dd'; // default format
    var parts = date.match(/(\d+)/g), 
        i = 0, fmt = {};
    // extract date-part indexes from the format
    format.replace(/(yyyy|dd|mm)/g, function(part) { fmt[part] = i++; });

    var months = ['Jan', 'Feb','Mar','Apr', 'May', 'Jun','Jul','Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
    return months[parts[fmt['mm']]-1] + ' ' + parts[fmt['dd']] + ', ' + parts[fmt['yyyy']] + ' at ' + date.substring(11, 16) + 'Hr';
    
}

function getValuesJson(obj, phrase){
    return $.map(obj, function(value, key){
        return key.indexOf(phrase) == 0 ? value:null;
    });
}
