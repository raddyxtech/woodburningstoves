/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


$(function(){

    $('.profile-step-btn,.info-list').click(function(e){
        var jumpStep = $(this).data('step');
        var currStep = $('.profile-step:visible').data('step');
        var stepElements = $('.profile-step:visible').find('input,select');
        //        console.log(stepElements);
        if (stepElements.length && !stepElements.valid()){
            $('.search-prorerty-info').find('li').eq(currStep).removeClass('info-list complet-info').addClass('incomplet-info');
            if ($(this).hasClass('next')){
                e.preventDefault(); return;
            }
        } else { //alert('hdb');
            $('.search-prorerty-info').find('li').eq(currStep).removeClass('info-list incomplet-info').addClass('complet-info');
        }


        $('.profile-step[data-step=' + jumpStep + ']').addClass('active-info').siblings('.profile-step').removeClass('active-info');
        $('.search-prorerty-info').find('li').eq(jumpStep).removeClass('complet-info incomplet-info').addClass('active-info');
        $('.search-prorerty-info').focus();
    });
    $.each($('.profile-step'), function(key, step){
        if ($(step).find('.form-error').length){
            $('.search-prorerty-info').find('li').eq(key).removeClass('info-list complet-info').addClass('incomplet-info');
        }
    });
});
$(".profile-step-btn").click(function() {
    $("html, body").animate({ scrollTop: $('.ajent-profile-left').offset().top }, "slow");
    return false;
});
            
// Form Submission            
$('#main_form').submit(function(e) {
    
    if($('[name="price"]').val()) {
        return true;
    } else {
        e.preventDefault();
        $.toast.error({text: 'Please Fill Up The Marketing Price'});
    }
});


// Code for Visibility of Tenure Period Field
function showTenurePeriod() {
    if ($('.for_tenure input:checked').val() === '0') {
        $('.for_lease').parent().hide();
    } else {
        $('.for_lease').parent().show();
    }
};
showTenurePeriod();
$('.for_tenure').on('click', function() {
    showTenurePeriod();
});
// Code For Toggling Property And Cutomer Information
$('.info-link li a').on('click', function() {
    $('.info-link li a').removeClass('active-info');
    $(this).addClass('active-info');
    if ($(this).attr('data-name') === 'seller') {
        $('.seller_information').show();
        $('.property_information').hide();
    } else {
        $('.seller_information').hide();
        $('.property_information').show();
    }

});

// Submitting the first step
$('.profile-step-btn.next[data-step="1"]').on('click', function() {

    var form = $(document.createElement('form'));
    $(form).append($('.property_information').clone()[0]);
    $(form).append($('.seller_information').clone()[0]);
    var input = $("<input>").attr("type", "hidden").attr("name", "unique_id").val(propUID);
    $(form).append($(input));
    var data = $(form).serialize();
    $.ajax({
        type: 'POST',
        url: siteUrl + 'agents/ajax-edit-property/',
        data:  data,
        dataType:'json',
        success: function(response) {
            if(response.status == 'success'){
                $.toast.success({text: response.message});
            } else if(response.status == 'error'){
                $.toast.error({text: response.message});
            }
            $(form).remove();
        }

    });
});
// Code for Selecting all the Property Images
$('.img_gallery .select_img').on('click', function() {

    if ($(this).find('svg').hasClass('fa-square')) {
        $('.property_images :input:enabled').prop('checked', true);
        if($('.property_images :input:enabled').length > 0) {
        var html = 'Select All&nbsp;<i class="far fa-check-square"></i>';
        $('.select_img').html(html);
    } else {
            $.toast.error({text: 'Default Image cannot be Deleted'});
        }
    } else {
        $('.property_images :input:enabled').prop('checked', false);
        var html = 'Select All&nbsp;<i class="far fa-square"></i>';
        $('.select_img').html(html);
    }

});
// Code for Selecting all the Floor Plan Images
$('.select_plans').on('click', function() {

    if ($(this).find('svg').hasClass('fa-square')) {
        $('.property_floor_plans :input').prop('checked', true);
        var html = 'Select All&nbsp;<i class="far fa-check-square"></i>';
        $('.select_plans').html(html);
    } else {
        $('.property_floor_plans :input').prop('checked', false);
        var html = 'Select All&nbsp;<i class="far fa-square"></i>';
        $('.select_plans').html(html);
    }

});

            

            
// Code for Deleting Selected Property Images
$(document).on('click', '.img_gallery .delete_img', function() {

if ($('.property_images .ab-check:checked').length) {

    if (confirm('Are You Sure Want To Delete ?')) {

    var ids = []; var imgs = [];
    $('.property_images .ab-check:checked').siblings('img').each(function(index, value) {
        ids.push($(this).attr('data-imgid'));
        imgs.push($(this).attr('alt'));
    });
    $.ajax({
    type: 'POST',
        url: siteUrl + 'agents/ajax-delete-property-images',
        data: {ids: ids, imgs: imgs},
        dataType: "json",
        success: function(response) {
            if (response.status === 'success') {
                $.toast.success({text: response.message});
                $('.property_images .ab-check:checked').parent().remove();
                // If there are no property images after deletion, hide the delete and select all options
                if($('.property_images li').length < 1) {
                    $('.img_gallery span.float-r').hide();
                    $('.property_images').html('<h2>Property Images Unavailble </h2>');
                }
            } else {
                $.toast.error({text: response.message});
            }
        }

    });
    }
    } else {
        $.toast.error({text: 'Please Select An Image To Delete'});
    }
});
// Code for Deleting Selected Floor Plan Images
$(document).on('click', '.delete_plans', function() {

    if ($('.property_floor_plans .ab-check:checked').length) {

        if (confirm('Are You Sure Want To Delete ?')) {

            var ids = []; var imgs = [];
            $('.property_floor_plans .ab-check:checked').siblings('img').each(function(index, value) {
                ids.push($(this).attr('data-imgid'));
                imgs.push($(this).attr('alt'));
            });
            $.ajax({
                type: 'POST',
                url: siteUrl + 'agents/ajax-delete-floor-plans',
                data: {ids: ids, imgs: imgs},
                dataType: "json",
                success: function(response) {
                    if (response.status === 'success') {
                        $.toast.success({text: response.message});
                        $('.property_floor_plans .ab-check:checked').parent().remove();
                        // If there are no floor plans after deletion, hide the delete and select all options
                        if($('.property_floor_plans li').length < 1) {
                            $('.property_floor_plans').siblings('span').hide();
                            $('.property_floor_plans').html('<h2>Floor Plans Unavailable </h2>');
                        }
                    } else {
                        $.toast.error({text: response.message});
                    }
                }
            });
        }
        
    } else {
        $.toast.error({text: 'Please Select An Image To Delete'});
    }

});
// Code for Deleting features
$(document).on('click', '.delete_feature', function() {

    if (confirm('Are You Sure Want To Delete ?')) {
    var num = $(this).attr('data-num');
    $.ajax({
        type: 'POST',
        url: siteUrl + 'agents/ajax-delete-feature',
        data: {num: num, unique_id: propUID},
        dataType: "json",
        success: function(response) {
            if (response.status === 'success') {
                fetchFeatures();
                $.toast.success({text: response.message});
            } else {
                $.toast.error({text: response.message});
            }
        }
    });
    }

});
// Code for Adding Feature
$('.add_feature').on('click', function() {
    var feature = $('[name="feature"]').val();
    if (feature) {

        $.ajax({
            type: 'POST',
            url: siteUrl + 'agents/ajax-add-feature',
            data: {feature: feature, unique_id: propUID},
            dataType: "json",
            success: function(response) {
                if (response.status === 'success') {
                    fetchFeatures();
                    $('[name="feature"]').val('');
                    $.toast.success({text: response.message});
                } else {
                    $('[name="feature"]').val('');
                    $.toast.error({text: response.message});
                }
            }
        });
        
    } else {
        $.toast.error({text: 'Please Enter A Feature'});
    }



});
function fetchFeatures() {

    $.ajax({
        type: 'POST',
        url: siteUrl + 'agents/ajax-fetch-features',
        data: {unique_id: propUID},
        dataType: "json",
        success: function(response) {

            var features = JSON.parse(response.key_features);
            var html = '';
            var length = features.length;
            // If there is no table initially in the document (i.e., No Key Features Present Of The Property)
            // Then Add Table Else Add Table
            if ($('#key_features_table_update').length) {
                html = '<tr><th>Key Features:</th></tr>';
            } else {
                html = '<table class="key-features-table" cellpadding="0" cellspacing="0" id="key_features_table_update"><tr><th>Key Features:</th></tr>';
            }

            // This is for checking no. of features present after deletion
            if (length) {
                for (var i = length - 1; i >= 0; i--) {
                    html += '<tr><td>';
                    html += '<strong>' + features[i] + '</strong> ';
                    html += '<a href="javascript:;" class="delete-btn delete_feature" data-num="' + i + '"><i class="fas fa-trash-alt"></i></a>';
                    html += '</td></tr>';
                }

                // If there is no table initially in the document (i.e., No Key Features Present Of The Property)
                // Then Add Table Else Add Table
                if ($('.key-features-table').length) {
                    $('.key-features-table').html(html);
                } else {
                    html += '</table>';
                    $('.key_features_part').html(html);
                }

            } else {
                html = '<h2>Key Features Unavailable </h2>';
                $('.key_features_part').html(html);
            }

        }

    });
}


         
// Code for Vertical Tab in Description Step
function openTab(evt, tabName) {
    var i, tabcontent, tablinks;
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }
    tablinks = document.getElementsByClassName("tablinks");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active", "");
    }
    document.getElementById(tabName).style.display = "block";
    evt.currentTarget.className += " active";
    $('.tabcontent textarea').prop('readonly', true).addClass('for_readonly');
}

// Get the element with id="defaultOpen" and click on it
if(document.getElementById("defaultOpen")) {
    document.getElementById("defaultOpen").click();
}


// Code for editing the the description of the selected feature
$(document).on('click', '.tabcontent .edit_icon', function() {
    toggleDescription($(this));
});
// Code for editing the the description of the selected feature
$(document).on('click', '.tabcontent .description_complete', function() {
    toggleDescription($(this));
});
    

function toggleDescription(element) {
    
    // This condition is to check wether to edit description or complete description
    if(element.hasClass('edit_icon')) {
        $(element).next().prop('readonly', false).removeClass('for_readonly').focus();
        $(element).siblings('input').prop('disabled', false).prop('checked', false);
    } else {
        $(element).prev().prop('readonly', true).addClass('for_readonly');
        $(element).prop('disabled', true);
        $(element).prev().on('change', function() {
            updateDescription();
        });
    }
    
}
    
// Code For Updating the descriptions of the Property
function updateDescription() {
    
    var descriptions = [];
    $('.description_step .tabcontent textarea').each(function(index, value) {
        descriptions.push(value.value);
    });
    
    $.ajax({
        type: 'POST',
        url: siteUrl + 'agents/ajax-save-description',
        data: {descriptions: descriptions, unique_id: propUID},
        dataType: "json",
        success: function(response) {
            if(response.status === 'success') {
                $.toast.success({text: response.message});
            } else {
                $.toast.error({text: response.message});
            }
        }
    });
    
}

// Code for Deleting Description
$(document).on('click', '.delete_description', function() {

    var num = $(this).attr('data-num');
    
    if(confirm('Are You Sure Want To Delete ?')) {
        
        $.ajax({
            type: 'POST',
            url: siteUrl + 'agents/ajax-delete-description',
            data: {id: num, unique_id: propUID},
            dataType: "json",
            success: function(response) {
                if(response.status === 'success') {
                    fetchDescriptions();
                    $.toast.success({text: response.message});
                } else {
                    $.toast.error({text: response.message});
                }
            }
        });
        
    }
    

});

// Code for adding description
$('.add_description').on('click', function() {  

    var feature = $('[name="description_feature"]').val();  
    if (feature) {

        $.ajax({
            type: 'POST',
            url: siteUrl + 'agents/ajax-add-description',
            data: {feature: feature, unique_id: propUID},
            dataType: "json",
            success: function(response) {
            if (response.status === 'success') {
                fetchDescriptions();
                $('[name="description_feature"]').val('');
                $.toast.success({text: 'Feature Added Successfully'});
            }
            }
        });
    } else {
        $.toast.error({text: 'Please Enter A Feature'});
    }

});

// Description Fetching Function
function fetchDescriptions() {

    $.ajax({
        type: 'POST',
        url: siteUrl + 'agents/ajax-fetch-descriptions',
        data: {unique_id: propUID},
        dataType: "json",
        success: function(response) {
            var descriptions = JSON.parse(response.description);  
            var tabs = '';
            var tabcontents = '';
            var html = '';
            var length = descriptions.length;
            // This is for checking no. of descriptions present after deletion
            if (length) {
                var showed = length-1;
                html += '<div class="tab">';
                var once = 'id="defaultOpen"';
                for (var i = showed; i >= 0; i--) {
                    tabs += '<span class="tablinks" onclick="openTab(event, \'tab'+ i +'\')" '+once+'>';
                    tabs += '<strong>'+descriptions[i].feature+'</strong>';
                    tabs += '<a href="javascript:;" class="delete-btn delete_description hint--top" data-num="'+ i +'" aria-label="Delete This Feature And Its Description"><i class="fas fa-trash-alt"></i></a>';
                    tabs += '</span>';
                    
                    tabcontents += '<div id="tab'+i+'" class="tabcontent">';
                    tabcontents += '<h3>Description for: '+descriptions[i].feature+'</h3>';
                    tabcontents += '<span class="hint--top edit_icon" aria-label="Edit This Feature\'s Description"><i class="fas fa-pen-square"></i></span>';
                    tabcontents += '<textarea rows="4" cols="45" readonly="readonly" class="for_readonly">'+descriptions[i].description+'</textarea>';
                    tabcontents += '<input type="checkbox" class="description_complete float-r" checked="checked" disabled="disabled"><span class="float-r description_complete_word">Description Complete&nbsp;</span>';
                    tabcontents += '</div>';
                    
                    once = '';
                }
                
                html += tabs;
                html += '</div>';
                html += tabcontents;

            } 
            
            $('.description_step').html(html).addClass('description_border');
            document.getElementById("defaultOpen").click();
        }
    });

}


function fetchPropertyPhotos() {

    $.ajax({
        type: 'POST',
        url: siteUrl + 'agents/ajax-fetch-property-photos',
        data: {property_id: propID },
        success: function(response) {
            var propertyGalleries = JSON.parse(response);
            var html = '';
            for (foto in propertyGalleries) {
                var check = (propertyGalleries[foto].default_image !== null);
                html += '<li class="foto_effect_list">';
                html += '<img alt="' + propertyGalleries[foto].file + '" src="' + siteUrl + 'files/property_images/' + propertyGalleries[foto].file + '" class="for_foto_effect" data-imgid="' + propertyGalleries[foto].id + '">';
                html += check ? '<span class="default_image_icon hint--top" style="display: none;" aria-label="Make This As This Default Image"><i class="fas fa-image"></i></span>' : '<span class="default_image_icon hint--top" aria-label="Make This As The Default Image"><i class="fas fa-image"></i></span>';
                html += check ? '<input type="checkbox" class="ab-check" name="property_gal[]" disabled >' : '<input type="checkbox" class="ab-check" name="property_gal[]" >';
                html += '</li>';
            }

            $('.property_images').html(html);
            // Initially no photos are present and new photos are added
            if($('.img_gallery span.float-r:visible').length === 0) {
                // If all the images were deleted by selecting "select all" option, then after uploading a photo, the checkbox should not be ticked
                $('.img_gallery span.float-r.select_img').html('Select All&nbsp;<i class="far fa-square"></i>');
                $('.img_gallery span.float-r').show();
        }
        }

    });
}


function fetchFloorPlans() {

    $.ajax({
        type: 'POST',
        url: siteUrl + 'agents/ajax-fetch-floor-plans',
        data: {property_id: propID },
        success: function(response) {
            var floorPlans = JSON.parse(response);
            var html = '';
            for (foto in floorPlans) {
                var check = (floorPlans[foto].default_image !== null);
                html += '<li class="foto_effect_list">';
                html += '<img alt="' + floorPlans[foto].file + '" src="' + siteUrl + 'files/property_images/' + floorPlans[foto].file + '" data-imgid="' + floorPlans[foto].id + '">';
                html += '<input type="checkbox" class="ab-check" name="floor_plans[]">';
                html += '</li>';
            }

            $('.property_floor_plans').html(html);
            // Initially no photos are present and new photos are added
            if($('.property_floor_plans').siblings('span:visible').length === 0) {
                // If all the images were deleted by selecting "select all" option, then after uploading a photo, the checkbox should not be ticked
                $('.property_floor_plans').siblings('span.select_plans').html('Select All&nbsp;<i class="far fa-square"></i>');
                $('.property_floor_plans').siblings('span').show();
        }
        }

    });
}

// Code for Uploading Questionnaire File
$('.upload_question_file').on('click', function() {

    var files = $('#questionnaireFile')[0].files;   
    var data = new FormData();
    $.each(files, function(key, value){
        data.append(key, value);
    });
    data.append('property_id', propID);
    data.append('file', 'questionnaire');
    
    if(files.length) {
        uploadCertificate('questionnaire', data);
    } else {
        $.toast.error({text: 'Please, Choose a PDF File'});
    }

});



// Code for Updating Property Compliance
//$('.profile-step-btn.next[data-step="2"]').on('click', function() {
//    
//    if($('.certificate_name a').text() === 'No File Uploaded') {
//        
//        $("#certificateFile").rules("add", {
//        required: true,
//        messages: {
//          required: "Required Certificate File"
//        }
//    });
//
//        $("#questionnaireFile").rules("add", {
//        required: true,
//        messages: {
//          required: "Required Questionnaire File"
//        }
//    });
//    
//        $("#online_input_url").rules("add", {
//        required: true,
//        messages: {
//            required: "Please Upload A Certificate File"
//        }
//    });
//
//    }
//
//    
//
//
//    var title = $('[name="property_compliance[title]"]:checked').val();
//    var first_name = $('[name="property_compliance[first_name]"]').val();
//    var last_name = $('[name="property_compliance[last_name]"]').val();
//    var certificate_file = ($('.certificate_name a').text() !== 'No File Uploaded') ? $('.certificate_name a').text() : null;
////    var status = $('[name="property_compliance[status]"]:checked').val();
////    var questionnaire_file = $('#questionnaireFile').val();
//    
//    var property_compliance = { 'title': title, 'first_name': first_name, 'last_name': last_name, 'certificate_file': certificate_file, 'property_id': propID };
//    
//    // Code for deleting properties with null or empty value
//    Object.keys(property_compliance).forEach((key) => (property_compliance[key] == null || property_compliance[key] == '') && delete property_compliance[key]);
//    var valid = Object.values(property_compliance).length; 
//    
//    // If Upload Button is not pressed, after giving online link
//    if(!certificate_file) {
//        $('#online_input_url').val('');
//    }
//    
//    if(valid && (valid >= 5)) {
////    if(valid) {
////        $.ajax({
////            type: 'POST',
////            url: siteUrl + 'agents/ajax-save-property-compliance',
////            data: property_compliance,
////            dataType: 'JSON',
////            success: function(response) {
////                if(response.status === 'success') {
////                    $.toast.success({text: response.message});
////                } else {
////                    $.toast.error({text: response.message});
////                }
////            }
////        });
//    } 
////    else {
////        $('.button-2 profile-step-btn prev[data-step="1"]').trigger('click');
//////        $.toast.error({text: 'Please Fill All The Fields Before Proceeding'});
////    }
//    
//
//    
//
//});





//});