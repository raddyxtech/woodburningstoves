
<div class="property_result_sec_right" id="mapsticky">
 <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2379.4808177881864!2d-2.9149358846765083!3d53.38833837926696!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x487b203777bfbe6f%3A0x12417b7f70955199!2s52-54+Allerton+Rd%2C+Liverpool+L18+1LN%2C+UK!5e0!3m2!1sen!2sin!4v1526637812285" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>

</div>
<div class="property_result_sec_left">

 <?php foreach ($dataProperties as $propertries) { ?>
    <div class="result_box">
     <div class="result_box_row">
      <div class="result_box_col">
       <div class="imgbox"><img src="<?php echo $propertries->profilePic ?>" alt="<?php echo $propertries->advert_heading ?>"/></div>
      </div>
      <div class="result_box_col">
       <h3> <a href="<?php echo HTTP_ROOT . 'property-for-sale-in-' . $this->Custom->makeSeoUrl(@$propertries->area) . '/' . $this->Custom->makeSeoUrl(@$propertries->advert_heading) . '-' . @$propertries->property_reference ?>"><?php echo $propertries->area ?>, <?php echo $propertries->town ?></a></h3>
       <h4><?php echo $propertries->price_text ?></h4>
       <p><?php echo $propertries->advert_heading ?></p>
       <ul>
        <li><a href="javascript:;"><i class="fa fa-star" aria-hidden="true"></i> Favourites</a></li>
        <li><a href="javascript:;"><i class="fa fa-comment" aria-hidden="true"></i> Enquire</a></li>
        <li><a href="javascript:;"><i class="fa fa-info-circle" aria-hidden="true"></i> More Details</a></li>
       </ul>
      </div>
     </div>
    </div>

   <?php } ?>

</div>

<script type="text/javascript">
 window.onscroll=function(){
  myFunction();
 }

 var searchBar=document.getElementById("searchbar");
 var mapSticky=document.getElementById("mapsticky");
 var sticky=searchBar.offsetTop;
 var sticky2=searchBar.offsetTop;
 function myFunction(){
  if(window.pageYOffset>=sticky){
   searchBar.classList.add("sticky");
   mapSticky.classList.add("sticky2");
  }else{
   searchBar.classList.remove("sticky");
   mapSticky.classList.remove("sticky2");
  }
 }

 $(document).ready(function(){
	$(window).scroll(function(){
		var stopBtm=$("#stopfixed").offset();
		var footerBottomH=parseInt($(".footer").outerHeight());
		//var footerTopH = parseInt($(".logo_row").outerHeight());
		//var allFooterH = parseInt(footerBottomH + footerTopH);
		var windowHight=parseInt($(window).height());
		var totalFooter=parseInt(windowHight+footerBottomH);
		var footerX=stopBtm.top;
		var stopFixed=parseInt(footerX-totalFooter);   
		if($(window).scrollTop()>=stopFixed){
		$("#mapsticky").addClass("sticky22");
		}else{
		$("#mapsticky").removeClass("sticky22");
		}
	});
 });
</script>
