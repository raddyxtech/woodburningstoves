(function ($) {
    var uploadUrl= $('#pageUrl').val() + 'vendors/ajax-upload-portfolio/';
    var myDropzone = Dropzone.options.portfolioUpload = {
        autoProcessQueue: false,
        autoDiscover: false,
        acceptedFiles: 'image/*',
        addRemoveLinks: true,
        parallelUploads: 10,
        dictDefaultMessage:'<span style="text-align:center;"><img src="img/dropzone.png" alt="Upload Portfolio Pictures"/> <h3>Drop Files Here</h3></span>',
        url: uploadUrl,
        init: function () {
            var myDropzone = this;
            var uploadedFileCount = 0;
            var acceptedFileCount = 0;
            // Update selector to match your button
            $("button.upload-portfolio").click(function (e) {
                e.preventDefault();
                myDropzone.processQueue();
            });

            this.on('sending', function (file, xhr, formData) {
                acceptedFileCount = this.getAcceptedFiles().length;
                // Append all form inputs to the formData Dropzone will POST
                var data = $('#portfolioUpload').serializeArray();
                $.each(data, function (key, el) {
                    formData.append(el.name, el.value);
                });
            });
            this.on('success', function (file, response, xhr) {
                var responseObj = JSON.parse(response);
                if (responseObj.status == 'success') {
                    var html = '<li><a class="image-thumb" href="files/vendor_pic/' + responseObj.data.file + '" ><img src="files/vendor_pic/Thumb-' + responseObj.data.file + '" alt="" /><a>' +
                            '<a href="javascript:;" data-id="' + responseObj.data.id + '" class="thumb-btn del-btn del-image hint--top" data-hint="Delete?" >' +
                            '<i aria-hidden="true"></i>' +
                            '</a>' +
                            '</li>';

                    $(".photo-gallery.portfolio-pic").append(html);
                    uploadedFileCount += 1;
                    this.removeFile(file);
                }
            });

            this.on('error', function (file, xhr, formData) {

            });

            this.on('complete', function (file) {
                if (acceptedFileCount && this.getUploadingFiles().length === 0 && this.getQueuedFiles().length === 0) {
                    if (uploadedFileCount && acceptedFileCount == uploadedFileCount) {
                        $.notify({message: 'All files uploaded successfully.'}, {type: 'success'});
                        uploadedFileCount = acceptedFileCount = 0;
                    } else if (acceptedFileCount > uploadedFileCount) {
                        $.notify({message: 'Some files couldnot uploaded try again!!'}, {type: 'danger'});
                    } else {
                        $.notify({message: 'Files not uploaded try again!!'}, {type: 'danger'});
                    }
                    $('#portfolioModal').modal('hide');
                }
            });

        }
    };
}(jQuery));
