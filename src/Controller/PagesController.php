<?php

namespace App\Controller;

use Cake\ORM\TableRegistry;
use App\Controller\AppController;
use Cake\Validation\Validation;

class PagesController extends AppController {

    function initialize() {
        parent::initialize();

        //load model
        $this->loadModel('Cms');
        $this->loadModel('Brands');
        $this->loadModel('Testimonials');
        $this->loadModel('CmsPages');
        $this->loadModel('Users');
        $this->loadModel('HomeImages');
        $this->loadModel('Company');
        $this->loadModel('Products');
        $this->loadModel('ProductVariations');
        $this->loadModel('OrdersProducts');
        $this->loadModel('Images');
        $this->loadModel('Categories');
        $this->loadModel('Faqs');
        $this->loadModel('FavoriteProducts');
        $this->loadModel('BannerImages');

        //load component
        $this->loadComponent('Flash');
        $this->loadComponent('Custom');
        $this->loadComponent('Paginator');

        $publicActions = ['index', 'deliveryInfo', 'returnsInfo', 'listbrands', 'reviews', 'reviewSubmit', 'adeptEkol',
            'boilerStove', 'buildingRegulations', 'ecoDesign', 'termConditions', 'privacyPolicy', 'listVideo',
            'installerBlog', 'visitShowroom', 'flexiLiner', 'twinWall', 'stoveGlossary',
            'galleryInstallations', 'installationAdvice', 'newProducts', 'peanutStove', 'sia', 'login', 'logout', 'contactus', 'faqs', 'returns', 'delivery'];

        $this->Auth->allow($publicActions);

        // Set Layout
        $this->viewBuilder()->setLayout('default');
    }

    public function index() {

        $home = $this->Cms->find()->where(['id' => 5])->first();
        $listHomeImgs = $this->HomeImages->find()->where(['is_active' => 1]);
        $brandImgs = $this->Brands->find()->where(['is_active' => 1]);
        $bannerImgs = $this->BannerImages->find()->where(['is_active' => 1]);
        $favProductList = $this->FavoriteProducts->find('list', ['keyField' => 'product_id', 'valueField' => 'product_id'])->where(['user_id' => $this->Auth->user('id')])->toArray();
        

        $CategorieImgs = $this->Categories->find()->where(['parent_id' => 0, 'baby_cat' => 'N']);
//        pj($CategorieImgs);EXIT;
        $cmsTopContent = $this->Cms->find()->where(['type' => 'home'])->first();
        $config = [
            'limit' => 6,
            'contain' => ['ProductVariations', 'OrdersProducts', 'Images', 'Brands'],
            'order' => ['Products.name' => 'ASC']
        ];

        $products = $this->Paginator->paginate($this->Products->find()->where(['Products.hidden' => 'N']), $config);
//        pj($products);exit;
        $this->set(compact(['listHomeImgs', 'brandImgs', 'cmsTopContent', 'brands', 'products', 'CategorieImgs', 'home','favProductList','bannerImgs']));
    }

    public function deliveryInfo() {
        $cms = $this->Cms->find()->where(['id' => 1])->first();
        $this->set(compact('cms'));
    }

    public function returnsInfo() {
        $cms = $this->Cms->find()->where(['id' => 3])->first();
        $this->set(compact('cms'));
    }

    public function listbrands() {
        $brands = $this->Brands->find();
        $this->set(compact(['brands']));
    }

    public function reviews() {
        $reviews = $this->Testimonials->find()->where(['approved' => 'Y']);
        $this->set(compact(['reviews']));
    }

    public function reviewSubmit() {
        if ($this->request->is('post')) {
            $data = $this->request->getData();
            $data['when_said'] = date("Y-m-d");
            $newEntity = $this->Testimonials->newEntity();
            $this->Testimonials->patchEntity($newEntity, $data);
//            pj($newEntity->getErrors());exit;
            if ($this->Testimonials->save($newEntity)) {
                $this->Flash->success('Thank You for your review');
            }
        }
    }

    public function adeptEkol() {
        $adeptEkol = $this->CmsPages->find()->where(['id' => 1])->first();
        $this->set(compact('adeptEkol'));
    }

    public function boilerStove() {
        $boilerStove = $this->CmsPages->find()->where(['id' => 2])->first();
        $this->set(compact('boilerStove'));
    }

    public function buildingRegulations() {
        $buildingRegulations = $this->CmsPages->find()->where(['id' => 4])->first();
        $this->set(compact('buildingRegulations'));
    }

    public function ecoDesign() {
        $ecoDesign = $this->CmsPages->find()->where(['id' => 17])->first();
        $this->set(compact('ecoDesign'));
    }

    public function termConditions() {
        $termConditions = $this->Cms->find()->where(['id' => 4])->first();
        $this->set(compact('termConditions'));
    }

    public function privacyPolicy() {
        
    }

    public function listVideo() {
        $listVideo = $this->CmsPages->find()->where(['id' => 7])->first();
        $this->set(compact('listVideo'));
    }

    public function installerBlog() {
        $installerBlog = $this->CmsPages->find()->where(['id' => 6])->first();
        $this->set(compact('installerBlog'));
    }

    public function visitShowroom() {
        $visitShowroom = $this->CmsPages->find()->where(['id' => 8])->first();
        $this->set(compact('visitShowroom'));
    }

    public function flexiLiner() {
        $flexiLiner = $this->CmsPages->find()->where(['id' => 9])->first();
        $this->set(compact('flexiLiner'));
    }

    public function twinWall() {
        $twinWall = $this->CmsPages->find()->where(['id' => 10])->first();
        $this->set(compact('twinWall'));
    }

    public function galleryInstallations() {
        $galleryInstallations = $this->CmsPages->find()->where(['id' => 11])->first();
        $this->set(compact('galleryInstallations'));
    }

    public function stoveGlossary() {
        $stoveGlossary = $this->CmsPages->find()->where(['id' => 12])->first();
        $this->set(compact('stoveGlossary'));
    }

    public function installationAdvice() {
        $installationAdvice = $this->CmsPages->find()->where(['id' => 13])->first();
        $this->set(compact('installationAdvice'));
    }

    public function newProducts() {
        $newProducts = $this->CmsPages->find()->where(['id' => 14])->first();
        $this->set(compact('newProducts'));
    }

    public function peanutStove() {
        $peanutStove = $this->CmsPages->find()->where(['id' => 15])->first();
        $this->set(compact('peanutStove'));
    }

    public function sia() {
        $sia = $this->CmsPages->find()->where(['id' => 15])->first();
        $this->set(compact('sia'));
    }

    public function faqs() {
        $faqs = $this->Faqs->find();
//        pj($faqs);exit;
        $this->set(compact('faqs'));
    }

    public function returns() {
        $returnCms = $this->Cms->find()->where(['id' => 3])->first();
        $this->set(compact('returnCms'));
    }

    public function delivery() {
        $deliveryCms = $this->Cms->find()->where(['id' => 1])->first();
        $this->set(compact('deliveryCms'));
    }

//contact us part
    public function contactus() {
        $companyInfo = $this->Company->find()->first();
        $this->set(compact('companyInfo'));

        try {
            if ($this->request->is('post')) {
                $data = $this->request->getData();
                $name = $data['name'];
                $email = filter_var($data['email'], FILTER_SANITIZE_EMAIL);
                $tel = $data['tel'];
                $enquiry = $data['enquiry'];
                $value = $data['g-recaptcha-response'];
                $subnewsletter = $data['sub_newsletter'];

                if (empty($name)) {
                    $this->Flash->error(__('Name Required.'));
                } else if (empty($email)) {
                    $this->Flash->error(__('Email Required.'));
                } else if (empty($tel)) {
                    $this->Flash->error(__('Telephone Number Required.'));
                } else if (empty($enquiry)) {
                    $this->Flash->error(__('Enquiry Required.'));
                } else if (!$this->Users->validateRecaptcha($value)) {
                    $this->Flash->error(__('Please Complete Recaptcha.'));
                } else if (empty($subnewsletter)) {
                    $this->Flash->error(__('Please check newsletter box.'));
                } else {
                    $this->_contactUsMail($data);
                    $this->Flash->success(__('Thanks for contacting us.'));
                    return $this->redirect($this->referer());
                }
            }
        } catch (Exception $ex) {
            
        }
    }

    /*
     * Send contact email to admin. Called Internally.
     */

    private function _contactUsMail($data = []) {
        try {
            $this->loadModel('Settings');
            $this->loadComponent('SendEmail');

            $template = $this->Settings->getTemplate('CONTACT_US');
            $settings = $this->Settings->getTemplate('ADMIN_EMAIL');
            $adminEmail = $settings->value;
            $subject = $template->display;
            $msg = $template->value;
            $msg = str_replace(array("[TEXT]"), $template->display, $msg);
            $msg = str_replace(array("[NAME]"), $data['name'], $msg);
            $msg = str_replace(array("[EMAIL]"), $data['email'], $msg);
            $msg = str_replace(array("[NUMBER]"), $data['tel'], $msg);
            $msg = str_replace(array("[MESSAGE]"), $data['enquiry'], $msg);
            $msg = str_replace(array("[SITENAME]"), SITE_NAME, $msg);

            $this->SendEmail->sendEmail($adminEmail, $subject, $msg);
            $this->SendEmail->sendEmail($data['email'], $subject, $msg);
        } catch (Exception $ex) {
            
        }
    }

}
