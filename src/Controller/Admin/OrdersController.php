<?php

namespace App\Controller\Admin;

use App\Controller\AppController;

class OrdersController extends AppController {

    public function initialize() {
        parent::initialize();
// Load Model
        $this->loadModel('Orders');
        $this->loadModel('Products');
        $this->loadModel('ProductVariations');
        $this->loadModel('OrdersProducts');
        $this->loadModel('Images');
        $this->loadModel('Users');
// Load Components
        $this->loadComponent('Custom');
        $this->loadComponent('Paginator');
//set layout
        $this->viewBuilder()->setLayout('admin');
    }

    public function orderDetails() {
        if ($this->request->is('post')) {
            $params = array_filter($this->request->getData());
            return $this->redirect(['prefix' => 'admin', 'controller' => 'Orders', 'action' => 'orderDetails', '?' => $params]);
        }

        $query = $this->request->getQueryParams();
        $conditions = [];

        if (!empty($query['fl_keywords'])) {
            $conditions['OR']['Orders.invoice_firstname LIKE'] = $query['fl_keywords'] . '%';
            $conditions['OR']['Orders.invoice_lastname LIKE'] = $query['fl_keywords'] . '%';
            $conditions['OR']['Orders.invoice_address1 LIKE'] = '%' . $query['fl_keywords'] . '%';
            $conditions['OR']['Orders.invoice_address2 LIKE'] = '%' . $query['fl_keywords'] . '%';
            $conditions['OR']['Orders.invoice_city LIKE'] = '%' . $query['fl_keywords'] . '%';
            $conditions['OR']['Orders.invoice_county LIKE'] = '%' . $query['fl_keywords'] . '%';
            $conditions['OR']['Orders.invoice_postcode LIKE'] = '%' . $query['fl_keywords'] . '%';
        }
        if (isset($query['fl_status']) && $query['fl_status'] != '') {
            $conditions['Orders.order_status'] = $query['fl_status'];
        }
        if (!empty($query['fl_date_from'])) {
            $conditions['DATE(Orders.date_created) >='] = date('Y-m-d', strtotime($query['fl_date_from']));
        }
        if (!empty($query['fl_date_to'])) {
            $conditions['DATE(Orders.date_created) <='] = date("Y-m-d", strtotime($query['fl_date_to']));
        }
        $orderLists = $this->Orders->find()->contain(['OrdersProducts'])->where($conditions);
        $config = [
            'limits' => 10
        ];
        $orderLists = $this->Paginator->paginate($orderLists, $config);
// pj($orderLists);exit;
        $this->set(compact(['orderLists', 'query']));
    }

    public function previewOrder( $id=null, $order_id =null) {
               
        $this->viewBuilder()->setLayout('admin');

        $order = $this->Orders->find()->contain(['OrdersProducts.products.ProductVariations'])->where(['Orders.order_id' => $order_id])->first();
        $order_products = $this->OrdersProducts->find()->where(['OrdersProducts.id' => $id])->first();
//         pj($order);exit;
        if (!empty($order->orders_products)) {

            foreach ($order->orders_products as $product)
// pj ($product);exit;
                $delivery_charge = $this->_getDeliveryChargeExclVATl($order->delivery_cost, $order->vat_rate);
            $sub_total = $this->_getProductsTotalExclVAT($product);
            $TotalExclVAT = $this->_getItemTotalExclVAT($product->unit_price, $product->qty, $product->vat_rate);
            $vats = $this->_getVAT($product, $delivery_charge, $order->vat_rate);
            $total = $product->total;
        }
        
        if ($this->request->is(['post', 'patch', 'put'])) {
            $data = $this->request->getData();
            $updateData = array_combine($data["id"], $data["qty_dispatched"]);
            $orderproducts = $this->OrdersProducts->find();
            foreach ($orderproducts as $orderproduct) {
                @$orderproduct->qty_dispatched = $updateData[$orderproduct->id];
            }
            $this->OrdersProducts->saveMany($orderproducts);
            return $this->redirect(['prefix' => 'admin', 'controller' => 'Orders', 'action' => 'preview-order', $data["oid"]]);
        }
        $this->set(compact('order', 'delivery_charge', 'TotalExclVAT', 'sub_total', 'vats', 'total', 'orderproducts', 'order_products'));
    }

    public function orderHistory() {
        if ($this->request->is(['post', 'put', 'patch'])) {
//            $this->viewBuilder()->setLayout('');
            $data = $this->request->getData();
            if ($data['status'] == 'CANCEL') {
                $update = $this->OrdersProducts->query()->update()->set(['cancel_status' => ORDSTATUS_CANCEL])->where(['id' => $data['id']])->execute();
                $userInfos = $this->OrdersProducts->find()->contain(['Products.Images', 'Users']);
            } elseif ($data['status'] == 'RETURN') {
                $this->OrdersProducts->sendEmail($data['id'], 'RETURN');
                exit;
                $update = $this->OrdersProducts->query()->update()->set(['return_status' => ORDSTATUS_RETURN])->where(['id' => $data['id']])->execute();
                $userInfos = $this->OrdersProducts->find()->contain(['Products.Images', 'Users']);
            }
        } else {
            $userInfos = $this->OrdersProducts->find()->contain(['Products.Images', 'Users']);
        }
        $this->set(compact('userInfos'));
    }

//approve order
    public function ajaxApprove() {
        $this->viewBuilder()->setLayout('ajax');

        if ($this->request->is(['post'])) {
            $data = $this->request->getData();
            $orderapprove = $this->Orders->query()->update()->set(['order_status' => ORDSTATUS_APPROVED])->where(['order_id' => $data['order_id']])->execute();
            if ($this->Orders->save($orderapprove)) {

                echo json_encode(['status' => 'success', 'msg' => 'update Successfully!!']);
            } else {
                echo json_encode(['status' => 'error', 'msg' => 'Error Occured!!']);
            }

            exit;
        }
    }

//pay order
    public function ajaxPay() {
        $this->viewBuilder()->setLayout('ajax');

        if ($this->request->is(['post'])) {
            $data = $this->request->getData();
// pj($data);exit;
// $orderfetch = $this->Orders->find()->where(['order_id' => $data['order_id']])->first();
            $orderPAY = $this->Orders->query()->update()->set(['paid' => 'Y'])->where(['order_id' => $data['order_id']])->execute();
// return $this->redirect(['prefix' => 'admin', 'controller' => 'Orders', 'action' => 'preview-order', $data['order_id']]);

            if ($this->Orders->save($orderPAY)) {

                echo json_encode(['status' => 'success', 'msg' => 'update Successfully!!']);
            } else {
                echo json_encode(['status' => 'error', 'msg' => 'Error Occured!!']);
            }

            exit;
        }
    }

    private function _getDeliveryChargeExclVATl($delivery_cost, $fVATRate = DEFAULT_VAT_RATE) {

        $calc_vat_included = ($delivery_cost / (100 + $fVATRate) * $fVATRate);
        $deduct_vat = $delivery_cost - number_format($calc_vat_included, 2, '.', '');
        return number_format($deduct_vat, 2, '.', '');
    }

    private function _getItemTotalExclVAT($unit_price, $qty, $fVATRate = '') {
        $fTotal = $unit_price * $qty;

        if ($fVATRate == "" || $fVATRate == 0.00) {
            return DEFAULT_VAT_RATE;
        }

        $calc_vat_include = ($fTotal / (100 + $fVATRate) * $fVATRate);
        $deduct = $fTotal - number_format($calc_vat_include, 2, '.', '');
        return number_format($deduct, 2, '.', '');
    }

    private function _getProductsTotalExclVAT($product) {
        $fTotal = 0.00;
        $fTotal += $this->_getItemTotalExclVAT($product->unit_price, $product->qty, $product->vat_rate);
        return number_format($fTotal, 2, '.', '');
    }

    private function _getSubTotal($product, $delivery_charge) {
        $fTotal = $this->_getProductsTotalExclVAT($product);
        $fTotal += $delivery_charge;

        return number_format($fTotal, 2, '.', '');
    }

    private function _getZeroVATProductsTotal($product) {
        $fTotal = 0.00;
        if ($product->unit_price = !NO) {

            $fUnitPrice = $product->unit_price;
            $Qty = $product->qty;
            $fTotal = $fUnitPrice * $Qty;
            return number_format($fTotal, 2, '.', '');
        }
    }

    private function _getVAT($product, $delivery_charge, $fVATRate) {
        $fSub = $this->_getSubTotal($product, $delivery_charge);
        $fVATLessSub = $this->_getZeroVATProductsTotal($product);
        $fSub = $fSub - $fVATLessSub;
        if ($fVATRate == "" || $fVATRate == 0.00) {
            return DEFAULT_VAT_RATE;
        }
        $fVATExcluded = $fSub * ($fVATRate / 100);
        return number_format($fVATExcluded, 2, '.', '');
    }

}
