<?php
namespace App\Controller\Admin;
use App\Controller\AppController;
class MiscellaneousController extends AdminController {
    public function initialize() {
        parent::initialize();
        // Load Model
        $this->loadModel('HomeImages');
        $this->loadModel('Faqs');
        $this->loadModel('Countries');
        $this->loadModel('Company');
        $this->loadModel('Testimonials');
        // Load Component
        $this->loadComponent('Custom');
        $this->viewBuilder()->setLayout('admin');
        $this->Auth->allow(['listHomeImages']);
    }
    public function listHomeImages() {
        $homeImages = $this->HomeImages->find();
        $this->set(compact(['homeImages']));
    }
    public function addNewHomeImage() {
        $homeImage = $this->HomeImages->newEntity();
        if ($this->request->is(['post', 'patch', 'put'])) {
            $data = $this->request->getData();
            $data['unique_id'] = $this->Custom->generateUniqNumber();
            $this->HomeImages->patchEntity($homeImage, $data);
            if ($this->HomeImages->save($homeImage)) {
                if (!empty($data['home_image'])) {
                    $result = $this->Custom->croppieImageUpload2($data['home_image'], HOME_PIC);
                    if ($result->status) {
                        $this->HomeImages->query()->update()->set(['filename' => $result->file])->where(['id' => $homeImage->id])->execute();
                    }
                }
                $this->Flash->success(__('Home Image Added successfully'));
                return $this->redirect(['action' => 'listHomeImages']);
            } else {
                $message = getFirstError($homeImage->getErrors());
                $this->Flash->error($message);
            }
        }
        $this->set(compact(['homeImage']));
    }
    public function homeImageStatus($uniqId = NULL) {
        $homeImage = $this->HomeImages->find()->where(['unique_id' => $uniqId])->first();
        if ($homeImage->is_active != 0) {
            $update = $this->HomeImages->query()->update()->set(['is_active' => 0])->where(['unique_id' => $uniqId])->execute();
            if ($update) {
                $this->Flash->success(__('Home Image In-activated successfully.'));
                return $this->redirect($this->referer());
            } else {
                $this->Flash->error(__('Home Image inactivation failed'));
                return $this->redirect($this->referer());
            }
        } else {
            $update = $this->HomeImages->query()->update()->set(['is_active' => 1])->where(['unique_id' => $uniqId])->execute();
            if ($update) {
                $this->Flash->success(__('Home Image Activated successfully'));
                return $this->redirect($this->referer());
            } else {
                $this->Flash->error(__('Home Image activation failed'));
                return $this->redirect($this->referer());
            }
        }
    }
    public function deleteHomeImage($uniqId = NULL) {
        $homeImage = $this->HomeImages->find()->where(['unique_id' => $uniqId])->first();
        if ($this->HomeImages->deleteAll(['HomeImages.id' => $homeImage->id])) {
            file_exists(WWW_ROOT . HOME_PIC . $homeImage->filename) && unlink(WWW_ROOT . HOME_PIC . $homeImage->filename);
            $this->Flash->success(__('Home Image deleted successfully'));
            return $this->redirect($this->referer());
        } else {
            $this->Flash->error(__('Error Occurred'));
            return $this->redirect($this->referer());
        }
    }
    public function listFaqs() {
        $faqs = $this->Faqs->find();
        $this->set(compact(['faqs']));
    }
    public function addNewFaq($uniqId = NULL) {
        if ($uniqId) {
            $faq = $this->Faqs->find()->where(['unique_id' => $uniqId])->first();
        } else {
            $faq = $this->Faqs->newEntity();
        }
        if ($this->request->is(['post', 'patch', 'put'])) {
            $data = $this->request->getData();
            if (!$uniqId) {
                $data['unique_id'] = $this->Custom->generateUniqNumber();
            }
            $this->Faqs->patchEntity($faq, $data);
            if ($this->Faqs->save($faq)) {
                if (!$uniqId) {
                    $this->Flash->success(__('Faq added successfully'));
                    return $this->redirect(['action' => 'listFaqs']);
                } else {
                    $this->Flash->success(__('Faq updated successfully'));
                    return $this->redirect(['action' => 'listFaqs']);
                }
            } else {
                $message = getFirstError($faq->getErrors());
                $this->Flash->error($message);
                return $this->redirect(['action' => 'listFaqs']);
            }
        }
        $this->set(compact(['faq']));
    }
    public function deleteFaq($uniqId = NULL) {
        $faq = $this->Faqs->find()->where(['unique_id' => $uniqId])->first();
        if ($this->Faqs->deleteAll(['Faqs.id' => $faq->id])) {
            $this->Flash->success(_('Faq Deleted Successfully'));
            return $this->redirect($this->referer());
        } else {
            $this->Flash->error(__('Error Occurred'));
            return $this->redirect($this->referer());
        }
    }
    public function countries($id = NULL) {
        $editId = $this->Countries->find()->where(['id' => $id])->first();
        if ($this->request->is(['post', 'patch', 'put'])) {
            $data = $this->request->getData();
            $this->Countries->patchEntity($editId, $data);
            if ($this->Countries->save($editId)) {
                $this->Flash->success(__('Updated Successfully'));
                return $this->redirect(['action' => 'countries']);
            }
        }
        $listCountries = $this->Countries->find();
        $this->set(compact(['listCountries', 'editId', 'id']));
    }
    public function changeCountryStatus($id) {
        $country = $this->Countries->find()->where(['id' => $id])->first();
        if ($country->is_active != 0) {
            $this->Countries->query()->update()->set(['is_active' => 0])->where(['id' => $country->id])->execute();
        } else {
            $this->Countries->query()->update()->set(['is_active' => 1])->where(['id' => $country->id])->execute();
        }
        $this->Flash->success(__('Status Updated Successfully'));
        return $this->redirect($this->referer());
    }
    ################################################### COMPANY SECTION ##############################################
    public function company() {
        $companyDtls = $this->Company->find()->where(['id' => 1])->first();
        if ($this->request->is(['post', 'patch', 'put'])) {
            $data = $this->request->getData();
            $this->Company->patchEntity($companyDtls, $data);
            if ($this->Company->save($companyDtls)) {
                $this->Flash->success(__('Company Details Updated Successfully'));
            } else {
                $this->Flash->error(__('Updated Unsuccessfully'));
            }
        }
        $this->set(compact('companyDtls'));
    }
    ##################################################### REVIEW SECTION ###############################################
    public function reviews($id = NULL) {
        $params = $this->request->getQueryParams();
        $edit = $add = false;
        if (isset($params['add']) && ($params['add'] == 'true')) {
            $add = true;
            $reviewEntity = $this->Testimonials->newEntity();
        } else if (!empty($id)) {
            $edit = false;
            $reviewEntity = $this->Testimonials->find()->where(['id' => $id])->first();
        }
        if ($this->request->is(['post', 'patch', 'put'])) {
            $data = $this->request->getData();
            $data['approved'] = 'N';
            $this->Testimonials->patchEntity($reviewEntity, $data);
            if ($this->Testimonials->save($reviewEntity)) {
                if (!$id) {
                    $this->Flash->success(__('Review added Successfully'));
                } else {
                    $this->Flash->success(__('Review updated Successfully'));
                }
                return $this->redirect(['action' => 'reviews']);
            }
        }
        $listReviews = $this->Testimonials->find();
        $this->set(compact(['listReviews', 'add', 'id', 'reviewEntity']));
    }
    public function changeReviewStatus($id) {
        $review = $this->Testimonials->find()->where(['id' => $id])->first();
        if ($review->approved == 'N') {
            $this->Testimonials->query()->update()->set(['approved' => 'Y'])->where(['id' => $review->id])->execute();
        } else {
            $this->Testimonials->query()->update()->set(['approved' => 'N'])->where(['id' => $review->id])->execute();
        }
        $this->Flash->success(__('Status Updated Successfully'));
        return $this->redirect($this->referer());
    }
    public function deleteReview($id) {
        $reviewId = $this->Testimonials->find()->where(['id' => $id])->first();
        if (!empty($reviewId)) {
            if ($this->Testimonials->delete($reviewId)) {
                $this->Flash->success(__('This Testimonial deleted successfully.'));
            } else {
                $this->Flash->error(__('Some error occurred while delete testimonial!!'));
            }
        } else {
            $this->Flash->error(__('This Testimonial not found!!'));
        }
        return $this->redirect($this->referer());
    }
}
