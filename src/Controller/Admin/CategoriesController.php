<?php
namespace App\Controller\Admin;
use App\Controller\AppController;
class CategoriesController extends AppController {
    public function initialize() {
        parent::initialize();
        // Load Model
        $this->loadModel('Categories');
        // Load Components
        $this->loadComponent('Custom');
        $this->loadComponent('Paginator');
        
        //set layout
        $this->viewBuilder()->setLayout('admin');
    }
    public function addCategories($parent_id = null) {
        $categories = $this->Categories->find()->contain(['ParentCategories']);
        $categoryList = $categories->find('list', ['keyField' => 'category_id', 'valueField' => function($category){
            if (is_object($category->parent_category)) {
                            return $category->parent_category->name . ': ' . $category->name;
                        }else{
                        return  $category->name;}
                    }]);
        if ($parent_id) {
            $category = $this->Categories->find()->contain(['ParentCategories'])->where(['Categories.parent_id' => $parent_id])->first();
            if (empty($category)) {
                $this->Flash->error(__('Category does not exists!!'));
                return $this->redirect(['action' => 'listManagers']);
            }
        } else {
            $category = $this->Categories->newEntity();
        }
        if ($this->request->is(['post', 'patch', 'put'])) {
            $data = $this->request->getData();
            $this->Categories->patchEntity($category, $data,['associated' => ['ParentCategories']]);
            
            // manager data is saved
            if ($this->Categories->save($category)) {
                if (!$parent_id) {
                    $this->Flash->success(__('Category added successfully'));
                } else {
                    $this->Flash->success(__('Category updated successfully'));
                }
                return $this->redirect(['action' => 'listCategories']);
            }
            $message = getFirstError($category->getErrors());
            $this->Flash->error(__($message));
        }
        $this->set(compact('category','categoryList'));
    }
    public function listCategories() {
        $listCategories = $this->Categories->find()->contain(['ParentCategories'])->order(['Categories.name' => 'ASC']);
       
        $this->set(compact(['listCategories']));
    }
    public function deleteCategory($parent_id) {
        $category = $this->Categories->find()->contain(['ParentCategories'])->where(['Categories.parent_id' => $parent_id])->first();
        if (!empty($category)) {
            if ($this->Categories->delete($category)) {
                $this->Flash->success(__('Category deleted successfully.'));
            } else {
                $this->Flash->error(__('Some error occurred while delete Category!!'));
            }
        } else {
            $this->Flash->error(__('Category not found!!'));
        }
        return $this->redirect($this->referer());
    }
}
