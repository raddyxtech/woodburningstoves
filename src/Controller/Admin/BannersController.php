<?php

namespace App\Controller\Admin;

use App\Controller\AppController;

class BannersController extends AppController {

    public function initialize() {
        parent::initialize();
        // Load ModelS
        $this->loadModel('BannerImages');

        // Load Components
        $this->loadComponent('Flash');
        $this->loadComponent('Custom');
        $this->loadComponent('Paginator');
        $this->viewBuilder()->setLayout('admin');
    }

    public function listbanners($id = null) {

        $images = $this->BannerImages->Find();
        $this->set(compact('images'));
    }

    public function delimages($id = null) {
        $image = $this->BannerImages->find()->select(['image_name'])->where(['BannerImages.id' => $id])->first();
        if (!empty($image)) {
              
//            unlink(HTTP_ROOT . 'images/banners/' . $image->image_name);
            unlink(BANNER_PATH . $image->image_name);
            $this->BannerImages->query()->update()->set(['image_name' => ''])->where(['BannerImages.id' => $id])->execute();
            $this->Flash->success(__('Image deleted successfully.'));
        } else {
            $this->Flash->error(__('Some error occurred while delete Image!!'));
        }
        return $this->redirect($this->referer());
    }

    public function addBanner($id = null) {

        if ($id) {
            $image = $this->BannerImages->find()->where(['BannerImages.id' => $id])->first();
        } 
        else {
            $image = $this->BannerImages->newEntity();
        }
        if ($this->request->is(['post', 'patch', 'put'])) {
            $data = $this->request->getData();
            $this->BannerImages->patchEntity($image, $data);
// pj($data);exit;
            if (!empty($data['image_name']['name'])) {
                $newFile = $this->Custom->uploadFile($data['image_name']['name'], $data['image_name']['tmp_name'], BANNER_PATH);
                if ($newFile) {
                    $image->image_name = $newFile;
                    $image->title = $data['title'];
                    $image->description = $data['description'];
                    if ($this->BannerImages->save($image)) {
                        if (!$id) {
                            $this->Flash->success(__('Image added successfully'));
                            return $this->redirect(['action' => 'listbanners']);
                        } else {
                            $this->Flash->success(__('Image updated uccessfully.'));
                             return $this->redirect(['action' => 'listbanners']);
                        }
                    }
                }
            }
        }
        $this->set(compact('image'));
    }

    public function changeStatus($id) {
        $banner = $this->BannerImages->find()->where(['BannerImages.id' => $id])->first();
//        pj($id);exit;
        if ($banner->is_active != 0) {
            $update = $this->BannerImages->query()->update()->set(['is_active' => 0])->where(['BannerImages.id' => $id])->execute();
//             pj($update);exit;
            if ($update) {
                $this->Flash->success(__('Banner Inactivated successfully'));
                return $this->redirect($this->referer());
            } else {
                $this->Flash->error(__('Banner Inactivation failed'));
                return $this->redirect($this->referer());
            }
        } else {
            $update = $this->BannerImages->query()->update()->set(['is_active' => 1])->where(['BannerImages.id' => $id])->execute();
//            pj($update);exit;
            if ($update) {
                $this->Flash->success(__('Banner Activated successfully'));
                return $this->redirect($this->referer());
            } else {
                $this->Flash->error(__('Banner Activation failed'));
                return $this->redirect($this->referer());
            }
        }
    }

    public function deleteimage($id) {
        $image = $this->BannerImages->find()->where(['BannerImages.id' => $id])->first();
        if (!empty($image)) {
            if ($this->BannerImages->delete($image)) {
                $this->Flash->success(__('Image deleted successfully.'));
            } else {
                $this->Flash->error(__('Some error occurred while delete Image!!'));
            }
        } else {
            $this->Flash->error(__('Image not found!!'));
        }
        return $this->redirect($this->referer());
    }

}
