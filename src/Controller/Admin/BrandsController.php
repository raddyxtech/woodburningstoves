<?php
namespace App\Controller\Admin;
use App\Controller\AppController;
class BrandsController extends AdminController {
    public function initialize() {
        parent::initialize();
        // Load Component
        $this->loadComponent('Custom');
        $this->viewBuilder()->setLayout('admin');
        $this->Auth->allow(['listbrands']);
    }
    public function listbrands() {
        $brands = $this->Brands->find();
        $this->set(compact(['brands']));
    }
    public function addBrand($uniqId = NULL) {
        if ($uniqId) {
            $brand = $this->Brands->find()->where(['Brands.unique_id' => $uniqId])->first();
        } else {
            $brand = $this->Brands->newEntity();
        }
        if ($this->request->is(['post', 'patch', 'put'])) {
            $data = $this->request->getData();
            if (!$uniqId) {
                $data['unique_id'] = $this->Custom->generateUniqNumber();
            }
            $this->Brands->patchEntity($brand, $data);
            if ($this->Brands->save($brand)) {
                if (!empty($data['brand_pic'])) {
                    $result = $this->Custom->croppieImageUpload2($data['brand_pic']);
                    // update table is photo is uploaded correctly
                    if ($result->status) {
                        // remove previous image if exists
                        !empty($brand->image) && file_exists(WWW_ROOT . BRAND_PIC . $brand->image) && unlink(WWW_ROOT . BRAND_PIC . $brand->image);
                        $this->Brands->query()->update()->set(['image' => $result->file])->where(['id' => $brand->id])->execute();
                    }
                }
                if (!$uniqId) {
                    $this->Flash->success(__('Brand Added successfully'));
                } else {
                    $this->Flash->success(__('Brand Updated successfully'));
                }
                return $this->redirect(['action' => 'listbrands']);
            }
            $message = getFirstError($brand->getErrors());
            $this->Flash->error(__($message));
        }
        $this->set(compact(['brand']));
    }
    public function changeStatus($uniqId) {
        $brand = $this->Brands->find()->where(['Brands.unique_id' => $uniqId])->first();
        if ($brand->is_active != 0) {
            $update = $this->Brands->query()->update()->set(['Brands.is_active' => 0])->where(['Brands.unique_id' => $uniqId])->execute();
            if ($update) {
                $this->Flash->success(__('Brand Inactivated successfully'));
                return $this->redirect($this->referer());
            } else {
                $this->Flash->error(__('Brand Inactivation failed'));
                return $this->redirect($this->referer());
            }
        } else {
            $update = $this->Brands->query()->update()->set(['Brands.is_active' => 1])->where(['Brands.unique_id' => $uniqId])->execute();
            if ($update) {
                $this->Flash->success(__('Brand Activated successfully'));
                return $this->redirect($this->referer());
            } else {
                $this->Flash->error(__('Brand Activation failed'));
                return $this->redirect($this->referer());
            }
        }
    }
    public function deleteBrand($uniqId) {
        $brand = $this->Brands->find()->where(['Brands.unique_id' => $uniqId])->first();
        if($this->Brands->deleteAll(['Brands.id' => $brand->id])){
            $this->Flash->success(__('Brand deleted successfully'));
            return $this->redirect($this->referer());
        } else {
            $this->Flash->error(__('Error Occurred'));
            return $this->redirect($this->referer());
        }
    }
}
