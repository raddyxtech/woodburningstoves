<?php
namespace App\Controller\Admin;
use App\Controller\AppController;
class DeliveryController extends AdminController {
    public function initialize() {
        parent::initialize();
        // Load Model
        $this->loadModel('DeliveryRegions');
        $this->loadModel('Countries');
        $this->loadModel('Deliveries');
        
        // Load Component
        $this->loadComponent('Custom');
        $this->viewBuilder()->setLayout('admin');
        $this->Auth->allow(['listRegions']);
    }
    public function listRegions() {
        $regions = $this->DeliveryRegions->find()->contain(['Countries']);
        $this->set(compact(['regions']));
    }
    public function listDeliveries() {
        $deliveries = $this->Deliveries->find()->contain(['DeliveryRegions.Countries']);
        $this->set(compact(['deliveries']));
    }
    public function addRegion($uniqId = NULL) {
        $countries = $this->Countries->find('list')->where(['Countries.is_active' => 1]);
        if ($uniqId) {
            $region = $this->DeliveryRegions->find()->where(['DeliveryRegions.unique_id' => $uniqId])->first();
        } else {
            $region = $this->DeliveryRegions->newEntity();
        }
        if ($this->request->is(['post', 'patch', 'put'])) {
            $data = $this->request->getData();
            //pj($data);exit;
            if (!$uniqId) {
                $data['unique_id'] = $this->Custom->generateUniqNumber();
            }
            $this->DeliveryRegions->patchEntity($region, $data);
            if ($this->DeliveryRegions->save($region)) {
                if (!$uniqId) {
                    $this->Flash->success(__('Delivery Region Added Successfully'));
                } else {
                    $this->Flash->success(__('Delivery Region Updated Successfully'));
                }
                return $this->redirect(['action' => 'listRegions']);
            }
            $message = getFirstError($region->getErrors());
            $this->Flash->error(__($message));
        }
        $this->set(compact(['region', 'countries']));
    }
    
    public function addDelivery($uniqId = NULL) {
        $deliveryRegions = $this->DeliveryRegions->find('list');
        if ($uniqId) {
            $delivery = $this->Deliveries->find()->where(['Deliveries.unique_id' => $uniqId])->first();
        } else {
            $delivery = $this->Deliveries->newEntity();
        }
        if ($this->request->is(['post', 'patch', 'put'])) {
            $data = $this->request->getData();
            if (!$uniqId) {
                $data['unique_id'] = $this->Custom->generateUniqNumber();
            }
            $this->Deliveries->patchEntity($delivery, $data);
            if ($this->Deliveries->save($delivery)) {
                if (!$uniqId) {
                    $this->Flash->success(__('Delivery Option Added Successfully'));
                } else {
                    $this->Flash->success(__('Delivery Option Updated Successfully'));
                }
                return $this->redirect(['action' => 'listDeliveries']);
            }
            $message = getFirstError($delivery->getErrors());
            $this->Flash->error(__($message));
        }
        $this->set(compact(['deliveryRegions', 'countries', 'delivery']));
    }
    public function deleteRegion($uniqId) {
        $region = $this->DeliveryRegions->find()->where(['DeliveryRegions.unique_id' => $uniqId])->first();
        if ($this->DeliveryRegions->deleteAll(['DeliveryRegions.id' => $region->id])) {
            $this->Flash->success(__('Region deleted successfully'));
            return $this->redirect($this->referer());
        } else {
            $this->Flash->error(__('Error Occurred'));
            return $this->redirect($this->referer());
        }
    }
    
    public function deleteDelivery($uniqId) {
        $delivery = $this->Deliveries->find()->where(['Deliveries.unique_id' => $uniqId])->first();
        if ($this->Deliveries->deleteAll(['Deliveries.id' => $delivery->id])) {
            $this->Flash->success(__('Delivery Option deleted successfully'));
            return $this->redirect($this->referer());
        } else {
            $this->Flash->error(__('Error Occurred'));
            return $this->redirect($this->referer());
        }
    }
}
