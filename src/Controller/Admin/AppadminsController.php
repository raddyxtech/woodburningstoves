<?php
namespace App\Controller\Admin;
use App\Controller\AppController;
class AppadminsController extends AdminController {
    public function initialize() {
        parent::initialize();
        // Load Components
        $this->loadComponent('Custom');
        $this->loadComponent('Paginator');
        $this->loadComponent('auth');
        //load Models
        $this->loadModel('Users');
        $this->Auth->allow(['dashboard']);
        $this->viewBuilder()->setLayout('admin');
    }
    /*
     * Developer    :  Pratap Kishore Swain
     * Date         :  28-06-2019
     * Description  :  Haldle to display the home page.
     */
    public function dashboard() {
        // Only admin should be allowed
        if ($this->Auth->user('user_type') != 1) {
            return $this->redirect(['prefix' => false, 'controller' => 'Users', 'action' => 'login']);
        }
    }
    public function emailTemplates() {
        $this->loadModel('Settings');
        $settings = $this->Settings->find();
        $this->set('datamailListings', $settings);
    }
    public function editmailSetting($id) {
        $this->loadModel('Settings');
        $emailTemplate = $this->Settings->find()->where(['id' => $id])->first();
        if ($this->request->is(['patch', 'post', 'put'])) {
            $data = $this->request->getData();
            $this->Settings->patchEntity($emailTemplate, $data);
            if ($this->Settings->save($emailTemplate)) {
                $this->Flash->success(__('Email Template  Updated Successfully!!'));
                return $this->redirect(HTTP_ROOT . 'admin/appadmins/emailTemplates');
            } else {
                $message = getFirstError($emailTemplate->getErrors());
                $this->Flash->error(__($message));
            }
        }
        $this->set(compact(['emailTemplate']));
    }
    public function changePassword() {
        $user_id = $this->Auth->user('id');
        if ($this->request->is(['patch', 'post', 'put'])) {
            $user = $this->Users->get($user_id);
            $data = $this->request->getData();
            
            $this->Users->patchEntity($user, $data, ['validate' => 'changePassword']);
            if ($errors = $user->getErrors()) {
                $this->Flash->error(getFirstError($errors));
            } else if ($this->Users->save($user)) {
                $this->Flash->success(__('Password changed successfuly'));
            }
            return $this->redirect($this->referer());
        }
    }
}
