<?php
namespace App\Controller\Admin;
use App\Controller\AppController;
class AdminController extends AppController {
    public function initialize() {
        parent::initialize();
        $this->loadComponent('Auth');
        if ($this->Auth->user('id') && $this->Auth->user('user_type') != 1) {
            $this->Flash->error(__('Unauthorised Access!!'));
            return $this->redirect(['prefix' => false, 'controller' => 'Users', 'action' => 'login']);
        }
    }
}
