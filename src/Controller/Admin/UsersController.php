<?php
namespace App\Controller\Admin;
use App\Controller\AppController;
class UsersController extends AdminController {
    public function initialize() {
        parent::initialize();
        // Load Models
        $this->loadModel('Users');
        // Load Components
        $this->loadComponent('Custom');
        $this->viewBuilder()->setLayout('admin');
        $this->Auth->allow(['login']);
    }
    public function login() {
        return $this->redirect(['prefix' => FALSE, 'controller' => 'Users', 'action' => 'login']);
    }
    public function listAdministrators() {
        $administrators = $this->Users->find()->where('is_active' == 1);
        $this->set(compact(['administrators']));
    }
    public function addAdministrators($uniqueId = NULL) {
        $this->viewBuilder()->setLayout('admin');
        if ($uniqueId) {
            $admin = $this->Users->find()->where(['Users.unique_id' => $uniqueId])->first();
        } else {
            $admin = $this->Users->newEntity();
        }
        if ($this->request->is(['post', 'patch', 'put'])) {
            $data = $this->request->getData();
            if (!$uniqueId) {
                $data['unique_id'] = $this->Custom->generateUniqNumber();
                $data['is_active'] = 0;
            }
            $data['user_type'] = 1;
            
            $this->Users->patchEntity($admin, $data);
            if ($this->Users->save($admin)) {
                if (!$uniqueId) {
                    $this->Flash->success(__('Administrator Added successfully'));
                } else {
                    $this->Flash->success(__('Administrator Updated successfully'));
                }
                return $this->redirect(['action' => 'listAdministrators']);
            }
        }
        $this->set(compact(['admin', 'uniqueId']));
    }
    public function changeStatus($uniqID) {
        $user = $this->Users->find()->where(['Users.unique_id' => $uniqID])->first();
        if ($user->is_active == 0) {
            $this->Users->query()->update()->set(['is_active' => 1])->where(['id' => $user->id])->execute();
        } else {
            $this->Users->query()->update()->set(['is_active' => 0])->where(['id' => $user->id])->execute();
        }
        $this->Flash->success(__('Status Updated Successfully'));
        return $this->redirect($this->referer());
    }
    public function deleteUser($unique_id) {
        $user = $this->Users->find()->where(['Users.unique_id' => $unique_id])->first();
        if (!empty($user)) {
            if ($this->Users->delete($user)) {
                $this->Flash->success(__('Sub-admin deleted successfully.'));
            } else {
                $this->Flash->error(__('Some error occurred while delete account!!'));
            }
        } else {
            $this->Flash->error(__('Users account not found!!'));
        }
        return $this->redirect($this->referer());
    }
}
