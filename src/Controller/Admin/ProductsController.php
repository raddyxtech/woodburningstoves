<?php

namespace App\Controller\Admin;

use App\Controller\AppController;

class ProductsController extends AppController {

    public function initialize() {
        parent::initialize();
        // Load ModelS
        $this->loadModel('Products');
        $this->loadModel('Categories');
        $this->loadModel('Brands');
        $this->loadModel('Images');
        $this->loadModel('Pdfs');
        $this->loadModel('ProductVariations');
        $this->loadModel('CategoriesProducts');
        $this->loadModel('ProductReviews');
        // Load Components
        $this->loadComponent('Flash');
        $this->loadComponent('Custom');
        $this->loadComponent('Paginator');
        $this->viewBuilder()->setLayout('admin');
    }

    public function addProducts($product_id = null) {
        $categories = $this->Categories->find()->contain(['ParentCategories']);
        $productList = $categories->find('list', ['keyField' => 'category_id', 'valueField' => function($category) {
                if (is_object($category->parent_category)) {
                    return $category->parent_category->name . ': ' . $category->name;
                } else {
                    return $category->name;
                }
            }]);
        $brandList = $this->Brands->find()->find('list');
        if ($product_id) {
            $product = $this->Products->find()->contain(['Categories', 'Brands'])->where(['Products.product_id' => $product_id])->first();
        } else {
            $product = $this->Products->newEntity();
        }
        if ($this->request->is(['post', 'patch', 'put'])) {
            $data = $this->request->getData();
            $this->Products->patchEntity($product, $data, ['associated' => ['ProductVariations']]);
            if ($this->Products->save($product)) {
                $productCat = $this->CategoriesProducts->newEntity();
                $productCat->category_id = $data['parent_id'];
                $productCat->product_id = $product->product_id;
                $this->CategoriesProducts->save($productCat);
                $this->Products->patchEntity($product, $data);
                if ($this->Products->save($product)) {
                    if (!$product_id) {
                        $this->Flash->success(__('Product added successfully'));
                        return $this->redirect(['action' => 'editVariations', $product->product_id]);
                    } else {
                        $this->Flash->success(__('Updated uccessfully.'));
                    }
                    return $this->redirect(['action' => 'listProducts']);
                }
                $message = getFirstError($product->getErrors());
                $this->Flash->error(__($message));
            } else {
                $message = getFirstError($product->getErrors());
                $this->Flash->error(__($message));
            }
        }
        $this->set(compact('product', 'productList', 'brandList'));
    }

    public function listProducts($product_id = null) {
         
        if ($this->request->is('post')) {
            $params = array_filter($this->request->getData());
            return $this->redirect(['prefix' => 'admin', 'controller' => 'products', 'action' => 'listProducts', '?' => $params]);
        }
        $queryParams = $this->request->getQueryParams();
//        $queryParams = $this->request->getQuery();
        $conditions = [];
        if (!empty($queryParams['keyword'])) {
            $conditions[] = ['OR' => [
                    ['Products.name LIKE' => "%" . trim(urldecode($queryParams['keyword'])) . "%"]
                ]
            ];
        }
        if (!empty($queryParams['from_date'])) {
            $conditions[] = ['Products.created >=' => $queryParams['from_date']];
        }
        if (!empty($queryParams['to_date'])) {
            $conditions[] = ['Products.created <=' => $queryParams['to_date']];
        }
        if (!empty($queryParams['parent_id'])) {
            $catId = trim(urldecode($queryParams['parent_id']));

            $prodList = $this->CategoriesProducts->find('list', ['keyField' => 'product_id', 'valueField' => 'product_id'])->where(['category_id' => $catId]);
            $conditions[] = ['Products.product_id IN' => $prodList];
        }
        $products = $this->Products->find()
                ->where($conditions)
                ->contain(['Categories', 'Brands'])
                ->order(['Products.name' => 'ASC']);
        $config = ['limit' => 10];
        $products = $this->Paginator->paginate($products, $config);
        $categories = $this->Categories->find()->contain(['ParentCategories']);
        $categoryList = $categories->find('list', ['keyField' => 'category_id', 'valueField' => function($category) {
                if (is_object($category->parent_category)) {
                    return $category->parent_category->name . ': ' . $category->name;
                } else {
                    return $category->name;
                }
            }]);

        $this->set(compact(['categoryList', 'products', 'queryParams']));
    }

    public function ajaxListProducts() {
        $this->viewBuilder()->setLayout('ajax');
        $cat_productss = '';
        $productajax = $this->request->getData('parent_id');
//        pj($productajax);exit;
        $cat_products = $this->CategoriesProducts->Find()->where(['category_id' => $productajax])->contain('Products');
        echo json_encode($cat_products);
        exit;
    }

    public function ajaxListCatProducts() {
        $this->viewBuilder()->setLayout('ajax');
        $options = '';
        if ($this->request->is(['post'])) {
            $data = $this->request->getData();
            $proList = $this->CategoriesProducts->find('all')->where(['CategoriesProducts.category_id' => $data['cat_id'], 'Products.name !=' => ''])->contain(['Products']);
//            pj($proList);exit;
            foreach ($proList as $value) {
                $options .= "<option value='{$value->product->product_id}'>{$value->product->name}</option>";
            }
        }
        echo $options;
        exit;
    }

    public function deleteProduct($product_id) {
        $product = $this->Products->find()->where(['Products.product_id' => $product_id])->first();
//        pj($product);exit;
        if (!empty($product)) {
            if ($this->Products->delete($product)) {
                $this->Flash->success(__('Product deleted successfully.'));
            } else {
                $this->Flash->error(__('Some error occurred while delete Product!!'));
            }
        } else {
            $this->Flash->error(__('Product not found!!'));
        }
        return $this->redirect($this->referer());
    }

    public function listImages($product_id = null) {
        $product = $this->Products->Find()->contain(['CategoriesProducts'])->where(['Products.product_id' => $product_id])->first();
        $images = $this->Images->Find()->contain('Products')->where(['Images.item_id' => $product_id]);
        foreach ($product->categories_products as $productcat)
            $this->set(compact('images', 'product', 'productcat'));
    }

    public function updateImages($product_id) {
        if ($this->request->is(['post', 'patch', 'put'])) {
            $data = $this->request->getData();
            $data['item_id'] = $product_id;
            if (!empty($data['filename']['name'])) {
                $newFile = $this->Custom->uploadFile($data['filename']['name'], $data['filename']['tmp_name'], UPLOAD_PATH);
                if ($newFile) {
                    $image = $this->Images->newEntity();
                    $image->filename = $newFile;
                    $image->title = $data['title'];
                    $image->item_id = $product_id;
                    $this->Images->save($image);
                    $this->Flash->success(__('image added successfully'));
                    return $this->redirect(['prefix' => 'admin', 'controller' => 'Products', 'action' => 'listImages', $product_id]);
                }
            }
        }
        $this->set(compact('image', 'product_id'));
    }

//        $this->set(compact('ima
    public function deleteimage($image_id) {
        $image = $this->Images->find()->where(['Images.image_id' => $image_id])->first();
        if (!empty($image)) {
            if ($this->Images->delete($image)) {
                $this->Flash->success(__('Image deleted successfully.'));
            } else {
                $this->Flash->error(__('Some error occurred while delete Image!!'));
            }
        } else {
            $this->Flash->error(__('Image not found!!'));
        }
        return $this->redirect($this->referer());
    }

    public function addPdfs($product_id) {

        if ($this->request->is(['post', 'patch', 'put'])) {
            $data = $this->request->getData();
//            pj($data);exit;
            $data['product_id '] = $product_id;

            if (!empty($data['pdf_filename']['name'])) {
                $newFile = $this->Custom->uploadPdf($data['pdf_filename']['name'], $data['pdf_filename']['tmp_name'], PDFS_PATH);
                if ($newFile) {
                    $pdfs = $this->Pdfs->newEntity();
                    $pdfs->pdf_filename = $newFile;
                    $pdfs->product_id = $product_id;
                    $this->Pdfs->save($pdfs);
                    $this->Flash->success(__('pdf added successfully'));
                    return $this->redirect(['prefix' => 'admin', 'controller' => 'Products', 'action' => 'listPdfs', $product_id]);
                } else {
                    $this->Flash->error(__('Error in adding pdf!'));
                }
            }
        }
    }

    public function listPdfs($product_id) {
        $product = $this->Products->Find()->contain(['CategoriesProducts'])->where(['Products.product_id' => $product_id])->first();
        foreach ($product->categories_products as $productcat)
            $pdfs = $this->Pdfs->Find()->contain('Products')->where(['Pdfs.product_id' => $product_id]);
        $this->set(compact('pdfs', 'product', 'productcat'));
    }

    public function deletepdf($product_id) {
        $pdf = $this->Pdfs->find()->where(['Pdfs.product_id' => $product_id])->first();
        if (!empty($pdf)) {
            if ($this->Pdfs->delete($pdf)) {
                $this->Flash->success(__('Pdf deleted successfully.'));
            } else {
                $this->Flash->error(__('Some error occurred while delete Pdf!!'));
            }
        } else {
            $this->Flash->error(__('Pdf not found!!'));
        }
        return $this->redirect($this->referer());
    }

    public function listVariations($product_id) {
        $product = $this->Products->Find()->contain(['CategoriesProducts'])->where(['Products.product_id' => $product_id])->first();
        $variations = $this->ProductVariations->Find()->where(['ProductVariations.product_id' => $product_id]);
        foreach ($product->categories_products as $productcat)
//            pj($productvar);exit;
            $this->set(compact('variations', 'product', 'productcat'));
    }

    public function editVariations($product_id) {
        $fetchvariation = $this->ProductVariations->find()->where(['ProductVariations.product_id' => $product_id])->first();

        if (empty($fetchvariation)) {
            $fetchvariation = $this->ProductVariations->newEntity();
        }
        if ($this->request->is(['post', 'patch', 'put'])) {
            $data = $this->request->getData();
            $this->ProductVariations->patchEntity($fetchvariation, $data);

            $data['product_id'] = $product_id;
            $this->ProductVariations->patchEntity($fetchvariation, $data);
            if ($this->ProductVariations->save($fetchvariation)) {
                if (!$product_id) {
                    $this->Flash->success(__('Variations added successfully'));
                } else {
                    $this->Flash->success(__('updated successfully'));
                }
                return $this->redirect(['prefix' => 'admin', 'controller' => 'Products', 'action' => 'list-variations', $product_id]);
            }
            $message = getFirstError($fetchvariation->getErrors());
            $this->Flash->error(__($message));
        }
        $this->set(compact('variations', 'fetchvariation'));
    }

    public function addAddons($productId = null) {
        $categories = $this->Categories->find()->contain(['ParentCategories']);
        $product = $this->Products->Find()->contain(['CategoriesProducts'])->where(['Products.product_id' => $productId])->first();

        foreach ($product->categories_products as $productcat)
            $categoriesList = $this->Categories->find()->find('list');
        $product = $this->Products->get($productId);
        if ($this->request->is(['post', 'patch', 'put'])) {
            $data = $this->request->getData();
            $products = array_filter($data['products'], function($product) {
                if ($product['CatId'] > 0 && $product['ProductId'] > 0) {
                    return $product;
                }
            });
            $this->Products->patchEntity($product, ['addons' => json_encode($products)]);
            // manager data is saved
            if ($this->Products->save($product)) {
                $this->Flash->success(__('Updated Successfully!'));
                //return $this->redirect(['prefix' => 'admin', 'controller' => 'Products', 'action' => 'list-variations', $product_id]);
            } else {
                $this->Flash->failure(__('Updated Successfully!'));
            }
        }
        $productsList = $this->Products->find('list', ['keyField' => 'product_id', 'valueField' => 'name']);
        $this->set(compact('productsList', 'categoriesList', 'productcat', 'product'));
    }

    public function test() {
        $cat_products = $this->CategoriesProducts->Find()->where(['category_id' => 11]);
        foreach ($cat_products as $cat_product):
            $products = $this->Products->Find()->where(['product_id' => $cat_product['product_id']])->first();
            echo $products->name . "</br>";
        endforeach;
        exit;
    }

    public function stockList() {
        $brands = $this->Brands->find('list');
        $this->set(compact(['brands']));
    }

    public function ajaxStockList() {
        $this->viewBuilder()->setLayout('ajax');
        $data = $this->request->getData();
        $brandId = $data['brand_id'];
        if ($brandId > 0) {
            $products = $this->Products->find()->contain(['Brands', 'ProductVariations'])->where(['brand_id' => $brandId]);
        } else {
            $products = $this->Products->find()->contain(['Brands', 'ProductVariations']);
        }
//        pj($products);exit;
        $this->set(compact(['products']));
    }

    public function updateStockList() {
        $products = $this->Products->find();
        $productVariations = $this->ProductVariations->find();
        $data = $this->request->getData();

        $status_new = $data['status_new'];
        $status_sale = $data['status_sale'];
        $status_clearance = $data['status_clearance'];
        $out_of_stock = $data['out_of_stock'];
        $hidden = $this->request->getData('hidden');
        if (!empty($status_new) && isset($status_new)) {
            foreach ($products as $product):
                if (in_array($product->product_id, $status_new)) {
                    $product->status_new = "Y";
                } else {
                    $product->status_new = "N";
                }
            endforeach;
        }
        if (!empty($status_sale) && isset($status_sale)) {
            foreach ($products as $product):
                if (in_array($product->product_id, $status_sale)) {
                    $product->status_sale = "Y";
                } else {
                    $product->status_sale = "N";
                }
            endforeach;
        }
        if (!empty($status_clearance) && isset($status_clearance)) {
            foreach ($products as $product):
                if (in_array($product->product_id, $status_clearance)) {
                    $product->status_clearance = "Y";
                } else {
                    $product->status_clearance = "N";
                }
            endforeach;
        }
        if (!empty($hidden) && isset($hidden)) {
            foreach ($products as $product):
                if (in_array($product->product_id, $hidden)) {
                    $product->hidden = "Y";
                } else {
                    $product->hidden = "N";
                }
            endforeach;
        }
        if (!empty($out_of_stock) && isset($out_of_stock)) {
            foreach ($productVariations as $productVariation):
                if (in_array($productVariation->variation_id, $out_of_stock)) {
                    $productVariation->out_of_stock = "Y";
                } else {
                    $productVariation->out_of_stock = "N";
                }
            endforeach;
        }
        if ($this->Products->saveMany($products)) {
            $this->ProductVariations->saveMany($productVariations);
            $this->Flash->success(__('Records updated successfully'));
        }
        return $this->redirect(['controller' => 'Products', 'action' => 'stockList']
        );
    }

    public function reviewRatings() {
        $reviewRatings = $this->ProductReviews->find()->contain(['Products.Images']);
        $this->set(compact('reviewRatings'));
    }

    public function deleteReview($reviewRating) {
        $productReviews = $this->ProductReviews->find()->where(['ProductReviews.id' => $reviewRating])->first();
//        pj($product);exit;
        if (!empty($productReviews)) {
            if ($this->ProductReviews->delete($productReviews)) {
                $this->Flash->success(__('Review deleted successfully.'));
            } else {
                $this->Flash->error(__('Some error occurred while delete Review!!'));
            }
        } else {
            $this->Flash->error(__('Review not found!!'));
        }
        return $this->redirect(['controller' => 'Products', 'action' => 'reviewRatings']);
    }
    public function changeProductStatus($productId) {
            $productDetail = $this->Products->find()->select(['hidden'])->where(['Products.product_id' => $productId])->first();
                if ($productDetail['hidden'] == 'N') {

                    $query = $this->Products->query()->update()->set(['hidden' => 'Y'])->where(['Products.product_id' => $productId])->execute();
                    $products = $this->Products->find()->contain(['Categories', 'Brands'])->order(['Products.name' => 'ASC']);
                    $this->Flash->success(__('Product Deactivate successfully.'));
                } else if ($productDetail['hidden'] == 'Y') {

                    $query = $this->Products->query()->update()->set(['hidden' => 'N'])->where(['Products.product_id' => $productId])->execute();
                    $products = $this->Products->find()->contain(['Categories', 'Brands'])->order(['Products.name' => 'ASC']);
                    $this->Flash->success(__('Product Activate successfully.'));
                } else {
                    $this->Flash->error(__('Your Product is not available'));
                }
                  $this->redirect(['controller' => 'Products', 'action' => 'listProducts']);
    }

}
