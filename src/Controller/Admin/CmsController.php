<?php
namespace App\Controller\Admin;
use App\Controller\AppController;
class CmsController extends AppController {
    public function initialize() {
        parent::initialize();
        $this->Auth->allow(['login']);
        // Load Models
        $this->loadModel('Cms');
        $this->loadModel('cmsPages');
        // Load Components
        $this->loadComponent('Custom');
        //layout
        $this->viewBuilder()->setLayout('admin');
    }
    public function addCms($id = null) {
        if ($id) {
            $newEntity = $this->Cms->find()->where(['id' => $id])->first();
        } else {
            $newEntity = $this->Cms->newEntity();
        }
        if ($this->request->is(['post', 'patch', 'put'])) {
            $data = $this->request->getData();
            $this->Cms->patchEntity($newEntity, $data);
            if ($this->Cms->save($newEntity)) {
                if ($id) {
                    $this->Flash->success(__('Cms Updated successfully'));
                } else {
                    $this->Flash->success(__('Cms Added successfully'));
                }
            } else {
                $this->Flash->error(__('Cms not added!!'));
            }
            return $this->redirect(['action' => 'listCms']);
        }
        $this->set(compact(['newEntity', 'id']));
    }
    public function homePageContent($id = null) {
        if ($id) {
            $homePage = $this->Cms->find()->where(['id' => $id])->first();
        } else {
            $homePage = $this->Cms->newEntity();
        }
        if ($this->request->is(['post', 'patch', 'put'])) {
            $data = $this->request->getData();
            $data['type'] = 'home';
            $this->Cms->patchEntity($homePage, $data);
            if ($this->Cms->save($homePage)) {
                if (!$id) {
                    $this->Flash->success(__('Home page added successfully'));
                } else {
                    $this->Flash->success(__('Home page updated successfully'));
                }
            }
            return $this->redirect(['action' => 'listCms']);
        }
        $this->set(compact(['homePage', 'id']));
    }
    public function listCms() {
        $listCms = $this->Cms->find();
        $this->set(compact('listCms'));
    }
    public function deleteCms($id) {
        $cms = $this->Cms->find()->where(['id' => $id])->first();
        if (!empty($cms)) {
            if ($this->Cms->delete($cms)) {
                $this->Flash->success(__('Cms deleted successfully.'));
            } else {
                $this->Flash->error(__('Some error occurred while delete Cms!!'));
            }
        } else {
            $this->Flash->error(__('Cms not found!!'));
        }
        return $this->redirect($this->referer());
    }
    //////////////////////////////////////////////// PAGE SECTOIN ////////////////////////////////////////////////
    public function newPage($uniqueId = NULL) {
        if ($uniqueId) {
            $cmsPageEntity = $this->cmsPages->find()->where(['unique_id' => $uniqueId])->first();
        } else {
            $cmsPageEntity = $this->cmsPages->newEntity();
        }
        if ($this->request->is(['post', 'patch', 'put'])) {
            $data = $this->request->getData();
            if (!$uniqueId) {
                $data['unique_id'] = $this->Custom->generateUniqNumber();
            }
            $this->cmsPages->patchEntity($cmsPageEntity, $data);
            if ($this->cmsPages->save($cmsPageEntity)) {
                $this->Flash->success(__('Page Added successfully'));
            } else {
                $this->Flash->error(__('Page not added!!'));
            }
            return $this->redirect(['action' => 'listPages']);
        }
        $this->set(compact(['cmsPageEntity', 'uniqueId']));
    }
    public function listPages() {
        $listPages = $this->cmsPages->find();
        $this->set(compact(['listPages']));
    }
    public function deletePage($unique_id) {
        $page = $this->cmsPages->find()->where(['cmsPages.unique_id' => $unique_id])->first();
        if (!empty($page)) {
            if ($this->cmsPages->delete($page)) {
                $this->Flash->success(__('Page deleted successfully.'));
            } else {
                $this->Flash->error(__('Some error occurred while delete account!!'));
            }
        } else {
            $this->Flash->error(__('Page not found!!'));
        }
        return $this->redirect($this->referer());
    }
}
