<?php

namespace App\Controller;

use App\Controller\AppController;
use Cake\Validation\Validation;
use App\Controller\Component\SendEmailComponent;

class UsersController extends AppController {

    function initialize() {
        parent::initialize();
// Load Models
        $this->loadModel('Users');
        $this->loadModel('Orders');
        $this->loadModel('OrdersProducts');
        $this->loadModel('Products');
        $this->loadModel('Countries');
        $this->loadModel('Images');

        $this->loadModel('ProductReviews');
        // Load Components
// Load Components

        $this->loadComponent('Custom');
        $this->loadComponent('SendEmail');

        $this->loadComponent('Auth');



        $publicActions = ['login', 'logout', 'index', 'register', 'resetPassword', 'activateAccount', 'forgotPassword'];
        $this->Auth->allow($publicActions);
// Set Layout
        $this->viewBuilder()->setLayout('default');
    }

    public function login() {
// Redirect if Alreday Login
        if (@$this->userId) {
            $this->_loginRedirect();
        }
        if ($this->request->is('post')) {
            $user = $this->Auth->identify();

            if (empty($user)) {
                $this->Flash->error(__('Invalid Email or Password, try again.'));
            } else if ($user['is_active'] == '0') {
                $this->Flash->error(__('You Account not activated yet. Please contact admin.'));
            } else {
                $this->Auth->setUser($user);
                $this->_loginRedirect();
            }
            return $this->redirect($this->referer());
        }
        $pageTitle = SITE_NAME . " : Login";
        $this->set(compact('pageTitle'));
    }

    private function _loginRedirect() {
        if ($this->Auth->user('user_type') == 1) {
            return $this->redirect(HTTP_ROOT . 'admin'); // Admin Dashboard
        } elseif ($this->Auth->user('user_type') == 2) {

            return $this->redirect(HTTP_ROOT . 'users/account-information'); // Admin Dashboard
        } else {
            return $this->redirect(HTTP_ROOT);
        }
    }

    public function logout() {
        if (isset($_COOKIE['rememberme'])) {
        }
        if (!isset($_SESSION)) {
            session_start();
        }
        session_destroy();
        $this->Auth->logout();
        $this->redirect(HTTP_ROOT . 'login');
    }

    public function register() {
        if ($this->request->is(['post', 'put', 'patch'])) {
            if ($data = $this->request->getData()) {
                $find = $this->Users->find()->where(['email' => $data['email']])->count();
                if ($find == 0) {
                    $data['user_type'] = 2;
                    $data['unique_id'] = $this->Custom->generateUniqNumber();
                    $data['qstr'] = $this->Custom->generateUniqNumber();
                    $newEntity = $this->Users->newEntity();
                    $this->Users->patchEntity($newEntity, $data);
                    if ($this->Users->save($newEntity)) {
                        $this->Users->sendEmail($newEntity, 'WELCOME_EMAIL', $data);
                        $this->Flash->success(__("Registration successfull."));
                        $this->redirect(HTTP_ROOT . 'login');
                    } else {
                        $this->Flash->error(__("Something is going to wrong."));
                    }
                } else {
                    $this->Flash->error(__("This mail_id is already register."));
                }
            }
        }
    }

    public function forgotPassword() {
        if ($this->request->is('post')) {

            $data = $this->request->getData();

            $email = $data['email'];

            $query = $this->Users->find()->where(['email' => $email]);

            if (!$query->count()) {
                $this->Flash->error(__("Email doesn't exist!!!!"));
            } else {
                $user = $query->first();

                $user->qstr = $this->Custom->generateUniqNumber();
                $this->Users->query()->update()->set(['qstr' => $user->qstr])->where(['id' => $user->id])->execute();

                $this->Users->sendEmail($user, 'FORGOT_PASSWORD');

                $this->Flash->success(__('Link to reset your password is sent to your email!!'));
                return $this->redirect(["controller" => "Users", "action" => "login"]);
            }

            return $this->redirect($this->referer());
        }
    }

    public function resetPassword($uniqueID, $qstr) {
        $query = $this->Users->find()->where(['unique_id' => $uniqueID, 'qstr' => $qstr]);
        if (!$query->count()) {
            $this->Flash->error(__('Link Expired!!'));
            return $this->redirect($this->referer());
        }

        if ($this->request->is('post')) {
            $data = $this->request->getData();

            if (empty($data['password'])) {
                $this->Flash->error(__('Please enter password!!'));
            } else if ($data['password'] != $data['conf_password']) {
                $this->Flash->error(__('Password & Conf Password are not matching!!'));
            } else {

                $user = $query->first();
                $user->password = $data['password'];

                if ($this->Users->save($user)) {

                    $this->Users->query()->update()->set(['qstr' => ''])->where(['id' => $user->id])->execute();
                    $this->Flash->success(__('Pasword changed successfully!!'));
                    return $this->redirect(["controller" => "Users", "action" => "login"]);
                } else {
                    $this->Flash->error(__('Password & Conf Password are not matching!!'));
                }
            }

            return $this->redirect($this->referer());
        }
    }

    public function activateAccount($uniqId) {
        $explode = explode("/", $uniqId);
        $uniq_id = $explode[0];
        $qstr = $explode[1];
        $find = $this->Users->find()->where(['qstr' => $qstr])->count();
        if ($find == 1) {
            $update = $this->Users->query()->update()->set(['is_active' => 1, 'qstr' => ''])->where(['qstr' => $qstr])->execute();
            if ($update) {
                $this->Flash->success(__("User activate successfully"));
            } else {
                $this->Flash->error(__("Something is going to wrong."));
            }
        } else {
            $this->Flash->error(__('Invalid link!!'));
        }
    }

    public function accountInformation() {
        $userInfo = $this->Users->find()->contain(['Orders', 'OrdersProducts'])->where(['Users.id' => $this->Auth->user('id')])->first();
        if ($this->request->is(['post', 'put', 'patch'])) {
            $data = $this->request->getData();
            $fullName = [$data['first_name'], $data['last_name']];
            $data['username'] = implode(" ", $fullName);
            $update = $this->Users->query()->update()->set(['username' => $data['username'], 'email' => $data['email'], 'mobile' => $data['mobile']])
                            ->where(['Users.id' => $userInfo->id])->execute();
            if ($update) {
                $this->Flash->success(__('Your profile updated successfully'));
                $userInfo = $this->Users->find()->where(['Users.id' => $userInfo->id])->first();
                $this->set(compact('userInfo'));
            } else {
                $this->Flash->error(__('Something is going to wrong'));
            }
        }
        $this->set(compact('userInfo'));
    }

    public function changePassword() {
        if ($this->request->is(['post', 'put', 'patch'])) {
            $data = $this->request->getData();
            $user = $this->Users->get($this->Auth->user('id'));
            $this->Users->patchEntity($user, $data, ['validate' => 'ChangePassword']);
            $update = $this->Users->query()->update()->set(['password' => $user['password']])->where(['id' => $user['id']])->execute();
            if ($update) {
                $this->Flash->success(__('Password changed successfuly'));
            }
        }
    }

    public function addressBook($order_id = null) {
        $countriesList = $this->Countries->find('list', ['keyField' => 'id', 'valueField' => 'name'])->where(['is_active' => 1]);
        if ($order_id) {
            $order = $this->Orders->find()->where(['Orders.order_id' => $order_id])->first();
        } else {
            $order = $this->Orders->newEntity();
        }

        if ($this->request->is(['post', 'patch', 'put'])) {
            $data = $this->request->getData();
            $data['user_id'] = $this->Auth->user('id');
            $this->Orders->patchEntity($order, $data);
//pj($order);exit;
            if ($this->Orders->save($order)) {
                $order->user_id = $this->Auth->user('id');
                pj($order->user_id);exit;
                if (!$order_id) {
                    $this->Flash->success(__('Added Successfully'));
                    return $this->redirect(['action' => 'listBook', $order->order_id]);
                } else {
                    $this->Flash->success(__('Updated Successfully.'));
                }
            }
        }
        $this->set(compact('order', 'countriesList'));
    }

    public function listBook() {

        $orderLists = $this->Orders->Find();
        $this->set(compact('orderLists'));
    }

    public function deleteAddress($order_id) {
        $order = $this->Orders->find()->where(['Orders.order_id' => $order_id])->first();
        if (!empty($order)) {
            if ($this->Orders->delete($order)) {
                $this->Flash->success(__('Address deleted successfully.'));
            } else {
                $this->Flash->error(__('Some error occurred while delete Address!!'));
            }
        } else {
            $this->Flash->error(__('Address not found!!'));
        }
        return $this->redirect($this->referer());
    }

    public function orderHistory() {
        if ($this->request->is(['post', 'put', 'patch'])) {
            $this->viewBuilder()->setLayout('');
            $data = $this->request->getData();
            if ($data['status'] == 'CANCEL') {
                $update = $this->OrdersProducts->query()->update()->set(['cancel_status' => ORDSTATUS_CANCEL])->where(['id' => $data['id']])->execute();
                if ($update) {
                    $userDetails = $this->Users->find()->where(['id' => $this->Auth->user('id')])->first();
                    $this->OrdersProducts->sendEmail($data['id'], 'CANCEL', $userDetails);
                    $userInfos = $this->OrdersProducts->find()->contain(['Products.Images', 'Users'])->where(['Users.id' => $this->Auth->user('id')]);
                    $countRecords = $userInfos->count();
                }
            } elseif ($data['status'] == 'RETURN') {
                $update = $this->OrdersProducts->query()->update()->set(['return_status' => ORDSTATUS_RETURN])->where(['id' => $data['id']])->execute();
                if ($update) {
                    $userDetails = $this->Users->find()->where(['id' => $this->Auth->user('id')])->first();
                    $this->OrdersProducts->sendEmail($data['id'], 'RETURN', $userDetails);
                    $userInfos = $this->OrdersProducts->find()->contain(['Products.Images', 'Users'])->where(['Users.id' => $this->Auth->user('id')]);
                    $countRecords = $userInfos->count();
                }
            }
        } else {
            $userInfos = $this->OrdersProducts->find()->contain(['Products.Images', 'Users'])->where(['Users.id' => $this->Auth->user('id')]);
            $countRecords = $userInfos->count();
        }
        $this->set(compact('userInfos', 'countRecords'));
//        pj($userInfos);exit;
    }

    public function returnRequests() {
        $userInfos = $this->OrdersProducts->find()->contain(['Products.Images', 'Users'])->where(['Users.id' => $this->Auth->user('id'), 'OrdersProducts.return_status' => "R"]);
        $countRecords = $userInfos->count();
        $this->set(compact('userInfos', 'countRecords'));
    }

    public function reviewRatings() {
        $reviewRatings = $this->ProductReviews->find()->contain(['Products.Images'])->where(['ProductReviews.user_id' => $this->Auth->user('id')]);
//        pj($reviewRatings);exit;
        $this->set(compact('reviewRatings'));
    }

    public function deleteReview($reviewRating) {
        $productReviews = $this->ProductReviews->find()->where(['ProductReviews.id' => $reviewRating])->first();
//        pj($product);exit;
        if (!empty($productReviews)) {
            if ($this->ProductReviews->delete($productReviews)) {
                $this->Flash->success(__('Review deleted successfully.'));
            } else {
                $this->Flash->error(__('Some error occurred while delete Review!!'));
            }
        } else {
            $this->Flash->error(__('Review not found!!'));
        }
        return $this->redirect(HTTP_ROOT . 'users/reviewRatings');
    }

}
