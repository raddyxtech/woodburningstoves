<?php

namespace App\Controller;

use Cake\Controller\Controller;
use Cake\Event\Event;
class AppController extends Controller {

    public function initialize() {
        parent::initialize();

        $this->userId = 0;
        $this->userType = 0;
        $this->loadComponent('RequestHandler', [
            'enableBeforeRedirect' => false,
        ]);
        $this->loadComponent('Flash');
        $this->loadComponent('Auth', [
            'authenticate' => [
                'Form' => [
                    'fields' => ['username' => 'email', 'password' => 'password']
                ]
            ],
            'logoutRedirect' => [
                'controller' => 'Users',
                'action' => 'login',
            ],
            'loginAction' => ['controller' => 'Users', 'action' => 'login'],
            'allowedActions' => ['home', 'register']
        ]);

        if ($this->Auth->user('id')) {
            $this->userId = $this->Auth->user('id');
            $this->userType = $this->Auth->user('user_type');
        }
    }

    public function beforeRender(Event $event) {
        parent::beforeRender($event);

        $is_logged_in = $this->request->getSession()->read('Auth.User.id') ? true : false;
        $user_type = $this->request->getSession()->read('Auth.User.user_type');
        $user_id = $this->request->getSession()->read('Auth.User.id');
        $user_name = $this->request->getSession()->read('Auth.User.username');
        if (@$this->Auth->user('user_type') == 2) {
            $orders = $this->OrdersProducts->find()->contain(['Products.Images'])->where(['OrdersProducts.user_id' => $this->Auth->user('id'), 
                'OrdersProducts.return_status' => 'A', 'OrdersProducts.cancel_status' => 'A']);
            $count = $this->OrdersProducts->find()->contain(['Products.Images'])->where(['OrdersProducts.user_id' => $this->Auth->user('id'), 
                'OrdersProducts.return_status' => 'A', 'OrdersProducts.cancel_status' => 'A'])->count();
            $total = 0;
            foreach ($orders as $order) {
                $total += $order->total;
//                 $order->count = $this->OrdersProducts->find()->count();
            }
        } 

        $this->set(compact(['user_type', 'is_logged_in','user_id', 'user_name', 'orders', 'total','count']));
    }

}
