<?php

namespace App\Controller;

use App\Controller\AppController;
use Cake\Validation\Validation;
use Cake\Database\Query;
use Cake\I18n\Time;
use Cake\Core\Configure;

class ProductsController extends AppController {

    function initialize() {
        parent::initialize();

        // Load Models
        $this->loadModel('Products');
        $this->loadModel('Users');
        $this->loadModel('Images');
        $this->loadModel('ProductVariations');
        $this->loadModel('Categories');
        $this->loadModel('Orders');
        $this->loadModel('OrdersProducts');
        $this->loadModel('CategoriesProducts');
        $this->loadModel('Countries');
        $this->loadModel('Brands');
        $this->loadModel('FavoriteProducts');
        $this->loadModel('ProductReviews');

        // Load Components
        $this->loadComponent('Custom');
        $this->loadComponent('Paginator');

        //allow  action
//        $this->Auth->allow(['productsList', 'productsInf', 'productsDetails', 'basket', 'terms']);
        $this->Auth->allow(['productsList', 'productsInf', 'productsDetails', 'terms', 'search', 'addreview','varation']);
        // Set Layout
        $this->viewBuilder()->setLayout('default');
    }

    public function productsList($id = '') {
        $brands = $this->Brands->find()->where(['is_active' => 1]);
        if ($this->request->is('post')) {

            $params = array_filter($this->request->getData());
            return $this->redirect(['controller' => 'Products', 'action' => 'index', '?' => $params]);
        }
        $params = $this->request->getQueryParams();
        $conditions = [];

        $categories = $this->Categories->find()->contain(['CategoriesProducts'])->where(['parent_id' => 0])->order(['name' => 'ASC']);
        foreach ($categories as $categorie):
            $categorie->count = $this->CategoriesProducts->find()->where(['category_id' => $categorie->category_id])->count();
        endforeach;


        if (!empty($params["brand_id"])) {
            $conditions[] = ['Products.brand_id LIKE' => $params["brand_id"]];
        }
        if (!empty($params["search"])) {
            $conditions[] = ['Products.name LIKE' => '%' . $params["search"] . '%'];
        }

        $favProductList = $this->FavoriteProducts->find('list', ['keyField' => 'product_id', 'valueField' => 'product_id'])->where(['user_id' => $this->Auth->user('id')])->toArray();
        
        $config = [
            'limit' => 30,
            'conditions' => $conditions,
            'contain' => ['ProductVariations', 'OrdersProducts', 'Images'],
            'order' => ['Products.name' => 'ASC']
        ];
        if (!empty($id)) {
            $productList = $this->CategoriesProducts->find('list', ['keyField' => 'product_id', 'valueField' => 'product_id'])->where(['category_id' => $id]);
            $products = $this->Paginator->paginate($this->Products->find()->where(['Products.hidden' => 'N','Products.product_id IN' => $productList]), $config);
        } else {
            $products = $this->Paginator->paginate($this->Products->find()->where(['Products.hidden' => 'N']), $config);

        }

        $this->set(compact(['categories', 'products', 'favoriteproduct', 'favProductList', 'brands']));
    }

    public function search() {
        $this->viewBuilder()->setLayout('ajax');
        
        $queryParams = $this->request->getQueryParams();
        
        $conditions = [];
        if (!empty($queryParams['keyword'])) {
            $conditions['Products.name LIKE'] = '%' . urldecode(trim($queryParams['keyword'])) . '%';
        }
        if (!empty($queryParams['minimum_price'])) {
//            $conditions['ProductVariations.retail_price >='] = $queryParams['minimum_price'];
            $productList = $this->ProductVariations->find('list', ['keyField' => 'product_id', 'valueField' => 'product_id'])
                    ->where(['ProductVariations.retail_price >= ' => $queryParams["minimum_price"]]);
            $conditions[] = ['Products.product_id IN' => $productList];
        }
        if (!empty($queryParams['maximum_price'])) {
//            $conditions['ProductVariations.retail_price >='] = $queryParams['maximum_price'];
            $productList = $this->ProductVariations->find('list', ['keyField' => 'product_id', 'valueField' => 'product_id'])
                    ->where(['ProductVariations.retail_price <= ' => $queryParams["maximum_price"]]);
            $conditions[] = ['Products.product_id IN' => $productList];
        }
        if (!empty($queryParams["brand_id"])) {
             $conditions['Products.brand_id IN'] = $queryParams["brand_id"];
        }
        if (!empty($queryParams["category_id"])) {
            $productList = $this->CategoriesProducts->find('list', ['keyField' => 'product_id', 'valueField' => 'product_id'])
                    ->where(['CategoriesProducts.category_id IN' => $queryParams["category_id"]]);
//            pj($productList);exit;
            $conditions[] = ['Products.product_id IN' => $productList];
        }
        $config = [
            'limit' => 30,
            'conditions' => $conditions,
            'contain' => ['ProductVariations', 'OrdersProducts', 'Images'],
            'order' => ['Products.name' => 'ASC']
        ];
        if (!empty($id)) {
            $productList = $this->CategoriesProducts->find('list', ['keyField' => 'product_id', 'valueField' => 'product_id'])->where(['category_id' => $id]);
            $products = $this->Paginator->paginate($this->Products->find()->where(['Products.hidden' => 'N', 'Products.product_id IN' => $productList]), $config);
        } else {
            $products = $this->Paginator->paginate($this->Products->find()->where(['Products.hidden' => 'N']), $config);
        }
        $favProductList = $this->FavoriteProducts->find('list', ['keyField' => 'product_id', 'valueField' => 'product_id'])->where(['user_id' => $this->Auth->user('id')])->toArray();
        $this->set(compact(['products','favProductList']));
    }

    public function productsInf($id) {
        $Parentcategories = $this->Categories->find()->where(['category_id' => $id])->order(['name' => 'ASC'])->first();
        $categories = $this->Categories->find()->where(['Categories.parent_id' => $id])->order(['name' => 'ASC']);
        $products = [];
        foreach ($categories as $category) {
            $cat_id = $category->category_id;
            $productscategory = $this->CategoriesProducts->find()->where(['CategoriesProducts.category_id' => $cat_id])->first();

            if (!empty($productscategory->product_id)) {
                $products[] = $this->Products->find()->contain(['ProductVariations', 'OrdersProducts', 'Images'])->where(['Products.product_id' => $productscategory->product_id])->order(['Products.name' => 'ASC']);
            }
        }
        $this->set(compact(['Parentcategories', 'categories', 'products']));
    }

    public function productsDetails($id) {
                $this->viewBuilder()->setLayout('default');
        $categories = $this->Categories->find()->contain([ 'CategoriesProducts'])->where(['parent_id' => 0])->order(['name' => 'ASC']);
//        $reviews = $this->ProductReviews->find()->where(['ProductReviews.product_id' => $id,'is_published'=>'1'])->order(['ProductReviews.id' => 'Desc']);
       $reviews = $this->ProductReviews->find()->where(['ProductReviews.product_id' => $id])->order(['ProductReviews.id' => 'Desc']);
       
       $products = $this->Products->find()->contain(['ProductVariations', 'Images', 'OrdersProducts', 'CategoriesProducts', 'Categories'])->where(['Products.product_id' => $id])->order(['Products.name' => 'ASC'])->first();
       
       $varationList = $this->ProductVariations->find('list', ['keyField' => 'var_data_1', 'valueField' => 'var_data_1'])->where(['product_id' => $id]);
       $varationsList = $this->ProductVariations->find('list', ['keyField' => 'var_data_2', 'valueField' => 'var_data_2'])->where(['product_id' => $id]);
               
        $favProductList = $this->FavoriteProducts->find('list', ['keyField' => 'product_id', 'valueField' => 'product_id'])->where(['user_id' => $this->Auth->user('id')])->toArray();

        foreach ($categories as $categorie):
            $categorie->count = $this->CategoriesProducts->find()->where(['category_id' => $categorie->category_id])->count();
        endforeach;

//        foreach ($products->product_variations as $varations){}
        if (!empty($products->addons)) {
            $addons = unserialize(@$products->addons);
            $product_id = 0;
            foreach ($addons as $catvalue) {
                $product_id = $catvalue['ProductId'];

                $releatedproducts = $this->Products->find()->contain(['ProductVariations', 'Images', 'Categories'])->where(['Products.product_id ' => $product_id]);
            }
        }
//        else {
//
//            $cat_id = 0;
//            foreach ($products->categories as $catvalue) {
//                $cat_id = $catvalue->category_id;
//            }
//
//            $releatedproducts = $this->Products->find()->contain(['ProductVariations', 'Images', 'Categories' => function (Query $q) use ($cat_id) {
//                            return $q->where(['categories.category_id' => $cat_id]);
//                        }])->where(['Products.product_id !=' => $id])->limit(3);
//                }
        $this->set(compact(['categories', 'products', 'releatedproducts', 'price', 'vid', 'qty', 'varations','varationList','varationsList', 'favProductList','reviews']));
    }

    public function varation() {
        $this->viewbuilder()->setLayout('ajax');

        $data = $this->request->getData();
        $products = $this->ProductVariations->find()->where(['ProductVariations.var_data_1' => $data['var_data_1']]);
        echo json_encode($products);

        exit;
    }

    public function basket() {
//                $session = $this->getRequest()->getSession();
        if ($this->request->is(['patch', 'post', 'put'])) {
            $data = $this->request->getData();
//                    pj($data);exit;
            $pid = $data['product_id'];
            if (@$data['submit'] == 'update') {
                $updateData = array_combine($data["product_id"], $data["iQty"]);
                $orderproducts = $this->OrdersProducts->find();
                foreach ($orderproducts as $orderproduct) {
                    @$orderproduct->qty = $updateData[$orderproduct->product_id];
                    $orderproduct->total = $orderproduct->unit_price * $orderproduct->qty;
                }
                $this->OrdersProducts->saveMany($orderproducts);
            } else {
                $orderproduct = $this->OrdersProducts->find()->where(['OrdersProducts.product_id' => $pid])->first();
                $query = $this->Orders->find()->select(['order_id'])->where(['Orders.user_id' => $this->Auth->user('id')])->first();
                if (!empty($orderproduct)) {
                    $orderproduct->qty += 1;
                    $orderproduct->total = $orderproduct->unit_price * $orderproduct->qty;
                    // $total += $orderproduct->total;
                } else {
                    $orderproduct = $this->OrdersProducts->newEntity();
                    $orderproduct->total = $data['unit_price'];
                    $orderproduct->user_id = $this->Auth->user('id');
                    $orderproduct->order_id = $query->order_id;
                    $orderproduct->unique_id = ORD_UNIQID.$this->Auth->user('id').$query->order_id.$pid; 
                    $this->OrdersProducts->patchEntity($orderproduct, $data);
                }
//                $unique_id = "ODRI".$this->Auth->user('id').$pid;
//                pj($unique_id);exit;
                $this->OrdersProducts->save($orderproduct);
//                pj($orderproduct->getErrors());exit;
//                        $id = $session->read($orderproduct->id);
//                        $session->id = $orderproduct->id;
            }
            if ($this->Auth->user('id')) {
                $orders = $this->OrdersProducts->find()->where(['OrdersProducts.user_id' => $this->Auth->user('id')]);
//                        pj($orders);exit;
            }
//                    else {
////                          $orders = $this->OrdersProducts->find()->contain(['Products.Images'])->where(['OrdersProducts.id' => $session->id]);
//                        $orders = $this->OrdersProducts->find()->contain(['Products.Images']);
//                    }
            $orderproduct = $this->OrdersProducts->find()->where(['OrdersProducts.qty' => 0])->first();
            if (!empty($orderproduct)) {
                $this->OrdersProducts->delete($orderproduct);
            }

            $total = 0;
            foreach ($orders as $order) {
                $total += $order->total;
            }
        } if ($this->Auth->user('id')) {
            $orders = $this->OrdersProducts->find()->contain(['Products.Images'])->where(['OrdersProducts.user_id' => $this->Auth->user('id')]);
//                        pj($orders);exit;
        }
//                    else {
////                          $orders = $this->OrdersProducts->find()->contain(['Products.Images'])->where(['OrdersProducts.id' => $session->id]);
//                        $orders = $this->OrdersProducts->find()->contain(['Products.Images']);
//                    }
        $orderproduct = $this->OrdersProducts->find()->where(['OrdersProducts.qty' => 0])->first();
        if (!empty($orderproduct)) {
            $this->OrdersProducts->delete($orderproduct);
        }

        $total = 0;
        foreach ($orders as $order) {
            $total += $order->total;
        }

        $this->set(compact('orders', 'total', 'qty', 'pid', 'orderproduct'));
    }

    public function checkOut() {
        $countriesList = $this->Countries->find('list', ['keyField' => 'id', 'valueField' => 'name'])->where(['is_active' => 1]);
        $order = $this->Orders->find()->where(['Orders.user_id' => $this->Auth->user('id')])->first();
        $usermail = $this->Users->find()->where(['Users.id' => $this->Auth->user('id')])->first();
      
        $defaultOrder = $this->Orders->find()->where(['Orders.user_id' => $this->Auth->user('id'),'choose_delivery' => 'Y'])->first();
        $orderaddresses = $this->Orders->find()->where(['Orders.user_id' => $this->Auth->user('id')]);
        $defaultAddressList = $this->Orders->find('list', ['keyField' => 'order_id', 'valueField' => 'order_id'])->where(['user_id' => $this->Auth->user('id')])->toArray();
        $orderlists = $this->OrdersProducts->find()->contain(['Products.Images'])->where(['OrdersProducts.user_id' => $this->Auth->user('id'),'return_status'=>'A','cancel_status'=>'A']);

         

//pj($order);exit;
        if (empty($order->user_id)) {
            $order = $this->Orders->newEntity();
            if ($this->request->is(['post'])) {
                $data = $this->request->getData();
                if (@$data['submit'] == 'Proceed' && @$data['cSameAddr'] == 'Y') {
                    $data['delivery_title'] = @$data['invoice_title'];
                    $data['delivery_firstname'] = @$data['invoice_firstname'];
                    $data['delivery_lastname'] = @$data['invoice_lastname'];
                    $data['delivery_email'] = @$data['invoice_email'];
                    $data['delivery_tel1'] = @$data['invoice_tel1'];
                    $data['delivery_tel2'] = @$data['invoice_tel2'];
                    $data['delivery_tel3'] = @$data['invoice_tel3'];
                    $data['delivery_company'] = @$data['invoice_company'];
                    $data['delivery_address1'] = @$data['invoice_address1'];
                    $data['delivery_address2'] = @$data['invoice_address2'];
                    $data['delivery_county'] = @$data['invoice_city'];
                    $data['delivery_postcode'] = @$data['invoice_postcode'];
                    $data['delivery_countryid'] = @$data['invoice_countryid'];
                    $this->Orders->patchEntity($order, $data);
                    $order->user_id = $this->Auth->user('id');

                    // pj($order->getErrors());exit;
                    if ($this->Orders->save($order)) {
                        // $this->Flash->success(__('InvoiceDetail added sucessfully!!'));
//                        return $this->redirect(['action' => 'payment']);
                    } else {
                        $this->Flash->error(__('Error!'));
                        return $this->redirect(['action' => 'checkOut']);
                    }
                } elseif (@$data['submit'] == 'Proceed') {
                    
                    $this->Orders->patchEntity($order, $data);
                    $order->user_id = $this->Auth->user('id');

                    if ($this->Orders->save($order)) {
                        return $this->redirect(['action' => 'deliveryCheck']);
                    } else {
                        $this->Flash->error(__('Error!'));
                        return $this->redirect(['action' => 'checkOut']);
                    }
                }
            }
            
        } else if ($order->user_id == $this->Auth->user('id')) {
            
            if ($this->request->is(['post', 'patch', 'put'])) {
                $data = $this->request->getData();
                $this->Orders->patchEntity($order, $data);
                if ($this->Orders->save($order)) {
                    return $this->redirect(['action' => 'payment']);
                } else {
                    return $this->redirect(['action' => 'checkOut']);
                }
            }
        }
        $this->set(compact('countriesList', 'order','orderaddresses', 'order_id','defaultAddressList','defaultOrder','orderlists','usermail'));
    }
    

    public function deliveryCheck() {
        $countriesList = $this->Countries->find('list', ['keyField' => 'id', 'valueField' => 'name'])->where(['is_active' => 1]);
        $order = $this->Orders->find()->where(['Orders.user_id' => $this->Auth->user('id')])->first();
        if ($this->request->is(['post', 'patch', 'put'])) {

            $data = $this->request->getData();
            $this->Orders->patchEntity($order, $data);
// pj($order->getErrors()); exit;
            if ($this->Orders->save($order)) {
// $order->user_id = $this->Auth->user('id');
// $this->Flash->success(__('Delivery details added sucessfully!!'));
                return $this->redirect(['action' => 'payment']);
            } else {
// $this->Flash->error(__('Error!'));
                return $this->redirect(['action' => 'deliveryCheck']);
            }
        }
        $this->set(compact('countriesList', 'order', 'order_id'));
    }

    public function terms() {
        $this->viewBuilder()->setLayout('');
    }

    public function payment() {
        $orderfetch = $this->Orders->find()->where(['user_id' => $this->Auth->user('id'),'choose_delivery' => 'Y'])->first();
        $this->OrdersProducts->query()->update()->set(['order_id' => $orderfetch->order_id])->where(['user_id' => $this->Auth->user('id')])->execute();
//        }
        $order = $this->Orders->find()->contain(['OrdersProducts.Products'])->where(['user_id' => $this->Auth->user('id')])->first();
//        pj($order);exit;
// $iDeliveryId = $oDeliveries->getFirstOptionId();

        $total = 0;
        $totalqty = 0;
        foreach ($order->orders_products as $orderproduct) {
            $total += $orderproduct->total;
            $totalqty += $orderproduct->qty;
//            $this->Orders->query()->update()->set(['order_id' => $orderfetch->order_id])->where(['user_id' => $this->Auth->user('id')])->execute();
        }
        foreach ($order->orders_products as $product)
            $delivery_charge = $this->_getDeliveryChargeExclVATl($order->delivery_cost, $order->vat_rate);
        $sub_total = $this->_getProductsTotalExclVAT(@$product);
        $TotalExclVAT = $this->_getItemTotalExclVAT(@$product->unit_price, @$product->qty, @$product->vat_rate);
        $vats = $this->_getVAT(@$product, @$delivery_charge, $order->vat_rate);

        $cat_ref = $this->_getorderref($order->order_id);
// $delivery = $this->_getCost($order->delivery_id);
// $this->Orders->query()->update()->set(['cart_ref' => $cat_ref,'delivery_cost' => $delivery])->where(['user_id' => $this->Auth->user('id')])->execute();
// $orderEntity = $this->Orders->newEntity();
        $orderData['order_status'] = ORDSTATUS_TEMP;
        $orderData['ip_address'] = $_SERVER['REMOTE_ADDR'];
        $orderData['vat_rate'] = DEFAULT_VAT_RATE;
        $orderData['date_created'] = date("Y-m-d H:i:s");

        $this->Orders->patchEntity($order, $orderData);
        if (!$this->Orders->save($order)) {
            $message = getFirstError($order->getErrors());
            $this->Flash->error($message);
            return $this->redirect(['controller' => 'products', 'action' => 'payment']);
        }
        $this->Orders->query()->update()->set(['cart_ref' => $cat_ref])->where(['user_id' => $this->Auth->user('id')])->execute();
        $this->set(compact('countriesList', 'vats', 'order', 'totalqty', 'total'));
    }
      public function procedePayment() {
          
        $this->viewBuilder()->setLayout('');
        $orderfetch = $this->Orders->find()->where(['user_id' => $this->Auth->user('id')])->first();
        $this->OrdersProducts->query()->update()->set(['order_id' => $orderfetch->order_id])->where(['user_id' => $this->Auth->user('id')])->execute();
        $order = $this->Orders->find()->contain(['OrdersProducts.Products'])->where(['user_id' => $this->Auth->user('id')])->first();
//        pj($order);exit;
// $iDeliveryId = $oDeliveries->getFirstOptionId();

        $total = 0;
        $totalqty = 0;
        foreach ($order->orders_products as $orderproduct) {
            $total += $orderproduct->total;
            $totalqty += $orderproduct->qty;
//            $this->Orders->query()->update()->set(['order_id' => $orderfetch->order_id])->where(['user_id' => $this->Auth->user('id')])->execute();
        }
        foreach ($order->orders_products as $product)
            $delivery_charge = $this->_getDeliveryChargeExclVATl($order->delivery_cost, $order->vat_rate);
            $sub_total = $this->_getProductsTotalExclVAT($product);
            $TotalExclVAT = $this->_getItemTotalExclVAT($product->unit_price, $product->qty, $product->vat_rate);
            $vats = $this->_getVAT($product, $delivery_charge, $order->vat_rate);
            $cat_ref = $this->_getorderref($order->order_id);
//          $delivery = $this->_getCost($order->delivery_id);
//          $this->Orders->query()->update()->set(['cart_ref' => $cat_ref,'delivery_cost' => $delivery])->where(['user_id' => $this->Auth->user('id')])->execute();
//            $orderEntity = $this->Orders->newEntity();
            $orderData['order_status'] = ORDSTATUS_TEMP;
            $orderData['ip_address'] = $_SERVER['REMOTE_ADDR'];
            $orderData['vat_rate'] = DEFAULT_VAT_RATE;
            $orderData['date_created'] = date("Y-m-d H:i:s");

            $this->Orders->patchEntity($order, $orderData);
        if (!$this->Orders->save($order)) {
            $message = getFirstError($order->getErrors());
            $this->Flash->error($message);
            return $this->redirect(['controller' => 'products', 'action' => 'payment']);
        }
            $paypayUrl['success'] = HTTP_ROOT . 'Products/sucess/' . $order->order_id;
            $paypayUrl['cancel'] = HTTP_ROOT . 'Products/payment';
            $paypayUrl['notify'] = HTTP_ROOT . 'Products/notify/'. $order->order_id;
            $this->Orders->query()->update()->set(['cart_ref' => $cat_ref])->where(['user_id' => $this->Auth->user('id')])->execute();
            $this->set(compact('countriesList', 'vats', 'order', 'totalqty','paypayUrl', 'total','orderEntity'));
    }

    public function cancel() {
        $this->viewBuilder()->setLayout('ajax');
        return $this->redirect(['controller' => 'products', 'action' => 'payment']);
    }

    public function sucess($id) {
        $order = $this->Orders->find()->contain(['OrdersProducts.Products.Images'])->where(['Orders.order_id' => $id])->first();
//        pj($order);exit;
         $this->OrdersProducts->query()->update()->set(['return_status' => 'P','cancel_status' => 'P'])->where(['order_id' => $id])->execute();
        $this->set(compact(['order']));
    }

    public function notify($id) {

        // constants having all payment status integer values
        $all_payment_status = Configure::read('Paypal.payment_status');
        $this->loadModel('PaypalIpnLogs');
        $this->loadComponent('Paypal');

        // Log IPN Data
        $paypalIpnLog = $this->PaypalIpnLogs->newEntity();
        $payPalData = $this->request->getData();
        $paypalIpnLog->ipn_data = json_encode($payPalData);

        $this->PaypalIpnLogs->save($paypalIpnLog);
        if ($this->Paypal->validateIPN()) { // validation of request data of paypal
            $template = 'PAYMENT_STATUS_EMAIL';

            $transaction = $this->Orders->find()->contain(['OrdersProducts.Products'])->where(['Orders.order_id' => $id])->first();

            $payment_status = $all_payment_status[$payPalData['payment_status']];
            $transactionData['date_placed'] = date('Y-m-d H:i:s');
            if ($transaction->price <= $payPalData['mc_gross']) {
                $template = 'PAYMENT_EMAIL';
                $transactionData['received_price'] = $payPalData['mc_gross']; //has to update only when payment is received
// $this->Users->query()->update()->set($userFields)->where(['Users.id' => $transaction->user_id])->execute();
            }

            $transactionData['txn_id'] = $payPalData['txn_id'];
            $transactionData['order_status'] = $payment_status;

            $transactionEntity = $this->Orders->find()->where(['id' => $id])->first();

            $this->Orders->patchEntity($transactionEntity, $transactionData);
            $this->OrdersProducts->query()->update()->set(['return_status' => $payment_status])->where(['order_id' => $id])->execute();

            if ($this->Orders->save($transactionEntity)) {
                //generate the pdf invoice file 
                $this->setRequest($this->getRequest()->withQueryParams(['generate-pdf' => 'true']));
                $this->invoicePreview($transactionEntity->order_id);
                
                //send email for the transaction
                $this->Orders->sendEmail($transactionEntity->order_id, $template, $data = []);
            }
        } else {
            $this->log('IPN not verified!!');
        }

        exit;
    }

    public function paymentHistory() {

        $conditions = [
            'Orders.user_id' => $this->user_id,
            'Orders.order_status !=' => 1
        ];
        $transactions = $this->Orders->find()->contain(['OrdersProducts.Products'])->where($conditions)->order(['Orders.order_id' => 'DESC']);
        $this->set(compact('transactions'));
    }

    public function invoicePreview($order_id) {

        $invoiceDetail = $this->Orders->find()->where(['Orders.order_id' => $order_id])->contain($contain)->first();
        $this->set(compact('invoiceDetail'));
        $filename = sprintf("%04d", $invoiceDetail->order_id) . ".pdf";

        if (isset($_GET['download'])) {
            $this->Mpdf->init();
            $this->Mpdf->setFilename($filename);
            $this->Mpdf->setOutput('D');
            $this->Mpdf->SetWatermarkText("Draft");
        } else if (isset($_GET['generate'])) {

            $this->Mpdf->init();
            $this->Mpdf->setFilename(WWW_ROOT . INVOICE . $filename);
            $this->Mpdf->setOutput('F');
            $this->Mpdf->SetWatermarkText("Draft");
            $response = $this->getResponse();
            return $response;
        } else {

            $this->Mpdf->init();
            $this->Mpdf->setFilename(INVOICE . $filename);
            $this->Mpdf->setOutput('I');
            $this->Mpdf->SetWatermarkText("Draft");
        }

        exit;
    }

    private function _getDeliveryChargeExclVATl($delivery_cost, $fVATRate = DEFAULT_VAT_RATE) {

        $calc_vat_included = ($delivery_cost / (100 + $fVATRate) * $fVATRate);
        $deduct_vat = $delivery_cost - number_format($calc_vat_included, 2, '.', '');
        return number_format($deduct_vat, 2, '.', '');
    }

    private function _getItemTotalExclVAT($unit_price, $qty, $fVATRate = '') {
        $fTotal = $unit_price * $qty;

        if ($fVATRate == "" || $fVATRate == 0.00) {
            return DEFAULT_VAT_RATE;
        }

        $calc_vat_include = ($fTotal / (100 + $fVATRate) * $fVATRate);
        $deduct = $fTotal - number_format($calc_vat_include, 2, '.', '');
        return number_format($deduct, 2, '.', '');
    }

    private function _getProductsTotalExclVAT($product) {
        $fTotal = 0.00;
        $fTotal += $this->_getItemTotalExclVAT(@$product->unit_price, @$product->qty, @$product->vat_rate);
        return number_format($fTotal, 2, '.', '');
    }

    private function _getSubTotal($product, $delivery_charge) {
        $fTotal = $this->_getProductsTotalExclVAT($product);
        $fTotal += $delivery_charge;

        return number_format($fTotal, 2, '.', '');
    }

    private function _getZeroVATProductsTotal($product) {
        $fTotal = 0.00;
        if (@$product->unit_price = !NO) {

            $fUnitPrice = $product->unit_price;
            $Qty = $product->qty;
            $fTotal = $fUnitPrice * $Qty;
            return number_format($fTotal, 2, '.', '');
        }
    }

    private function _getVAT($product, $delivery_charge, $fVATRate) {
        $fSub = $this->_getSubTotal($product, $delivery_charge);
        $fVATLessSub = $this->_getZeroVATProductsTotal($product);
        $fSub = $fSub - $fVATLessSub;
        if ($fVATRate == "" || $fVATRate == 0.00) {
            return DEFAULT_VAT_RATE;
        }
        $fVATExcluded = $fSub * ($fVATRate / 100);
        return number_format($fVATExcluded, 2, '.', '');
    }

    private function _getorderref($iCustId) {
        $sCustomerId = "SFS";
        if ($iCustId != 0) {
            $sCustomerId = $iCustId;
        }
// just belt and braces, must never have duplicate
// do {
        srand((double) microtime() * 1000000);
        $sOrderRef = sprintf("%s-%07d", $sCustomerId, rand(1000, 9999999));

// $sSQLQuery = "SELECT cart_ref FROM orders WHERE cart_ref = " . $oDB->Safe($sOrderRef);
// logg($sSQLQuery, SQLLEVEL);
// $oDB->ExecuteSQL($sSQLQuery);
        /*
          if ($oDB->m_iCount == 0)
          {
          $sSQLQuery = "SELECT order_ref FROM temp_orders_store WHERE cart_ref = " . make_safe($sOrderRef);
          logg($sSQLQuery, SQLLEVEL);
          $oDB->ExecuteSQL($sSQLQuery);
          }
         */
// }
// while ($oDB->m_iCount != 0);
        return $sOrderRef;
    }

    public function addFavourite() {
        $this->viewBuilder()->setLayout('ajax');
        $favoriteproduct = $this->FavoriteProducts->newEntity();

        if ($this->request->is(['post'])) {
            $data = $this->request->getData();
            $favoriteproduct->product_id = $data['product_id'];
            $favoriteproduct->user_id = $this->Auth->user('id');

            if ($this->FavoriteProducts->save($favoriteproduct)) {
                echo json_encode(['status' => 'success', 'msg' => 'update Successfully!!']);
            } else {
                echo json_encode(['status' => 'error', 'msg' => 'Error Occured!!']);
            }

            exit;
        }
    }

    public function deleteFavourite() {
        $this->viewBuilder()->setLayout('ajax');

        if ($this->request->is(['post'])) {
            $data = $this->request->getData();
            $favoriteproduct = $this->FavoriteProducts->find()->where(['product_id' => $data['product_id'], 'user_id' => $this->Auth->user('id')])->first();
            if ($this->FavoriteProducts->delete($favoriteproduct)) {
                echo json_encode(['status' => 'success', 'msg' => 'delete Successfully!!']);
            } else {
                echo json_encode(['status' => 'error', 'msg' => 'Error Occured!!']);
            }

            exit;
        }
    }

    public function ajaxDelete() {
        $this->viewBuilder()->setLayout('ajax');

        if ($this->request->is(['post'])) {
            $data = $this->request->getData();
            $order = $this->OrdersProducts->find()->where(['OrdersProducts.id' => $data['id'], 'user_id' => $this->Auth->user('id')])->first();

            if ($this->OrdersProducts->delete($order)) {
                echo json_encode(['status' => 'success', 'msg' => 'Delete Successfully!!']);
            } else {
                echo json_encode(['status' => 'error', 'msg' => 'Error Occured!!']);
            }

            exit;
        }
    }

    public function addreview() {
        $review = $this->ProductReviews->newEntity();
        $user_id = '';
//        $date = date('d M Y');
        if ($this->request->is(['patch', 'post', 'put'])) {
            $data = $this->request->getData();
            $data['date'] = date('d M Y');
            if ($this->Auth->user('id')){
                $user_id = $this->Auth->user('id');
                $data['user_id'] = $user_id;
            }
            $this->ProductReviews->patchEntity($review, $data);
            if ($this->ProductReviews->save($review)) {
//                return $this->redirect(['action' => 'editVariations', $data->product_id]);
                 echo json_encode(['status' => 'success', 'msg' => 'Added Successfully!!']);
            } else {
                echo json_encode(['status' => 'error', 'msg' => 'Error Occured!!']);
            }
            exit;
        }
    }
    public function chooseDelivery() {
        $review = $this->ProductReviews->newEntity();
        if ($this->request->is(['post'])) {
            $data = $this->request->getData();
//            $orderdetail = $this->Orders->find()->where(['Orders.order_id !=' => $data['order_id'], 'user_id' => $this->Auth->user('id')]);
            
            $update = $this->Orders->query()->update()->set(['choose_delivery' => 'N'])->where(['Orders.order_id !=' => $data['order_id'],'user_id' => $this->Auth->user('id')])->execute();
            
            $defaultdelivery = $this->Orders->query()->update()->set(['choose_delivery' => 'Y'])->where(['order_id' => $data['order_id'],'user_id' => $this->Auth->user('id')])->execute();
            exit;
        }
    }
    public function ajaxAddaddress() {
       $orderentity = $this->Orders->newEntity();
        if ($this->request->is(['post'])) {
            $data = $this->request->getData();

            $this->Orders->patchEntity($orderentity, $data);
             $orderentity->user_id = $this->Auth->user('id');
            if ($this->Orders->save($orderentity)) {
                echo json_encode(['status' => 'success', 'msg' => 'Added Successfully!!']);
            } else {

                echo json_encode(['status' => 'error', 'msg' => 'Error Occured!!']);
            }
            exit;
        }
    }
     public function ajaxView() {

        header('Content-Type:application/json');
        $this->viewbuilder()->setLayout('ajax');

        $data = $this->request->getData();
        $getorder = $this->Orders->find()->where(['order_id' => $data['id']])->first();
        if ($getorder) {
            echo json_encode(['status' => 'sucess', 'msg' => 'sucess Occured!!', 'data' => $getorder]);
        } else {
            echo json_encode(['status' => 'error', 'msg' => 'error Occured!!']);
        } exit;
    }
     public function addressEdit() {

        header('Content-Type:application/json');
        $this->viewbuilder()->setLayout('ajax');

        $data = $this->request->getData();
        $orderaddress = $this->Orders->find()->where(['order_id' => $data['order_id']])->first();
        if ($this->request->is(['post', 'patch', 'put'])) {

            $this->Orders->patchEntity($orderaddress, $data);
//            pj($orderaddress->getErrors());exit;
            if ($this->Orders->save($orderaddress)) {
                echo json_encode(['status' => 'success', 'msg' => 'Update Successfully!!']);
            } else {
                echo json_encode(['status' => 'error', 'msg' => 'Error Occured!!']);
            }exit;
        }
    }


}
