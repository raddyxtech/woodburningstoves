<?php

namespace App\Controller;

use App\Controller\AppController;
use Cake\Validation\Validation;

class UsersController extends AppController {

    function initialize() {
        parent::initialize();

        // Load Models
        $this->loadModel('Users');

        // Load Components
        $this->loadComponent('Custom');

        $publicActions = ['login', 'logout'];
        $this->Auth->allow($publicActions);

        // Set Layout
        $this->viewBuilder()->setLayout('');
    }

    public function login() {
        $this->viewBuilder()->setLayout('');

        // Redirect if Alreday Login
        if ($this->userId) {
            $this->_loginRedirect();
        }

        if ($this->request->is('post')) {

            $user = $this->Auth->identify();

            if (empty($user)) {
                $this->Flash->error(__('Invalid Email or Password, try again.'));
            } else if ($user['is_active'] == '0') {
                $this->Flash->error(__('You Account not activated yet. Please contact admin.'));
            } else {

                // Store User data into Session
                $this->Auth->setUser($user);

                // Login and Redirect to appropriate user dashboard
                $this->_loginRedirect();
            }
            return $this->redirect($this->referer());
        }

        $pageTitle = SITE_NAME . " : Login";
        $this->set(compact('pageTitle'));
    }

    // Redirect to default landing page based on user type.
    private function _loginRedirect() {
        if ($this->Auth->user('user_type') == 1) {
            return $this->redirect(HTTP_ROOT . 'admin'); // Admin Dashboard
        } else {
            return $this->redirect(HTTP_ROOT);
        }
    }

    public function logout() {
        if (isset($_COOKIE['rememberme'])) {
            setcookie("rememberme", $_COOKIE['rememberme'], time() - (86400 * 30), "/"); //Removing cookie
        }
        if (!isset($_SESSION)) {
            session_start();
        }
        session_destroy();
        $this->Auth->logout();
        $this->redirect(HTTP_ROOT . 'login');
    }

}
