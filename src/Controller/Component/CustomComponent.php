<?php

namespace App\Controller\Component;

use Cake\Controller\Component;

class CustomComponent extends Component {

    function __construct($prompt = null) {
        
    }

    function generateUniqNumber($id = NULL) {
        $uniq = uniqid(rand());
        if ($id) {
            return md5($uniq . time() . $id);
        } else {
            return md5($uniq . time());
        }
    }

    function getRealIpAddress() {
        if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        } else if (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else {
            $ip = $_SERVER['REMOTE_ADDR'];
        }
        return $ip;
    }

    function get_ip_address() {
        if (isSet($_SERVER)) {
            if (isSet($_SERVER["HTTP_X_FORWARDED_FOR"])) {
                $realip = $_SERVER["HTTP_X_FORWARDED_FOR"];
            } elseif (isSet($_SERVER["HTTP_CLIENT_IP"])) {
                $realip = $_SERVER["HTTP_CLIENT_IP"];
            } else {
                $realip = $_SERVER["REMOTE_ADDR"];
            }
        } else {
            if (getenv('HTTP_X_FORWARDED_FOR')) {
                $realip = getenv('HTTP_X_FORWARDED_FOR');
            } elseif (getenv('HTTP_CLIENT_IP')) {
                $realip = getenv('HTTP_CLIENT_IP');
            } else {
                $realip = getenv('REMOTE_ADDR');
            }
        }

        return $realip;
    }

    public function encrypt($id) {
        return urlencode(base64_encode($id));
    }

    public function decrypt($id) {
        return base64_decode(urldecode($id));
    }

    public function getExtension($filename = NUll) {
        if ($filename) {
            return strtolower(pathinfo($filename, PATHINFO_EXTENSION));
        }
        return false;
    }

    public function uploadImage($fileArr, $path = NULL) {
        try {
            $filename = strtolower($fileArr['name']);
            $tmpName = $fileArr['tmp_name'];
            $ext = $this->getExtension($filename);
            $allowedExt = ['jpg', 'jpeg', 'png'];

            if (!$path) {
                $path = WWW_ROOT . PROFILE_PIC;
            }
            if (!in_array($ext, $allowedExt)) {
                return FALSE;
            } else if (filesize($tmpName) > MAX_FILE_SIZE) {
                return FALSE;
            } else if (!is_dir($path)) {
                mkdir($path);
            }

            if (!file_exists($path . $filename) && strlen($filename) < 150) {
                $newfilename = $filename;
            } else {
                $filenamesubstr = substr(pathinfo($filename, PATHINFO_FILENAME), 0, 100);
                $newfilename = $filenamesubstr . "-" . time() . rand(1000, 9999) . "." . $ext;
            }
            $targetPath = $path . $newfilename;
            if (move_uploaded_file($tmpName, $targetPath)) {
                return $newfilename;
            }
        } catch (\Exception $ex) {
            
        }
        return FALSE;
    }

    public function getNewFileName($path, $fileName = 'file') {
        $newfilename = NULL;
        $ext = $this->getExtension($fileName);
        if ($fileName && !file_exists($path . $fileName) && strlen($fileName) < 100) {
            $newfilename = $fileName;
        } else {
            $filenamesubstr = substr(pathinfo($fileName, PATHINFO_FILENAME), 0, 80);
            $newfilename = $filenamesubstr . "-" . time() . rand(1000, 9999) . "." . $ext;
        }
        return $newfilename;
    }

    function makeSeoUrl($url) {
        if ($url) {
            $url = trim($url);
            $value = preg_replace("![^a-z0-9]+!i", "-", $url);
            $value = trim($value, "-");
            return strtolower($value);
        }
    }

    function filterData($data) {
        /* this function is meant for filtering whole data received from the screen */
        $filteredData = array_map(function($v) {
            if (is_array($v)) {
                return $this->filterData($v);
            } else if (is_string($v)) {
                return trim($v);
            } else {
                return $v;
            }
        }, $data);

        return $filteredData;
    }

    public function uploadImage2($sourceFile, $targetFile, $allowedExt = []) {
        if (!$allowedExt) {
            $allowedExt = ['jpg', 'png', 'gif'];
        }
        $ext = $this->getExtension($targetFile);
        $path = strtolower(pathinfo($targetFile, PATHINFO_DIRNAME));
        if (file_exists($targetFile) || !file_exists($sourceFile)) {
            return FALSE;
        } else if (!in_array($ext, $allowedExt)) {
            return FALSE;
        } else if (filesize($sourceFile) > MAX_FILE_SIZE) {
            return FALSE;
        } else if (!is_dir($path)) {
            mkdir($path);
        }

        if (move_uploaded_file($sourceFile, $targetFile)) {
            return TRUE;
        }
        return FALSE;
    }

    public function priceFormat($inputNumber) {
        $decimal_format = CURRENCY_SYMBOL . ' ' . number_format($inputNumber, 2);
        return $decimal_format;
    }

    public function croppieImageUpload($picArray, $base64Data, $path = NULL) {

        try {
            list(, $imgdata) = explode(';', $base64Data);
            list(, $newimgdata) = explode(',', $imgdata);
            $decodedimgdata = base64_decode($newimgdata);
            $filename = strtolower($picArray['name']);
            $tmpName = $picArray['tmp_name'];
            $ext = $this->getExtension($filename);
            $allowedExt = ['jpg', 'jpeg', 'png'];

            if (!$path) {
                $path = WWW_ROOT . PROFILE_PIC;
            }
            if (!in_array($ext, $allowedExt)) {
                return FALSE;
            } else if (filesize($tmpName) > MAX_FILE_SIZE) {
                return FALSE;
            } else if (!is_dir($path)) {
                mkdir($path);
            }

            if (!file_exists($path . $filename) && strlen($filename) < 150) {
                $newfilename = $filename;
            } else {
                $filenamesubstr = substr(pathinfo($filename, PATHINFO_FILENAME), 0, 100);
                $newfilename = $filenamesubstr . "-" . time() . rand(1000, 9999) . "." . $ext;
            }
            $targetPath = $path . $newfilename;

            if (file_put_contents($targetPath, $decodedimgdata)) {
                return $newfilename;
            }
        } catch (\Exception $ex) {
            
        }
        return FALSE;
    }

    public function croppieImageUpload2($rawImage, $path = NULL) {
        $result = (object) ['status' => false, 'file' => NULL, 'message' => 'Invalid File!!'];
        $photoAttribute = ['max_size' => MAX_FILE_SIZE, 'height' => 400, 'width' => 400, 'ext' => ['jpg', 'jpeg', 'png']];
        try {
            if (!$path) {
                $path = WWW_ROOT . BRAND_PIC; // for default location to save profileimage
            } else if (!is_dir($path)) {
                mkdir($path);
            }
            if (!$rawImage) {
                $result->message = 'File missing!!';
                return $result;
            }
            list(, $imgdata) = explode(';', $rawImage);
            list(, $base64Data) = explode(',', $imgdata);
            $base64Decoded = base64_decode($base64Data);
            if (!$base64Decoded) {
                $result->message = 'Invalid image format!!';
                return $result;
            }
            $fileSize = strlen($base64Decoded);
            $imageSize = getimagesizefromstring($base64Decoded);
            $imageHeight = $imageSize[0];
            $imageWidth = $imageSize[1];

            $extension = basename($imageSize['mime']);
            if ($fileSize > $photoAttribute['max_size']) {
                $result->message = 'Image size should not exceed' . ( $photoAttribute['max_size'] / 1024 ) . 'KB !!';
            } else if ($imageHeight > $photoAttribute['height'] || $imageWidth > $photoAttribute['width']) {
                $result->message = 'Image dimension should be' . $photoAttribute['height'] . 'H' . ' X ' . $photoAttribute['width'] . 'W';
            } else if (!in_array($extension, $photoAttribute['ext'])) {
                $result->message = 'Allowed image extension are' . implode(',', $photoAttribute['ext']);
            } else {
                $newfilename = $this->getNewFileName($path, 'photo' . '.' . $extension);
                $targetPath = $path . $newfilename;
                if (file_put_contents($targetPath, $base64Decoded)) {
                    $result = (object) ['status' => true, 'file' => $newfilename, 'message' => 'Upload success.'];
                }
            }
        } catch (\Exception $ex) {
            $result->message = $ex->getMessage();
        }
        return $result;
    }

    public function getPropertiesFromZip($zipcode) {
        $response = ['status' => 400, 'message' => 'Not found!!', 'data' => []];
        $zipcode = str_replace(' ', '', $zipcode);
        if ($zipcode) {
            $apiUrl = "https://api.getaddress.io/find/" . $zipcode . "?api-key=" . GET_ADDRESS_API_KEY . "&expand=true";
            $session = curl_init($apiUrl);
            curl_setopt($session, CURLOPT_RETURNTRANSFER, true);
            $headers = array('Content-Type: application/json');
            curl_setopt($session, CURLOPT_HTTPHEADER, $headers);
            // Execute cURL on the session handle
            $curlResult = curl_exec($session);
            $response['status'] = curl_getinfo($session, CURLINFO_HTTP_CODE);
            if (curl_error($session)) {
                $error = curl_error($session);
                $response['message'] = $error['message'];
            } else {
                // Convert JSON into an object
                $response['data'] = json_decode($curlResult, true);
            }
            curl_close($session);
        }
        return (object) $response;
    }

    public function uploadCertificate($file, $uploadType) {

        $path = WWW_ROOT . PROPERTY_DOCUMENTS;

        if ($uploadType != 'online') {

            $filename = strtolower($file['name']);
            $tmpName = $file['tmp_name'];
            $ext = $this->getExtension($filename);

            if ($ext != 'pdf') {
                return ['message' => 'Please, choose a pdf file'];
            }

            $newfilename = $this->getNewFileName($path, $filename);
            $targetPath = $path . $newfilename;
            if (move_uploaded_file($tmpName, $targetPath)) {
                return $newfilename;
            } else {
                return ['message' => 'File Uploading Failed'];
            }
        } else {

            $ext = $this->getExtension($file);

            if ($ext != 'pdf') {
                return ['message' => 'Please, choose a pdf file'];
            }

            $newfilename = "online_" . mt_rand(1, 10000) . "_document_" . mt_rand(1, 10000) . ".pdf";
            $fileUploaded = file_put_contents($path . $newfilename, fopen($file, 'r'));

            if ($fileUploaded) {
                return $newfilename;
            } else {
                return ['message' => 'File Uploading Failed'];
            }
        }
    }

    public function formatDate($stringDate) {

        $stringDate = stripslashes($stringDate);
        $dateArray = preg_replace("/(\d+)\/(\d+)\/(\d+)/", "$3-$1-$2", $stringDate);

        return $dateArray;
    }

    public function getFileExtension($filename) {
        return pathinfo($filename, PATHINFO_EXTENSION);
    }

    function uploadFile($filename, $source, $targetPath, $allowedExt = ['jpg', 'jpeg', 'png']) {
        $ext = $this->getFileExtension($filename);
        if (!in_array($ext, $allowedExt)) {
            return NULL;
        } else {
            $filename = pathinfo($filename, PATHINFO_FILENAME);
            $newFile = $filename . '_' . time() . '.' . $ext;

            if (move_uploaded_file($source, WWW_ROOT . $targetPath . $newFile)) {
                return $newFile;
            }
        }
        return NULL;
    }

    function uploadPdf($filename, $source, $targetPath, $allowedExt = ['pdf']) {
        $ext = $this->getFileExtension($filename);
        if (!in_array($ext, $allowedExt)) {
            return NULL;
        } else {
            $filename = pathinfo($filename, PATHINFO_FILENAME);
            $newFile = $filename . '_' . time() . '.' . $ext;

            if (move_uploaded_file($source, WWW_ROOT . $targetPath . $newFile)) {
                return $newFile;
            }
        }
        return NULL;
    }

    function generateAuthCode($sDateKey, $iOrderId) {
        srand((double) microtime() * 1000000);
        $sKey = md5(uniqid(rand()));
//        $oHash = Crypt_HMAC($sKey);

        return $sKey->hash($sDateKey, $iOrderId);
    }

    function hash($data) {
        $func = $this->_func;
        return $func($this->_opad . pack($this->_pack, $func($this->_ipad . $data)));
    }

}
