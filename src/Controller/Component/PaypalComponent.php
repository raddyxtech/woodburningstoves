<?php

namespace App\Controller\Component;

use Cake\Controller\Component;
use Cake\Core\Configure;
class PaypalComponent extends Component {

    function __construct($prompt = null) {
        
    }

    public function validateIPN() {
        try {

            // STEP 1: read POST data
            // Reading POSTed data directly from $_POST causes serialization issues with array data in the POST.
            // Instead, read raw POST data from the input stream.

            $raw_post_data = file_get_contents('php://input');
            $raw_post_array = explode('&', $raw_post_data);

            $myPost = array();
            foreach ($raw_post_array as $keyval) {
                $keyval = explode('=', $keyval);
                if (count($keyval) == 2)
                    $myPost[$keyval[0]] = urldecode($keyval[1]);
            }

            // read the IPN message sent from PayPal and prepend 'cmd=_notify-validate'
            $req = 'cmd=_notify-validate';

            if (function_exists('get_magic_quotes_gpc')) {
                $get_magic_quotes_exists = true;
            }

            foreach ($myPost as $key => $value) {
                if ($get_magic_quotes_exists == true && get_magic_quotes_gpc() == 1) {
                    $value = urlencode(stripslashes($value));
                } else {
                    $value = urlencode($value);
                }
                $req .= "&$key=$value";
            }

            // Step 2: POST IPN data back to PayPal to validate
            $ch = curl_init(Configure::read('Paypal.PAYPAL_PAYMENT_URL'));
            curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $req);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
            curl_setopt($ch, CURLOPT_FORBID_REUSE, 1);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Connection: Close'));

            if (!($res = curl_exec($ch))) {
                curl_close($ch);
                return FALSE;
            }
            curl_close($ch);          

            // inspect IPN validation result and act accordingly
            if (strcmp(trim($res), "VERIFIED") == 0) {
                return TRUE;
            }
        } catch (\Exception $ex) {
            
        }
        return FALSE;
    }

}
