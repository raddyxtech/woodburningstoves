<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta content="<?= SITE_NAME; ?>" name="description" />
        <meta content="<?= SITE_NAME; ?>" name="author" />
        <meta content="<?= SITE_NAME; ?>" name="title" />
        <base href="<?= HTTP_ROOT; ?>"/>
        <title><?= SITE_NAME; ?></title>
        <link rel="shortcut icon" href="favicon.ico">
        <link rel="icon" href="<?= HTTP_ROOT; ?>images/favicon.ico" type="image/ico">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i" rel="stylesheet">
        <?php $linkVersion = '1.6.1'; ?>
        <!--<= $this->Html->css('moverez.css?version=' . $linkVersion); ?>-->		
        <?= $this->Html->css('developer.css?version=' . $linkVersion); ?>		
        <!--< $this->Html->css(['owl.carousel', 'owl.theme.default.min']); ?>-->		
        <?= $this->Html->css('https://use.fontawesome.com/releases/v5.5.0/css/all.css', ['integrity' => 'sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU', 'crossorigin' => 'anonymous']); ?>		
        <?= $this->Html->css('/plugins/jquery-modal/jquery.modal.min'); ?>
        <?= $this->Html->css('/css/newstyle'); ?>
        <?= $this->Html->css('/css/jquery.jqzoom.css'); ?>
        <script src="<?= HTTP_ROOT ?>validation/dist/jquery.validate.min.js" language="javascript"></script>
        <!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>-->
        <?php /* <?= $this->element('common_constants'); ?> */ ?>
        <?= $this->Html->script('common.js?version=' . $linkVersion); ?>
        <link href="css/newstyle.css" media="screen" rel="stylesheet">
        <link rel="stylesheet" href="css/jquery.jqzoom.css" type="text/css">
        <script src="js/jquery-1.7.2.min.js"></script> 
        <script src="js/html5.js"></script>
        <script src="js/main.js"></script>
        <script src="js/jquery.carouFredSel-6.2.0-packed.js"></script>
        <script src="js/jquery.touchSwipe.min.js"></script>
        <script src="js/checkbox.js"></script>
        <script src="js/radio.js"></script>
        <script src="js/selectBox.js"></script>
        <script src="js/jquery.jqzoom-core.js"></script>


        <?= $this->Html->script('/plugins/jquery-modal/jquery.modal.min'); ?>				
        <!--//<= $this->Html->script('owl.carousel'); ?>-->	
        <?= $this->Html->script('https://use.fontawesome.com/releases/v5.5.0/js/all.js', ['integrity' => 'sha384-GqVMZRt5Gn7tB9D9q7ONtcp4gtHIUEW/yG7h98J7IpE3kpi+srfFyyB/04OV6pG0', 'crossorigin' => 'anonymous']); ?>	

        <script type="text/javascript">
            var siteUrl = '<?= HTTP_ROOT; ?>';
            $(document).ready(function () {

                $(".mob-menu").click(function () {
                    $(".nav").slideToggle();
                });

                $(".nav-close").click(function () {
                    $(".nav").fadeOut();
                });

                $(window).resize(function () {
                    var width = $(window).width();
                    if (width < 991) {
                        $('html').click(function (navv) {
                            if ($(navv.target).closest('.mob-menu, .nav-close,.nav').length === 0) {
                                $(".nav").fadeOut();
                            }
                        });
                    } else {
                        $('html').unbind('click');
                        $(".nav").fadeIn();
                    }
                });



                $("li.sub-menu").click(function () {
                    if (!$($(this).find(".dropdown-menu")).is(':visible')) {
                        $(".dropdown-menu").fadeOut();
                        $(this).find(".dropdown-menu").fadeIn();
                    } else {
                        $(this).find(".dropdown-menu").fadeOut();
                    }

                });

                $('html').click(function (subNav) {
                    if ($(subNav.target).closest('.sub-menu, .dropdown-menu').length === 0) {
                        $(".dropdown-menu").fadeOut();
                    }
                });
            });
            $.modal.defaults.spinnerHtml = '<img src="img/interwind3.gif"/>';

        </script>
    </head>

    <body>
        <div id="wrapper">
            <?= $this->element('default/header'); ?>  
            <?= $this->Flash->render() ?>
            <?= $this->fetch('content'); ?>
            <?= $this->element('default/footer'); ?>
        </div>
    </body>
</html>