<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Moverez</title>
        <link rel="icon" href="images/favicon.ico" type="image/ico">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i" rel="stylesheet">
        <?= $this->Html->css('moverez'); ?>	
        <?= $this->Html->css('developer'); ?>	
        <?= $this->Html->css(['owl.carousel', 'owl.theme.default.min']); ?>		
        <?= $this->Html->css('https://use.fontawesome.com/releases/v5.5.0/css/all.css', ['integrity' => 'sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU', 'crossorigin' => 'anonymous']); ?>		

        <?= $this->Html->script('jquery-1.10.2.min'); ?>		
        <?= $this->Html->script('owl.carousel.min'); ?>	
        <?= $this->Html->script('https://use.fontawesome.com/releases/v5.5.0/js/all.js', ['integrity' => 'sha384-GqVMZRt5Gn7tB9D9q7ONtcp4gtHIUEW/yG7h98J7IpE3kpi+srfFyyB/04OV6pG0', 'crossorigin' => 'anonymous']); ?>	
    </head>
    <body>	
        <?= $this->Flash->render() ?>
        <div class="banner login-banner inner-banner">
            <img src="images/login-bg.jpg" alt="Moverez banner" />
            <div class="login-banner-content">
                <h2>FIND YOUR DREAM HOME</h2>
                 <p>Looking for a house? <br/>Register now to jump to the front of the que.</p>
            </div>
             <?= $this->fetch('content'); ?>
        </div>
       
        <?php echo $this->element('header'); ?>

        <section class="row">
            <br/>		
        </section>
        <?php echo $this->element('footer'); ?>
        <script type="text/javascript">
            var siteUrl = '<?= HTTP_ROOT; ?>';
            $(document).ready(function () {

                $(".mob-menu").click(function () {
                    $(".nav").slideToggle();
                });

                $(".nav-close").click(function () {
                    $(".nav").fadeOut();
                });

                $(window).resize(function () {
                    var width = $(window).width();
                    if (width < 991) {
                        $('html').click(function (navv) {
                            if ($(navv.target).closest('.mob-menu, .nav-close,.nav').length === 0) {
                                $(".nav").fadeOut();
                            }
                        });
                    } else {
                        $('html').unbind('click');
                        $(".nav").fadeIn();
                    }
                });



                $("li.sub-menu").click(function () {
                    if (!$($(this).find(".dropdown-menu")).is(':visible')) {
                        $(".dropdown-menu").fadeOut();
                        $(this).find(".dropdown-menu").fadeIn();
                    }
                    else {
                        //$(".dropdown-menu").fadeOut();
                        $(this).find(".dropdown-menu").fadeOut();
                    }

                });

                $('html').click(function (subNav) {
                    if ($(subNav.target).closest('.sub-menu, .dropdown-menu').length === 0) {
                        $(".dropdown-menu").fadeOut();
                    }
                });
            });
        </script>
    </body>
</html>