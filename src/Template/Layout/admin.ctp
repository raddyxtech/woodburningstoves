<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <base href="<?= HTTP_ROOT ?>" />
        <meta content="Move Rez" name="description" />
        <meta content="Move Rez" name="author" />
        <meta content="Move Rez" name="title" />
        <title><?= SITE_NAME; ?></title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />

        <!-- App favicon -->
        <link rel="icon" href="<?= HTTP_ROOT; ?>images/favicon.ico" type="image/ico">

        <!-- C3 charts css -->
        <?= $this->Html->css('../plugins/c3/c3.min'); ?>        
        <!-- Plugins css -->
        <?= $this->Html->css('../plugins/bootstrap-datepicker/css/bootstrap-datepicker.min'); ?>        
        <!-- Datatable css -->
        <?= $this->Html->css('../plugins/datatables/dataTables.bootstrap4.min'); ?>        
        <!-- Responsive Datatable css -->
        <?= $this->Html->css('../plugins/datatables/responsive.bootstrap4.min'); ?>        
        <!-- App css -->
        <?= $this->Html->css(['../assets/css/bootstrap.min', '../assets/css/icons', '../assets/css/metismenu.min', '../assets/css/style']); ?>
        <?= $this->Html->css('admin/developer-style'); ?>
        <?= $this->Html->css('web-css/developer'); ?>
        <?= $this->Html->css('admin/hint'); ?>
        <?= $this->Html->css('/plugins/jquery-toastr/jquery.toast.min'); ?>

        <script>
            var siteUrl = '<?= HTTP_ROOT ?>';
        </script>

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
        <script> window.jQuery || document.write('<script src="<?= HTTP_ROOT; ?>assets/js/jquery.min.js"><\/script>');</script>   
        <script src="<?= HTTP_ROOT ?>validation/dist/jquery.validate.min.js" language="javascript"></script>
    </head>

    <!--<body class="hold-transition skin-blue sidebar-mini">-->
    <body>
        <div id="wrapper">
            <!-- Top Bar Start -->
            <?= $this->element('admin/topbar') ?>
            <!-- Top Bar End -->

            <!-- ========== Left Sidebar Start ========== -->
            <?= $this->element('admin/sidebar') ?>
            <!-- Left Sidebar End -->

            <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container-fluid">
                        <?= $this->Flash->render() ?>
                      
                        <?= $this->fetch('content') ?>
                    </div>
                </div>
                <!-- content -->
                <?= $this->element('admin/footer') ?>
            </div>
        </div>

        <?= $this->Html->script("/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min"); ?>
        <script>
            $(function () {
                $('.datepicker').datepicker({
                    autoclose: true,
                    todayHighlight: true,
                    format: "yyyy-mm-dd"
                });
            });
        </script>
        <!-- jQuery  -->
        <!-- Date Picker -->

        <?= $this->Html->script('../assets/js/modernizr.min'); ?>
        <script src="assets/js/popper.min.js"></script><!-- Popper for Bootstrap -->
        <script src="assets/js/bootstrap.min.js"></script>
        <script src="assets/js/metisMenu.min.js"></script>
        <script src="assets/js/waves.js"></script>
        <script src="assets/js/jquery.slimscroll.js"></script>
        <!-- Counter js  -->
        <script src="plugins/waypoints/jquery.waypoints.min.js"></script>
        <script src="plugins/counterup/jquery.counterup.min.js"></script>
        <!--C3 Chart-->
        <script type="text/javascript" src="plugins/d3/d3.min.js"></script>
        <script type="text/javascript" src="plugins/c3/c3.min.js"></script>
        <!--Echart Chart-->
        <script src="plugins/echart/echarts-all.js"></script>
        <!-- Dashboard init -->
        <script src="assets/pages/jquery.dashboard.js"></script>
        <!-- App js -->
        <script src="assets/js/jquery.core.js"></script>
        <script src="assets/js/jquery.app.js"></script>

        <script type="text/javascript">
            $(document).ready(function () {
                if($('#datatable').length) {
                    $('#datatable').DataTable();
                    // Buttons examples
                    var table = $('#datatable-buttons').DataTable({
                        lengthChange: false,
                        buttons: ['copy', 'excel', 'pdf', 'colvis']
                    });
                    table.buttons().container().appendTo('#datatable-buttons_wrapper .col-md-6:eq(0)');
                }
            });
        </script>    
        <?= $this->Html->script('/plugins/jquery-toastr/jquery.toast.min'); ?>
    </body>
</html>

