<!--<style>
    .btn-purple { height: 38px; }
</style>-->
<!-- Page Title Section Starts -->
<div class="row">
    <div class="col-12">
        <div class="page-title-box">
            <h4 class="page-title float-left"><?= $uniqueId ? 'Edit' : 'Add' ?> Administrator</h4>
            <ol class="breadcrumb float-right">
                <li class="breadcrumb-item"><a href="<?= h(HTTP_ROOT) ?>admin/dashboard"><i class="fa fa-dashboard"></i>&nbsp;Home</a></li>
                <li class="breadcrumb-item active"><a href="<?= $this->Url->build(['prefix' => 'admin', 'controller' => 'Users', 'action' => 'listAdministrators']); ?>">Manage Manager</a></li>
                <li class="breadcrumb-item active"><a href="<?= $this->Url->build(['prefix' => 'admin', 'controller' => 'Users', 'action' => 'addAdministrators']); ?>">Add Administrator</a></li>
            </ol>
            <div class="clearfix"></div>
        </div>
    </div>
</div>
<!-- Page Title Section Ends -->

<!-- Main content -->    
<div class="row">
    <div class="col-md-12">
        <div class="card-box">

            <h4 class="m-t-0 header-title"><?= $uniqueId ? 'Edit' : 'Add' ?> Administrator</h4>
            <p class="text-muted m-b-30 font-13">
                All (<span style="color:#FF0000">*</span>) fields are mandatory
            </p>

            <?= $this->Form->create($admin, ['id' => 'wizard-callbacks', 'type' => 'file']) ?>

            <div class="row">
                <div class="col-md-9">
                    <div class="card-box">
                        <h4 class="m-t-0 m-b-30 header-title">Basic Information</h4>

                        <div class="form-group">
                            <label>Username <span style="color:#FF0000">*</span></label>
                            <?= $this->Form->control('username', ['placeholder' => "Enter Userame", 'class' => "form-control", 'div' => false, 'label' => false]); ?>
                        </div>
                        <div class="form-group">
                            <label>Email <span style="color:#FF0000">*</span></label>
                            <?= $this->Form->control('email', ['placeholder' => "Enter Email", 'class' => "form-control", 'disabled' => $uniqueId ? 'true' : 'false' ,'div' => false, 'label' => false]); ?>
                        </div>
                        <div class="form-group">
                            <label>Password <span style="color:#FF0000">*</span></label>
                            <?= $this->Form->control('password', ['placeholder' => "Enter Password", 'class' => "form-control", 'div' => false, 'label' => false]); ?>
                            <?php if(!$uniqueId){ ?><input type="checkbox" id="myFunction">Show Password <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
            <button type="submit" class="btn btn-primary" id="pic-update-btn"><?= $uniqueId ? 'Update':'Submit' ?></button>

            <?= $this->Form->end() ?>

        </div>    
    </div>
</div>

<script>
    $(document).ready(function () {

        $('#wizard-callbacks').validate({
            rules: {
                username: "required",
                email: "required",
                password:"required"
            },
            messages: {
                username: "Name Required",
                email: {
                    required: 'Email Required',
                    email: 'Please Eneter A Valid Email'
                },
                password: "Enter Password"
            }

        });

    });
</script>
<script>
    $(document).ready(function () {

        $('#myFunction').click(function () {
            var x = document.getElementById("password");
            if (x.type === "password") {
                x.type = "text";
            } else {
                x.type = "password";
            }
        });
    });
</script>