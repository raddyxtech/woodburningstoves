<link href="plugins/sweet-alert2/sweetalert2.min.css" rel="stylesheet" type="text/css">
<!-- Page Title Section Starts -->
<div class="row">
    <div class="col-12">
        <div class="page-title-box">
            <h4 class="page-title float-left">List Administrators</h4>

            <ol class="breadcrumb float-right">
                <li class="breadcrumb-item"><a href="<?= h(HTTP_ROOT) ?>admin/dashboard"><i class="fa fa-dashboard"></i>&nbsp;Home</a></li>
                <li class="breadcrumb-item active"><a href="javascript:;">Manage Administrators</a></li>
                <li class="breadcrumb-item active"><a href="<?= $this->Url->build(['prefix' => 'admin', 'controller' => 'Users', 'action' => 'addAdministrators']); ?>">Add Administrators</a></li>
            </ol>

            <div class="clearfix"></div>
        </div>
    </div>
</div>
<!-- Page Title Section Ends -->

<!-- Main content -->    
<div class="row">
    <div class="col-md-12">
        <div class="card-box">
            <table id="datatable" class="table table-bordered table-striped users-list">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th style="text-align: center;">Email</th>
                        <th style="text-align: center;">Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($administrators as $administrator): ?>
                        <tr id="<?= $administrator->id; ?>" class="message_box">  
                            <td><?= $administrator->username; ?></td>
                            <td style="text-align: center;"><?= $administrator->email; ?></td>
                            <td style="text-align: center;">
                                <a class="btn btn-xs btn-warning hint--top" data-hint="Edit" href="<?= $this->Url->build(['prefix'=>'admin','controller'=>'Users','action'=>'addAdministrators',$administrator->unique_id]); ?>" > <i class ="fa fa-edit"></i> </a>
                                <a class="btn btn-xs btn-<?= ($administrator->is_active == 1) ? 'success':'danger'; ?> sw-alert-link hint--top" data-hint="<?= ($administrator->is_active == 1) ? 'Click To Deactivate':'Click To Activate'; ?>" href="<?= $this->Url->build(['prefix'=>'admin','controller'=>'Users','action'=>'changeStatus',$administrator->unique_id]); ?>"> <i class="fa fa-toggle-<?= ($administrator->is_active == 1) ? 'on':'off'; ?>" ></i>  </a>
                                <a class="btn btn-xs bg-danger text-white sw-alert-link hint--left" data-hint="Remove" href="<?= HTTP_ROOT . 'admin/Users/deleteUser/' . $administrator->unique_id; ?>" > <i class="fa fa-trash"></i> </a>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>  
        </div>
    </div>
</div>
<!-- Sweet-Alert  -->
<script src="plugins/sweet-alert2/sweetalert2.min.js"></script>
<!-- Required datatable js -->
<script src="plugins/datatables/jquery.dataTables.min.js"></script>
<script src="plugins/datatables/dataTables.bootstrap4.min.js"></script>
<script>
    
    $(document).ready(function() {
        
        // For Data Tables
        $('#example').DataTable({
            searching: false,
            paging: false,
            info: false
        });
        
       $('.sw-alert-link').on('click', function(e) {
            e.preventDefault();
            var link = $(this).attr('href');
            let dynamicText = 'This will change the status of the Administrator';
            if(link.indexOf('deleteUser') > 0) {
                dynamicText = 'Want to delete this Account!';
            }
            
           swal({
                title: 'Are you sure?',
                text: dynamicText,
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: 'OK',
                cancelButtonText: 'No',
                confirmButtonClass: 'btn btn-success',
                cancelButtonClass: 'btn btn-danger m-l-10',
                buttonsStyling: false
            }).then(function () {
                window.location.href = link; 
            });
           
       });
        
    });
    
</script>