<?php
include("../SITE-CONFIG.inc");
include(DIR_CLASS . "/class_Validation.php");
include(DIR_CLASS . "/class_User.php");
include(DIR_CLASS . "/class_Product.php");
include(DIR_CLASS . "/class_Countries.php");
include(DIR_CLASS . "/class_Order.php");
include(DIR_CLASS . "/class_Date_Time.php");
include(DIR_INCL . "/payment_methods.inc");
include(DIR_INCL . "/orders.inc");
//-----------------------------------------------------------------------------
//	FUNCTIONS
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
// MAIN
//-----------------------------------------------------------------------------
$sMsg = "";
$oCountries = new Countries();
$oDate = new Date_Time();
$oProduct = new Product();
$oAdmin = new User(DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, DB_NAME, "admin", "email", "passwd");
if (!$oAdmin->isLoggedIn()) {
    exit;
}
if (($iOrderId = $_REQUEST['oid']) == "") {
    exit;
}
$oOrder = new TheOrder(true);
if (!$oOrder->manualOrderRetrival($iOrderId)) {
    exit;
}

//-----------------------------------------------------------------------------
// HTML
//-----------------------------------------------------------------------------
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
        <title>Order Details</title>
        <style type="text/css">
            <!--
            body {
                margin-left: 0px;
                margin-top: 0px;
                margin-right: 0px;
                margin-bottom: 0px;
            }
            -->
        </style>
        <base href="<?= HTTP_ROOT ?>"/>
        <link href="admin-print.css" rel="stylesheet" type="text/css"/>
    </head>
    <body onLoad="window.print()">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td width="40%"><h1>ORDER DETAILS </h1></td>
                <td width="60%" align="right">&nbsp;</td>
            </tr>
            <tr>
                <td colspan="2" class="msg-block"><?= $sMsg; ?>
                    &nbsp;</td>
            </tr>
            <tr>
                <td colspan="2" class="gap">&nbsp;</td>
            </tr>
            <tr>
                <td width="15%" class="col-headings">Cart Ref:</td>
                <td width="85%" class="data-text"><?= $oOrder->getCartRef(); ?></td>
            </tr>
            <tr>
                <td colspan="2" class="line-light">&nbsp;</td>
            </tr>
            <tr>
                <td colspan="2">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td width="50%" class="col-headings">Invoice Details</td>
                            <td width="50%" class="col-headings">Delivery Details</td>
                        </tr>
                        <tr class="gap">
                            <td colspan="2">&nbsp;</td>
                        </tr>
                        <tr>
                            <td class="data-text">
                                <?= format_it($oOrder->getInvoiceTitle() . " " . $oOrder->getInvoiceFirstname() . " " . $oOrder->getInvoiceLastname(), '', '<br />'); ?>
                                <?= format_it($oOrder->getInvoiceCompany(), '', '<br />'); ?>
                                <?= format_it($oOrder->getInvoiceAddress1(), '', '<br />'); ?>
                                <?= format_it($oOrder->getInvoiceAddress2(), '', '<br />'); ?>
                                <?= format_it($oOrder->getInvoiceCity(), '', '<br />'); ?>
                                <?= format_it($oOrder->getInvoiceCounty(), '', '<br />'); ?>
                                <?= format_it($oOrder->getInvoicePostcode(), '', '<br />'); ?>
                                <?= format_it($oCountries->getNameFromId($oOrder->getInvoiceCountryId()), '', '<br />'); ?><br/>
                                <?= format_it($oOrder->getInvoiceTel1(), 'Tel 1: ', '<br />'); ?>
                                <?= format_it($oOrder->getInvoiceTel2(), 'Tel 2: ', '<br />'); ?>
                                <?= format_it($oOrder->getInvoiceTel3(), 'Tel 3: ', '<br />'); ?><br/>
                                <?= format_it($oOrder->getInvoiceEmail(), '', '<br />'); ?>
                            </td>
                            <td class="data-text">
                                <?= format_it($oOrder->getDeliveryTitle() . " " . $oOrder->getDeliveryFirstname() . " " . $oOrder->getDeliveryLastname(), '', '<br />'); ?>
                                <?= format_it($oOrder->getDeliveryCompany(), '', '<br />'); ?>
                                <?= format_it($oOrder->getDeliveryAddress1(), '', '<br />'); ?>
                                <?= format_it($oOrder->getDeliveryAddress2(), '', '<br />'); ?>
                                <?= format_it($oOrder->getDeliveryCity(), '', '<br />'); ?>
                                <?= format_it($oOrder->getDeliveryCounty(), '', '<br />'); ?>
                                <?= format_it($oOrder->getDeliveryPostcode(), '', '<br />'); ?>
                                <?= format_it($oCountries->getNameFromId($oOrder->getDeliveryCountryId()), '', '<br />'); ?>
                                <br/>
                                <?= format_it($oOrder->getDeliveryTel1(), 'Tel 1: ', '<br />'); ?>
                                <?= format_it($oOrder->getDeliveryTel2(), 'Tel 2: ', '<br />'); ?>
                                <?= format_it($oOrder->getDeliveryTel3(), 'Tel 3: ', '<br />'); ?><br/>
                                <?= format_it($oOrder->getDeliveryEmail(), '', '<br />'); ?>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="2" class="line-light">&nbsp;</td>
            </tr>
            <tr>
                <td colspan="2">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td width="15%" class="col-headings">Status:</td>
                            <td width="35%" class="data-text"><?= get_order_status_TXT($oOrder->getStatus()); ?></td>
                            <td width="15%" class="col-headings">Order Date:</td>
                            <td width="35%"
                                class="data-text"><?= $oDate->dateMysql2Display($oOrder->getPlacedDate(), true); ?></td>
                        </tr>
                        <tr class="gap-small">
                            <td colspan="4">&nbsp;</td>
                        </tr>
                        <tr>
                            <td class="col-headings">Order Value:</td>
                            <td class="data-text">&pound;<?= number_format($oOrder->getProductsTotalExclVAT() + $oOrder->getDeliveryChargeExclVAT() + $oOrder->getVAT(), 2); ?></td>
                            <td class="col-headings">&nbsp;</td>
                            <td class="data-text">&nbsp;</td>
                        </tr>
                        <tr class="gap-small">
                            <td colspan="4">&nbsp;</td>
                        </tr>
                        <tr>
                            <td class="col-headings">Payment Method:</td>
                            <td class="data-text"><?= get_payment_method_TXT($oOrder->getPaymentMethod()); ?></td>
                            <td class="col-headings">Paid:</td>
                            <td class="data-text"><?= yesno_TXT($oOrder->getPaid()); ?></td>
                        </tr>
                        <tr class="line-light">
                            <td colspan="4">&nbsp;</td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr class="col-headings">
                            <td width="45%">product</td>
                            <td width="17%" align="right">Left to Dispatch</td>
                            <td width="15%" align="right">Ordered Qty</td>
                            <td width="23%" align="right">Total Price</td>
                        </tr>
                        <tr class="gap">
                            <td colspan="4">&nbsp;</td>
                        </tr>
                        <?php
                        foreach ($oOrder->getOrderProducts() as $iVariationId => $aInfo) {
                            $sName = $oOrder->getBrand($iVariationId) . " | " . $oOrder->getName($iVariationId) . " " .
                                    $oOrder->getVariationData($iVariationId);
                            $sQtyVar = "iQty-" . $iVariationId;
                            ?>
                            <tr valign="top" class="data-text">
                                <td><?= $sName; ?></td>
                                <td align="right"><?= $aInfo['qty'] - $aInfo['qty_dispatched']; ?></td>
                                <td align="right"><?= $aInfo['qty']; ?></td>
                                <td align="right">&pound;<?= $oOrder->getItemTotalExclVAT($iVariationId); ?></td>
                            </tr>
                            <tr class="gap-small">
                                <td colspan="4">&nbsp;</td>
                            </tr>
                            <?php
                        }
                        ?>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="2" class="line-light">&nbsp;</td>
            </tr>
            <tr>
                <td colspan="2">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td align="right" class="col-headings">Delivery Charge:</td>
                            <td align="right" class="data-text">&pound;<?= $oOrder->getDeliveryChargeExclVAT(); ?></td>
                        </tr>
                        <tr class="gap-small">
                            <td colspan="2" align="right">&nbsp;</td>
                        </tr>
                        <tr>
                            <td align="right" class="col-headings">Sub Total:</td>
                            <td align="right"
                                class="data-text">&pound;<?= number_format($oOrder->getProductsTotalExclVAT() + $oOrder->getDeliveryChargeExclVAT(), 2); ?></td>
                        </tr>
                        <tr class="gap-small">
                            <td colspan="2" align="right">&nbsp;</td>
                        </tr>
                        <tr>
                            <td align="right" class="col-headings">VAT:</td>
                            <td align="right" class="data-text">&pound;<?= $oOrder->getVAT(); ?></td>
                        </tr>
                        <tr class="gap-small">
                            <td colspan="2" align="right">&nbsp;</td>
                        </tr>
                        <tr>
                            <td width="85%" align="right" class="col-headings">GRAND TOTAL:</td>
                            <td width="15%" align="right"
                                class="data-text">&pound;<?= number_format($oOrder->getProductsTotalExclVAT() + $oOrder->getDeliveryChargeExclVAT() + $oOrder->getVAT(), 2); ?></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="2" class="line-light">&nbsp;</td>
            </tr>
        </table>
    </body>
</html>
