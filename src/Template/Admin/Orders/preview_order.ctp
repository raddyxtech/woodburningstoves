<!--<link href="<?= HTTP_ROOT ?>css/preview.css" rel="stylesheet" type="text/css"/>-->
<!--<script src="https://cdnjs.cloudflare.com/ajax/libs/jQuery.print/1.6.0/jQuery.print.min.js"></script>-->
<style>
    .input-30-numeric {
        font-family: Arial, Helvetica, sans-serif;
        font-size: 12px;
        color: #000000;
        background-color: #FFFFFF;
        height: auto;
        width: 30px;
        border: 1px solid #999999;
        text-align: right;
        padding-right: 5px;
    }
</style>
<!--<div class="printableArea">-->
<div class="row">
    <div class="col-12">
        <div class="page-title-box">
            <h4 class="page-title float-left">Order Details</h4>
            <ol class="breadcrumb float-right">
                <li class="breadcrumb-item"><a href="<?= h(HTTP_ROOT) ?>admin/dashboard"><i class="fa fa-dashboard"></i>&nbsp;Home</a></li>
                <li class="breadcrumb-item"><a href="<?= $this->Url->build(['prefix' => 'admin', 'controller' => 'Orders', 'action' => 'orderDetails']); ?>"><i class="fa fa-backward"></i>&nbsp;Ordera</a></li>
                <li class="breadcrumb-item active"><a href="javascript:;">Order Details</a></li>               
            </ol>
            <div class="clearfix"></div>
        </div>
    </div>
</div> 
<div class="row ">
    <div class="col-md-12">
        <div class="card-box table-responsive valuation-requests-list">
            <table width="100%" border="0" cellspacing="0" cellpadding="0" color="white">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td width="15%" class="col-headings">Cart Ref:</td>
                        <td width="85%" class="data-text"><?= $order->cart_ref; ?></td>
                    </tr>
                    <tr>
                        <td colspan="2" class="line-light">&nbsp;</td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td width="50%" class="col-headings">Invoice Details</td>
                                    <td width="50%" class="col-headings">Delivery Details</td>
                                </tr>
                                <tr class="gap">
                                    <td colspan="2">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td class="data-text">
                                        <?= $order->invoice_title; ?> <?= $order->invoice_firstname; ?> <?= $order->invoice_lastname; ?><br /> 
                                        <?= $order->invoice_address1; ?><br />  <?= @$order->invoice_address2; ?><br /> 
                                        <?= $order->invoice_city; ?><br /> 
                                        <?= $order->invoice_county; ?><br/> 
                                        <?= $order->invoice_countryid; ?><br/> 
                                        <?= $order->invoice_postcode; ?><br/> 
                                        Tel 1:  <?= $order->invoice_tel1; ?><br /> 
                                        <?php if (!empty($order->invoice_tel2)) { ?>
                                            Tel 2:  <?= $order->invoice_tel2; ?><br/>
                                        <?php } ?>
                                        <?= $order->invoice_email; ?><br />  
                                    </td>
                                    <td class="data-text">
                                        <?= $order->delivery_title; ?> <?= $order->delivery_firstname; ?> <?= $order->delivery_lastname; ?> <br /> 
                                        <?= $order->delivery_address1; ?><br />  <?= @$order->delivery_address2; ?><br /> 
                                        <?= $order->delivery_city; ?><br /> 
                                        <?= $order->delivery_county; ?><br/> 
                                        <?= $order->delivery_countryid; ?><br/> 
                                        <?= $order->delivery_postcode; ?><br/> 
                                        Tel 1:  <?= $order->delivery_tel1; ?><br />
                                        <?php if (!empty($order->delivery_tel2)) { ?>
                                            Tel 2:  <?= $order->delivery_tel2; ?><br/>
                                        <?php } ?>                                  <br/>
                                        <?= $order->delivery_email; ?><br />                                                    
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" class="line-light">&nbsp;</td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td width="15%" class="col-headings">Status:</td>
                                    <!--<td width="35%" class="data-text"><span style="color: green;"><?= $order->order_status; ?></span></td>-->
                                    <td width="35%" class="data-text"><span style="color: green;"><?= get_order_status_TXT($order->order_status); ?></span></td>
                                    <td width="15%" class="col-headings">Order Date:</td>
                                    <?php if(!empty($order->date_created)){ ?>
                                    <td width="35%" class="data-text"><?= date_format($order->date_created, 'd M, Y'); ?></td>
                                    <?php } else { ?>
                                    <td width="35%" class="data-text"></td>
                                    <?php } ?>
                                    
                                </tr>
                                <tr class="gap-small">
                                    <td colspan="4">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td class="col-headings">Order Value:</td>
                                    <td class="data-text">&pound;<?= number_format($total+$delivery_charge + $vats, 2, '.', ''); ?></td>
                                    <td class="col-headings">&nbsp;</td>
                                    <td class="data-text">&nbsp;</td>
                                </tr>
                                <tr class="gap-small">
                                    <td colspan="4">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td class="col-headings">Payment Method:</td>
                                    <td class="data-text"><?= get_payment_method_TXT($order->payment_method_id); ?></td>
                                    <td class="col-headings">Paid:</td>
                                     <!--<?//php  if ($order->order_status == ORDSTATUS_APPROVED || $order->order_status == ORDSTATUS_PART_DISPATCHED) { ?>-->
                                    <!--<?//php if($order->paid == 'N'){ ?>-->
                                    <!--<td class="data-text"><?//= yesno_TXT($order->paid); ?></td>-->
                                    <!--<td class = 'btn btn-primary' Onclick= "payOrder('<//?= $order['order_id'] ?>')" class="btn btn-sucess"/>PAID</td>--> 
                                    <!--<?//php } else { ?>-->
                                    <!--<td class="data-text"><?//= yesno_TXT($order->paid); ?></td>-->
                                     <!--<?//php } }?>-->
                                    
                                </tr>
                                <tr class="line-light">
                                    <td colspan="4">&nbsp;</td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
              
                    <!--//<= $this->Form->create($orderproducts, ['id' => 'login-form','method' => 'post']); ?>-->
                    <?= $this->Form->create(NULL, ['id' => 'login-form','method' => 'post']); ?>
                    <input name="oid" type="hidden" value="<?= $order->order_id; ?>"/>
                    <tr>
                        <td colspan="2">
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr class="col-headings">
                                    <td width="43%">product</td>
                                    <td width="15%" align="right">Left to Dispatch</td>
                                    <td width="12%" align="right">Dispatch</td>
                                    <td width="12%" align="right">Ordered Qty</td>
                                    <td width="18%" align="right">Total Price</td>
                                </tr>
                                <tr class="gap">
                                    <td colspan="5">&nbsp;</td>
                                </tr>
                                <tr valign="top" class="data-text">
                                    
                                   
                                        <?php if (!empty($order_products)) { ?>
                                            <td align="left"> <?= $order_products->name . '|' . $order_products->variations . '|' . $order_products->brand; ?><br/></td>  
                                            <td align="right"><?= $order_products->qty - $order_products->qty_dispatched ; ?></td>
                                            <td align="right">
                                                <?php if (($order_products->qty - $order_products->qty_dispatched) > 0) { ?>
                                                    <?= $this->Form->control('qty_dispatched[]', ['type' => 'text','class'=>'input-30-numeric', 'div' => false, 'label' => false, 'required' => true,'value' => $order_products->qty_dispatched]); ?>  
                                       
                                                 <?= $this->Form->control('id[]', ['type' => 'hidden','value' => $order_products->id]); ?>  
                                                </td>   
                                            <?php  } } ?>
                                        </td>
                                    <?php if (!empty($order_products)) { ?>
                                            <td align="right"><?= $order_products->qty; ?></td>
                                            
                                            <td align="right">&pound; <?= number_format($order_products->total, 2, '.', ''); ?></td>
                                     <?php } ?>
                                    </tr>
                                    <tr class="gap-small">
                                        <td colspan="5">&nbsp;</td>
                                    </tr>
                                    
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" class="line-light">&nbsp;</td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <?php if(!empty($delivery_charge) && !empty($sub_total) && !empty($vats)) { ?>
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td align="right" class="col-headings">Delivery Charge:</td>
                                    <td align="right" class="data-text">&pound;<?= $delivery_charge; ?></td>
                                </tr>
                                <tr class="gap-small">
                                    <td colspan="2" align="right">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td align="right" class="col-headings">Sub Total:</td>
                                    <!--<td width="15%" align="right"class="data-text">&pound;<?= $sub_total + $delivery_charge; ?></td>-->
                                    <td width="15%" align="right"class="data-text">&pound;<?= number_format($order_products->total, 2, '.', ''); ?></td>
                                    
                                </tr>
                                <tr class="gap-small">
                                    <td colspan="2" align="right">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td align="right" class="col-headings">VAT:</td>
                                    <td align="right" class="data-text">&pound;<?= $vats; ?></td>
                                </tr>
                                <tr class="gap-small">
                                    <td colspan="2" align="right">&nbsp;</td>
                                </tr>
                                <tr>
                                    
                                    <td width="85%" align="right" class="col-headings">GRAND TOTAL:</td>
                                    <!--<td width="15%" align="right"class="data-text">&pound;<?= $sub_total + $delivery_charge + $vats; ?></td>-->
                              <td width="15%" align="right"class="data-text">&pound;<?= number_format($order_products->total+$delivery_charge + $vats, 2, '.', ''); ?>
                                    </td>
                                </tr>
                            </table>
                            <?php } ?>
                        </td>
                    </tr>
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td colspan="2" class="line-light">&nbsp;</td>
                        </tr>
                        <td colspan="2">
                        <tr>
                            <td><input name="rtn" type="button" class="btn btn-danger" id="rtn" value="Return"onclick="location.href = '<?= $this->Url->build(['prefix' => 'admin', 'controller' => 'Orders', 'action' => 'orderDetails']); ?>';"/>
                            </td>
<!--                                           <td><a href="<?= HTTP_ROOT . 'admin/orders/preview-order/' . $order->order_id; ?>#" class="btn btn-warning" id="rtn"onClick="window.open('admin/orders/print-order/' . <?= $order->order_id; ?>', 'orderdetailsprint', 'width=700, height=700, scrollbars=yes')">Print</a></td>-->
                            <td><a href="javascript:void(0);" onclick="printPage()" class="btn btn-warning">Print</a>  </td>
<?php if ($order->order_status == ORDSTATUS_TEMP || $order->order_status == ORDSTATUS_SUSPECT) { ?>
                                <td align="right">
                                    <p class = 'btn btn-primary' Onclick= "approveOrder('<?= $order['order_id'] ?>')" class="btn btn-sucess"/>Approve</p> 
                                </td>
                                <!--<?//php } else if ($order->order_status == ORDSTATUS_APPROVED || $order->order_status == ORDSTATUS_PART_DISPATCHED) { ?>-->
<!--                                <td align="right">
                                    <//?= $this->Form->submit('Dispatch', ['class' => 'btn btn-primary']); ?>
                                </td>-->
                            <?php }else { ?>
                                
                            <?php } ?>
                                
                        </tr>
                    </table>
                 <?= $this->Form->end(); ?>
            </table>
        </div>
    </div>
</div>
<!--</div>-->
<script>
    function printPage() {
        window.print();
    }
   
    function approveOrder(order_id) {
//    alert("#product-" + id);

        if (confirm("Are you sure want to Approve?")) {
            $.ajax({
                type: 'POST',
                url: siteUrl + 'admin/orders/ajaxApprove',
                data: {'order_id': order_id},
                dataType: 'json',
                success: function (response) {
                    if (response.status == 'success') {
//                        $("#product" + id).remove();
                        window.location.href = "order-details";

                    } else {
                        alert(response.msg);
                    }
                }
            });
        }
    }
    function payOrder(order_id) {
//    alert("#product-" + id);

        if (confirm("Are you sure want to Pay?")) {
            $.ajax({
                type: 'POST',
                url: siteUrl + 'admin/orders/ajaxPay',
                data: {'order_id': order_id},
                dataType: 'json',
                success: function (response) {
                    if (response.status == 'success') {
//                        $("#product" + id).remove();
                        window.location.href = "order-details";

                    } else {
                        alert(response.msg);
                    }
                }
            });
        }
    }
        
</script>
<!--<script>
$(document).ready(function(){
    $("#printButton").click(function(){
        var mode = 'iframe'; //popup
        var close = mode == "popup";
        var options = { mode : mode, popClose : close};
        $("div.printableArea").printArea( options );
    });
});
</script>-->
