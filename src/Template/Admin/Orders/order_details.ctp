<?= $this->Html->css('/plugins/spinkit/spinkit'); ?>
<?php
$this->Paginator->setTemplates([
    'prevDisabled' => '<li class="page-item page-link disabled">{{text}}</li>',
    'nextDisabled' => '<li class="page-item page-link disabled">{{text}}</li>',
    'number' => '<li class="page-item"><a class="page-link" href="{{url}}">{{text}}</a></li>',
    'prevActive' => '<li class="page-item"><a class="page-link" href="{{url}}" aria-label="Previous">{{text}}</a></li>',
    'nextActive' => '<li class="page-item"><a class="page-link" href="{{url}}" aria-label="Next">{{text}}</a></li>',
    'current' => '<li class="page-item active"><a class="page-link" href="{{url}}">{{text}}</a></li>'
]);
?>
<div class="row">
    <div class="col-12">
        <div class="page-title-box">
            <h4 class="page-title float-left">Manage Orders</h4>
            <ol class="breadcrumb float-right">
                <li class="breadcrumb-item"><a href="<?= h(HTTP_ROOT) ?>admin/dashboard"><i class="fa fa-dashboard"></i>&nbsp;Home</a></li>
                <li class="breadcrumb-item active"><a href="javascript:;">Manage Orders</a></li>               
            </ol>
            <div class="clearfix"></div>
        </div>
    </div>
</div> 
<div class="box-header bg-gray-light">
    <?= $this->Form->create(NULL, ['id' => 'filterForm', 'method' => 'POST']); ?>
    <div class="row">                           
        <div class="form-group col-sm-3">
            <label for="invoice_number">Keyword: </label>
            <?= $this->Form->text('fl_keywords', ['class' => 'form-control', "value" => isset($query['fl_keywords']) ? $query['fl_keywords'] : '', 'placeholder' => ' Name/Postcode ']); ?>
        </div>
        <div class="form-group col-sm-2">
            <label for="created">From Date: </label>
            <?= $this->Form->text('fl_date_from', ['class' => 'datepicker form-control', "value" => isset($query['fl_date_from']) ? $query['fl_date_from'] : '', 'placeholder' => 'From Date']); ?> 
        </div>
        <div class="form-group col-sm-2">
            <label for="created">To Date: </label>
            <?= $this->Form->text('fl_date_to', ['class' => 'datepicker form-control', "value" => isset($query['fl_date_to']) ? $query['fl_date_to'] : '', 'placeholder' => 'To Date']); ?> 
        </div>
        <div class="form-group col-sm-2">
            <label for="valid_upto"> Status: </label>
            <?= $this->Form->select('fl_status', [ 'B' => 'Pending', 'L' => 'Failed', 'C' => 'Approved', 'G' => 'Dispatched', 'R' => 'Part Dispatched', 'P' => 'Invalid - possibly spoofed', 'Q' => 'Unknown - possible serious', 'A' => 'Temp Orders'], ['empty' => 'Select Status', 'default' => 'All', "value" => isset($query['fl_status']) ? $query['fl_status'] : '', 'div' => false, 'class' => 'form-control select2']); ?>
        </div> 
        <div class="col-md-3 nopadding">
            <label style="width: 100%;">&nbsp;</label>
            <?= $this->Form->button('Filter', ['class' => 'btn btn-info', 'type' => 'submit']); ?>
            <a class="btn btn-warning" href="<?= HTTP_ROOT . 'admin/Orders/order-details/'; ?>" >Clear</a>
        </div>
        <div class="clearfix"></div>                            
    </div>
    <div class="clearfix"></div>   
    <?= $this->Form->end(); ?>
</div>
<div class="row ">
    <?= $this->element('admin/loader'); ?>
    <div class="col-md-12">
        <div class="card-box table-responsive valuation-requests-list">
            <table class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th>Cart Ref</th>
                        <th>Name</th>
                        <th>Order Date</th>
                        <!--<th>Value</th>-->
                        <th style="text-align:center;">Details</th>
                    </tr>
                </thead>
                <tbody>
                   
                        <?php foreach ($orderLists as $orders): ?>
                            <tr>
                                <?php if (!empty($orders->cart_ref)) { ?>
                                    <td>
                                        <?php if ($orders->order_status == 'R' || $orders->order_status == 'C' || $orders->order_status == 'G') { ?>
                                            <?= green($orders->cart_ref); ?>
                                        <?php } elseif ($orders->order_status == 'L' || $orders->order_status == 'Q') { ?>                                                                          <?= red($orders->cart_ref); ?>
                                        <?php } else { ?>
                                            <?= $orders->cart_ref; ?>
                                        <?php } ?>
                                    </td>
                                    <td><?= $orders['invoice_lastname'] . ", " . $orders['invoice_firstname']; ?></td>
                                    <?php if(!empty($orders->date_created)) { ?>
                                                <td><?= date_format($orders->date_created, 'd M, Y'); ?></td>
                                            <?php } else{ ?>
                                                  <td></td>
                                            <?php } ?>
                                    <!--<td>&pound;<?= number_format($total+$delivery_charge + $vats, 2, '.', ''); ?></td>-->
                                    <td style="text-align:center;">
                                        <a href="<?= $this->Url->build(['prefix' => 'admin', 'controller' => 'orders', 'action' => 'previewOrder/'.$orders->order_id]); ?>"  data-hint="View Detail" style="padding: 0 5px!important;font-weight:bold;" class="btn btn-warning hint--top  hint" ><i class="fa fa-eye"></i></a>
                                    </td>
                                <?php } ?>
                            </tr>
                        <?php endforeach; ?>
                    
                </tbody>
            </table>  
            <div class="paginationRow">
                <div class="paginationLeft">
                    <p><?= $this->Paginator->counter('Showing {{start}}-{{end}} Orders out of {{count}}'); ?></p>
                </div>
                <div class="paginationRight">
                    <?php if ($this->Paginator->numbers()): ?>
                        <ul class="pagination pagination-split">
                            <?= $this->Paginator->prev('<span aria-hidden="true"> << </span><span class="sr-only">Previous</span>', ['escape' => false]) ?>                                    
                            <?= $this->Paginator->numbers() ?>
                            <?= $this->Paginator->next('<span aria-hidden="true"> >> </span><span class="sr-only">Next</span>', ['escape' => false]) ?>
                        </ul>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</div>

