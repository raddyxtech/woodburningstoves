<link href="plugins/sweet-alert2/sweetalert2.min.css" rel="stylesheet" type="text/css">
<!-- Page Title Section Starts -->
<div class="row">
    <div class="col-12">
        <div class="page-title-box">
            <h4 class="page-title float-left">List Delivery Regions</h4>
            <ol class="breadcrumb float-right">
                <li class="breadcrumb-item"><a href="<?= h(HTTP_ROOT) ?>admin/dashboard"><i class="fa fa-dashboard"></i>&nbsp;Home</a></li>
                <li class="breadcrumb-item active"><a href="javascript:;">Manage Delivery Regions</a></li>
                <li class="breadcrumb-item active"><a href="javascript:;">List Delivery Regions</a></li>
            </ol>
            <div class="clearfix"></div>
        </div>
    </div>
</div>
<!-- Page Title Section Ends -->
<!-- Main content -->    
<div class="row">
    <div class="col-md-12">
        <div class="card-box">
            <table id="datatable" class="table table-bordered table-striped users-list">
                <thead>
                    <tr>
                        <th class="thead" style="text-align: center;">Image</th>
                        <th class="thead" style="text-align: center;">Product Name</th>
                        <th class="thead" style="text-align: center;">Quantity</th>
                        <th class="thead" style="text-align: center;">Price</th>
                        <th class="thead" style="text-align: center;">Order ID</th>   
                        <th class="thead" style="text-align: center;">Delivery On</th>   
                        <th class="thead" style="text-align: center;"></th>   
                    </tr>
                </thead>
                <tbody>
                    <?php
                    foreach ($userInfos as $userInfo):
                        if ($userInfo['return_status'] != "A" && $userInfo['cancel_status'] != "A") {
                            ?>
                            <tr id="<?= $userInfo['id']; ?>" class="message_box">  
                                <td style="text-align: center;">
                                    <?php
                                    foreach ($userInfo['product']['images'] as $image) :
                                        if ($image['main_image'] == "Y") {
                                            ?>
                                            <img style="height: 80px; width: 60px;" src="<?= HTTP_ROOT . PRODUCT_PIC . $image['filename']; ?>" />
                                            <?php
                                        }
                                    endforeach;
                                    ?>        
                                </td>
                                <td style="text-align: center;"><?= $userInfo['product']['name']; ?></td>
                                <td style="text-align: center;">
        <?= $userInfo['qty']; ?>
                                </td>
                                <td style="text-align: center;">
                                    $<?= $userInfo['total']; ?>
                                </td>
                                <td style="text-align: center;"><?= $userInfo['unique_id']; ?></td>
                                <td style="text-align: center;">12th Dec'13</td>
                                <td class="order-status" style="text-align: center;">

                                    <a href="<?= $this->Url->build(['prefix' => 'admin', 'controller' => 'orders', 'action' => 'previewOrder/'.$userInfo['id'],$userInfo['order_id']]); ?>"<button class="btn btn-theme btn-theme-dark" style="padding-top: 8px;background-color: #f1dce3;">VIEW ORDER</button></a>

                                </td>        </tr>
    <?php } endforeach; ?>
                </tbody>
            </table>  
        </div>
    </div>
</div>
<!-- Sweet-Alert  -->
<script src="plugins/sweet-alert2/sweetalert2.min.js"></script>
<!-- Required datatable js -->
<script src="plugins/datatables/jquery.dataTables.min.js"></script>
<script src="plugins/datatables/dataTables.bootstrap4.min.js"></script>
<script>

                                        $(document).ready(function () {
                                            $('.delete-user').click(function () {
                                                var delUserUrl = $(this).data('url');
                                                swal({
                                                    title: 'Are you sure?',
                                                    text: "Delivery Option will be deleted, this can\'t be undone!",
                                                    type: 'error',
                                                    showCancelButton: true,
                                                    confirmButtonColor: '#d57171',
                                                    confirmButtonText: 'Delete'
                                                }).then(function () {
                                                    window.location = delUserUrl;
                                                });
                                            });
                                            // For Data Tables
                                            $('#example').DataTable({
                                                searching: false,
                                                paging: false,
                                                info: false
                                            });
                                        });

</script>