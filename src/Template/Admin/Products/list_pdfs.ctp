
<link href="plugins/sweet-alert2/sweetalert2.min.css" rel="stylesheet" type="text/css">
<div class="row">
    <div class="col-12">
        <div class="page-title-box">
            <h4 class="page-title float-left">PRODUCT PDF FOR : <?= $product->name; ?></h4>
            <a class="btn btn-danger" style="float: right;" href="<?= $this->Url->build(['prefix' => 'admin', 'controller' => 'Products', 'action' => 'listProducts', 'parent_id' => $productcat->category_id]); ?>">Return</a>
            <a class="btn btn-success" style="float: right; margin-right: 6px;" id="addPdf" href='javascript:;'>Add New Pdf</a>
            <div class="clearfix"></div>
        </div>
    </div>
</div>
<div class="row" style="display: none;" id="form">
        <div class="col-md-12">
            <div class="card-box">
                <!--<h4 class="m-t-0 header-title">Add Category</h4>-->
                <p class="text-muted m-b-30 font-13">
                    All (<span style="color:#FF0000">*</span>) fields are mandatory
                </p>
                <div class="row m-t-20">
                    <div class="col-md-12">
                        <div class="card-box">
                            <!--<h4 class="m-t-0 m-b-30 header-title">Basic Information</h4>-->
                            <?= $this->Form->create(null, ['action' => 'addPdfs/' . $product->product_id,'method' => 'POST', 'type' => 'file', 'validate' => 'noValidate']); ?>
                            <div class="form-group">
                                <label> Add PDF: <span style="color:#FF0000">*</span></label>
                                <?= $this->Form->control('pdf_filename', ['type' => 'file', 'label' => FALSE]) ?>                       
                            </div>

                        </div>
                    </div>
                </div>
                <?= $this->Form->button('Submit', ['class' => 'btn btn-primary', 'id' => 'pic-update-btn']); ?>
                <a class="btn btn-danger" href="<?= $this->Url->build(['prefix' => 'admin', 'controller' => 'Products', 'action' => 'listProducts']); ?>">Cancel</a>
                <?= $this->Form->end() ?>
            </div>    
        </div>
    </div>
<div class="row">
    <div class="col-md-12">
        <div class="card-box">
            <table class="table table-bordered table-striped users-list">
                <thead>
                    <tr>
                        <th>PDFs Name:</th>
                        <th style="text-align: center;">Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    foreach ($pdfs as $pdf) {
                        ;
                        ?>
                        <tr>
                            <td>
                     <?= $pdf->pdf_filename; ?>
                            </td>
                            <td style="text-align: center;">
                                <a href="javascript:;" class="btn btn-xs bg-pink text-white hint--left delete-user" data-hint="Delete pdf?" data-url="<?= $this->Url->build(['prefix' => 'admin', 'controller' => 'Products', 'action' => 'deletepdf', $pdf->product_id]); ?>" > <i class ="fa fa-trash"></i> </a> 
                            </td>
                        </tr>
                   <?php } ?>
                </tbody>
            </table>  
        </div>
    </div>
</div>
<script src="plugins/sweet-alert2/sweetalert2.min.js"></script>
<script src="plugins/datatables/jquery.dataTables.min.js"></script>
<script src="plugins/datatables/dataTables.bootstrap4.min.js"></script>
<script>

    document.getElementById("addPdf").onclick = function () {
        $("#form").show();
    };
    $(document).ready(function () {
        $('.delete-user').click(function () {
            var delUserUrl = $(this).data('url');
            swal({
                title: 'Are you sure?',
                text: "Accoount will be deleted, this can\'t be undone!",
                type: 'error',
                showCancelButton: true,
                confirmButtonColor: '#d57171',
                confirmButtonText: 'Delete'
            }).then(function () {
                window.location = delUserUrl;
            });
        });
        $('#example').DataTable({
            searching: false,
            paging: false,
            info: false
        });
        $('.sw-alert-link').on('click', function (e) {
            e.preventDefault();
            var link = $(this).attr('href');
            let dynamicText = 'This will change the status of the Manager';
            if (link.indexOf('resetPassword') > 0) {
                dynamicText = 'This will reset the password of the Manager';
            }
            swal({
                title: 'Are you sure?',
                text: dynamicText,
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: 'OK',
                cancelButtonText: 'No',
                confirmButtonClass: 'btn btn-success',
                cancelButtonClass: 'btn btn-danger m-l-10',
                buttonsStyling: false
            }).then(function () {
                window.location.href = link;
            });
        });
    });
</script>