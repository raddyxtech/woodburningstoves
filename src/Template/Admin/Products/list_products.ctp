<?php
$paginator = $this->Paginator->setTemplates([
    'number' => '<li class="page-item"><a href="{{url}}" class="page-link">{{text}}</a></li>',
    'current' => '<li class="page-item active"><a href="{{url}}" class="page-link">{{text}}</a></li>',
    'first' => '<li class="page-item"><a href="{{url}}" class="page-link">&laquo;</a></li>',
    'last' => '<li class="page-item"><a href="{{url}}" class="page-link">&raquo;</a></li>',
    'prevActive' => '<li class="page-item"><a href="{{url}}" class="page-link">&lt;</a></li>',
    'nextActive' => '<li class="page-item"><a href="{{url}}" class="page-link">&gt;</a></li>',
        ]);
?>
<link href="plugins/sweet-alert2/sweetalert2.min.css" rel="stylesheet" type="text/css">
<div class="row">
    <div class="col-12">
        <div class="page-title-box">
            <h4 class="page-title float-left">List Products</h4>
            <ol class="breadcrumb float-right">
                <li class="breadcrumb-item"><a href="<?= h(HTTP_ROOT) ?>admin/dashboard"><i class="fa fa-dashboard"></i>&nbsp;Home</a></li>
                <li class="breadcrumb-item active"><a href="javascript:;">List Products</a></li>
                <li class="breadcrumb-item active"><a href="javascript:;">Add New Products</a></li>
            </ol>
            <div class="clearfix"></div>
        </div>
    </div>
</div>
<div class="box-header bg-gray-light">
    <?= $this->Form->create(NULL, ['id' => 'filterForm']); ?>
    <div class="row">                           
        <div class="form-group col-sm-3">
            <label for="keyword">Keyword: </label>
            <?= $this->Form->text('keyword', ['class' => 'form-control', "value" => isset($queryParams['keyword']) ? $queryParams['keyword'] : '', 'placeholder' => ' Name']); ?>
        </div>
        <div class="form-group col-sm-2">
            <label for="from_date">From Date: </label>
            <?= $this->Form->text('from_date', ['class' => 'datepicker form-control', "value" => isset($queryParams['from_date']) ? $queryParams['from_date'] : '', 'placeholder' => 'From Date']); ?> 
        </div>
        <div class="form-group col-sm-2">
            <label for="to_date">To Date: </label>
            <?= $this->Form->text('to_date', ['class' => 'datepicker form-control', "value" => isset($queryParams['to_date']) ? $queryParams['to_date'] : '', 'placeholder' => 'To Date']); ?> 
        </div>      
        <div class="form-group col-sm-3">
            <label>Category:</label>
            <?= $this->Form->select('parent_id', $categoryList, ['empty' => 'Select category', "value" => isset($queryParams['parent_id']) ? $queryParams['parent_id'] : '', 'label' => false, 'class' => 'form-control']); ?>
        </div>
        <div class="col-md-2 nopadding">
            <label style="width: 100%;">&nbsp;</label>
            <?= $this->Form->button('Search', ['class' => 'btn btn-info', 'type' => 'submit']); ?>
            <a class="btn btn-warning" href="<?= HTTP_ROOT . 'admin/products/list-products/'; ?>" >Clear</a>
        </div>
        <div class="clearfix"></div>                            
    </div>
    <div class="clearfix"></div>   
    <?= $this->Form->end(); ?>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="card-box" style="padding-bottom: 50px;">
            <table class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th>Product</th>
                        <th style="text-align: center;">Created</th>
                        <th style="text-align: center;">Modified</th>
                        <th style="text-align: center;">Actions</th>
                    </tr>
                </thead>
                <tbody id="proTbl">
                    <tr>
                        <?php foreach ($products as $product): ?>      
                            <td class="div2Content"><?= $product->name ?></td>
                           <?php if(!empty($product->created)) { ?>
                            <td style="text-align: center;"><?= date_format($product->created, "d M, Y") ?></td>
                           <?php }else{ ?>
                             <td style="text-align: center;"></td>
                           <?php } ?>
                            
                            <td style="text-align: center;"><?= date_format($product->date_updated, "d M, Y") ?></td>
                            <td style="text-align: center;">
                                <a class="btn btn-xs btn-<?= ($product->hidden == 'N') ? 'success' : 'danger'; ?> sw-alert-link hint--top" data-hint="<?= ($product->hidden == 'N') ? 'Click To Deactivate' : 'Click To Activate'; ?>" href="<?= $this->Url->build(['prefix' => 'admin', 'controller' => 'Products', 'action' => 'changeProductStatus', $product->product_id]); ?>"> <i class="fa fa-toggle-<?= ($product->hidden == 'N') ? 'on' : 'off'; ?>"></i>  </a>
                                <a class="btn btn-xs btn-warning hint--top" data-hint="Edit" href="<?= $this->Url->build(['prefix' => 'admin', 'controller' => 'Products', 'action' => 'addProducts', $product->product_id]); ?>" > <i class ="fa fa-edit"></i> </a>
                                <a class="btn btn-xs btn-primary hint--top" data-hint="Variations" href="<?= $this->Url->build(['prefix' => 'admin', 'controller' => 'Products', 'action' => 'listVariations', $product->product_id]); ?>" > <i class="fa fa-stack-exchange"></i> </a>
                                <a class="btn btn-xs btn-purple hint--top" data-hint="Addons" href="<?= $this->Url->build(['prefix' => 'admin', 'controller' => 'Products', 'action' => 'addAddons', $product->product_id]); ?>" ><i class="fa fa-puzzle-piece"></i></a>
                                <a class="btn btn-xs btn-success hint--top" data-hint="Images" href="<?= $this->Url->build(['prefix' => 'admin', 'controller' => 'Products', 'action' => 'listImages', $product->product_id]); ?>" > <i class="fa fa-picture-o"></i></a>
                                <a class="btn btn-xs btn-info hint--top" data-hint="Pdfs" href="<?= $this->Url->build(['prefix' => 'admin', 'controller' => 'Products', 'action' => 'listPdfs', $product->product_id]); ?>" > <i class="fa fa-file-pdf-o"></i></a>
                                <a href="javascript:;" class="btn btn-xs bg-pink text-white hint--left delete-user" data-hint="Delete product?" data-url="<?= $this->Url->build(['prefix' => 'admin', 'controller' => 'Products', 'action' => 'deleteProduct', $product->product_id]); ?>" > <i class ="fa fa-trash"></i> </a> 
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>            
            <div class="paginationRow">
                <div class="paginationLeft">
                    <p><?= $this->Paginator->counter('Showing {{start}}-{{end}} Orders out of {{count}}'); ?></p>
                </div>
                <div class="paginationRight">
                    <?php if ($this->Paginator->numbers()): ?>
                        <ul class="pagination pagination-split">
                            <?= $this->Paginator->prev('<span aria-hidden="true"> << </span><span class="sr-only">Previous</span>', ['escape' => false]) ?>                                    
                            <?= $this->Paginator->numbers() ?>
                            <?= $this->Paginator->next('<span aria-hidden="true"> >> </span><span class="sr-only">Next</span>', ['escape' => false]) ?>
                        </ul>
                    <?php endif; ?>
                </div>
            </div>            
        </div>
    </div>
</div>
<script src="plugins/sweet-alert2/sweetalert2.min.js"></script>
<script>
    $(document).ready(function () {
        $(document).on('click', '.delete-user', function () {
            var delUserUrl = $(this).data('url');
            swal({
                title: 'Are you sure?',
                text: "It will be deleted, this can\'t be undone!",
                type: 'error',
                showCancelButton: true,
                confirmButtonColor: '#d57171',
                confirmButtonText: 'Delete'
            }).then(function () {
                window.location = delUserUrl;
            });
        });
    });
    function myProducts(categoryId) {
        $("#proTbl").html("<tr><td colspan='6' style='text-align: center;'><img src='img/loder.gif' /></td></tr>");
        $.ajax({
            type: 'post',
            url: "<?= $this->Url->build(['controller' => 'Products', 'action' => 'ajaxListProducts']); ?>",
            data: {"parent_id": categoryId},
            success: function (response) {
                var data = JSON.parse(response);
                console.log(data);
                var html = "<tr>";
                $.each(data, function (index, value) {
                    html += "<td>" + data[index].product.name + "</td>" +
                            "<td style='text-align: center;'>" +
                            "<a class='btn btn-xs btn-warning hint--top' data-hint='Edit' href='<?= $this->Url->build(["prefix" => "admin", "controller" => "Products", "action" => "addProducts", $product->product_id]); ?>' > <i class ='fa fa-edit'></i> </a> " +
                            "<a class='btn btn-xs btn-primary hint--top' data-hint='Variations' href='<?= $this->Url->build(["prefix" => "admin", "controller" => "Products", "action" => "listVariations", $product->product_id]); ?>' > <i class='fa fa-stack-exchange'></i> </a> " +
                            "<a class='btn btn-xs btn-purple hint--top' data-hint='Addons' href='<?= $this->Url->build(["prefix" => "admin", "controller" => "Products", "action" => "addAddons", $product->product_id]); ?>' ><i class='fa fa-puzzle-piece'></i></a> " +
                            "<a class='btn btn-xs btn-success hint--top' data-hint='Images' href='<?= $this->Url->build(["prefix" => "admin", "controller" => "Products", "action" => "listImages", $product->product_id]); ?>' > <i class='fa fa-picture-o'></i></a> " +
                            "<a class='btn btn-xs btn-info hint--top' data-hint='Pdfs' href='<?= $this->Url->build(["prefix" => "admin", "controller" => "Products", "action" => "listPdfs", $product->product_id]); ?>' > <i class='fa fa-file-pdf-o'></i></a> " +
                            "<a href='javascript:;' class='btn btn-xs bg-pink text-white hint--left delete-user' data-hint='Delete product?' data-url='<?= $this->Url->build(["prefix" => "admin", "controller" => "Products", "action" => "deleteProduct", $product->product_id]); ?>' > <i class ='fa fa-trash'></i> </a> " +
                            "</td>"
                            + "</tr>";
                });
                $('#proTbl').html(html);
            },
            error: function (e) {
                alert("An error occurred: ");
                console.log(e);
            }
        });
    }
//    function hiddenStatus(value){
//   $.ajax({
//            type: "POST", //or POST
//                    url: "<//?= $this->Url->build(['controller' => 'Products', 'action' => 'changeProductStatus']); ?>",
//                    data: {status: value},
//                    success: function (responsedata) {
//                        console.log(responsedata);
//                    $('#proTbl').html(responsedata);
//                    }
//            })
//    }
</script>
