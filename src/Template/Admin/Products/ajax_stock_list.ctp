<?php
//pj($products['product_variation']);exit;
if ($products->count()):
    foreach ($products as $product):
        ?>
        <tr class="message_box">  
            <td>
                <ul><li><?= $product['name']; ?>
                        <?php
                        foreach ($product['product_variations'] as $productVariation):
                            if (!empty($productVariation['var_data_1']) && isset($productVariation['var_data_1'])) {
                                ?>
                                <ul><li><?= $productVariation['var_data_1']; ?></li></ul>  
                            <?php } endforeach; ?></li></ul>
            </td>
            <td style="text-align: center;">
                <input id="status_new" type="checkbox" name="status_new[]" value="<?= $product['product_id']; ?>" <?php if ($product['status_new'] == 'Y') { ?> checked <?php } ?>/>
            </td>
            <td style="text-align: center;">
                <input id="status_sale" type="checkbox" name="status_sale[]" value="<?= $product['product_id']; ?>" <?php if ($product['status_sale'] == 'Y') { ?> checked <?php } ?>/>
            </td>
            <td style="text-align: center;">
                <input id="status_clearance" type="checkbox" name="status_clearance[]" value="<?= $product['product_id']; ?>" <?php if ($product['status_clearance'] == 'Y') { ?> checked <?php } ?>/>
            </td>
            <td style="text-align: center;"><br/>
                <?php foreach ($product['product_variations'] as $productVariation): ?>
                    <input id="out_of_stock" type="checkbox" name="out_of_stock[]" value="<?= $productVariation['variation_id']; ?>" <?php if ($productVariation['out_of_stock'] == 'Y') { ?> checked <?php } ?>/><br/>
                <?php endforeach; ?>
            </td>
            <td style="text-align: center;">
                <input id="hidden" type="checkbox" name="hidden[]" value="<?= $product['product_id']; ?>" <?php if ($product['hidden'] == 'Y') { ?> checked <?php } ?>/>
            </td>
        </tr>
        <?php
    endforeach;
else:
    ?>
    <tr class="message_box">  
        <td colspan="6" style="text-align: center;">No Product Found!!</td>
    </tr>
<?php endif; ?>
