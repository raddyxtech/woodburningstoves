<script>
    <link rel="stylesheet" type="text/css" href="admin.css"/>
</script>
    <style>
        .btn-purple { height: 38px; }
    </style>
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <h4 class="page-title float-left">Add Image</h4>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="card-box">
                <div class="row m-t-20">
                    <div class="col-md-12">
                        <div class="card-box">
                            <!--<h4 class="m-t-0 m-b-30 header-title">Basic Information</h4>-->
                            <?= $this->Form->create(null, ['id' => 'image', 'method' => 'POST', 'type' => 'file']); ?>
                            <div class="form-group">
                                <label> New Image: </label>
                                <?= $this->Form->control('filename', ['type' => 'file', 'label' => FALSE]) ?>                        </div>
                            <div class="form-group">
                                <label> New Image Title:</label>
                                <?= $this->Form->control('title', ['placeholder' => "Enter image title", 'class' => "form-control", 'div' => false, 'label' => false]); ?>
                            </div>
                        </div>
                    </div>
                </div>
                <a class="btn btn-danger" href="<?= $this->Url->build(['prefix' => 'admin', 'controller' => 'Products', 'action' => 'listImages', $product_id]); ?>">Cancel</a>
                <?= $this->Form->button('Submit', ['class' => 'btn btn-primary', 'id' => 'pic-update-btn']); ?>
                <?= $this->Form->end() ?>
            </div>    
        </div>
    </div>
    <?= $this->Html->script('/plugins/ckeditor/ckeditor'); ?>      
    <!-- For File Browser  -->
    <script src="plugins/bootstrap-filestyle/js/bootstrap-filestyle.min.js" type="text/javascript"></script>
    <!-- For Croppie Plugin  -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/croppie/2.6.2/croppie.css" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/exif-js/2.3.0/exif.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/croppie/2.6.2/croppie.js"></script>
    <script>
    $("#image").validate({
        rules: {
            filename: "required",
            title: "required",
        },
        messages: {
            filename: "Please select a image",
            title: "Please enter title",
        },
    });
    </script>