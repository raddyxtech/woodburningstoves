<script>
    <link rel="stylesheet" type="text/css" href="admin.css"/>
</script>
    <style>
        .btn-purple { height: 38px; }
    </style>
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <h4 class="page-title float-left">Edit Variations</h4>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="card-box">
                <?= $this->Form->create($fetchvariation, ['id' => 'wizard-callbacks', 'validate' => 'noValidate']) ?>
                <div class="row m-t-20">
                    <div class="col-md-12">
                        <div class="card-box">
                            <div class="form-group">
                                <label>Part No./Code:</label>
                                <?= $this->Form->control('code', ['placeholder' => "Enter Code", 'class' => "form-control", 'div' => false, 'label' => false]); ?>
                            </div>
                            <div class="form-group">
                                <label>Weight(gramms):</label>
                                <?= $this->Form->control('weight', ['placeholder' => "Enter Weight", 'class' => "form-control", 'div' => false, 'label' => false]); ?>
                            </div>
                            <div class="form-group">
                                <label>Retail Price:(incl VAT)</label>
                                <?= $this->Form->control('retail_price', ['placeholder' => "Enter Price", 'class' => "form-control", 'div' => false, 'label' => false]); ?>
                            </div>
                            <div class="form-group">
                                <label>Out of Stock:</label>
                                <?=
                                $this->Form->checkbox('out_of_stock', [
                                    'value' => 'Y',
                                    'hiddenField' => 'N',]);
                                ?>       
                            </div>
                            <div class="form-group">
                                <label>Zero VAT:</label>
                                <?=
                                $this->Form->checkbox('zero_vat', [
                                    'value' => 'Y',
                                    'hiddenField' => 'N',]);
                                ?>  
                            </div>
                        </div>
                    </div>
                </div>
                <button type="submit" class="btn btn-primary" id="pic-update-btn">Submit</button>
                <a class="btn btn-danger" href="<?= $this->Url->build(['prefix' => 'admin', 'controller' => 'Products', 'action' => 'listProducts']); ?>">Cancel</a>
<?= $this->Form->end() ?>
            </div>    
        </div>
    </div>
<?= $this->Html->script('/plugins/ckeditor/ckeditor'); ?>      
    <script>
        $(function () {
            CKEDITOR.replace('editor1', {
                uiColor: '#cccccc'
            });
        });</script>
    <!-- For File Browser  -->
    <script src="plugins/bootstrap-filestyle/js/bootstrap-filestyle.min.js" type="text/javascript"></script>
    <!-- For Croppie Plugin  -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/croppie/2.6.2/croppie.css" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/exif-js/2.3.0/exif.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/croppie/2.6.2/croppie.js"></script>
    <script>
        $("#wizard-callbacks").validate({
            rules: {
                code: "required",
                weight: "required",
                retail_price: "required"
            },
            messages: {
                code: "Please provide a code",
                weight: "Please enter weight",
                retail_price: "Please enter price"
            },
        });
    </script>
