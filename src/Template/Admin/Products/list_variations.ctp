
<link href="plugins/sweet-alert2/sweetalert2.min.css" rel="stylesheet" type="text/css">
<div class="row">
    <div class="col-12">
        <div class="page-title-box">
            <h4 class="page-title float-left">PRODUCT VARIATIONS FOR : <?= $product->name; ?></h4>
            <a class="btn btn-danger" style="float: right;" href="<?= $this->Url->build(['prefix' => 'admin', 'controller' => 'Products', 'action' => 'listProducts','parent_id'=> $productcat->category_id]); ?>">Return</a>
            <a class="btn btn-success" style="float: right;margin-right: 6px;" href="<?= $this->Url->build(['prefix' => 'admin', 'controller' => 'Products', 'action' => 'edit-variations/' . $product->product_id]); ?>">Add Variations</a>
            <div class="clearfix"></div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="card-box">
            <table class="table table-bordered table-striped users-list">
                <thead>
                    <tr>
                        <th>Part No./Code:</th>
                        <th>Weight(gramms):</th>
                        <th>Retail Price:(inclVAT)</th>
                        <th style="text-align: center;">Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($variations as $variation) {
                        ;
                        ?>
                        <tr>
                            <td><?= $variation->code ?></td>
                            <td><?= $variation->weight ?></td>
                            <td><?= $variation->retail_price ?></td>
        <!--                    <td><?= $variation->out_of_stock ?></td>
                            <td><?= $variation->zero_vat ?></td>-->
                            <td style="text-align: center;">
                                <a class="btn btn-xs btn-warning hint--top" data-hint="Edit" href="<?= $this->Url->build(['prefix' => 'admin', 'controller' => 'Products', 'action' => 'editVariations', $variation->product_id]); ?>" > <i class ="fa fa-edit"></i> </a>
                            </td>
                        </tr>
                     <?php } ?>    
                </tbody>
            </table>  
        </div>
    </div>
</div>
<script src="plugins/sweet-alert2/sweetalert2.min.js"></script>
<script src="plugins/datatables/jquery.dataTables.min.js"></script>
<script src="plugins/datatables/dataTables.bootstrap4.min.js"></script>
<script>
    $(document).ready(function () {
        $('.delete-user').click(function () {
            var delUserUrl = $(this).data('url');
            swal({
                title: 'Are you sure?',
                text: "Accoount will be deleted, this can\'t be undone!",
                type: 'error',
                showCancelButton: true,
                confirmButtonColor: '#d57171',
                confirmButtonText: 'Delete'
            }).then(function () {
                window.location = delUserUrl;
            });
        });
        $('#example').DataTable({
            searching: false,
            paging: false,
            info: false
        });
        $('.sw-alert-link').on('click', function (e) {
            e.preventDefault();
            var link = $(this).attr('href');
            let dynamicText = 'This will change the status of the Manager';
            if (link.indexOf('resetPassword') > 0) {
                dynamicText = 'This will reset the password of the Manager';
            }
            swal({
                title: 'Are you sure?',
                text: dynamicText,
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: 'OK',
                cancelButtonText: 'No',
                confirmButtonClass: 'btn btn-success',
                cancelButtonClass: 'btn btn-danger m-l-10',
                buttonsStyling: false
            }).then(function () {
                window.location.href = link;
            });
        });
    });
</script>