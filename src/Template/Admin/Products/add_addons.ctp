<style>
    .btn-purple { height: 38px; }
</style>
<div class="row">
    <div class="col-12">
        <div class="page-title-box">
            <h4 class="page-title float-left">PRODUCT Addons FOR : <?= $product->name; ?></h4>
            <div class="clearfix"></div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="card-box">
            <p class="text-muted m-b-30 font-13">
                All (<span style="color:#FF0000">*</span>) fields are mandatory
            </p>
            <?= $this->Form->create(NULL, ['id' => 'product']) ?>
            <div class="row">
                <div class="col-md-6">
                    <div class="card-box">
                        <div class="form-group">
                            <label> Categories:<span style="color:#FF0000">*</span> </label>
                            <?= $this->Form->select('products.0.CatId', $categoriesList, ['empty' => '----Select Categories----', 'label' => false, 'class' => 'form-control selectCat']); ?><br/>
                            <label>Products:<span style="color:#FF0000">*</span></label>
                            <?= $this->Form->select('products.0.ProductId', $productsList, ['empty' => '----Select Products----', 'label' => false, 'class' => 'form-control selectProd']); ?>
                        </div>
                        <hr/><br/>
                        <div class="form-group">
                            <label> Categories:<span style="color:#FF0000">*</span> </label>
                            <?= $this->Form->select('products.1.CatId', $categoriesList, ['empty' => '----Select Categories----', 'label' => false, 'class' => 'form-control selectCat']); ?><br/>
                            <label> Products:<span style="color:#FF0000">*</span> </label>
                            <?= $this->Form->select('products.1.ProductId', $productsList, ['empty' => '----Select Products----', 'label' => false, 'class' => 'form-control selectProd']); ?>
                        </div>
                        <hr/><br/>
                        <div class="form-group">
                            <label> Categories:<span style="color:#FF0000">*</span> </label>
                            <?= $this->Form->select('products.2.CatId', $categoriesList, ['empty' => '----Select Categories----', 'label' => false, 'class' => 'form-control selectCat']); ?><br/>
                            <label> Products:<span style="color:#FF0000">*</span> </label>
                            <?= $this->Form->select('products.2.ProductId', $productsList, ['empty' => '----Select Products----', 'label' => false, 'class' => 'form-control selectProd']); ?>
                        </div>
                        <hr/><br/>
                        <div class="form-group">
                            <label> Categories:<span style="color:#FF0000">*</span> </label>
                            <?= $this->Form->select('products.3.CatId', $categoriesList, ['empty' => '----Select Categories----', 'label' => false, 'class' => 'form-control selectCat']); ?><br/>
                            <label> Products:<span style="color:#FF0000">*</span> </label>
                            <?= $this->Form->select('products.3.ProductId', $productsList, ['empty' => '----Select Products----', 'label' => false, 'class' => 'form-control selectProd']); ?>
                        </div>
                        <hr/><br/>
                        <div class="form-group">
                            <label> Categories:<span style="color:#FF0000">*</span> </label>
                            <?= $this->Form->select('products.4.CatId', $categoriesList, ['empty' => '----Select Categories----', 'label' => false, 'class' => 'form-control selectCat']); ?><br/>
                            <label> Products:<span style="color:#FF0000">*</span> </label>
                            <?= $this->Form->select('products.4.ProductId', $productsList, ['empty' => '----Select Products----', 'label' => false, 'class' => 'form-control selectProd']); ?>
                        </div>
                        <hr/><br/>
                        <div class="form-group">
                            <label> Categories:<span style="color:#FF0000">*</span> </label>
                            <?= $this->Form->select('products.5.CatId', $categoriesList, ['empty' => '----Select Categories----', 'label' => false, 'class' => 'form-control selectCat']); ?><br/>
                            <label>Products:<span style="color:#FF0000">*</span></label>
                            <?= $this->Form->select('products.5.ProductId', $productsList, ['empty' => '----Select Products----', 'label' => false, 'class' => 'form-control selectProd']); ?>
                        </div>
                        <hr/><br/>
                        <div class="form-group">
                            <label> Categories:<span style="color:#FF0000">*</span> </label>
                            <?= $this->Form->select('products.6.CatId', $categoriesList, ['empty' => '----Select Categories----', 'label' => false, 'class' => 'form-control selectCat']); ?><br/>
                            <label>Products:<span style="color:#FF0000">*</span> </label>
                            <?= $this->Form->select('products.6.ProductId', $productsList, ['empty' => '----Select Products----', 'label' => false, 'class' => 'form-control selectProd']); ?>
                        </div>
                        <hr/><br/>
                        <div class="form-group">
                            <label> Categories:<span style="color:#FF0000">*</span> </label>
                            <?= $this->Form->select('products.7.CatId', $categoriesList, ['empty' => '----Select Categories----', 'label' => false, 'class' => 'form-control selectCat']); ?><br/>
                            <label> Products:<span style="color:#FF0000">*</span> </label>
                            <?= $this->Form->select('products.7.ProductId', $productsList, ['empty' => '----Select Products----', 'label' => false, 'class' => 'form-control selectProd']); ?>
                        </div>
                    </div>
                </div>
            </div>
            <button type="submit" class="btn btn-primary" id="pic-update-btn">Update</button>
            <a class="btn btn-danger" href="<?= $this->Url->build(['prefix' => 'admin', 'controller' => 'Products', 'action' => 'listProducts','parent_id'=> $productcat->category_id]); ?>">Cancel</a>
            <?= $this->Form->end() ?>
        </div>    
    </div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script>
    $(document).ready(function () {
        $('.selectCat').on('change', function () {
            var catID = $(this).val();
            var prodSelect = $(this).closest('.form-group').find('.selectProd');
            prodSelect.empty().append("<option value=''>Select Product</option>");
            if (catID > 0) {
                $.ajax({
                    type: 'POST',
                    url: "<?= $this->Url->build(['controller' => 'Products', 'action' => 'ajaxListCatProducts']); ?>",
                    data: {'cat_id': catID},
                    dataType: 'html',
                    success: function (options) {
                        console.log(options);
                        prodSelect.append("<option value=''>Select Product</option>").append(options);
                    }
                });
            }
        });
    });
</script>