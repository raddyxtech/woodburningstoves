
<link href="plugins/sweet-alert2/sweetalert2.min.css" rel="stylesheet" type="text/css">
<div class="row">
    <div class="col-12">
        <div class="page-title-box">
            <h4 class="page-title float-left">PRODUCT IMAGES FOR : <?= $product->name; ?></h4>
            <a class="btn btn-danger" style="float: right;" href="<?= $this->Url->build(['prefix' => 'admin', 'controller' => 'Products', 'action' => 'listProducts','parent_id'=> $productcat->category_id]); ?>">Return</a>
            <a class="btn btn-success" style="float: right;margin-right: 6px;" id="addImagelist" href='javascript:;'>Add New Image</a>
            <div class="clearfix"></div>
        </div>
    </div>
</div>
<div class="row" style="display: none;" id="form">
    <div class="col-md-12">
        <div class="card-box">
            <div class="row m-t-20">
                <div class="col-md-12">
                    <div class="card-box">
                        <!--<h4 class="m-t-0 m-b-30 header-title">Basic Information</h4>-->
                        <?= $this->Form->create(null, ['action' => 'update-images/' . $product->product_id, 'id' => 'image', 'method' => 'POST', 'type' => 'file']); ?>
                        <div class="form-group">
                            <label> New Image: </label>
                            <?= $this->Form->control('filename', ['type' => 'file', 'label' => FALSE]) ?></div>
                        <div class="form-group">
                            <label> New Image Title:</label>
                            <?= $this->Form->control('title', ['placeholder' => "Enter image title", 'class' => "form-control", 'div' => false, 'label' => false]); ?>
                        </div>
                    </div>
                </div>
            </div>
            <?= $this->Form->button('Submit', ['class' => 'btn btn-primary', 'id' => 'pic-update-btn']); ?>
            <?= $this->Form->end() ?>
        </div>    
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="card-box">
            <table class="table table-bordered table-striped users-list">
                <thead>
                    <tr>
                        <th>Title/Name:</th>
                        <th>Image:</th>
                        <th style="text-align: center;">Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($images as $image) {
                        ;
                        ?>
                        <tr>
                            <td><?= $image->title ?></td>
                            <td>
                                <img src="<?= HTTP_ROOT . 'images/products/' . $image->filename; ?>" height="80px" width="60px"/>
                            </td>                    
                            <td style="text-align: center;">
                                <a href="javascript:;" class="btn btn-xs bg-pink text-white hint--left delete-user" data-hint="Delete image?" data-url="<?= $this->Url->build(['prefix' => 'admin', 'controller' => 'Products', 'action' => 'deleteimage', $image->image_id]); ?>" > <i class ="fa fa-trash"></i> </a> 
                            </td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>  
        </div>
    </div>
</div>
<script src="plugins/sweet-alert2/sweetalert2.min.js"></script>
<script src="plugins/datatables/jquery.dataTables.min.js"></script>
<script src="plugins/datatables/dataTables.bootstrap4.min.js"></script>
<script>
    document.getElementById("addImagelist").onclick = function () {
        $("#form").show();
    };
    $(document).ready(function () {
        $('.delete-user').click(function () {
            var delUserUrl = $(this).data('url');
            swal({
                title: 'Are you sure?',
                text: "It will be deleted, this can\'t be undone!",
                type: 'error',
                showCancelButton: true,
                confirmButtonColor: '#d57171',
                confirmButtonText: 'Delete'
            }).then(function () {
                window.location = delUserUrl;
            });
        });
        $('#example').DataTable({
            searching: false,
            paging: false,
            info: false
        });
        $('.sw-alert-link').on('click', function (e) {
            e.preventDefault();
            var link = $(this).attr('href');
            let dynamicText = 'This will change the status of the Manager';
            if (link.indexOf('resetPassword') > 0) {
                dynamicText = 'This will reset the password of the Manager';
            }
            swal({
                title: 'Are you sure?',
                text: dynamicText,
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: 'OK',
                cancelButtonText: 'No',
                confirmButtonClass: 'btn btn-success',
                cancelButtonClass: 'btn btn-danger m-l-10',
                buttonsStyling: false
            }).then(function () {
                window.location.href = link;
            });
        });
    });
</script>