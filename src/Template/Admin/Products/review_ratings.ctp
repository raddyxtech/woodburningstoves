<link href="plugins/sweet-alert2/sweetalert2.min.css" rel="stylesheet" type="text/css">
<!-- Page Title Section Starts -->
<div class="row">
    <div class="col-12">
        <div class="page-title-box">
            <h4 class="page-title float-left">List Delivery Regions</h4>
            <ol class="breadcrumb float-right">
                <li class="breadcrumb-item"><a href="<?= h(HTTP_ROOT) ?>admin/dashboard"><i class="fa fa-dashboard"></i>&nbsp;Home</a></li>
                <li class="breadcrumb-item active"><a href="javascript:;">Manage Delivery Regions</a></li>
                <li class="breadcrumb-item active"><a href="javascript:;">List Delivery Regions</a></li>
            </ol>
            <div class="clearfix"></div>
        </div>
    </div>
</div>
<!-- Page Title Section Ends -->
<!-- Main content -->    
<div class="row">
    <div class="col-md-12">
        <div class="card-box">
            <table id="datatable" class="table table-bordered table-striped users-list">
                <thead>
                    <tr>
                        <th class="thead">Image</th>
                        <th class="thead">Product Name</th>
                        <th class="thead">Quality</th>
                        <th class="thead">Price</th>
                        <th class="thead">Review</th>   
                        <th class="thead">Date</th>   
                        <th class="thead">Action</th>   
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($reviewRatings as $reviewRating): ?>
                        <tr id="<?= $reviewRating['id']; ?>" class="message_box">  
                            <td>
                                <?php
                                foreach ($reviewRating['product']['images'] as $image) :
                                    if ($image['main_image'] == "Y") {
                                        ?>
                                        <img style="height: 80px; width: 60px;" src="<?= HTTP_ROOT . PRODUCT_PIC . $image['filename']; ?>" />
                                        <?php
                                    }
                                endforeach;
                                ?>        
                            </td>
                            <td><?= $reviewRating['product']['name']; ?></td>
                            <td>
                                <?php if ($reviewRating['quality_rating'] == '1') { ?>
                                    <img src="<?= HTTP_ROOT ?>images/download.png" height="15px" width="15px">
                                    <img src="<?= HTTP_ROOT ?>images/download-blank.jpg" height="15px" width="15px">
                                    <img src="<?= HTTP_ROOT ?>images/download-blank.jpg" height="15px" width="15px">
                                    <img src="<?= HTTP_ROOT ?>images/download-blank.jpg" height="15px" width="15px">
                                    <img src="<?= HTTP_ROOT ?>images/download-blank.jpg" height="15px" width="15px">
                                <?php } elseif ($reviewRating['quality_rating'] == '2') { ?>
                                    <img src="<?= HTTP_ROOT ?>images/download.png" height="15px" width="15px">
                                    <img src="<?= HTTP_ROOT ?>images/download.png" height="15px" width="15px">
                                    <img src="<?= HTTP_ROOT ?>images/download-blank.jpg" height="15px" width="15px">
                                    <img src="<?= HTTP_ROOT ?>images/download-blank.jpg" height="15px" width="15px">
                                    <img src="<?= HTTP_ROOT ?>images/download-blank.jpg" height="15px" width="15px">
                                <?php } elseif ($reviewRating['quality_rating'] == '3') { ?>
                                    <img src="<?= HTTP_ROOT ?>images/download.png" height="15px" width="15px">
                                    <img src="<?= HTTP_ROOT ?>images/download.png" height="15px" width="15px">
                                    <img src="<?= HTTP_ROOT ?>images/download.png" height="15px" width="15px">
                                    <img src="<?= HTTP_ROOT ?>images/download-blank.jpg" height="15px" width="15px">
                                    <img src="<?= HTTP_ROOT ?>images/download-blank.jpg" height="15px" width="15px">
                                <?php } elseif ($reviewRating['quality_rating'] == '4') { ?>
                                    <img src="<?= HTTP_ROOT ?>images/download.png" height="15px" width="15px">
                                    <img src="<?= HTTP_ROOT ?>images/download.png" height="15px" width="15px">
                                    <img src="<?= HTTP_ROOT ?>images/download.png" height="15px" width="15px">
                                    <img src="<?= HTTP_ROOT ?>images/download.png" height="15px" width="15px">
                                    <img src="<?= HTTP_ROOT ?>images/download-blank.jpg" height="15px" width="15px">
                                <?php } elseif ($reviewRating['quality_rating'] == '5') { ?>
                                    <img src="<?= HTTP_ROOT ?>images/download.png" height="15px" width="15px">
                                    <img src="<?= HTTP_ROOT ?>images/download.png" height="15px" width="15px">
                                    <img src="<?= HTTP_ROOT ?>images/download.png" height="15px" width="15px">
                                    <img src="<?= HTTP_ROOT ?>images/download.png" height="15px" width="15px">
                                    <img src="<?= HTTP_ROOT ?>images/download.png" height="15px" width="15px">
                                <?php } else { ?>
                                    <img src="<?= HTTP_ROOT ?>images/download-blank.jpg" height="15px" width="15px">
                                    <img src="<?= HTTP_ROOT ?>images/download-blank.jpg" height="15px" width="15px">
                                    <img src="<?= HTTP_ROOT ?>images/download-blank.jpg" height="15px" width="15px">
                                    <img src="<?= HTTP_ROOT ?>images/download-blank.jpg" height="15px" width="15px">
                                    <img src="<?= HTTP_ROOT ?>images/download-blank.jpg" height="15px" width="15px">
                                <?php }
                                ?>
                            </td>
                            <td>
                                <?php if ($reviewRating['price_rating'] == '1') { ?>
                                    <img src="<?= HTTP_ROOT ?>images/download.png" height="15px" width="15px">
                                    <img src="<?= HTTP_ROOT ?>images/download-blank.jpg" height="15px" width="15px">
                                    <img src="<?= HTTP_ROOT ?>images/download-blank.jpg" height="15px" width="15px">
                                    <img src="<?= HTTP_ROOT ?>images/download-blank.jpg" height="15px" width="15px">
                                    <img src="<?= HTTP_ROOT ?>images/download-blank.jpg" height="15px" width="15px">
                                <?php } elseif ($reviewRating['price_rating'] == '2') { ?>
                                    <img src="<?= HTTP_ROOT ?>images/download.png" height="15px" width="15px">
                                    <img src="<?= HTTP_ROOT ?>images/download.png" height="15px" width="15px">
                                    <img src="<?= HTTP_ROOT ?>images/download-blank.jpg" height="15px" width="15px">
                                    <img src="<?= HTTP_ROOT ?>images/download-blank.jpg" height="15px" width="15px">
                                    <img src="<?= HTTP_ROOT ?>images/download-blank.jpg" height="15px" width="15px">
                                <?php } elseif ($reviewRating['price_rating'] == '3') { ?>
                                    <img src="<?= HTTP_ROOT ?>images/download.png" height="15px" width="15px">
                                    <img src="<?= HTTP_ROOT ?>images/download.png" height="15px" width="15px">
                                    <img src="<?= HTTP_ROOT ?>images/download.png" height="15px" width="15px">
                                    <img src="<?= HTTP_ROOT ?>images/download-blank.jpg" height="15px" width="15px">
                                    <img src="<?= HTTP_ROOT ?>images/download-blank.jpg" height="15px" width="15px">
                                <?php } elseif ($reviewRating['price_rating'] == '4') { ?>
                                    <img src="<?= HTTP_ROOT ?>images/download.png" height="15px" width="15px">
                                    <img src="<?= HTTP_ROOT ?>images/download.png" height="15px" width="15px">
                                    <img src="<?= HTTP_ROOT ?>images/download.png" height="15px" width="15px">
                                    <img src="<?= HTTP_ROOT ?>images/download.png" height="15px" width="15px">
                                    <img src="<?= HTTP_ROOT ?>images/download-blank.jpg" height="15px" width="15px">
                                <?php } elseif ($reviewRating['price_rating'] == '5') { ?>
                                    <img src="<?= HTTP_ROOT ?>images/download.png" height="15px" width="15px">
                                    <img src="<?= HTTP_ROOT ?>images/download.png" height="15px" width="15px">
                                    <img src="<?= HTTP_ROOT ?>images/download.png" height="15px" width="15px">
                                    <img src="<?= HTTP_ROOT ?>images/download.png" height="15px" width="15px">
                                    <img src="<?= HTTP_ROOT ?>images/download.png" height="15px" width="15px">
                                <?php } else { ?>
                                    <img src="<?= HTTP_ROOT ?>images/download-blank.jpg" height="15px" width="15px">
                                    <img src="<?= HTTP_ROOT ?>images/download-blank.jpg" height="15px" width="15px">
                                    <img src="<?= HTTP_ROOT ?>images/download-blank.jpg" height="15px" width="15px">
                                    <img src="<?= HTTP_ROOT ?>images/download-blank.jpg" height="15px" width="15px">
                                    <img src="<?= HTTP_ROOT ?>images/download-blank.jpg" height="15px" width="15px">
                                <?php }
                                ?>
                            </td>
                            <td><?= $reviewRating['review']; ?></td>
                            <td><?= $reviewRating['created']; ?></td>
                            <td style="text-align: center;"><a href="javascript:;" class="btn btn-xs bg-pink text-white hint--left delete-user" data-hint="Delete Region?" data-url="<?= $this->Url->build(['prefix' => 'admin', 'controller' => 'Products', 'action' => 'deleteReview', $reviewRating['id']]); ?>" > <i class ="fa fa-trash"></i> </a></td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>  
        </div>
    </div>
</div>
<!-- Sweet-Alert  -->
<script src="plugins/sweet-alert2/sweetalert2.min.js"></script>
<!-- Required datatable js -->
<script src="plugins/datatables/jquery.dataTables.min.js"></script>
<script src="plugins/datatables/dataTables.bootstrap4.min.js"></script>
<script>

    $(document).ready(function () {
        $('.delete-user').click(function () {
            var delUserUrl = $(this).data('url');
            swal({
                title: 'Are you sure?',
                text: "Delivery Option will be deleted, this can\'t be undone!",
                type: 'error',
                showCancelButton: true,
                confirmButtonColor: '#d57171',
                confirmButtonText: 'Delete'
            }).then(function () {
                window.location = delUserUrl;
            });
        });
        // For Data Tables
        $('#example').DataTable({
            searching: false,
            paging: false,
            info: false
        });
    });

</script>