<script>
    <link rel="stylesheet" type="text/css" href="admin.css"/>
</script>
    <style>
        .btn-purple { height: 38px; }
    </style>
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <h4 class="page-title float-left">Add PDF</h4>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="card-box">
                <!--<h4 class="m-t-0 header-title">Add Category</h4>-->
                <p class="text-muted m-b-30 font-13">
                    All (<span style="color:#FF0000">*</span>) fields are mandatory
                </p>
                <div class="row m-t-20">
                    <div class="col-md-12">
                        <div class="card-box">
                            <!--<h4 class="m-t-0 m-b-30 header-title">Basic Information</h4>-->
                            <?= $this->Form->create(null, ['method' => 'POST', 'type' => 'file','id'=>'wizard-callbacks','validate' => 'noValidate']); ?>
                            <div class="form-group">
                                <label> Add PDF: <span style="color:#FF0000">*</span></label>
                                <?= $this->Form->control('pdf_filename', ['type' => 'file', 'label' => FALSE]) ?>                       
                            </div>

                        </div>
                    </div>
                </div>
                <?= $this->Form->button('Submit', ['class' => 'btn btn-primary', 'id' => 'pic-update-btn']); ?>
                <a class="btn btn-danger" href="<?= $this->Url->build(['prefix' => 'admin', 'controller' => 'Products', 'action' => 'listProducts']); ?>">Cancel</a>
                <?= $this->Form->end() ?>
            </div>    
        </div>
    </div>
    <?= $this->Html->script('/plugins/ckeditor/ckeditor'); ?>      

    <!-- For File Browser  -->
    <script src="plugins/bootstrap-filestyle/js/bootstrap-filestyle.min.js" type="text/javascript"></script>
    <!-- For Croppie Plugin  -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/croppie/2.6.2/croppie.css" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/exif-js/2.3.0/exif.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/croppie/2.6.2/croppie.js"></script>
    <script>
            $("#wizard-callbacks").validate({
        rules: {
            pdf_filename: "required",
        },
        messages: {
            pdf_filename: "Please choose a file",  
        },
        submitHandler: function (form) {
            form.submit();
        }
    });
        </script>