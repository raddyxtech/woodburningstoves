<link rel="stylesheet" type="text/css" href="admin.css"/>
<style>
    .btn-purple { height: 38px; }
    .radio_div label {margin: 8px;}
    .radio_div input {margin-right: 5px;}
</style>
<div class="row">
    <div class="col-12">
        <div class="page-title-box">
            <h4 class="page-title float-left">Add Product</h4>
            <ol class="breadcrumb float-right">
                <li class="breadcrumb-item"><a href="<?= h(HTTP_ROOT) ?>admin/dashboard"><i class="fa fa-dashboard"></i>&nbsp;Home</a></li>
                <li class="breadcrumb-item active"><a href="javascript:;">List Products</a></li>
                <li class="breadcrumb-item active"><a href="javascript:;">Add New Products</a></li>
            </ol>
            <div class="clearfix"></div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="card-box">
            <p class="text-muted m-b-30 font-13">
                All (<span style="color:#FF0000">*</span>) fields are mandatory
            </p>
            <?= $this->Form->create($product, ['id' => 'wizard-callbacks', 'type' => 'file', 'validate' => 'noValidate']) ?>
            <div class="row m-t-20">
                <div class="col-md-12">
                    <div class="card-box">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label> Name: <span style="color:#FF0000">*</span></label>
                                    <?= $this->Form->control('name', ['placeholder' => "Enter product Name", 'class' => "form-control", 'div' => false, 'label' => false]); ?>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label> Categories:<span style="color:#FF0000">*</span> </label>
                                    <?= $this->Form->select('parent_id', $productList, ['empty' => '--------Select categories-------', 'label' => false, 'class' => 'form-control']); ?>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label> Brand: <span style="color:#FF0000">*</span></label>
                                    <?= $this->Form->select('brand_id', $brandList, ['empty' => '--------Select brand--------', 'id' => 'brand', 'label' => false, 'class' => 'form-control']); ?>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label> Full Description:<span style="color:#FF0000">*</span> </label>
                                    <?= $this->Form->textarea('desc_full', ['label' => 'Description', 'class' => 'ckeditor', 'id' => 'description', 'rows' => '10']); ?>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label> Key Features / Info:<span style="color:#FF0000">*</span> </label>
                                    <?= $this->Form->textarea('key_features ', ['placeholder' => "Enter Key Features", 'class' => "form-control", 'div' => false, 'label' => false]); ?>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label> Keywords:<span style="color:#FF0000">*</span> </label>
                                    <?= $this->Form->textarea('keywords', ['placeholder' => "Enter Keyword", 'class' => "form-control", 'div' => false, 'label' => false]); ?>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label> Variation Name 1: <span style="color:#FF0000">*</span></label>
                                    <?= $this->Form->control('product_variation.var_name_1', ['class' => "form-control", 'div' => false, 'label' => false]); ?>
                                </div>
                                <div class="form-group">
                                    <label> Variation 1:<span style="color:#FF0000">*</span> </label>
                                    <?= $this->Form->textarea('product_variation.var_data_1', ['class' => "form-control", 'div' => false, 'label' => false]); ?>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label> Variation Name 2: <span style="color:#FF0000">*</span></label>
                                    <?= $this->Form->control('product_variation.var_name_2', ['class' => "form-control", 'div' => false, 'label' => false]); ?>
                                </div>
                                <div class="form-group">
                                    <label> Variation 2:<span style="color:#FF0000">*</span> </label>
                                    <?= $this->Form->textarea('product_variation.var_data_2', ['class' => "form-control", 'div' => false, 'label' => false]); ?>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="radio_div">
                                        <label> Featured:<span style="color:#FF0000">*</span> </label>
                                        <?=
                                        $this->Form->radio('featured', [
                                            ['value' => 'Y', 'text' => 'Yes', 'label' => ['class' => 'Yes']],
                                            ['value' => 'N', 'text' => 'No', 'label' => ['class' => 'No']],
                                        ]);
                                        ?>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="radio_div">
                                        <label> New: <span style="color:#FF0000">*</span></label>
                                        <?=
                                        $this->Form->radio('status_new', [
                                            ['value' => 'Y', 'text' => 'Yes', 'label' => ['class' => 'Yes']],
                                            ['value' => 'N', 'text' => 'No', 'label' => ['class' => 'No']],
                                        ]);
                                        ?>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="radio_div">
                                        <label> Clearance: <span style="color:#FF0000">*</span></label>
                                        <?=
                                        $this->Form->radio('status_clearance', [
                                            ['value' => 'Y', 'text' => 'Yes', 'label' => ['class' => 'Yes']],
                                            ['value' => 'N', 'text' => 'No', 'label' => ['class' => 'No']],
                                        ]);
                                        ?>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="radio_div">
                                        <label> Sale: <span style="color:#FF0000">*</span></label>
                                        <?=
                                        $this->Form->radio('status_sale', [
                                            ['value' => 'Y', 'text' => 'Yes', 'label' => ['class' => 'Yes']],
                                            ['value' => 'N', 'text' => 'No', 'label' => ['class' => 'No']],
                                        ]);
                                        ?>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="radio_div">
                                        <label> Hidden: <span style="color:#FF0000">*</span></label>
                                        <?=
                                        $this->Form->radio('hidden', [
                                            ['value' => 'Y', 'text' => 'Yes', 'label' => ['class' => 'Yes']],
                                            ['value' => 'N', 'text' => 'No', 'label' => ['class' => 'No']],
                                        ]);
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="clear"></div>
                        <div class="form-group">
                            <h4 style="text-decoration:bold;color:#313a46;">Specifications</h4><hr/>
                            <div class="row">
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label> Heat Output:<span style="color:#FF0000">*</span> </label>
                                        <?= $this->Form->control('spec_heatoutput', ['class' => "form-control", 'div' => false, 'label' => false]); ?>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label> Width: <span style="color:#FF0000">*</span></label>
                                        <?= $this->Form->control('spec_width', ['class' => "form-control", 'div' => false, 'label' => false]); ?>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label> Height: <span style="color:#FF0000">*</span></label>
                                        <?= $this->Form->control('spec_height', ['class' => "form-control", 'div' => false, 'label' => false]); ?>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label> Depth:<span style="color:#FF0000">*</span> </label>
                                        <?= $this->Form->control('spec_depth', ['class' => "form-control", 'div' => false, 'label' => false]); ?>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label> Flue Diameter:<span style="color:#FF0000">*</span> </label>
                                        <?= $this->Form->control('spec_fluediameter', ['class' => "form-control", 'div' => false, 'label' => false]); ?>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <div class="form-group">
                                            <label> Approx Weight: </label>
                                            <?= $this->Form->control('spec_approxweight', ['class' => "form-control", 'div' => false, 'label' => false]); ?>
                                        </div>  
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label> Other Specifications: <span style="color:#FF0000">*</span></label>
                            <?= $this->Form->textarea('spec_misc', ['class' => "form-control", 'div' => false, 'label' => false]); ?>
                        </div>
                    </div>
                </div>
            </div>
            <button type="submit" class="btn btn-primary" id="pic-update-btn">Submit</button>
            <a class="btn btn-danger" href="<?= $this->Url->build(['prefix' => 'admin', 'controller' => 'Products', 'action' => 'listProducts']); ?>">Cancel</a>
            <?= $this->Form->end() ?>
        </div>    
    </div>
</div>
<?= $this->Html->script('/plugins/ckeditor/ckeditor'); ?>      
<script>
    $(function () {
        CKEDITOR.replace('editor1', {
            uiColor: '#cccccc'
        });
    });</script>
<!-- For File Browser  -->
<script src="plugins/bootstrap-filestyle/js/bootstrap-filestyle.min.js" type="text/javascript"></script>
<!-- For Croppie Plugin  -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/croppie/2.6.2/croppie.css" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/exif-js/2.3.0/exif.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/croppie/2.6.2/croppie.js"></script>
<script>
    $("#wizard-callbacks").validate({
        rules: {
            name: "required",
            parent_id: "required",
            brand_id: "required",
            desc_full: "required",
            key_features: "required",
//                keywords: "required",
//                sVarName1: "required",
//                sVar1: "required",
//                sVarName2: "required",
//                sVar2: "required",
//                featured: "required",
            status_new: "required",
            status_clearance: "required",
            status_sale: "required",
            hidden: "required",
            spec_heatoutput: "required",
            spec_width: "required",
            spec_height: "required",
            spec_depth: "required",
            spec_fluediameter: "required",
            spec_approxweight: "required",
            spec_misc: "required",
        },
        messages: {
            name: "Please Enter product name",
            parent_id: "Please select a category",
            brand_id: "Please select a brand",
            desc_full: "Please Enter desc",
            key_features: "Please Enter ",
//                keywords: "Please Enter this field",
////                sVarName1: "Please Enter this field",
////                sVarName2: "Please Enter this field",
////                sVar2: "Please select field",
//                featured: "Please select field",
            status_new: "Please select field",
            status_clearance: "Please select field",
            status_sale: "Please select field",
            hidden: "Please select field",
            spec_heatoutput: "Please Enter output",
            spec_width: "Please Enter width ",
            spec_height: "Please Enter height ",
            spec_depth: "Please Enter defth",
            spec_fluediameter: "Please Enter diameter ",
            spec_approxweight: "Please Enter weight",
            spec_misc: "Please Enter misc",
        },
        submitHandler: function (form) {
            form.submit();
        }
    });
</script>