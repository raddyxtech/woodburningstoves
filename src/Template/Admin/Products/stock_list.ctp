<script src="webroot/js/jquery-3.3.1.js"></script>
<link rel="stylesheet" type = "text/css" href="admin.css" />
<style>
    .btn-purple { height: 38px; }
</style>
<div class="row">      
    <div class="col-md-12">
        <div class="card-box">
            <?= $this->Form->create(); ?>
            <div class="form-group">
                <label>Brand<span style="color:#FF0000">*</span></label>
                <?= $this->Form->control('id', ['class' => "form-control", 'div' => false, 'label' => false, 'options' => $brands, 'empty' => '--Select Brand--', 'onChange' => 'chooseBrand(this.value)', 'id' => 'name']); ?>
            </div> 
            <?= $this->Form->end() ?>
        </div>    
    </div>
</div>
<div id="result1"> 
    <div class="row">
        <div class="col-md-12">
            <div class="card-box">
                <?= $this->Form->create(NULL, ['url' => ['controller' => 'Products', 'action' => 'UpdateStockList']]) ?>
                <table id="datatable" class="table table-bordered table-striped users-list">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th style="text-align: center;">New</th>
                            <th style="text-align: center;">Sale</th>
                            <th style="text-align: center;">Clearance</th>
                            <th style="text-align: center;">Out Of Stock</th>
                            <th style="text-align: center;">Hidden</th>
                        </tr>
                    </thead>
                    <tbody id="prodTblBody">
                    </tbody>
                    <td colspan="6" align="right"><?= $this->Form->button('Update button', ['class' => 'btn btn-primary']); ?></td>
                </table> 
                <?= $this->Form->end(); ?>
            </div>
        </div>
    </div>
</div>
<script>
    $(function () {
        chooseBrand(null);
    });
    function chooseBrand(brandId) {
        $("#prodTblBody").html("<tr><td colspan='6' style='text-align: center;'><img src='img/loder.gif' /></td></tr>");
        $.ajax({
            type: 'post',
            url: "<?= $this->Url->build(['controller' => 'Products', 'action' => 'ajaxStockList']); ?>",
            data: {"brand_id": brandId},
            success: function (result) {
                $('#prodTblBody').html(result);
            },
            error: function (e) {
                alert("An error occurred: ");
                console.log(e);
            }
        });
    }
    function myFunction() {
//        var selected = new Array();
        $('#datatable tr').each(function (row, tr) {
            TableData = TableData
                    + $(tr).find('td:eq(0)').text() + ' '  // Task No.
                    + $(tr).find('td:eq(1)').text() + ' '  // Date
                    + $(tr).find('td:eq(2)').text() + ' '  // Description
                    + $(tr).find('td:eq(3)').text() + ' '  // Task
                    + '\n';
        });
        alert(value);
    }
</script>