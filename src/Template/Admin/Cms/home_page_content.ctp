
<!-- Page Title Section Starts -->
<div class="row">
    <div class="col-12">
        <div class="page-title-box">
            <h4 class="page-title float-left">Home Page Content</h4>
            <ol class="breadcrumb float-right">
                <li class="breadcrumb-item"><a href="<?= h(HTTP_ROOT) ?>admin/dashboard"><i class="fa fa-dashboard"></i>&nbsp;Home</a></li>
            </ol>
            <div class="clearfix"></div>
        </div>
    </div>
</div>
<!-- Page Title Section Ends -->
<!-- Main content -->    
<div class="row">
    <div class="col-md-12">
        <div class="card-box">
            <?= $this->Form->create($homePage); ?>
            <div class="form-group">
                <?= $this->Form->control('name', ['placeholder' => "Enter name", 'class' => "form-control", 'label' => 'Name', 'required']); ?>
            </div>
            <div class="form-group">
                <label>Top Block</label>
                <?= $this->Form->textarea('value1', ['label' => 'Description', 'class' => 'ckeditor', 'id' => 'editor1', 'rows' => '10']); ?>
            </div>
            <div class="form-group">
                <label>Bottom Block</label>
                <?= $this->Form->textarea('value2', ['label' => 'Description', 'class' => 'ckeditor', 'id' => 'editor2', 'rows' => '10']); ?>
            </div>
            
            <?= $this->Form->button($id ? 'Update' : 'Submit', ['type' => 'submit', 'class' => 'btn btn-success inline', 'div' => false]) ?>
            
            <?= $this->Form->end() ?>      
        </div>
    </div>
</div>

<?= $this->Html->script('/plugins/ckeditor/ckeditor'); ?>      
<script>
    $(function () {
        CKEDITOR.replace('editor1', {
            uiColor: '#cccccc'
        });
        CKEDITOR.replace('editor2', {
            uiColor: '#cccccc'
        });
    });
</script>
