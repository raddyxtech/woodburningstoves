<link href="plugins/sweet-alert2/sweetalert2.min.css" rel="stylesheet" type="text/css">
<!-- Page Title Section Starts -->
<div class="row">
    <div class="col-12">
        <div class="page-title-box">
            <h4 class="page-title float-left">List Pages</h4>
            <ol class="breadcrumb float-right">
                <li class="breadcrumb-item"><a href="<?= h(HTTP_ROOT) ?>admin/dashboard"><i class="fa fa-dashboard"></i>&nbsp;Home</a></li>
                <li class="breadcrumb-item"><a href="javascript:;">&nbsp;List Pages</a></li>
                <li class="breadcrumb-item active"><a class="btn btn-xs btn-success" href="<?= $this->Url->build(['prefix' => 'admin', 'controller' => 'Cms', 'action' => 'newPage']); ?>">Add New Pages</a></li>
            </ol>
            <div class="clearfix"></div>
        </div>
    </div>
</div>
<!-- Page Title Section Ends -->
<!-- Main content -->    
<div class="row">
    <div class="col-md-12">
        <div class="card-box">
            <table id="datatable" class="table table-bordered table-striped users-list">
                <thead>
                    <tr>
                        <th>Heading</th>
                        <th style="text-align: center;">Menu</th>
                        <th style="text-align: center;">Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($listPages as $listPage): ?>
                        <tr id="<?= $listPage->id; ?>" class="message_box">  
                            <td><?= $listPage->header_tag; ?></td>
                            <td style="text-align: center;"><?= $listPage->display_name; ?></td>
                            <td style="text-align: center;">
                                <a class="btn btn-xs btn-warning hint--top" data-hint="Edit" href="<?= $this->Url->build(['prefix'=>'admin','controller'=>'Cms','action'=>'newPage',$listPage->unique_id]); ?>" > <i class ="fa fa-edit"></i> </a>
                                <a class="btn btn-xs bg-danger text-white sw-alert-link hint--left" data-hint="Remove" href="<?= HTTP_ROOT . 'admin/Cms/deletePage/' . $listPage->unique_id; ?>" > <i class="fa fa-trash"></i> </a>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>  
        </div>
    </div>
</div>
<!-- Sweet-Alert  -->
<script src="plugins/sweet-alert2/sweetalert2.min.js"></script>
<!-- Required datatable js -->
<script src="plugins/datatables/jquery.dataTables.min.js"></script>
<script src="plugins/datatables/dataTables.bootstrap4.min.js"></script>
<script>
    
    $(document).ready(function() {
        
        // For Data Tables
        $('#example').DataTable({
            searching: false,
            paging: false,
            info: false
        });
        
       $('.sw-alert-link').on('click', function(e) {
            e.preventDefault();
            var link = $(this).attr('href');
            let dynamicText = 'This will change the status of the Page';
            if(link.indexOf('deletePage') > 0) {
                dynamicText = 'Want to delete this Page!';
            }
            
           swal({
                title: 'Are you sure?',
                text: dynamicText,
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: 'OK',
                cancelButtonText: 'No',
                confirmButtonClass: 'btn btn-success',
                cancelButtonClass: 'btn btn-danger m-l-10',
                buttonsStyling: false
            }).then(function () {
                window.location.href = link; 
            });
           
       });
        
    });
    
</script>