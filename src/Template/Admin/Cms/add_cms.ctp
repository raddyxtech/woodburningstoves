<!-- Page Title Section Starts -->
<div class="row">
    <div class="col-12">
        <div class="page-title-box">
            <h4 class="page-title float-left"><?= $id ? 'Edit' : 'Add' ?> Cms</h4>
            <ol class="breadcrumb float-right">
                <li class="breadcrumb-item"><a href="<?= h(HTTP_ROOT) ?>admin/dashboard"><i class="fa fa-dashboard"></i>&nbsp;Home</a></li>
                <li class="breadcrumb-item active"><a href="<?= $this->Url->build(['prefix' => 'admin', 'controller' => 'Cms', 'action' => 'listCms']); ?>">List Cms</a></li>
            </ol>
            <div class="clearfix"></div>
        </div>
    </div>
</div>
<!-- Page Title Section Ends -->
<!-- Main content -->    
<div class="row">
    <div class="col-md-12">
        <div class="card-box">
            <?= $this->Form->create($newEntity, ['id' => 'addCms']); ?>
            <div class="form-group">
                <label>Name <span style="color:#FF0000">*</span></label>
                <?= $this->Form->control('name', ['placeholder' => "Enter name", 'class' => "form-control", 'label' => FALSE]); ?>
            </div>
            <div class="form-group">
                <label>Content <span style="color:#FF0000">*</span></label>
                <?= $this->Form->textarea('value1', ['label' => FALSE, 'class' => 'ckeditor', 'id' => 'editor1', 'rows' => '10']); ?>
            </div>
            <?= $this->Form->button($id ? 'Update' : 'Submit', ['type' => 'submit', 'class' => 'btn btn-success inline', 'div' => false]) ?>
            <a class="btn btn-danger" href="<?= $this->Url->build(['prefix' => 'admin', 'controller' => 'Cms', 'action' => 'listCms']); ?>">Cancel</a>
            <?= $this->Form->end() ?>      
        </div>
    </div>
</div>

<?= $this->Html->script('/plugins/ckeditor/ckeditor'); ?>      
<script>
    $(function () {
        CKEDITOR.replace('editor1', {
            uiColor: '#cccccc'
        });
    });
    $(document).ready(function () {
//        CKEDITOR.instances.value1.updateElement();
        $('#addCms').validate({
            rules: {
                name: "required",
                value1: "required",
            },
            messages: {
                name: "Name Required",
                value1: "Content Required"
            }
        });
    });
</script>
