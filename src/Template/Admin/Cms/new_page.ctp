<!-- Page Title Section Starts -->
<div class="row">
    <div class="col-12">
        <div class="page-title-box">
            <h4 class="page-title float-left">Add New Page</h4>
            <ol class="breadcrumb float-right">
                <li class="breadcrumb-item"><a href="<?= h(HTTP_ROOT) ?>admin/dashboard"><i class="fa fa-dashboard"></i>&nbsp;Home</a></li>
                <li class="breadcrumb-item active"><a href="<?= $this->Url->build(['prefix' => 'admin', 'controller' => 'Cms', 'action' => 'listPages']); ?>">List Pages</a></li>
            </ol>
            <div class="clearfix"></div>
        </div>
    </div>
</div>
<!-- Page Title Section Ends -->
<!-- Main content -->    
<div class="row">
    <div class="col-md-12">
        <div class="card-box">
            <?= $this->Form->create($cmsPageEntity, ['id' => 'wizard-callbacks']); ?>
            <div class="form-group">
                <label>Browser Title <span style="color:#FF0000">*</span></label>
                <?= $this->Form->control('title', ['placeholder' => "Enter Browser Title", 'class' => "form-control", 'label' => FALSE]); ?>
            </div>
            <div class="form-group">
                <label>Meta Tags <span style="color:#FF0000">*</span></label>
                <?= $this->Form->control('tag', ['placeholder' => "Enter Meta Tags", 'class' => "form-control", 'label' => FALSE]); ?>
            </div>
            <div class="form-group">
                <label>Meta Description <span style="color:#FF0000">*</span></label>
                <?= $this->Form->control('meta_desc', ['placeholder' => "Enter Meta Description", 'class' => "form-control", 'label' => FALSE]); ?>
            </div>
            <div class="form-group">
                <label>Heading (h1 tag) <span style="color:#FF0000">*</span></label>
                <?= $this->Form->control('header_tag', ['placeholder' => "Enter Heading", 'class' => "form-control", 'label' => FALSE]); ?>
            </div>          
            <div class="form-group">
                <label>Content:</label>
                <label>Description <span style="color:#FF0000">*</span></label>
                <?= $this->Form->textarea('content', ['label' => FALSE, 'class' => 'ckeditor', 'id' => 'editor1', 'rows' => '10']); ?>
            </div>
            <div class="form-group">
                <label>Menu Display Name <span style="color:#FF0000">*</span></label>
                <?= $this->Form->control('display_name', ['placeholder' => "Enter Menu Display Name", 'class' => "form-control", 'label' => FALSE]); ?>
            </div>
            <?= $this->Form->button($uniqueId ? 'Update' : 'Submit', ['type' => 'submit', 'class' => 'btn btn-success inline', 'div' => false]) ?>
            <a class="btn btn-danger" href="<?= $this->Url->build(['prefix' => 'admin', 'controller' => 'Cms', 'action' => 'listPages']); ?>">Cancel</a>
            <?= $this->Form->end() ?>      
        </div>
    </div>
</div>

<?= $this->Html->script('/plugins/ckeditor/ckeditor'); ?>      
<script>
    $(function () {
        CKEDITOR.replace('editor1', {
            uiColor: '#cccccc'
        });
    });
</script>
<script>
    $(document).ready(function () {
        $('#wizard-callbacks').validate({
//            ignore: '*:not([name])',
            rules: {
                title: "required",
                tag: "required",
                meta_desc: "required",
                header_tag: "required"
            },
            messages: {
                title: "Browser Title Required",
                tag: "Meta Tags Required",
                meta_desc: "Meta Description Required",
                header_tag: "Heading Required"
            }
        });
    });
</script>