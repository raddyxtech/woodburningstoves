<link href="plugins/sweet-alert2/sweetalert2.min.css" rel="stylesheet" type="text/css">
<!-- Page Title Section Starts -->
<div class="row">
    <div class="col-12">
        <div class="page-title-box">
            <h4 class="page-title float-left">List Delivery Regions</h4>
            <ol class="breadcrumb float-right">
                <li class="breadcrumb-item"><a href="<?= h(HTTP_ROOT) ?>admin/dashboard"><i class="fa fa-dashboard"></i>&nbsp;Home</a></li>
                <li class="breadcrumb-item active"><a href="javascript:;">Manage Delivery Regions</a></li>
                <li class="breadcrumb-item active"><a href="javascript:;">List Delivery Regions</a></li>
            </ol>
            <div class="clearfix"></div>
        </div>
    </div>
</div>
<!-- Page Title Section Ends -->
<!-- Main content -->    
<div class="row">
    <div class="col-md-12">
        <div class="card-box">
            <table id="datatable" class="table table-bordered table-striped users-list">
                <thead>
                    <tr>
                        <th>Courier Name</th>
                        <th>Delivery Name</th>
                        <th>Region</th>
                        <th>Weight Min/Max</th>
                        <th>Value Min/Max</th>
                        <th>Cost</th>
                        <th style="text-align: center;">Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($deliveries as $delivery): ?>
                        <tr id="<?= $delivery->id; ?>" class="message_box">  
                            <td><?= $delivery->courier; ?></td>
                            <td><?= $delivery->name; ?></td>
                            <td><?= $delivery->delivery_region->name; ?></td>
                            <td><?= $delivery->min_weight . '/' . $delivery->max_weight; ?></td>
                            <td><?= $delivery->min_value . '/' . $delivery->max_value; ?></td>
                            <td><?= $delivery->cost; ?></td>
                            <td style="text-align: center;">
                                <a class="btn btn-xs btn-warning hint--top" data-hint="Edit" href="<?= $this->Url->build(['prefix'=>'admin','controller'=>'Delivery','action'=>'addDelivery',$delivery->unique_id]); ?>" > <i class ="fa fa-edit"></i> </a>
                                <a href="javascript:;" class="btn btn-xs bg-pink text-white hint--left delete-user" data-hint="Delete Region?" data-url="<?= $this->Url->build(['prefix' => 'admin', 'controller' => 'Delivery', 'action' => 'deleteDelivery', $delivery->unique_id]); ?>" > <i class ="fa fa-trash"></i> </a>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>  
        </div>
    </div>
</div>
<!-- Sweet-Alert  -->
<script src="plugins/sweet-alert2/sweetalert2.min.js"></script>
<!-- Required datatable js -->
<script src="plugins/datatables/jquery.dataTables.min.js"></script>
<script src="plugins/datatables/dataTables.bootstrap4.min.js"></script>
<script>
    
    $(document).ready(function() {
        $('.delete-user').click(function () {
            var delUserUrl = $(this).data('url');
            swal({
                title: 'Are you sure?',
                text: "Delivery Option will be deleted, this can\'t be undone!",
                type: 'error',
                showCancelButton: true,
                confirmButtonColor: '#d57171',
                confirmButtonText: 'Delete'
            }).then(function () {
                window.location = delUserUrl;
            });
        });
        // For Data Tables
        $('#example').DataTable({
            searching: false,
            paging: false,
            info: false
        });        
    });
    
</script>