<style>
    .btn-purple { height: 38px; }
</style>
<!-- Page Title Section Starts -->
<div class="row">
    <div class="col-12">
        <div class="page-title-box">
            <h4 class="page-title float-left">Manage Delivery Regions</h4>
            <ol class="breadcrumb float-right">
                <li class="breadcrumb-item"><a href="<?= h(HTTP_ROOT) ?>admin/dashboard"><i class="fa fa-dashboard"></i>&nbsp;Home</a></li>
                <li class="breadcrumb-item active"><a href="javascript:;">Manage Delivery Regions</a></li>
                <li class="breadcrumb-item active"><a href="javascript:;">Add New Delivery </a></li>
            </ol>
            <div class="clearfix"></div>
        </div>
    </div>
</div>
<!-- Page Title Section Ends -->
<!-- Main content -->    
<div class="row">
    <div class="col-md-12">
        <div class="card-box">
            <h4 class="m-t-0 header-title">Add New Delivery</h4>
            <p class="text-muted m-b-30 font-13">
                All (<span style="color:#FF0000">*</span>) fields are mandatory
            </p>
            <?= $this->Form->create($delivery, ['id' => 'wizard-callbacks']) ?>
            <div class="row">
                <div class="col-md-6">
                    <div class="card-box">
                        <div class="form-group">
                            <label>Courier Name <span style="color:#FF0000">*</span></label>
                            <?= $this->Form->control('courier', ['placeholder' => "Enter Courier Name", 'class' => "form-control", 'div' => false, 'label' => false]); ?>
                        </div>
                        <div class="form-group">
                            <label>Delivery Name<span style="color:#FF0000">*</span></label>
                            <?= $this->Form->control('name', ['placeholder' => "Enter Delivery Name", 'class' => "form-control", 'div' => false, 'label' => false]); ?>
                        </div>
                        <div class="form-group">
                            <label>Delivery Region<span style="color:#FF0000">*</span></label>
                            <?= $this->Form->control('delivery_region_id', ['class' => "form-control", 'div' => false, 'label' => false, 'options' => $deliveryRegions, 'empty' => 'Select Delivery Region']); ?>
                        </div>
                        <p class="text-muted font-13">
                            NOTE: If both min and max weights are zero = order weight must be zero 
                        </p>
                        <div class="form-group">
                            <label>Min. Weight: (gramms)<span style="color:#FF0000">*</span></label>
                            <?= $this->Form->control('min_weight', ['placeholder' => "Enter Minimum Weight", 'class' => "form-control", 'div' => false, 'label' => false]); ?>
                        </div>
                        <div class="form-group">
                            <label>Max. Weight: (gramms)<span style="color:#FF0000">*</span></label>
                            <?= $this->Form->control('max_weight', ['placeholder' => "Enter Maximum Weight", 'class' => "form-control", 'div' => false, 'label' => false]); ?>
                        </div>
                        <p class="text-muted font-13">
                            NOTE: If both min and max values are zero = order value is not taken into account
                        </p>                        
                        <div class="form-group">
                            <label>Min. Value: (£)<span style="color:#FF0000">*</span></label>
                            <?= $this->Form->control('min_value', ['placeholder' => "Enter Minimum Value", 'class' => "form-control", 'div' => false, 'label' => false]); ?>
                        </div>
                        <div class="form-group">
                            <label>Max. Value: (£)<span style="color:#FF0000">*</span></label>
                            <?= $this->Form->control('max_value', ['placeholder' => "Enter Maximum Value", 'class' => "form-control", 'div' => false, 'label' => false]); ?>
                        </div>
                        <div class="form-group">
                            <label>Cost: (£)<span style="color:#FF0000">*</span></label>
                            <?= $this->Form->control('cost', ['placeholder' => "Enter Cost", 'class' => "form-control", 'div' => false, 'label' => false]); ?>
                        </div>
                        <div class="form-group">
                            <?=
                            $this->Form->control('is_active', [
                                'options' => ['0' => 'Yes', '1' => 'No'],
                                'type' => 'radio',
                                'class' => 'custom-control-input',
                                'templates' => [
                                    'nestingLabel' => '{{hidden}}{{input}}<label{{attrs}} class="custom-control-label">{{text}}</label> &nbsp;',
                                    'radioWrapper' => '<div class="custom-control custom-radio" style="display:inline;">{{label}}</div>',
                                    'radioContainer' => '<div class="form-group">{{content}}</div>'
                                ],
                                'label' => ['text' => 'Hidden <span style="color:#FF0000">*</span>', 'escape' => false, 'class' => 'd-block'],
                                'hiddenField' => false
                            ]);
                            ?>
                        </div>
                    </div>
                </div>
            </div>
            <button type="submit" class="btn btn-primary" id="pic-update-btn">Submit</button>
            <?= $this->Form->end() ?>
        </div>    
    </div>
</div>
<!-- For File Browser  -->
<script src="plugins/bootstrap-filestyle/js/bootstrap-filestyle.min.js" type="text/javascript"></script>
<script>
    $(document).ready(function () {
        $('#wizard-callbacks').validate({
            rules: {
                courier: "required",
                name: "required",
                delivery_region_id: "required",
                min_weight: "required",
                max_weight: "required",
                min_value: "required",
                max_value: "required",
                cost: "required",
                is_active: "required",
            },
            messages: {
                courier: "Courier Required",
                name: "Delivery Required",
                delivery_region_id: "Select Delivery Region",
                min_weight: "Minimum Weight Required",
                max_weight: "Maxmimum Weight Required",
                min_value: "Minimum Value Required",
                max_value: "Maximum Value Required",
                cost: "Cost Required",
                is_active: "Please select atleast one",
            }
        });
    });
</script>