<style>
    .btn-purple { height: 38px; }
</style>
<!-- Page Title Section Starts -->
<div class="row">
    <div class="col-12">
        <div class="page-title-box">
            <h4 class="page-title float-left">Manage Delivery Regions</h4>
            <ol class="breadcrumb float-right">
                <li class="breadcrumb-item"><a href="<?= h(HTTP_ROOT) ?>admin/dashboard"><i class="fa fa-dashboard"></i>&nbsp;Home</a></li>
                <li class="breadcrumb-item active"><a href="javascript:;">Manage Delivery Regions</a></li>
                <li class="breadcrumb-item active"><a href="javascript:;">Add New Region</a></li>
            </ol>
            <div class="clearfix"></div>
        </div>
    </div>
</div>
<!-- Page Title Section Ends -->
<!-- Main content -->    
<div class="row">
    <div class="col-md-12">
        <div class="card-box">
            <h4 class="m-t-0 header-title">Add New Region</h4>
            <p class="text-muted m-b-30 font-13">
                All (<span style="color:#FF0000">*</span>) fields are mandatory
            </p>
            <?= $this->Form->create($region, ['id' => 'wizard-callbacks', 'type' => 'file']) ?>
            <div class="row">
                <div class="col-md-6">
                    <div class="card-box">
                        <div class="form-group">
                            <label>Name <span style="color:#FF0000">*</span></label>
                            <?= $this->Form->control('name', ['placeholder' => "Enter Brand Name", 'class' => "form-control", 'div' => false, 'label' => false]); ?>
                        </div>
                        <div class="form-group">
                            <label>Country<span style="color:#FF0000">*</span></label>
                            <?= $this->Form->control('countries_id', ['class' => "form-control", 'div' => false, 'label' => false, 'options' => $countries, 'empty' => 'Select Country']); ?>
                        </div>
                        <div class="form-group">
                            <label>Postcodes<span style="color:#FF0000">*</span></label>
                            <?= $this->Form->control('postcodes', ['placeholder' => "Enter Brand Name", 'class' => "form-control", 'div' => false, 'label' => false]); ?>
                        </div>
                    </div>
                </div>
            </div>
            <button type="submit" class="btn btn-primary" id="pic-update-btn">Submit</button>
            <?= $this->Form->end() ?>
        </div>    
    </div>
</div>
<!-- For File Browser  -->
<script src="plugins/bootstrap-filestyle/js/bootstrap-filestyle.min.js" type="text/javascript"></script>
<!-- For Croppie Plugin  -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/croppie/2.6.2/croppie.css" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/exif-js/2.3.0/exif.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/croppie/2.6.2/croppie.js"></script>
<script>
    $(document).ready(function () {
        $('#wizard-callbacks').validate({
            rules: {
                name: "required",
                countries_id: "required",
                postcodes: "required",
            },
            messages: {
                name: "Name Required",
                countries_id: "Country Required",
                postcodes: "Postcodes Required",
            }
        });
    });
</script>