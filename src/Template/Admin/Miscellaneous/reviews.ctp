<link href="plugins/sweet-alert2/sweetalert2.min.css" rel="stylesheet" type="text/css">
<!-- review Section Starts -->
<div class="row">
    <div class="col-12">
        <div class="page-title-box">
            <h4 class="page-title float-left">Reviews</h4>

            <ol class="breadcrumb float-right">
                <li class="breadcrumb-item"><a href="<?= h(HTTP_ROOT) ?>admin/dashboard"><i class="fa fa-dashboard"></i>&nbsp;Home</a></li>
                <li><a href="<?= $this->Url->build(['controller' => 'Miscellaneous', 'action' => 'reviews', '?' => ['add' => 'true']]); ?>" class="btn btn-xs btn-success float-right" ><i class="mdi mdi-plus"></i>Add New Testimonial</a></li>
            </ol>

            <div class="clearfix"></div>
        </div>
    </div>
</div>

<!-- Review Section Add and Edit -->
<?php if($add || $id){ ?>
<div class="row">
    <div class="col-md-12">
        <div class="card-box">

            <h4 class="m-t-0 header-title"><?= $id ? 'Edit' : 'Add' ?> Testimonial</h4>
            <p class="text-muted m-b-30 font-13">
                All (<span style="color:#FF0000">*</span>) fields are mandatory
            </p>

            <?= $this->Form->create($reviewEntity, ['id' => 'wizard-callbacks']) ?>

            <div class="row">
                <div class="col-md-9">
                    <div class="card-box">
                        
                        <div class="form-group">
                            <label>Testimonial <span style="color:#FF0000">*</span></label>
                            <?= $this->Form->textarea('testimonial', ['placeholder' => "Write something...", 'class' => "form-control", 'div' => false, 'label' => false]); ?>
                        </div>
                        <div class="form-group">
                            <label>Date <span style="color:#FF0000">*</span></label>
                            <?= $this->Form->control('when_said', [ 'class' => "form-control", 'div' => false, 'label' => false, 'id' => 'datepicker']); ?>
                        </div>
                        <div class="form-group">
                            <label>Testimonial By <span style="color:#FF0000">*</span></label>
                            <?= $this->Form->control('who', ['placeholder' => "Enter Name", 'class' => "form-control", 'div' => false, 'label' => false]); ?>
                        </div>
                    </div>
                </div>
            </div>
            <button type="submit" class="btn btn-primary" id="pic-update-btn"><?= $id ? 'UPDATE' : 'ADD' ?></button>
            <a class="btn btn-danger" href="<?= $this->Url->build(['prefix' => 'admin', 'controller' => 'Miscellaneous', 'action' => 'reviews']); ?>">Cancel</a>

            <?= $this->Form->end() ?>

        </div>    
    </div>
</div>
<?php } ?>

<!-- Main content -->    
<div class="row">
    <div class="col-md-12">
        <div class="card-box">
            <table id="datatable" class="table table-bordered table-striped users-list">
                <thead>
                    <tr>
                        <th>UserName</th>
                        <th style="text-align: center;">Testimonial</th>
                        <th style="text-align: center;">Created</th>
                        <th style="text-align: center;">Status</th>
                        <th style="text-align: center;">Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($listReviews as $listReview): ?>
                        <tr id="<?= $listReview->id; ?>" class="message_box">  
                            <td><?= $listReview->who; ?></td>
                            <td><?= $listReview->testimonial; ?></td>
                            <td><?= $listReview->when_said; ?></td>
                            <td style="text-align: center;">
                                <a class="btn btn-xs btn-<?= ($listReview->approved == 'Y') ? 'success' : 'danger'; ?> sw-alert-link hint--top" data-hint="<?= ($listReview->approved == 'Y') ? 'Click To Deactivate' : 'Click To Activate'; ?>" href="<?= $this->Url->build(['prefix' => 'admin', 'controller' => 'Miscs', 'action' => 'changeReviewStatus', $listReview->id]); ?>"> <i class="fa fa-toggle-<?= ($listReview->approved == 'Y') ? 'on' : 'off'; ?>" ></i></a>
                            </td>
                            <td style="text-align: center;">
                                <a class="btn btn-xs btn-warning hint--top" data-hint="Edit" href="<?= $this->Url->build(['prefix'=>'admin','controller'=>'Miscellaneous','action'=>'reviews',$listReview->id]); ?>" > <i class ="fa fa-edit"></i> </a>
                                <a class="btn btn-xs bg-danger text-white sw-alert-link hint--left" data-hint="Remove" href="<?= HTTP_ROOT . 'admin/Miscellaneous/deleteReview/' . $listReview->id; ?>" > <i class="fa fa-trash"></i> </a>
                                
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>  
        </div>
    </div>
</div>
<!-- Sweet-Alert  -->
<script src="plugins/sweet-alert2/sweetalert2.min.js"></script>
<!-- Required datatable js -->
<script src="plugins/datatables/jquery.dataTables.min.js"></script>
<script src="plugins/datatables/dataTables.bootstrap4.min.js"></script>
<!----- Date picker ---->
<script src="plugins/datepicker/bootstrap-datepicker.js"></script>
<script>

    $(document).ready(function () {

        // For Data Tables
        $('#example').DataTable({
            searching: false,
            paging: false,
            info: false
        });

        $('.sw-alert-link').on('click', function (e) {
            e.preventDefault();
            var link = $(this).attr('href');
            let dynamicText = 'This will change the status of the Review';
            if(link.indexOf('deleteReview') > 0) {
                dynamicText = 'Want to delete this Review!';
            }

            swal({
                title: 'Are you sure?',
                text: dynamicText,
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: 'OK',
                cancelButtonText: 'No',
                confirmButtonClass: 'btn btn-success',
                cancelButtonClass: 'btn btn-danger m-l-10',
                buttonsStyling: false
            }).then(function () {
                window.location.href = link;
            });

        });
        
        $( "#datepicker" ).datepicker();
    });

</script>
