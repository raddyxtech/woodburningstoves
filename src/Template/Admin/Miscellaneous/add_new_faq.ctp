<style>
    .btn-purple { height: 38px; }
</style>
<!-- Page Title Section Starts -->
<div class="row">
    <div class="col-12">
        <div class="page-title-box">
            <h4 class="page-title float-left">Add New Faq</h4>
            <ol class="breadcrumb float-right">
                <li class="breadcrumb-item"><a href="<?= h(HTTP_ROOT) ?>admin/dashboard"><i class="fa fa-dashboard"></i>&nbsp;Home</a></li>
                <li class="breadcrumb-item active"><a href="javascript:;">Manage Faq's</a></li>
                <li class="breadcrumb-item active"><a href="javascript:;">Add New Faq</a></li>
            </ol>
            <div class="clearfix"></div>
        </div>
    </div>
</div>
<!-- Page Title Section Ends -->

<!-- Main content -->    
<div class="row">
    <div class="col-md-12">
        <div class="card-box">

            <h4 class="m-t-0 header-title">Add New Faq</h4>
            <p class="text-muted m-b-30 font-13">
                All (<span style="color:#FF0000">*</span>) fields are mandatory
            </p>

            <?= $this->Form->create($faq, ['id' => 'wizard-callbacks', 'type' => 'file']) ?>

            <div class="row">
                <div class="col-md-6">
                    <div class="card-box">
                        <h4 class="m-t-0 m-b-30 header-title">Basic Information</h4>

                        <div class="form-group">
                            <label>Question <span style="color:#FF0000">*</span></label>
                            <?= $this->Form->control('question', ['type' => 'textarea', 'placeholder' => "Enter Question", 'required' => TRUE, 'class' => "form-control", 'div' => false, 'label' => false]); ?>
                        </div>
                        <div class="form-group">
                            <label>Answer <span style="color:#FF0000">*</span></label>
                            <?= $this->Form->control('answer', ['type' => 'textarea', 'placeholder' => "Enter Answer", 'required' => TRUE, 'class' => "form-control", 'div' => false, 'label' => false]); ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="box-footer submit" >
                <?= $this->Form->button('Submit', ['type' => 'submit', 'class' => 'btn btn-success']); ?>
                <a class="btn btn-danger" href="<?= $this->Url->build(['prefix' => 'admin', 'controller' => 'Miscellaneous', 'action' => 'listFaqs']); ?>">Cancel</a>                          
            </div>

            <?= $this->Form->end() ?>

        </div>    
    </div>
</div>