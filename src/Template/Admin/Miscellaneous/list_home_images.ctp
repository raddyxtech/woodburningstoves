<link href="plugins/sweet-alert2/sweetalert2.min.css" rel="stylesheet" type="text/css">
<!-- Page Title Section Starts -->
<div class="row">
    <div class="col-12">
        <div class="page-title-box">
            <h4 class="page-title float-left">List Home Page Images</h4>

            <ol class="breadcrumb float-right">
                <li class="breadcrumb-item"><a href="<?= h(HTTP_ROOT) ?>admin/dashboard"><i class="fa fa-dashboard"></i>&nbsp;Home</a></li>
                <li class="breadcrumb-item active"><a href="javascript:;">Manage Misc</a></li>
                <li><a href="javascript:;" onclick="$('#addImage').toggle()" class="btn btn-xs btn-success">Add New Home Page Image</a></li>
            </ol>

            <div class="clearfix"></div>
        </div>
    </div>
</div>
<!-- Page Title Section Ends -->

<!-- Main content -->    
<div class="row">
    <div class="col-md-12">
        <div class="card-box" id="addImage" style="display: none;">

            <h4 class="m-t-0 header-title">Add Brand</h4>
            <p class="text-muted m-b-30 font-13">
                All (<span style="color:#FF0000">*</span>) fields are mandatory
            </p>

            <?= $this->Form->create(NULL, ['id' => 'wizard-callbacks', 'type' => 'file', 'url' => '/admin/add-new-home-image']) ?>

            <div class="row">
                <div class="col-md-6">
                    <div class="card-box">
                        <h4 class="m-t-0 m-b-30 header-title">Basic Information</h4>

                        <div class="form-group">
                            <label>Image Title <span style="color:#FF0000">*</span></label>
                            <?= $this->Form->control('title', ['placeholder' => "Enter Image Title", 'class' => "form-control", 'div' => false, 'label' => false, 'required' => true]); ?>
                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="card-box">
                        <h4 class="m-t-0 m-b-30 header-title">Image</h4>
                        <div class="form-group">
                            <div class="uploaded-img"></div>
                        </div>

                        <div class="form-group">
                            <input type="file" class="filestyle" data-placeholder="No file" data-buttonname="btn-purple" id="file-browser" required>
                            <input type="hidden" name="home_image" id="profilePic" disabled="disabled">
                        </div>
                    </div>
                </div>
            </div>
            <button type="submit" class="btn btn-primary" id="pic-update-btn">Submit</button>

            <?= $this->Form->end() ?>

        </div>
        <div class="card-box">
            <table id="datatable" class="table table-bordered table-striped users-list">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Photo</th>
                        <th style="text-align: center;">Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($homeImages as $homeImage): ?>
                        <tr id="<?= $homeImage->id; ?>" class="message_box">  
                            <td><?= $homeImage->title ?></td>
                            <td style="text-align: center;">
                                <div class="img_div">
                                    <img src="<?= HTTP_ROOT . HOME_PIC . (!empty($homeImage->filename) ? $homeImage->filename : 'no-photo.jpg'); ?>" alt="Image Not Present" >
                                </div>
                            </td>
                            <td style="text-align: center;">                                
                                <a class="btn btn-xs btn-<?= ($homeImage->is_active == 1) ? 'success' : 'danger'; ?> sw-alert-link hint--top" data-hint="<?= ($homeImage->is_active == 1) ? 'Click To Deactivate' : 'Click To Activate'; ?>" href="<?= $this->Url->build(['prefix' => 'admin', 'controller' => 'Miscellaneous', 'action' => 'homeImageStatus', $homeImage->unique_id]); ?>"> <i class="fa fa-toggle-<?= ($homeImage->is_active == 1) ? 'on' : 'off'; ?>" ></i>  </a>
                                <a href="javascript:;" class="btn btn-xs bg-pink text-white hint--left delete-image" data-hint="Delete Image?" data-url="<?= $this->Url->build(['prefix' => 'admin', 'controller' => 'Miscellaneous', 'action' => 'deleteHomeImage', $homeImage->unique_id]); ?>" > <i class ="fa fa-trash"></i> </a>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>  
        </div>
    </div>
</div>
<!-- Sweet-Alert  -->
<script src="plugins/sweet-alert2/sweetalert2.min.js"></script>
<!-- Required datatable js -->
<script src="plugins/datatables/jquery.dataTables.min.js"></script>
<script src="plugins/datatables/dataTables.bootstrap4.min.js"></script>
<script src="plugins/bootstrap-filestyle/js/bootstrap-filestyle.min.js" type="text/javascript"></script>
<!-- For Croppie Plugin  -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/croppie/2.6.2/croppie.css" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/exif-js/2.3.0/exif.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/croppie/2.6.2/croppie.js"></script>

<script>
$(document).ready(function() {
    
    $myCroppie = $('.uploaded-img').croppie({
            enableExif: true,
            <?php if (!empty($brand->image)) { ?>
                url: '<?= HTTP_ROOT . BRAND_PIC . $brand->image ?>',
            <?php } ?>
            viewport: {
                width: 150,
                height: 150,
                type: 'square'
            },
            boundary: {
                width: 200,
                height: 200
            }
        });

        $('#file-browser').on('change', function () {
            var reader = new FileReader();
            reader.onload = function (e) {
                $myCroppie.croppie('bind', {
                    url: e.target.result
                });
            };
            reader.readAsDataURL(this.files[0]);
            $('#profilePic').prop('disabled',false);
        });

        $('#pic-update-btn').on('click', function () {
            $myCroppie.croppie('result').then(function (resp) {
                $('#profilePic').val(resp);
            });
        });  
        $('.delete-image').click(function () {
            var delUserUrl = $(this).data('url');
            swal({
                title: 'Are you sure?',
                text: "Image will be deleted, this can\'t be undone!",
                type: 'error',
                showCancelButton: true,
                confirmButtonColor: '#d57171',
                confirmButtonText: 'Delete'
            }).then(function () {
                window.location = delUserUrl;
            });
        });
});
</script>