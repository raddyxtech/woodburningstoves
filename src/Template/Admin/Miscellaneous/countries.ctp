<link href="plugins/sweet-alert2/sweetalert2.min.css" rel="stylesheet" type="text/css">
<!-- country Section Starts -->
<div class="row">
    <div class="col-12">
        <div class="page-title-box">
            <h4 class="page-title float-left">List Countries</h4>

            <ol class="breadcrumb float-right">
                <li class="breadcrumb-item"><a href="<?= h(HTTP_ROOT) ?>admin/dashboard"><i class="fa fa-dashboard"></i>&nbsp;Home</a></li>                
            </ol>

            <div class="clearfix"></div>
        </div>
    </div>
</div>
<!-- Country Section Edit -->

<?php if ($id) { ?>
    <div class="row">
        <div class="col-md-12">
            <div class="card-box">
                <h4 class="m-t-0 header-title" style="margin-bottom: 21px;"><b>EDIT VAT RATE</b></h4>
                <div class="row m-b-30">
                    <div class="col-sm-12">                        
                        <?= $this->Form->create($editId, ['class' => 'form-inline']) ?>
                        <?php echo $this->Form->control('name', ['disabled' => TRUE, 'class' => 'form-control', 'type' => 'text', 'label' => false, 'style' => 'margin-right: 10px;']); ?>
                        <?php echo $this->Form->control('tax_rate', ['class' => 'form-control', 'type' => 'text', 'label' => false, 'style' => 'margin-right: 10px;', 'required' => TRUE]); ?>

                        <?= $this->Form->button('Update', ['class' => 'btn btn-primary', 'style' => 'margin-right: 10px;']) ?>
                        <a class="btn btn-danger" href="<?= $this->Url->build(['prefix' => 'admin', 'controller' => 'Miscellaneous', 'action' => 'countries']); ?>">Cancel</a>
                        <?= $this->Form->end() ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php } ?>

<!-- Main content -->    
<div class="row">
    <div class="col-md-12">
        <strong>Note:</strong> <span style="color: blue">In-active countries do not show on any country list on the front end of the site.</span>
        <div class="card-box">
            <table id="datatable" class="table table-bordered table-striped users-list">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Abreviation</th>
                        <th>ISO 3166</th>
                        <th>Code</th>
                        <th>VAT Rate</th>
                        <th style="text-align: center;">Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($listCountries as $listCountry): ?>
                        <tr id="<?= $listCountry->id; ?>" class="message_box">  
                            <td><?= $listCountry->name; ?></td>
                            <td><?= $listCountry->iso_3166; ?></td>
                            <td><?= $listCountry->code_abv; ?></td>
                            <td><?= $listCountry->code_numeric; ?></td>
                            <td><?= $listCountry->tax_rate; ?></td>
                            <td style="text-align: center;">
                                <a class="btn btn-xs btn-<?= ($listCountry->is_active == 1) ? 'success' : 'danger'; ?> sw-alert-link hint--top" data-hint="<?= ($listCountry->is_active == 1) ? 'Click To Deactivate' : 'Click To Activate'; ?>" href="<?= $this->Url->build(['prefix' => 'admin', 'controller' => 'Miscellaneous', 'action' => 'changeCountryStatus', $listCountry->id]); ?>"> <i class="fa fa-toggle-<?= ($listCountry->is_active == 1) ? 'on' : 'off'; ?>" ></i>  </a>
                                <a class="btn btn-xs btn-warning hint--top" data-hint="Edit" href="<?= $this->Url->build(['prefix' => 'admin', 'controller' => 'Miscellaneous', 'action' => 'countries', $listCountry->id]); ?>" > <i class ="fa fa-edit"></i> </a>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>  
        </div>
    </div>
</div>
<!-- Sweet-Alert  -->
<script src="plugins/sweet-alert2/sweetalert2.min.js"></script>
<!-- Required datatable js -->
<script src="plugins/datatables/jquery.dataTables.min.js"></script>
<script src="plugins/datatables/dataTables.bootstrap4.min.js"></script>
<script>

    $(document).ready(function () {

        // For Data Tables
        $('#example').DataTable({
            searching: false,
            paging: false,
            info: false
        });

        $('.sw-alert-link').on('click', function (e) {
            e.preventDefault();
            var link = $(this).attr('href');
            let dynamicText = 'This will change the status of the Country';

            swal({
                title: 'Are you sure?',
                text: dynamicText,
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: 'OK',
                cancelButtonText: 'No',
                confirmButtonClass: 'btn btn-success',
                cancelButtonClass: 'btn btn-danger m-l-10',
                buttonsStyling: false
            }).then(function () {
                window.location.href = link;
            });

        });

    });

</script>