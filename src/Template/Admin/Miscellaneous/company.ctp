<!-- Page Title Section Starts -->
<div class="row">
    <div class="col-12">
        <div class="page-title-box">
            <h4 class="page-title float-left">CONFIGURATION: COMPANY DETAILS</h4>

            <ol class="breadcrumb float-right">
                <li class="breadcrumb-item"><a href="<?= h(HTTP_ROOT) ?>admin/dashboard"><i class="fa fa-dashboard"></i>&nbsp;Home</a></li>
            </ol>

            <div class="clearfix"></div>
        </div>
    </div>
</div>
<!-- Page Title Section Ends -->

<!-- Main content -->    
<div class="row">
    <div class="col-md-12">
        <div class="card-box">
            <div class="col-md-9">
                <?= $this->Form->create($companyDtls, ['id' => 'company']); ?>

                <div class="form-group">
                    <label>Company name <span style="color:#FF0000">*</span></label>
                    <?= $this->Form->control('company_name', ['class' => "form-control", 'label' => false, 'required']); ?>
                </div>
                <div class="form-group">
                    <label>Address <span style="color:#FF0000">*</span></label>
                    <?= $this->Form->control('company_address_1', ['class' => "form-control", 'label' => false, 'required']); ?>
                </div>
                <div class="form-group">
                    <label>Additional Address <span style="color:#FF0000"></span></label>
                    <?= $this->Form->control('company_address_2', ['class' => "form-control", 'label' => false]); ?>
                </div>
                <div class="form-group">
                    <label>City <span style="color:#FF0000">*</span></label>
                    <?= $this->Form->control('company_city', ['class' => "form-control", 'label' => false, 'required']); ?>
                </div>
                <div class="form-group">
                    <label>County <span style="color:#FF0000">*</span></label>
                    <?= $this->Form->control('company_county', ['class' => "form-control", 'label' => false, 'required']); ?>
                </div>
                <div class="form-group">
                    <label>Country <span style="color:#FF0000">*</span></label>
                    <?= $this->Form->control('company_country', ['class' => "form-control", 'label' => false, 'required']); ?>
                </div>
                <div class="form-group">
                    <label>Postcode <span style="color:#FF0000">*</span></label>
                    <?= $this->Form->control('company_postcode', ['class' => "form-control", 'label' => false, 'required']); ?>
                </div>
                <div class="form-group">
                    <label>Telephone No <span style="color:#FF0000">*</span></label>
                    <?= $this->Form->control('company_tel', ['class' => "form-control", 'label' => false, 'required']); ?>
                </div>
                <div class="form-group">
                    <label>Fax No <span style="color:#FF0000"></span></label>
                    <?= $this->Form->control('company_fax', ['class' => "form-control", 'label' => false]); ?>
                </div>
                <div class="form-group">
                    <label>Email - Sales <span style="color:#FF0000"></span></label>
                    <?= $this->Form->control('company_email_sales', ['class' => "form-control", 'label' => false]); ?>
                </div>
                <div class="form-group">
                    <label>Email - Enquiries <span style="color:#FF0000">*</span></label>
                    <?= $this->Form->control('company_email_enquiries', ['class' => "form-control", 'label' => false, 'required']); ?>
                </div>
                <div class="form-group">
                    <label>Email - Support <span style="color:#FF0000"></span></label>
                    <?= $this->Form->control('company_email_support', ['class' => "form-control", 'label' => false]); ?>
                </div>
                <div class="form-group">
                    <label>Company Number <span style="color:#FF0000">*</span></label>
                    <?= $this->Form->control('company_number', ['class' => "form-control", 'label' => false, 'required']); ?>
                </div>
                <div class="form-group">
                    <label>VAT Number <span style="color:#FF0000">*</span></label>
                    <?= $this->Form->control('company_vat_no', ['class' => "form-control", 'label' => false, 'required']); ?>
                </div>


                <?= $this->Form->button('Update', ['type' => 'submit', 'class' => 'btn btn-success inline', 'div' => false]) ?>

                <a class="btn btn-danger" href="<?= $this->Url->build(['prefix' => 'admin', 'controller' => 'Appadmins', 'action' => 'dashboard']); ?>">Cancel</a>
                <?= $this->Form->end() ?>      
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function () {
        $('#company').validate({
            rules: {
                company_name: "required",
                company_address_1: "required",
                company_postcode: "required",
                company_city: "required",
                company_county: "required",
                company_country: "required",
                company_tel: "required",
                company_email_enquiries: 
                        {required : true,
                        email : true
                        },
                company_number: "required",
                company_vat_no: "required"
            },
            messages: {
                company_name: "Company name can't be blank",
                company_address_1: "Company address can't be blank",
                company_postcode: "Company postcode can't be blank",
                company_city: "City can't be blank",
                company_county: "County can't be blank",
                company_country: "Country can't be blank",
                company_tel: "Company telephone no. can't be blank",
                company_email_enquiries: { required :"Enquiry email can't be blank",
                                           email : "Enter a valid email" },
                company_number: "Company number can't be blank",
                company_vat_no: "Company vat number can't be blank"
            }

        });

    });
</script>