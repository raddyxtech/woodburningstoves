<link href="plugins/sweet-alert2/sweetalert2.min.css" rel="stylesheet" type="text/css">
<!-- Page Title Section Starts -->
<div class="row">
    <div class="col-12">
        <div class="page-title-box">
            <h4 class="page-title float-left">List Faq's</h4>

            <ol class="breadcrumb float-right">
                <li class="breadcrumb-item"><a href="<?= h(HTTP_ROOT) ?>admin/dashboard"><i class="fa fa-dashboard"></i>&nbsp;Home</a></li>
                <li class="breadcrumb-item active"><a href="javascript:;">Manage Faq's</a></li>
               <li><a href="<?= $this->Url->build(['controller' => 'Miscellaneous', 'action' => 'addNewFaq']) ?>" class="btn btn-xs btn-success">Add New Faq's</a></li>
            </ol>

            <div class="clearfix"></div>
        </div>
    </div>
</div>
<!-- Page Title Section Ends -->

<!-- Main content -->    
<div class="row">
    <div class="col-md-12">
        <div class="card-box">
            <table id="datatable" class="table table-bordered table-striped users-list">
                <thead>
                    <tr>
                        <th>Question</th>
                        <th>Answer</th>
                        <th style="text-align: center;">Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($faqs as $faq): ?>
                        <tr id="<?= $faq->id; ?>" class="message_box">  
                            <td><?= $faq->question ?></td>
                            <td><?= $faq->answer ?></td>
                            <td style="text-align: center;">    
                                <a class="btn btn-xs btn-warning hint--top" data-hint="Edit" href="<?= $this->Url->build(['prefix'=>'admin','controller'=>'Miscellaneous','action'=>'addNewFaq',$faq->unique_id]); ?>" > <i class ="fa fa-edit"></i> </a>
                                <a href="javascript:;" class="btn btn-xs bg-pink text-white hint--left delete-image" data-hint="Delete Faq?" data-url="<?= $this->Url->build(['prefix' => 'admin', 'controller' => 'Miscellaneous', 'action' => 'deleteFaq', $faq->unique_id]); ?>" > <i class ="fa fa-trash"></i> </a>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>  
        </div>
    </div>
</div>
<!-- Sweet-Alert  -->
<script src="plugins/sweet-alert2/sweetalert2.min.js"></script>
<!-- Required datatable js -->
<script src="plugins/datatables/jquery.dataTables.min.js"></script>
<script src="plugins/datatables/dataTables.bootstrap4.min.js"></script>

<script>
    $(document).ready(function () {
        $('.delete-image').click(function () {
            var delUserUrl = $(this).data('url');
            swal({
                title: 'Are you sure?',
                text: "Faq will be deleted, this can\'t be undone!",
                type: 'error',
                showCancelButton: true,
                confirmButtonColor: '#d57171',
                confirmButtonText: 'Delete'
            }).then(function () {
                window.location = delUserUrl;
            });
        });
    });
</script>