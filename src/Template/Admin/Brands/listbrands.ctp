<link href="plugins/sweet-alert2/sweetalert2.min.css" rel="stylesheet" type="text/css">
<!-- Page Title Section Starts -->
<div class="row">
    <div class="col-12">
        <div class="page-title-box">
            <h4 class="page-title float-left">List Brands</h4>
            <ol class="breadcrumb float-right">
                <li class="breadcrumb-item"><a href="<?= h(HTTP_ROOT) ?>admin/dashboard"><i class="fa fa-dashboard"></i>&nbsp;Home</a></li>
                <li class="breadcrumb-item active"><a href="javascript:;">Manage Brands</a></li>
                <li class="breadcrumb-item active"><a href="javascript:;">List Brands</a></li>
            </ol>
            <div class="clearfix"></div>
        </div>
    </div>
</div>
<!-- Page Title Section Ends -->
<!-- Main content -->    
<div class="row">
    <div class="col-md-12">
        <div class="card-box">
            <table id="datatable" class="table table-bordered table-striped users-list">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Photo</th>
                        <th style="text-align: center;">Status</th>
                        <th style="text-align: center;">Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($brands as $brand): ?>
                        <tr id="<?= $brand->id; ?>" class="message_box">  
                            <td><?= $brand->name ?></td>
                            <td style="text-align: center;">
                                <div class="img_div">
                                    <img src="<?= HTTP_ROOT . BRAND_PIC . (!empty($brand->image) ? $brand->image : 'no-photo.jpg'); ?>" alt="Image Not Present" >
                                </div>
                            </td>
                            <td style="text-align: center;">
                                <a class="btn btn-xs btn-<?= ($brand->is_active == 1) ? 'success' : 'danger'; ?> sw-alert-link hint--top" data-hint="<?= ($brand->is_active == 1) ? 'Click To Deactivate' : 'Click To Activate'; ?>" href="<?= $this->Url->build(['prefix' => 'admin', 'controller' => 'Brands', 'action' => 'changeStatus', $brand->unique_id]); ?>"> <i class="fa fa-toggle-<?= ($brand->is_active == 1) ? 'on' : 'off'; ?>" ></i>  </a>
                            </td>
                            <td style="text-align: center;">
                                <a class="btn btn-xs btn-warning hint--top" data-hint="Edit" href="<?= $this->Url->build(['prefix' => 'admin', 'controller' => 'Brands', 'action' => 'addBrand', $brand->unique_id]); ?>" > <i class ="fa fa-edit"></i> </a>
                                <a href="javascript:;" class="btn btn-xs bg-pink text-white hint--left delete-user" data-hint="Delete Brand?" data-url="<?= $this->Url->build(['prefix' => 'admin', 'controller' => 'Brands', 'action' => 'deleteBrand', $brand->unique_id]); ?>" > <i class ="fa fa-trash"></i> </a>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>  
        </div>
    </div>
</div>
<!-- Sweet-Alert  -->
<script src="plugins/sweet-alert2/sweetalert2.min.js"></script>
<!-- Required datatable js -->
<script src="plugins/datatables/jquery.dataTables.min.js"></script>
<script src="plugins/datatables/dataTables.bootstrap4.min.js"></script>
<script>
    $(document).ready(function () {
        $('.delete-user').click(function () {
            var delUserUrl = $(this).data('url');
            swal({
                title: 'Are you sure?',
                text: "Brand will be deleted, this can\'t be undone!",
                type: 'error',
                showCancelButton: true,
                confirmButtonColor: '#d57171',
                confirmButtonText: 'Delete'
            }).then(function () {
                window.location = delUserUrl;
            });
        });
        // For Data Tables
        $('#example').DataTable({
            searching: false,
            paging: false,
            info: false
        });
        $('.sw-alert-link').on('click', function (e) {
            e.preventDefault();
            var link = $(this).attr('href');
            let dynamicText = 'This will change the status of the Brand';
            if (link.indexOf('resetPassword') > 0) {
                dynamicText = 'This will reset the password of the Brand';
            }
            swal({
                title: 'Are you sure?',
                text: dynamicText,
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: 'OK',
                cancelButtonText: 'No',
                confirmButtonClass: 'btn btn-success',
                cancelButtonClass: 'btn btn-danger m-l-10',
                buttonsStyling: false
            }).then(function () {
                window.location.href = link;
            });
        });
    });
</script>