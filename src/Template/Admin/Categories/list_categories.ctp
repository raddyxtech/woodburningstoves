
<link href="plugins/sweet-alert2/sweetalert2.min.css" rel="stylesheet" type="text/css">
<!-- Page Title Section Starts -->
<div class="row">
    <div class="col-12">
        <div class="page-title-box">
            <h4 class="page-title float-left">List Categories</h4>
            <ol class="breadcrumb float-right">
                <li class="breadcrumb-item"><a href="<?= h(HTTP_ROOT) ?>admin/dashboard"><i class="fa fa-dashboard"></i>&nbsp;Home</a></li>
                <li class="breadcrumb-item active"><a href="javascript:;">Manage category</a></li>
                <li class="breadcrumb-item active"><a href="javascript:;">List Categories</a></li>
            </ol>
            <div class="clearfix"></div>
        </div>
    </div>
</div>   
<div class="row">
    <div class="col-md-12">
        <div class="card-box">
            <table id="datatable" class="table table-bordered table-striped users-list">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th style="text-align: center;">Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($listCategories as $categories): ?>
                        <?php if (is_object($categories->parent_category)) { ?>
                            <tr id="<?php echo $categories->parent_id; ?>" class="message_box">
                                <td><?= $categories->parent_category->name . ': ' . $categories->name ?></td>
                                <td style="text-align: center;">
                                    <a class="btn btn-xs btn-warning hint--top" data-hint="Edit" href="<?= $this->Url->build(['prefix' => 'admin', 'controller' => 'Categories', 'action' => 'addCategories', $categories->parent_id]); ?>" > <i class ="fa fa-edit"></i> </a>
                                    <a href="javascript:;" class="btn btn-xs bg-pink text-white hint--left delete-user" data-hint="Delete Catagory?" data-url="<?= $this->Url->build(['prefix' => 'admin', 'controller' => 'Categories', 'action' => 'deleteCategory', $categories->parent_id]); ?>" > <i class ="fa fa-trash"></i> </a> 
                                <?php } ?>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>  
        </div>
    </div>
</div>
<!-- Sweet-Alert  -->
<script src="plugins/sweet-alert2/sweetalert2.min.js"></script>
<!-- Required datatable js -->
<script src="plugins/datatables/jquery.dataTables.min.js"></script>
<script src="plugins/datatables/dataTables.bootstrap4.min.js"></script>
<script>
    $(document).ready(function () {
        $('.delete-user').click(function () {
            var delUserUrl = $(this).data('url');
            swal({
                title: 'Are you sure?',
                text: "Accoount will be deleted, this can\'t be undone!",
                type: 'error',
                showCancelButton: true,
                confirmButtonColor: '#d57171',
                confirmButtonText: 'Delete'
            }).then(function () {
                window.location = delUserUrl;
            });
        });
        // For Data Tables
        $('#example').DataTable({
            searching: false,
            paging: false,
            info: false
        });
        $('.sw-alert-link').on('click', function (e) {
            e.preventDefault();
            var link = $(this).attr('href');
            let dynamicText = 'This will change the status of the Manager';
            if (link.indexOf('resetPassword') > 0) {
                dynamicText = 'This will reset the password of the Manager';
            }
            swal({
                title: 'Are you sure?',
                text: dynamicText,
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: 'OK',
                cancelButtonText: 'No',
                confirmButtonClass: 'btn btn-success',
                cancelButtonClass: 'btn btn-danger m-l-10',
                buttonsStyling: false
            }).then(function () {
                window.location.href = link;
            });
        });
    });
</script>