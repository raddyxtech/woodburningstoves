<style>
    .btn-purple { height: 38px; }
</style>
<!-- Page Title Section Starts -->
<div class="row">
    <div class="col-12">
        <div class="page-title-box">
            <h4 class="page-title float-left">Manage Category</h4>
            <ol class="breadcrumb float-right">
                <li class="breadcrumb-item"><a href="<?= h(HTTP_ROOT) ?>admin/dashboard"><i class="fa fa-dashboard"></i>&nbsp;Home</a></li>
                <li class="breadcrumb-item active"><a href="javascript:;">Manage Manager</a></li>
                <li class="breadcrumb-item active"><a href="javascript:;">Add Manager</a></li>
            </ol>
            <div class="clearfix"></div>
        </div>
    </div>
</div>
<!-- Page Title Section Ends -->
<!-- Main content -->    
<div class="row">
    <div class="col-md-12">
        <div class="card-box">
            <!--<h4 class="m-t-0 header-title">Add Category</h4>-->
            <p class="text-muted m-b-30 font-13">
                All (<span style="color:#FF0000">*</span>) fields are mandatory
            </p>
            <!--//<= $this->Form->create($user, ['id' => 'wizard-callbacks', 'type' => 'file']) ?>-->
            <?= $this->Form->create($category, ['id' => 'wizard-callbacks', 'type' => 'file']) ?>
            <div class="row">
                <div class="col-md-6">
                    <div class="card-box">
                        <!--<h4 class="m-t-0 m-b-30 header-title">Basic Information</h4>-->
                        <div class="form-group">
                            <label> Parent Category: <span style="color:#FF0000">*</span></label>
                            <?= $this->Form->select('parent_id', $categoryList, ['empty' => 'Select category', 'id' => 'cat', 'label' => false, 'class' => 'form-control']); ?>
                        </div>
                        <div class="form-group">
                            <label>New Category: <span style="color:#FF0000">*</span></label>
                            <?= $this->Form->control('name', ['placeholder' => "Enter Category Name", 'class' => "form-control", 'div' => false, 'label' => false]); ?>
                        </div>
                    </div>
                </div>
            </div>
            <button type="submit" class="btn btn-primary" id="pic-update-btn">Submit</button>
            <a class="btn btn-danger" href="<?= $this->Url->build(['prefix' => 'admin', 'controller' => 'Categories', 'action' => 'listCategories']); ?>">Cancel</a>
            <?= $this->Form->end() ?>
        </div>    
    </div>
</div>
<!-- For File Browser  -->
<script src="plugins/bootstrap-filestyle/js/bootstrap-filestyle.min.js" type="text/javascript"></script>
<!-- For Croppie Plugin  -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/croppie/2.6.2/croppie.css" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/exif-js/2.3.0/exif.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/croppie/2.6.2/croppie.js"></script>
<script>
            $(document).ready(function() {
    $myCroppie = $('.uploaded-img').croppie({
    enableExif: true,
<?php if (!empty($user->profile_pic)) { ?>
        url: '<?= HTTP_ROOT . PROFILE_PIC . $user->profile_pic ?>',
<?php } ?>
    viewport: {
    width: 150,
            height: 150,
            type: 'square'
    },
            boundary: {
            width: 200,
                    height: 200
            }
    });
            $('#file-browser').on('change', function () {
    var reader = new FileReader();
            reader.onload = function (e) {
            $myCroppie.croppie('bind', {
            url: e.target.result
            });
            };
            reader.readAsDataURL(this.files[0]);
            $('#profilePic').prop('disabled', false);
    });
            $('#pic-update-btn').on('click', function () {
    $myCroppie.croppie('result').then(function (resp) {
    $('#profilePic').val(resp);
    });
    });
            $('#wizard-callbacks').validate({
    ignore: '*:not([name])',
            rules: {
            "admin[name]": "required",
                    email: {
                    required: true,
                            email: true
                    }
            },
            messages: {
            "admin[name]": "Name Required",
                    email: {
                    required: 'Email Required',
                            email: 'Please Eneter A Valid Email'
                    }
            }
    });
    });
</script>