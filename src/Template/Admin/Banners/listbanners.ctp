
<link href="plugins/sweet-alert2/sweetalert2.min.css" rel="stylesheet" type="text/css">
<div class="row" style="display: none;" id="form">
    <div class="col-md-12">
        <div class="card-box">
            <div class="row m-t-20">
                <div class="col-md-12">
                    <div class="card-box">
                        <!--<h4 class="m-t-0 m-b-30 header-title">Basic Information</h4>-->
                        <?= $this->Form->create(null, ['id' => 'image', 'method' => 'POST', 'type' => 'file']); ?>
                        <div class="form-group">
                            <label> New Image: </label>
                            <?= $this->Form->control('filename', ['type' => 'file', 'label' => FALSE]) ?></div>
                        <div class="form-group">
                            <label> New Image Title:</label>
                            <?= $this->Form->control('title', ['placeholder' => "Enter image title", 'class' => "form-control", 'div' => false, 'label' => false]); ?>
                        </div>
                    </div>
                </div>
            </div>
            <?= $this->Form->button('Submit', ['class' => 'btn btn-primary', 'id' => 'pic-update-btn']); ?>
            <?= $this->Form->end() ?>
        </div>    
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="card-box">
            <table class="table table-bordered table-striped users-list">
                <thead>
                    <tr>
                        <th>Title/Name:</th>
                        <th>Description:</th>
                        <th style="text-align: center;">Image:</th>
                        <th style="text-align: center;">Status</th>
                        <th style="text-align: center;">Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    foreach ($images as $image) { ?>
                        <tr>
                            <td><?= $image->title ?></td>
                            <td><?= $image->description ?></td>
                            <td style="text-align: center;">
                                <img src="<?= HTTP_ROOT . 'files/banners/' . $image->image_name; ?>" height="80px" width="60px"/>
                            </td>   files
                            <td  style="text-align: center;">
                                <a class="btn btn-xs btn-<?= ($image->is_active == 1) ? 'success' : 'danger'; ?> sw-alert-link hint--top" data-hint="<?= ($image->is_active == 1) ? 'Click To Deactivate' : 'Click To Activate'; ?>" href="<?= $this->Url->build(['prefix' => 'admin', 'controller' => 'Banners', 'action' => 'changeStatus', $image->id]); ?>"> <i class="fa fa-toggle-<?= ($image->is_active == 1) ? 'on' : 'off'; ?>" ></i>  </a>
                            </td>
                            <td style="text-align: center;">
                                <a class="btn btn-xs btn-warning hint--top" data-hint="Edit" href="<?= $this->Url->build(['prefix' => 'admin', 'controller' => 'Banners', 'action' => 'addBanner', $image->id]); ?>" > <i class ="fa fa-edit"></i> </a>
                                <a href="javascript:;" class="btn btn-xs bg-pink text-white hint--left delete-user" data-hint="Delete image?" data-url="<?= $this->Url->build(['prefix' => 'admin', 'controller' => 'Banners', 'action' => 'deleteimage', $image->id]); ?>" > <i class ="fa fa-trash"></i> </a> 
                            </td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>  
        </div>
    </div>
</div>
<script src="plugins/sweet-alert2/sweetalert2.min.js"></script>
<script src="plugins/datatables/jquery.dataTables.min.js"></script>
<script src="plugins/datatables/dataTables.bootstrap4.min.js"></script>
<script>
//    document.getElementById("addImagelist").onclick = function () {
//        $("#form").show();
//    };
    $(document).ready(function () {
        $('.delete-user').click(function () {
            var delUserUrl = $(this).data('url');
            swal({
                title: 'Are you sure?',
                text: "It will be deleted, this can\'t be undone!",
                type: 'error',
                showCancelButton: true,
                confirmButtonColor: '#d57171',
                confirmButtonText: 'Delete'
            }).then(function () {
                window.location = delUserUrl;
            });
        });
        $('#example').DataTable({
            searching: false,
            paging: false,
            info: false
        });
        $('.sw-alert-link').on('click', function (e) {
            e.preventDefault();
            var link = $(this).attr('href');
            let dynamicText = 'This will change the status of the Manager';
            if (link.indexOf('resetPassword') > 0) {
                dynamicText = 'This will reset the password of the Manager';
            }
            swal({
                title: 'Are you sure?',
                text: dynamicText,
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: 'OK',
                cancelButtonText: 'No',
                confirmButtonClass: 'btn btn-success',
                cancelButtonClass: 'btn btn-danger m-l-10',
                buttonsStyling: false
            }).then(function () {
                window.location.href = link;
            });
        });
    });
</script>