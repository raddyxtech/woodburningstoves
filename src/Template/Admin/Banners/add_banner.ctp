<script>
    <link rel="stylesheet" type="text/css" href="admin.css"/>
</script>
    <style>
        .btn-purple { height: 38px; }
    </style>
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <h4 class="page-title float-left">Add Image</h4>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12"> 
            <div class="card-box">
                <div class="row m-t-20">
                    <div class="col-md-12">
                        <div class="card-box">
                            <!--<h4 class="m-t-0 m-b-30 header-title">Basic Information</h4>-->
                            <?= $this->Form->create($image, ['id' => 'image', 'method' => 'POST', 'type' => 'file']); ?>
                            <div class="form-group">
                                <?php if (!empty($image->image_name)) { ?>
                                    <div class="form-group">
                                        <label style="margin-left:101px;">
                                            <img src="<?= HTTP_ROOT . 'files/banners/' . $image->image_name; ?>" height="100px" width="100px" style="float:left; margin: 9px 0px -20px -216px;"/>
                                        </label>
                                        <label style="float:left; margin-left:93px;">
                                            <a href="javascript:;" class="btn btn-xs bg-pink text-white hint--left delete-user" data-hint="Remove image?" data-url="<?= $this->Url->build(['prefix' => 'admin', 'controller' => 'Banners', 'action' => 'delimages', $image->id]); ?>" ><i class="fa fa-window-close-o"></i></a> 
                                        </label>
                                    <?php } else { ?>
                                        <label> Add Image: </label>
                                        <?= $this->Form->control('image_name', ['type' => 'file', 'label' => FALSE]) ?>
                                    <?php } ?>
                                </div>

                                <div class="form-group">
                                    <label> Image Title:</label>
                                    <?= $this->Form->control('title', ['placeholder' => "Enter image title", 'class' => "form-control", 'div' => false, 'label' => false]); ?>
                                </div>
                                <div class="form-group">
                                    <label> Description:</label>
                                    <?= $this->Form->textarea('description', ['placeholder' => "Enter Description", 'class' => "form-control", 'div' => false, 'label' => false]); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?= $this->Form->button('Submit', ['class' => 'btn btn-primary', 'id' => 'pic-update-btn']); ?>
                    <a class="btn btn-danger" href="<?= $this->Url->build(['prefix' => 'admin', 'controller' => 'Banners', 'action' => 'listbanners']); ?>" style="margin-left: 6px;">Cancel</a>

                    <?= $this->Form->end() ?>
                </div>    
            </div>
        </div>
        <?= $this->Html->script('/plugins/ckeditor/ckeditor'); ?>      
        <!-- For File Browser  -->
        <script src="plugins/bootstrap-filestyle/js/bootstrap-filestyle.min.js" type="text/javascript"></script>
        <link href="plugins/sweet-alert2/sweetalert2.min.css" rel="stylesheet" type="text/css">
        <script src="plugins/sweet-alert2/sweetalert2.min.js"></script>
        <script>
    $(document).ready(function () {
        $(document).on('click', '.delete-user', function () {
            var delUserUrl = $(this).data('url');
            swal({
                title: 'Are you sure?',
                text: "It will be deleted, this can\'t be undone!",
                type: 'error',
                showCancelButton: true,
                confirmButtonColor: '#d57171',
                confirmButtonText: 'Delete'
            }).then(function () {
                window.location = delUserUrl;
            });
        });
    });

        </script>