<div class="row">
    <div class="col-12">
        <div class="page-title-box">
            <h4 class="page-title float-left">Change Password</h4>
            <ol class="breadcrumb float-right">
                <li class="breadcrumb-item"><a href="<?= h(HTTP_ROOT) ?>admin/dashboard"><i class="fa fa-dashboard"></i>&nbsp;Home</a></li>
                <li class="breadcrumb-item active"><a href="javascript:;">Change Password</a></li>
            </ol>
            <div class="clearfix"></div>
        </div>
    </div>
</div>
<!-- Page Title Section Ends -->

<!-- Main content -->    
<div class="row">
    <div class="col-md-12">
        <div class="card-box">

            <h4 class="m-t-0 header-title">Change Password</h4>
            <p class="text-muted m-b-30 font-13">
                All (<span style="color:#FF0000">*</span>) fields are mandatory
            </p>

            <?= $this->Form->create(NULL, ['id' => 'profile']); ?>
            <div class="row">
                <div class="col-md-9">
                    <div class="card-box">
                        <div class="form-group">
                            <label for="curr_password">Current Password<span style="color:#FF0000">*</span></label>
                            <?= $this->Form->control('curr_password', ['type' => 'password', 'placeholder' => "Enter current password", 'class' => "form-control", 'label' => false, 'kl_virtual_keyboard_secure_input' => "on"]); ?>
                        </div>
                        <div class="form-group">
                            <label for="password">Password<span style="color:#FF0000">*</span></span></label>
                            <?= $this->Form->control('password', ['placeholder' => "Enter password", 'class' => "form-control", 'label' => false, 'kl_virtual_keyboard_secure_input' => "on"]); ?>
                        </div> 
                        <div class="form-group">
                            <label for="password">Confirm Password<span style="color:#FF0000">*</span></span></label>
                            <?= $this->Form->control('re_password', ['placeholder' => "Retype password",'type' => 'password', 'class' => "form-control", 'label' => false, 'kl_virtual_keyboard_secure_input' => "on"]); ?>
                        </div> 
                        <div class="box-footer">                       
                            <?= $this->Form->submit('Change password', ['type' => 'submit', 'class' => 'btn btn-primary', 'name' => 'changepassword', 'style' => 'margin-left:15px;']) ?> 
                        </div>
                    </div>
                    <?= $this->Form->end() ?>
                </div>
            </div>
            <!--                <button type="submit" class="btn btn-primary" id="pic-update-btn">Submit</button>-->

            </form>
        </div>    
    </div>
</div>
<script>
    $("#profile").validate({
        rules: {
            curr_password: "required",
            password: "required",
            re_password: {
                required: true,
                equalTo: password
            }
        },
        messages: {
            curr_password: "Please provide a password",
            password: "Please enter a new password",
            re_password: {
                required: 'Retype password required',
                equalTo: 'Retype password did not match'
            }
        },
        submitHandler: function (form) {
            form.submit();
        }
    });
</script>

