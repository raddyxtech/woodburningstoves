<?php $paramNamed = $this->request->getQuery(); ?>
<style>
   .single-check.custom-control.custom-checkbox{display: flex;align-content: center;justify-content: center;}
</style>
<!-- Page Title Section Starts -->
<div class="row">
    <div class="col-12">
        <div class="page-title-box">
            <h4 class="page-title float-left">Manager's Permissions</h4>

            <ol class="breadcrumb float-right">
                <li class="breadcrumb-item"><a href="<?= h(HTTP_ROOT) ?>admin/dashboard"><i class="fa fa-dashboard"></i>&nbsp;Home</a></li>
                <li class="breadcrumb-item active"><a href="javascript:;">Manager's Permissions</a></li>
            </ol>

            <div class="clearfix"></div>
        </div>
    </div>
</div>
<!-- Main content -->    
<div class="row">
    <div class="col-md-12">
        <div class="card-box table-responsive">
             <?= $this->Form->create($managerPermission); ?>
            <table id="datatable" class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th>Name</th>                                          
                        <th style="text-align: center;">Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($allPermissions as $key=>$permission): ?>
                        <tr class="message_box">                           
                            <td><?=  $this->Form->label($key); ?></td>                            
                            <td style="text-align: center;">
                                <div class="single-check custom-control custom-checkbox">
                                       <?= $this->Form->checkbox($key,['value'=>1,'class'=>'custom-control-input','id'=>$key]); ?>
                                       <label class="custom-control-label" for="<?= $key; ?>"></label>
                                </div>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>  
            <div class="box-footer submit" >
                    <?= $this->Form->button('Submit', ['type' => 'submit', 'class' => 'btn btn-success']); ?>
                    <a class="btn btn-danger" href="<?= $this->Url->build(['prefix'=>'admin','controller'=>'Appadmins','action'=>'dashboard']); ?>">Cancel</a>                          
            </div>
            <?= $this->Form->end(); ?>
        </div>
    </div>
</div>
