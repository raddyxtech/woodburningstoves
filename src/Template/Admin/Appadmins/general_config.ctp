

<!-- Page Title Section Starts -->
<div class="row">
    <div class="col-12">
        <div class="page-title-box">
            <h4 class="page-title float-left">Application Config</h4>

            <ol class="breadcrumb float-right">
                <li class="breadcrumb-item"><a href="<?= h(HTTP_ROOT) ?>admin/dashboard"><i class="fa fa-dashboard"></i>&nbsp;Home</a></li>
                <li class="breadcrumb-item active"><a href="javascript:;">Settings</a></li>
                <li class="breadcrumb-item active"><a href="javascript:;">Application Config</a></li>
            </ol>

            <div class="clearfix"></div>
        </div>
    </div>
</div>
<!-- Page Title Section Ends -->


<!-- Main content -->    
<div class="row">
    <div class="col-md-12">
        <div class="card-box">
            <?= $this->Form->create($dataConfigs) ?>
            <div class="form-row">
                <div class="form-group col-md-6">
                    <?php echo $this->Form->control('sparkpost_key', ['style' => 'width:450px;', 'class' => 'form-control', 'id' => 'setting', 'type' => 'text', 'label' => 'SPARKPOST KEY', 'required' => false]); ?>
                </div>
                <div class="form-group col-md-6">
                    <?php echo $this->Form->control('admin_email', ['style' => 'width:450px;', 'class' => 'form-control', 'id' => 'setting', 'type' => 'text', 'label' => 'ADMIN EMAIL', 'required' => false]); ?>
                </div>
                <div class="form-group col-md-6">
                    <?php echo $this->Form->control('from_email', ['style' => 'width:450px;', 'class' => 'form-control', 'id' => 'setting', 'type' => 'text', 'label' => 'FROM EMAIL', 'required' => false]); ?>
                </div>
                <div class="form-group col-md-6">
                    <?php echo $this->Form->control('bcc_email', ['style' => 'width:450px;', 'class' => 'form-control', 'id' => 'setting', 'type' => 'text', 'label' => 'BCC EMAIL', 'required' => false]); ?>
                </div>
                <div class="form-group col-md-6">
                    <?php echo $this->Form->control('google_api_key', ['style' => 'width:450px;', 'class' => 'form-control', 'id' => 'setting', 'type' => 'text', 'label' => 'GOOGLE API KEY', 'required' => false]); ?>
                </div>
                <div class="form-group col-md-6">
                    <?php echo $this->Form->control('get_address_api_key', ['style' => 'width:450px;', 'class' => 'form-control', 'id' => 'setting', 'type' => 'text', 'label' => 'GET ADDRESS API KEY', 'required' => false]); ?>
            </div>
            </div>
            <?= $this->Form->button('Submit', ['class' => 'btn btn-primary']) ?>
            <?= $this->Form->end() ?>      
        </div>
    </div>
</div>

