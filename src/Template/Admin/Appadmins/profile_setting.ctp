

<!-- Page Title Section Starts -->
<div class="row">
    <div class="col-12">
        <div class="page-title-box">
            <h4 class="page-title float-left">Profile Setting</h4>

            <ol class="breadcrumb float-right">
                <li class="breadcrumb-item"><a href="<?= h(HTTP_ROOT) ?>admin/dashboard"><i class="fa fa-dashboard"></i>&nbsp;Home</a></li>
                <li class="breadcrumb-item active"><a href="javascript:;">Profile Setting</a></li>
            </ol>

            <div class="clearfix"></div>
        </div>
    </div>
</div>
<!-- Page Title Section Ends -->


<!-- Main content -->    
<div class="row">
    <div class="col-md-12">
        <div class="card-box">
            <p class="note">All (<span style="color:#FF0000">*</span>) fields are mandatory</p>
            <div class="row">

                <!-- Profile Setting Form Starts Here -->
                <div class="col-md-6">
                    <div class="card-box">
                        <h4 class="m-t-0 m-b-30 header-title">Profile Setting</h4>
                        <?= $this->Form->create($user, ['type' => 'file', 'id' => 'profileForm']) ?>
                        <div class="form-group">
                             <?= $this->Form->control('title',[
                                'options'=>['Mr.'=>'Mr.','Mrs.'=>'Mrs.','Miss.'=>'Miss.','Ms.'=>'Ms.','Dr'=>'Dr','Prof.'=>'Prof.'],
                                'type'=>'radio',
                                'class'=>'custom-control-input',
                                'templates' => [
                                                'nestingLabel' => '{{hidden}}{{input}}<label{{attrs}} class="custom-control-label">{{text}}</label> &nbsp;',
                                                'radioWrapper' => '<div class="custom-control custom-radio" style="display:inline;">{{label}}</div>',
                                                'radioContainer' => '<div class="form-group">{{content}}</div>'
                                             ],
                                'label'=>['text'=>'Title <span style="color:#FF0000">*</span>','escape'=>false,'class'=>'d-block'],
                                'hiddenField'=>false
                            ]); ?>
                        </div>
                        <div class="form-group">
                            <label>First Name <span style="color:#FF0000">*</span></label>
                            <?= $this->Form->control('first_name', ['placeholder' => "Enter First Name", 'class' => "form-control", 'div' => false, 'label' => false]); ?>
                        </div>
                        <div class="form-group">
                            <label>Last Name <span style="color:#FF0000">*</span></label>
                            <?= $this->Form->control('last_name', ['placeholder' => "Enter Last Name", 'class' => "form-control", 'div' => false, 'label' => false]); ?>
                        </div>
                        <div class="form-group">
                            <label for="email">Username</label>
                            <?= $this->Form->control('username', ['placeholder' => "Enter username", 'class' => "form-control",'id' => 'usernameID', 'div' => false, 'label' => false]); ?>
                        </div>
                        <div class="form-group">
                            <label for="email">Login Email</label>
                            <?= $this->Form->control('email', ['placeholder' => "Enter login email", 'class' => "form-control", 'style' => 'cursor: not-allowed;', 'div' => false, 'label' => false]); ?>
                        </div>
                        <input type="submit" class="btn btn-success" value="Update">
                        <a class="btn btn-danger" href="<?= HTTP_ROOT . "admin/dashboard" ?>">Cancel</a>
                        <?= $this->Form->end() ?>
                    </div>
                </div>
                <!-- Profile Setting Form Ends Here -->

                <!-- Change Password Form Starts Here -->
                <div class="col-md-6">
                    <div class="card-box">
                        <h4 class="m-t-0 m-b-30 header-title">Change Password</h4>

                        <?= $this->Form->create(NULL, ['id' => 'passwordForm', 'url' => ['controller' => 'Appadmins', 'action' => 'changePassword']]); ?>
                        <div class="form-group">
                            <label for="curr_password">Current Password <span style="color:#FF0000">*</span></label>
                            <?= $this->Form->control('curr_password', ['type' => 'password', 'placeholder' => "Enter current password", 'class' => "form-control", 'label' => false, 'kl_virtual_keyboard_secure_input' => "on"]); ?>
                        </div>
                        <div class="form-group">
                            <label for="password">Password <span style="color:#FF0000">*</span></span></label>
                            <?= $this->Form->control('password', ['placeholder' => "Enter password", 'class' => "form-control", 'label' => false, 'kl_virtual_keyboard_secure_input' => "on"]); ?>
                        </div>
                        <div class="form-group">
                            <label for="re_password">Confirm Password <span style="color:#FF0000">*</span></label>
                            <?= $this->Form->control('re_password', ['placeholder' => "Enter confirm password", 'type' => 'password', 'class' => "form-control", 'label' => false, 'kl_virtual_keyboard_secure_input' => "on"]); ?>
                        </div>
                        <input type="submit" class="btn btn-success" value="Change password">
                        <input type="reset" class="btn btn-danger" value="Reset">
                        <?= $this->Form->end() ?>
                    </div>
                </div>
                <!-- Profile Setting Form Ends Here -->

            </div>
        </div>
    </div>
</div>



<script>
    $('#profileForm').validate({
        rules: {
            "username": {
                    regex: /^[a-zA-Z]([._-]?[a-zA-Z0-9]+)*$/i,
                    remote: {
                        url:"users/checkUserName",
                                data:{username:function(){return $('#usernameID').val(); }},
                                async:false,
                                beforeSend:function(){
                                $('#usernameID').parent('div').addClass('inputLoader');
                                }, complete:function(){
                        $('#usernameID').parent('div').removeClass('inputLoader');
                        }
                     }
            }
        },
        messages: {
            "username": {
                            regex:"Username must be aplanumeric,no gaps(.,_,- allowed).",
                            remote:"Username already exists!!"
            }
        },
        submitHandler: function (form) {
            form.submit();
        }
    });
    
    $.validator.addMethod("regex",
                    function(value, element, regexp) {
                        var re = new RegExp(regexp);
                        return this.optional(element) || re.test(value);
                    },"Invalid pattern.");
                    
    $("#passwordForm").validate({
        rules: {
            curr_password: "required",
            password: "required",
            re_password: {
                required: true,
                equalTo: password
            }
        },
        messages: {
            curr_password: "Please provide a password",
            password: "Please enter a new password",
            re_password: {
                required: 'Retype password required',
                equalTo: 'Retype password did not match'
            }
        },
        submitHandler: function (form) {
            form.submit();
        }
    });
</script>