<style>
    .header-title { text-align: center; }
</style>

<input type="hidden" value="" id="checkboxcount"  /> 

<!-- Page Title Section Starts -->
<div class="row">
    <div class="col-12">
        <div class="page-title-box">
            <h4 class="page-title float-left">Dashboard</h4>

            <ol class="breadcrumb float-right">
                <li class="breadcrumb-item active"><a href="javascript:;"><i class="fa fa-dashboard"></i>&nbsp;Home</a></li>
            </ol>

            <div class="clearfix"></div>
        </div>
    </div>
</div>
<!-- Page Title Section Ends -->

<!-- Main content -->    
<div class="row">
    <div class="col-md-12">
        <div class="card-box" style="height: 60vh;">
            <h1 class="m-t-20 m-b-30 header-title" style="font-size: 22px;">Welcome to <a href="<?= HTTP_ROOT ?>" target="_blank"><?= SITE_NAME_TEXT ?></a> Admin Panel.</h1>
        </div>
    </div>
</div>

