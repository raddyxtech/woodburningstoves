

<!-- Page Title Section Starts -->
<div class="row">
    <div class="col-12">
        <div class="page-title-box">
            <h4 class="page-title float-left">Edit Email Templates</h4>

            <ol class="breadcrumb float-right">
                <li class="breadcrumb-item"><a href="<?= h(HTTP_ROOT) ?>admin/dashboard"><i class="fa fa-dashboard"></i>&nbsp;Home</a></li>
                <li class="breadcrumb-item active"><a href="<?= HTTP_ROOT ?>admin/appadmins/email-templates">Email Templates</a></li>
                <li class="breadcrumb-item active"><a href="javascript:;">Edit Email Templates</a></li>
            </ol>

            <div class="clearfix"></div>
        </div>
    </div>
</div>
<!-- Page Title Section Ends -->

<!-- Main content -->    
<div class="row">
    <div class="col-md-12">
        <div class="card-box">
            <?= $this->Form->create($emailTemplate); ?>

            <div class="form-group">
                <?= $this->Form->control('display', ['placeholder' => "Enter name", 'class' => "form-control", 'label' => 'Name']); ?>
            </div>
            <div class="form-group">
                <?= $this->Form->textarea('value', ['label' => 'Description', 'class' => 'ckeditor', 'id' => 'editor1', 'rows' => '10']); ?>
            </div>
            <?= $this->Form->button('Update', ['type' => 'submit', 'class' => 'btn btn-success inline', 'div' => false]) ?>

            <a class="btn btn-danger" href="<?= HTTP_ROOT . "admin/appadmins/email-templates" ?>">Cancel</a>
            <?= $this->Form->end() ?>      
        </div>
    </div>
</div>


<?= $this->Html->script('/plugins/ckeditor/ckeditor'); ?>      
<script>
    $(function () {
        CKEDITOR.replace('editor1', {
            uiColor: '#cccccc'
        });
    });
</script>
