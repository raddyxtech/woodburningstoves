

<!-- Page Title Section Starts -->
<div class="row">
    <div class="col-12">
        <div class="page-title-box">
            <h4 class="page-title float-left">Email Templates</h4>

            <ol class="breadcrumb float-right">
                <li class="breadcrumb-item"><a href="<?= h(HTTP_ROOT) ?>admin/dashboard"><i class="fa fa-dashboard"></i>&nbsp;Home</a></li>
                <li class="breadcrumb-item active"><a href="javascript:;">Email Templates</a></li>
            </ol>

            <div class="clearfix"></div>
        </div>
    </div>
</div>
<!-- Page Title Section Ends -->


<!-- Main content -->    
<div class="row">
    <div class="col-md-12">
        <div class="card-box">
            <table id="example1" class="table table-bordered table-striped email-templates-list">
                <thead>
                    <tr>
                        <th>Template Name</th>
                        <th>Subject</th>
                        <th class="text-center">Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($datamailListings as $datamailListing): ?>
                        <tr id="<?php echo $datamailListing->id; ?>" class="message_box">
                            <td><?= h($datamailListing->name) ?></td>
                            <td><?= h($datamailListing->display) ?></td>
                            <td class="text-center">
                                <a class="btn btn-xs btn-warning" href="<?= $this->Url->build(['prefix'=>'admin','controller'=>'Appadmins','action'=>'editmailSetting',$datamailListing->id]); ?>" > <i class ="fa fa-edit"></i> </a>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>  
            <div class="clearfix"></div>
        </div>
    </div>
</div>

