<footer>
    <div class="f_navigation">
        <div class="container_12">
            <div class="grid_3">
                <h3>Information</h3>
                <nav class="f_menu">
                    <ul>
                        <li><a href="<?= $this->Url->build(['controller' => 'Pages', 'action' => 'index']) ?>">Home</a></li>
                        <li><a href="<?= $this->Url->build(['controller' => 'Products', 'action' => 'productsList']) ?>">Products</a></li>
                        <li><a href="<?= $this->Url->build(['controller' => 'Pages', 'action' => 'listbrands']) ?>">Brands</a></li>
                        <li><a href="<?= $this->Url->build(['controller' => 'Pages', 'action' => 'returnsInfo']) ?>">Returns Info</a></li>
                    </ul>
                </nav><!-- .private -->
            </div><!-- .grid_3 -->

            <div class="grid_3">
                <h3>Costumer Servise</h3>
                <nav class="f_menu">
                    <ul>
                        <li><a href="<?= $this->Url->build(['controller' => 'Pages', 'action' => 'deliveryInfo']) ?>">Delivery Info</a></li>
                        <li><a href="<?= $this->Url->build(['controller' => 'Pages', 'action' => 'boilerStove']) ?>">Boiler Stoves Info</a></li>
                        <li><a href="<?= $this->Url->build(['controller' => 'Pages', 'action' => 'flexiLiner']) ?>">Flexi Liner Advice</a></li>
                    </ul>
                </nav>
            </div>

            <div class="grid_3">
                <h3>My Account</h3>
                <nav class="f_menu">
                    <ul>

                        <li><a href="<?= $this->Url->build(['controller' => 'Pages', 'action' => 'galleryInstallations']) ?>">Installation Gallery</a></li>
                        <li><a href="<?= $this->Url->build(['controller' => 'Pages', 'action' => 'installationAdvice']) ?>">Installation Advice</a></li>
                        <li><a href="<?= $this->Url->build(['controller' => 'Pages', 'action' => 'newProducts']) ?>">News / New Products</a></li>
                        <li><a href="<?= $this->Url->build(['controller' => 'Pages', 'action' => 'sia']) ?>">SIA</a></li>
                    </ul>
                </nav>
            </div>

            <div class="grid_3">
                <h3>Information</h3>
                <nav class="f_menu">
                    <ul>    
                        <li><a href="<?= $this->Url->build(['controller' => 'Pages', 'action' => 'contactUs']) ?>">Contact Us </a></li>
                        <li><a href="<?= $this->Url->build(['controller' => 'Pages', 'action' => 'visitShowroom']) ?>">Visit Our Showroom</a></li>
                        <li><a href="<?= $this->Url->build(['controller' => 'Pages', 'action' => 'termConditions']) ?>">Terms and conditions</a></li>
                        <li><a href="<?= $this->Url->build(['controller' => 'Pages', 'action' => 'privacyPolicy']) ?>">Privacy Policy</a></li>

                    </ul>
                </nav><!-- .private -->
            </div><!-- .grid_3 -->

            <div class="clear"></div>
        </div><!-- .container_12 -->
    </div><!-- .f_navigation -->

    <div class="f_info">
        <div class="container_12">
            <div class="grid_6">
                <p class="copyright"><?= SITE_NAME_TEXT ?>, <?= date('Y'); ?></p>
            </div><!-- .grid_6 -->
            <div class="grid_6">
                <div class="soc">
                    <a class="google" href="#"></a>
                    <a class="twitter" href="#"></a>
                    <a class="facebook" href="#"></a>
                </div><!-- .soc -->
            </div><!-- .grid_6 -->
            <div class="clear"></div>
        </div><!-- .container_12 -->
    </div><!-- .f_info -->

</footer>