
<p class="bold">Find your perfect stove</p>

<form name="frmSrch" method="get" action="search-results.htm">
    <p><label for="searchbrand">By brand</label>
        <br />
        <!--<= $this->Form->select('id', $brandLists, ['empty' => 'Select Brand', 'label' => false, 'class' => "large-select"]); ?>-->
    </p>
    <p>
        <label for="searchcategory">By category</label>
        <br />
        <!--<= $this->Form->select('id', $categoryLists, ['empty' => 'Select Category', 'label' => false, 'class' => "large-select"]); ?>-->

    </p>
    <p>
        <label for="searchkeywords">By keyword</label>
        <br />
        <input name="sKeywordSearch" id="searchkeywords" class="large-input" onclick="frmSrch.searchcategory.selectedIndex = '';
                frmSrch.searchbrand.selectedIndex = '';" value=""  />
    </p>
    <p>
        <button  style="background-color:#2a3635;color:#fff " type="submit" class="small-btn" id="pic-update-btn">Submit</button>
    </p>
</form>

<p class="bold">Your basket</p>
<p><span class="float-left">No. of Items:</span><span class="float-right">0</span></p>
<p class="clearblock bold red"><span class="float-left">TOTAL:</span><span class="float-right">&pound;0.00</span></p>
<hr class="clearblock"/>
<p><input name="go2basket" type="button" class="go2basket-btn" value="Checkout" onclick="location.href = 'basket.htm';"/></p>
<hr/>
<div id="socialmedia">
    <a href="http://www.facebook.com/#!/pages/Saltfire-Stoves-Ltd/211161172230666" target="_blank" title="Visit Woodburning Stoves Direct on Facebook" class="facebook"><img src="webroot/images/facebook-36x36.jpg" width="36" height="36" alt="Our Facebook page"/></a>
    <a href="http://twitter.com/SaltfireStoves" target="_blank" title="Follow us on Twitter"><img src="webroot/images/twitter-36x36.jpg" width="36" height="36" alt="Our Twitter feed"/></a>
</div>    
<hr/>
<div id="reviews">
    <p class="bold">Reviews</p>
    <div class="sidetestimonial-blk">
        <img src="webroot/images/quotes.png" width="19" height="18" class="left-quote" alt="" />
        <p class="sidetestimonial">very helpful and pleasant phone manner, answered all my ques ...&rdquo;</p>
        <p><span class="sidetestimonial-by">paul vickers (sheffield)</span></p>
    </div>
    <div class="sidetestimonial-blk">
        <img src="webroot/images/quotes.png" width="19" height="18" class="left-quote" alt="" />
        <p class="sidetestimonial">first class service thank you cheaper than original&rdquo;</p>
        <p><span class="sidetestimonial-by">bobby palmer</span></p>
    </div>
    <div class="sidetestimonial-blk">
        <img src="webroot/images/quotes.png" width="19" height="18" class="left-quote" alt="" />
        <p class="sidetestimonial">Excellent fire super fast delivery A1 &#128077;&#128077;&#12 ...&rdquo;</p>
        <p><span class="sidetestimonial-by">V Valente,<br />Blackpool</span></p>
    </div>
</div>    
