<?php
$paramController = $this->request->getParam('controller');
$paramAction = $this->request->getParam('action');
?>
    <div class="container_12">
        <div id="top">
            <div class="grid_4 ">
                <div class="phone_top">
                    <span>Call Us +777 (100) 1234</span>
                </div><!-- .phone_top -->
            </div><!-- .grid_3 -->

            <div class="grid_8" style="text-align: right;">
                <div class="welcome">
                    Welcome  <?php if ($is_logged_in) { ?>
                        <?= $user_name; ?> |
                        <?php if ($user_type == 1) { ?> 
                            <a href="<?= HTTP_ROOT ?>admin/dashboard">Dashboard</a>|
                        <?php } else if ($user_type == 2) { ?>
                            <a href="<?= HTTP_ROOT ?>users/account-information" style="color: #59b7c2;">MyAccount</a>|
                        <?php } ?>
                        <a href="<?= HTTP_ROOT ?>users/logout" style="color: #59b7c2;">Logout</a>
                    <?php } else { ?>
                        visitor you can <a href="<?= HTTP_ROOT . 'login' ?>">login</a> or <a href="<?= HTTP_ROOT . 'users/register' ?>">create an account</a>.
                    <?php } ?>

                </div><!-- .welcome -->
            </div><!-- .grid_6 -->
        </div><!-- #top -->

        <div class="clear"></div>
        <header id="branding">
            <div class="grid_3">
                <hgroup>
                    <a href="<?= HTTP_ROOT ?>" title=""><img src="images/logo.png" alt=""/></a>                       
                </hgroup>
            </div><!-- .grid_3 -->
            <div class="grid_3">
                <form class="search" action="products/productsList">
                    <input type="text" name="search" class="entry_form" value="" placeholder="Search by product name..."/>
                </form>

            </div><!-- .grid_3 -->
            <div class="grid_6">
                <ul id="cart_nav">
                    <?php if ($is_logged_in) { ?>
                        <?php if ($user_type == 1) { ?> 
<!--                            <li><a href="<//?= HTTP_ROOT ?>admin/dashboard">MyAccount</a></li>-->
                        <?php } else if ($user_type == 2) { ?>
                            <li>
                                <?php // foreach ($orders as $order) { ?>
                                <?php  if($count != 0){ ?>
                                <span class="badge" style="background-color: #ec6794"><?= $count; ?></span>
                                <?php } ?>
                                <a class="cart_li" href="<?= $this->Url->build(['controller' => 'Products', 'action' => 'basket/']); ?>">Cart <span>&pound;<?= number_format($total, 2, '.', ''); ?></span></a>
                                <?php if(!empty($orders)) { ?>
                                <ul class="cart_cont">
                                    <?php // foreach ($orders as $order) { ?>
                                    <li class="no_border"><p>Recently added item(s)</p></li>
                                    <?php foreach ($orders as $order) { ?>
                                        <?php if ($order->qty > 0) { ?>
                                    <li id="order<?= $order->id ?>">
                                                <?php if ($order->product['images'][0]['main_image'] == 'Y') { ?>
                                                    <a href="<?= HTTP_ROOT ?>Products/products-details/<?= @$order->product_id; ?>" class="prev_cart"><div class="cart_vert"><img src="<?= HTTP_ROOT . PRODUCT_PIC . $order->product['images'][0]['filename']; ?>" width="110px" height="110px"/> </div></a> 
                                                <?php } else { ?>
                                                    <img src= "<?= HTTP_ROOT ?>/images/no_image_available.jpg " />
                                                <?php } ?>
                                                <div class="cont_cart">
                                                    <h4><?= $order->name; ?></h4>
                                                    <div class="price">
                                                        <?= $order->qty; ?> x &pound;<?= number_format($order->unit_price, 2, '.', ''); ?>
                                                    </div>
                                                </div>
                                              <!--<a title="close" onclick="this.parentElement.style.display='none';" class="close" href="javascript:;"></a>-->
                                                <a title="close" Onclick="deleteProduct('<?= $order['id'] ?>')" href="javascript:;" class="close"></a>
                                                <div class="clear"></div>
                                            </li>
                                            <?php
                                        }
                                    }
                                    ?>
                                    <li class="no_border">
                                        <a href="<?= $this->Url->build(['controller' => 'Products', 'action' => 'basket/']); ?>" class="view_cart">View shopping cart</a>
                                        <a href="<?= $this->Url->build(['controller' => 'Products', 'action' => 'checkOut/']); ?>" class="checkout">Procced to Checkout</a>
                                    </li>
                                </ul>
                                <?php } else { ?>
                                <ul class="cart_cont">
                                    <li class="no_border"><p> NO item)</p></li>
                                </ul>
                                <?php } ?>
                            </li>
                            <?php
                        }
                    } else {
                        ?>
                        <a class="cart_li" href="<?= HTTP_ROOT . 'login' ?>">Cart</a>
                    <?php } ?>
                </ul>

                <nav class="private">
                    <ul>
                        <?php if ($is_logged_in) { ?>
                            <?php if ($user_type == 1) { ?> 
                                <li><a href="<?= HTTP_ROOT ?>admin/dashboard">My Account</a></li>
                                <li class="separator">|</li>
                            <?php } else if ($user_type == 2) { ?>
                                <li><a href="<?= HTTP_ROOT ?>users/account-information">My Account</a></li>  
                            <?php } ?>
                        <?php } else { ?>
                            <!--<li><a href="javascript:;">My Account</a></li>-->
                            <li class="separator">|</li>
                            <li><a href="<?= HTTP_ROOT . 'login' ?>">Log In</a></li>
                            <li class="separator">|</li>
                            <li><a href="<?= HTTP_ROOT . 'users/register' ?>">Sign Up</a></li>
                        <?php } ?>
                    </ul>
                </nav><!-- .private -->
            </div><!-- .grid_6 -->
        </header><!-- #branding -->
    </div><!-- .container_12 -->

        <div class="clear"></div>

        <div id="block_nav_primary">
            <div class="container_12">
                <div class="grid_12">
                    <nav class="primary" id="nav-id">
                        <a class="menu-select" href="javascript:;">Catalog</a>
                        <ul>
                            <li class="<?= $paramAction == 'index' ? 'curent' : '' ?>"><a href="<?= $this->Url->build(['controller' => 'Pages', 'action' => 'index']) ?>" title="Saltfire Stoves home page">Home</a></li>
                            <li class="<?php if ($paramAction == 'productsList') { ?> curent <?php } ?>"><a href="<?= $this->Url->build(['controller' => 'Products', 'action' => 'productsList']) ?>" title="View our wood burning stoves">Products</a></li>
                            <li class="<?php if ($paramAction == 'listbrands') { ?> curent <?php } ?>"><a href="<?= $this->Url->build(['controller' => 'Pages', 'action' => 'listbrands']) ?>" title="Woodburing stove brands">Brands</a></li>
                            <li class="<?= $paramAction == 'faqs' ? 'curent' : '' ?>">
                                <a href="<?= $this->Url->build(['controller' => 'Pages', 'action' => 'faqs']); ?>" title="FAQs regarding wood burning stoves">FAQs</a>
                            </li>
                            <li class="<?= $paramAction == 'contactUs' ? 'curent' : '' ?>"><a href="<?= $this->Url->build(['controller' => 'Pages', 'action' => 'contactUs']); ?>" title="Contact Saltfire Stoves">Contact Us</a></li>
                            <li class="<?php if ($paramAction == 'returnsInfo') { ?> curent <?php } ?>"><a href="<?= $this->Url->build(['controller' => 'Pages', 'action' => 'returnsInfo']) ?>" title="Saltfire Stoves returns policy">Returns Info</a></li>
                            <li class="<?php if ($paramAction == 'deliveryInfo') { ?> curent <?php } ?>"><a href="<?= $this->Url->build(['controller' => 'Pages', 'action' => 'deliveryInfo']) ?>" title="Saltfire Stoves delivery information">Delivery Info</a></li>
                        </ul>
                    </nav><!-- .primary -->
                </div><!-- .grid_12 -->
            </div><!-- .container_12 -->
        </div><!-- .block_nav_primary -->
        <div class="clear"></div>


   <script>
      function deleteProduct(id) {
//    alert("#product-" + id);

        if (confirm("Are you sure want to delete?")) {
            $.ajax({
                type: 'POST',
                url: siteUrl + 'products/ajaxDelete',
                data: {'id': id},
                dataType: 'json',
                success: function (response) {
                    if (response.status == 'success') {
                        $("#order" + id).remove();
//                         window.location.reload();
//                        window.location.href = "retrive";
                    } else {
                        alert(response.msg);
                    }
                }
            });
        }
    }
        </script>