<?php
$paramController = $this->request->getParam('controller');
$paramAction = $this->request->getParam('action');
?>
<header class="">		
    <div id="header-outer" style="background: url(images/banner-bg.jpg) repeat-x;">
        <div id="header">
            <img src="images/admin-logo.jpg" alt="Wood Burning stoves at Saltfire Stoves" width="177" height="100" id="banner-logo"/>

            <p id="customerservice">Customer Service</p>

            <p id="customerservicetel">01929 555 211</p>
            <ul id="menu">
                <li class="<?= $paramAction == 'index' ? 'selected' : '' ?>"><a href="<?= $this->Url->build(['controller' => 'Pages', 'action' => 'index']) ?>" title="Saltfire Stoves home page">Home</a></li>

                <li class="<?php if ($paramAction == 'productsList') { ?> selected <?php } ?>"><a href="<?= $this->Url->build(['controller' => 'Products', 'action' => 'productsList']) ?>" title="View our wood burning stoves">Products</a></li>


                <li class="<?php if ($paramAction == 'listbrands') { ?> selected <?php } ?>"><a href="<?= $this->Url->build(['controller' => 'Pages', 'action' => 'listbrands']) ?>" title="Woodburing stove brands">Brands</a></li>

                 <li class="<?= $paramAction == 'faqs' ? 'selected' : '' ?>"><a href="<?= $this->Url->build(['controller' => 'Pages', 'action' => 'faqs']); ?>" title="FAQs regarding wood burning stoves">FAQs</a></li>
                 <li class="<?= $paramAction == 'contactUs' ? 'selected' : '' ?>"><a href="<?= $this->Url->build(['controller' => 'Pages', 'action' => 'contactUs']); ?>" title="Contact Saltfire Stoves">Contact Us</a></li>
               

                <li class="<?php if ($paramAction == 'returnsInfo') { ?> selected <?php } ?>"><a href="<?= $this->Url->build(['controller' => 'Pages', 'action' => 'returnsInfo']) ?>" title="Saltfire Stoves returns policy">Returns Info</a></li>

                <li class="<?php if ($paramAction == 'deliveryInfo') { ?> selected <?php } ?>"><a href="<?= $this->Url->build(['controller' => 'Pages', 'action' => 'deliveryInfo']) ?>" title="Saltfire Stoves delivery information">Delivery Info</a></li>

            </ul>

            <ul class="responsive_menu">
                <li class="menu"> <i class="line">&nbsp;</i>
                    <ul>
                        <li class="<?php echo (strpos($_SERVER['PHP_SELF'], "index.php") !== false) ? 'selected' : ''; ?>"><a href="Home" title="Saltfire Stoves home page">Home</a></li>

                        <li class="<?php echo (strpos($_SERVER['PHP_SELF'], "product-list.php") !== false) ? 'selected' : ''; ?>"><a href="list-product" title="View our wood burning stoves">Products</a></li>

                        <li class="<?php echo (strpos($_SERVER['PHP_SELF'], "brands.php") !== false) ? 'selected' : ''; ?>"><a href="brands" title="Woodburing stove brands">Brands</a></li>

                        <li class="<?php echo (strpos($_SERVER['PHP_SELF'], "faqs.php") !== false) ? 'selected' : ''; ?>"><a href="faqs.htm" title="FAQs regarding wood burning stoves">FAQs</a></li>

                        <li class="<?php echo (strpos($_SERVER['PHP_SELF'], "contact-us.php") !== false) ? 'selected' : ''; ?>"><a href="contact-us.htm" title="Contact Saltfire Stoves">Contact Us</a></li>

                        <li class="<?php echo (strpos($_SERVER['PHP_SELF'], "returns.php") !== false) ? 'selected' : ''; ?>"><a href="returns" title="Saltfire Stoves returns policy">Returns Info</a></li>

                        <li class="<?php echo (strpos($_SERVER['PHP_SELF'], "delivery.php") !== false) ? 'selected' : ''; ?>"><a href="delivery" title="Saltfire Stoves delivery information">Delivery Info</a></li>
                    </ul>
                </li>
            </ul>
        </div>

    </div>
</header>
