<?php
$prefix = $this->request->getParam('prefix');
if (!isset($params['escape']) || $params['escape'] !== false) {
    $message = h($message);
}
?>


<?php if ($prefix != 'admin') { ?>
    <div class="success-message" id="flashSuccessMsg"  onclick="this.classList.add('hidden');"><p><i class="fas fa-info-circle"></i> <?= h($message) ?></p></div>
<?php } else { ?>
    
    <div class="row" id="flashSuccessMsg">
        <div class="col-12">
            <div class="alert alert-icon alert-success alert-dismissible fade show page-title-box" role="alert" style="margin: 1px 0px 0px -16px;position: absolute;z-index: 999;width: 101%;">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <i class="mdi mdi-check-all"></i><?= $message ?>
            </div>
        </div>
    </div>
    
<?php } ?>
<script>
    setTimeout(function(){
        document.getElementById('flashSuccessMsg').style.display = 'none'
    }, 5000);
</script>

