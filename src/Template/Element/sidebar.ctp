<?php
$paramController = $this->request->getParam('controller');
$paramAction = $this->request->getParam('action');
?>
<link href="css/bootstrap.min.css" rel="stylesheet">
<link href="css/theme.css" rel="stylesheet">
<!--start sidebar-->

<div class="col-lg-3 col-md-3 col-sm-4">
    <div class="widget account-details">
        <h2 class="widget-title">Account</h2>
        <ul>

            <li class="<?= $paramAction == 'accountInformation' ? 'active' : '' ?>">
                <a href="<?= $this->Url->build(['controller' => 'Users', 'action' => 'account-information']); ?>">Account Information</a>
            </li>
            <li class="<?= $paramAction == 'changePassword' ? 'active' : '' ?>">
                <a href="<?= $this->Url->build(['controller' => 'Users', 'action' => 'change-password']); ?>">Change Password</a>
            </li>
            <li class="<?= $paramAction == 'listBook' ? 'active' : '' ?>">
                <a href="<?= $this->Url->build(['controller' => 'Users', 'action' => 'listBook']); ?>">Address Books</a>
            </li>
           
            <li class="<?= $paramAction == 'orderHistory' ? 'active' : '' ?>">
                <a href="<?= $this->Url->build(['controller' => 'Users', 'action' => 'order-history']); ?>">Order History</a>
            </li>
            <li class="<?= $paramAction == 'reviewRatings' ? 'active' : '' ?>">
                <a href="<?= $this->Url->build(['controller' => 'Users', 'action' => 'review-ratings']); ?>">Reviews and Ratings</a>

            </li>
            <li class="<?= $paramAction == 'returnRequests' ? 'active' : '' ?>">
                <a href="<?= $this->Url->build(['controller' => 'Users', 'action' => 'return-requests']); ?>">Returns Requests</a>

            </li>
        </ul>
    </div>
</div>
