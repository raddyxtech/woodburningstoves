            <div id="footer-outer">
    <div id="footer">
        <ul class="foot-menu float-left">
            <li><a href="Home" target="_self" title="Wood burning Stoves home page">Home</a></li>
            <li><a href="list-product" target="_self" title="View our wood burning stoves">Products</a></li>
            <li><a href="brands" target="_self" title="Woodburning stove brands">Brands</a></li>
        </ul>
        <ul class="foot-menu float-left">
            <!--<li><a href="faqs.htm" target="_self" title="Woodburning FAQs">FAQs</a></li>-->
            <li><a href="<?= $this->Url->build(["controller" => "Pages", "action" => "faqs"]); ?>"><span>FAQs</span></a></li>
            <li><a href="returns" target="_self" title="Woodburning Stoves returns policy">Returns Info</a></li>
            <li><a href="delivery" target="_self" title="Woodburning Stoves delivery information">Delivery Info</a></li>
            <li><a href="reviews" target="_self" title="Reviews of Wood burning Stoves">Reviews</a></li>
        </ul>
                    <ul class="foot-menu float-left">
                                        <li><a href="boiler-stove" target="_self" title="Boiler Stove Installation Advice">Boiler Stoves Info</a></li>
                                        <li><a href="building-regulations" target="_self" title="Building Regulations For Stove And Flue Installations">Building Regulations</a></li>
                                        <li><a href="ecodesign-2022" target="_self" title="EcoDesign 2022">EcoDesign 2022</a></li>
                    </ul><ul class="foot-menu float-left">                    <li><a href="flexiLiner" target="_self" title="Flexible Chimney Flue Liner">Flexi Liner Advice</a></li>
                                        <li><a href="gallery-installations" target="_self" title="Gallery Of Installations">Installation Gallery</a></li>
                    </ul><ul class="foot-menu float-left">                    <li><a href="installation-advice" target="_self" title="Installation Advice">Installation Advice</a></li>
                                        <li><a href="new-products" target="_self" title="News and New Products">News / New Products</a></li>
                                        <li><a href="sia" target="_self" title="SIA">SIA</a></li>
                    </ul><ul class="foot-menu float-left">                    <li><a href="Installer" target="_self" title="Stove Installer's Diary Blog">Installer's Blog</a></li>
                                        <li><a href="Video" target="_self" title="Videos">Videos</a></li>
                                        <li><a href="Showroom" target="_self" title="Visit Our Showroom">Visit Our Showroom</a></li>
                    <li>&nbsp;</li>            </ul>
                    <ul class="foot-menu float-left">
            <li><a href="term-conditions" target="_self" title="Woodburning Stoves terms and conditions">Terms and conditions</a></li>
            <li><a href="privacy-policy" target="_self" title="Wood burning Stoves privacy policy">Privacy Policy</a></li>
             <li><a href="<?= $this->Url->build(["controller" => "Pages", "action" => "contactus"]); ?>"><span>Contact Us </span></a></li>
                    </ul>
        <a href="#">
            <img src="https://ssl.comodo.com/images/trusted-site-seal.png" alt="Multi Domain SSL" width="113" height="59" style="border: 0px;"><br> 
            <span style="font-weight:bold; font-size:7pt">Multi Domain SSL</span>
        </a>
        <p class="copyright dark-grey text-center">Copyright 2019 &copy; Saltfire Stoves Ltd.</p>
    </div>
    <p class="float-left grey left-pad">Registered Office: Station Works, Johns Road, Wareham, Near Poole / Bournemouth, Dorset, BH20 4BG, UK</p>

    <p class="float-right grey right-pad"><a href="http://www.harbour-design.co.uk" class="grey" title="Website by Harbour Design - www.harbour-design.co.uk">site by harbour design</a></p>
</div>