<style>
    .logo span { color: #fff; }
    .navbar-custom .nav-link { line-height: 0; }
</style>
<div class="topbar">

    <!-- LOGO -->
    <div class="topbar-left">
        <a target="_blank" href="<?= HTTP_ROOT ?>" class="logo">
            <span>
                <?= SITE_NAME_TEXT; ?>
            </span>
            <i><img src="favicon.ico" alt="" height="28"></i>
        </a>
    </div>

    <nav class="navbar-custom">

        <ul class="list-inline float-right mb-0">
            
            

            <li class="list-inline-item dropdown notification-list">
                <a class="nav-link dropdown-toggle waves-effect waves-light nav-user" data-toggle="dropdown" href="javascript:;" role="button"
                   aria-haspopup="false" aria-expanded="false" style="display: block; margin-top: 6px; margin-left: 11em;">
                    <img src="images/favicon.png" alt="user" class="rounded-circle">
                </a>
                <span style="color: #fff;">Last Login: <?= date('M d, Y \a\t H:i'); ?></span>
                <div class="dropdown-menu dropdown-menu-right profile-dropdown " aria-labelledby="Preview">
                    <!-- item-->
                    <div class="dropdown-item noti-title">
                        <h5 class="text-overflow"><small>Hi ! &nbsp;<?= $this->request->getSession()->read('Auth.User.name'); ?></small> </h5>
                    </div>

                    <!-- item-->
                    <a href="<?= HTTP_ROOT ?>admin/appadmins/profile-setting" class="dropdown-item notify-item">
                        <i class="mdi mdi-account-circle"></i> <span>Profile</span>
                    </a>

                    <!-- item-->
                    <a href="<?= h(HTTP_ROOT) ?>users/logout" class="dropdown-item notify-item">
                        <i class="mdi mdi-power"></i> <span>Logout</span>
                    </a>

                </div>
            </li>
            
        </ul>

        <ul class="list-inline menu-left mb-0">
            <li class="float-left">
                <button class="button-menu-mobile open-left waves-light waves-effect">
                    <i class="dripicons-menu"></i>
                </button>
            </li>
<!--            <li class="hide-phone app-search">
                <form role="search" class="">
                    <input type="text" placeholder="Search..." class="form-control">
                    <a href=""><i class="fa fa-search"></i></a>
                </form>
            </li>-->
        </ul>

    </nav>

</div>