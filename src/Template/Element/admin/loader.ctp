<div class="loader-div hidden">
    <div class="load_cont">
        <div class="sk-double-bounce">
            <div class="sk-child sk-double-bounce1"></div>
            <div class="sk-child sk-double-bounce2"></div>
        </div>
        <h4 class="m-t-0 header-title">Loading.. Please Wait!!</h4>
    </div>
</div>