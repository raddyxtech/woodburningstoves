<?php
$paramController = $this->request->getParam('controller');
$paramAction = $this->request->getParam('action');
?>
<style>
    .left side-menu .nav-second-level li i{padding-bottom: 3px;}
    .hidden {display: none;}
</style>
<div class="left side-menu">
    <div class="slimscroll-menu" id="remove-scroll">

        <!--- Sidemenu -->
        <div id="sidebar-menu">
            <!-- Left Menu Start -->
            <ul class="metismenu" id="side-menu">
                <li>
                    <a href="<?php echo $this->Url->build(['prefix' => 'admin', 'controller' => 'Appadmins', 'action' => 'dashboard']); ?>"><i class="fi-air-play"></i><span> Dashboard </span></a>
                </li>   
                <li>
                    <a href="javascript:;"><i class="fa fa-user-secret"></i> <span> Manage Category  </span> <span class="menu-arrow"></span></a>
                    <ul class="nav-second-level" aria-expanded="false">
                        <li><a href="<?= $this->Url->build(['prefix' => 'admin', 'controller' => 'Categories', 'action' => 'listCategories']); ?>"><i class="fa fa-list-alt"></i>List Categories</a></li>
                        <li><a href="<?= $this->Url->build(['prefix' => 'admin', 'controller' => 'Categories', 'action' => 'addCategories']); ?>"><i class="fa fa-plus"></i>Add New Category</a></li>
                    </ul>
                </li>

                <li>
                    <a href="javascript:;"><i class="fa fa-btc"></i> <span> Manage Brands </span> <span class="menu-arrow"></span></a>
                    <ul class="nav-second-level" aria-expanded="false">
                        <li><a href="<?= $this->Url->build(['prefix' => 'admin', 'controller' => 'Brands', 'action' => 'listbrands']); ?>"><i class="fa fa-list-alt"></i>List Brands</a></li>
                        <li><a href="<?= $this->Url->build(['prefix' => 'admin', 'controller' => 'Brands', 'action' => 'addBrand']); ?>"><i class="fa fa-plus"></i>Add New Brand</a></li>
                    </ul>
                </li>
                <li>
                    <a href="javascript:;"><i class="fa fa-picture-o"></i><span> Manage Banners </span> <span class="menu-arrow"></span></a>
                    <ul class="nav-second-level" aria-expanded="false">
                        <li><a href="<?= $this->Url->build(['prefix' => 'admin', 'controller' => 'Banners', 'action' => 'listbanners']); ?>"><i class="fa fa-picture-o"></i>List Banners</a></li>
                        <li><a href="<?= $this->Url->build(['prefix' => 'admin', 'controller' => 'Banners', 'action' => 'addBanner']); ?>"><i class="fa fa-plus-square-o"></i>Add Banner</a></li>
                    </ul>
                </li>
                <li>
                    <a href="javascript:;"><i class="fa fa-picture-o"></i><span> Manage Orders </span> <span class="menu-arrow"></span></a>
                    <ul class="nav-second-level" aria-expanded="false">
                        <!--<li><a href="<//?= $this->Url->build(['prefix' => 'admin', 'controller' => 'Orders', 'action' => 'orderDetails']); ?>"><i class="mdi mdi-basket-fill"></i>Orders</a></li>-->
                        <li><a href="<?= $this->Url->build(['prefix' => 'admin', 'controller' => 'Orders', 'action' => 'orderHistory']); ?>"><i class="fa fa-list-alt"></i>Order History</a></li>
                    </ul>
                </li>
                <li class="">
                    <a href="javascript:;"><i class="fa fa-user-secret"></i> <span> Manage Product  </span> <span class="menu-arrow"></span></a>
                    <ul class="nav-second-level" aria-expanded="false">
                        <li><a href="<?= $this->Url->build(['prefix' => 'admin', 'controller' => 'Products', 'action' => 'listProducts']); ?>"><i class="fa fa-list-alt"></i>List Products</a></li>
                        <li><a href="<?= $this->Url->build(['prefix' => 'admin', 'controller' => 'Products', 'action' => 'addProducts']); ?>"><i class="fa fa-plus"></i>Add Product</a></li>
                        <li><a href="<?= $this->Url->build(['prefix' => 'admin', 'controller' => 'Products', 'action' => 'stockList']); ?>"><i class="fa fa-list-alt"></i>Stock List</a></li>
                        <li><a href="<?= $this->Url->build(['prefix' => 'admin', 'controller' => 'Products', 'action' => 'reviewRatings']); ?>"><i class="fa fa-list-alt"></i>Review & Ratings</a></li>
                    </ul>
                </li>
                <li>
                    <a href="javascript:;"><i class="fa fa-truck"></i> <span> Manage Delivery</span> <span class="menu-arrow"></span></a>
                    <ul class="nav-second-level" aria-expanded="false">
                        <li><a href="<?= $this->Url->build(['prefix' => 'admin', 'controller' => 'Delivery', 'action' => 'listRegions']); ?>"><i class="fa fa-list-alt"></i>Delivery Regions</a></li>
                        <li><a href="<?= $this->Url->build(['prefix' => 'admin', 'controller' => 'Delivery', 'action' => 'addRegion']); ?>"><i class="fa fa-plus"></i>Add New Region</a></li>
                        <li><a href="<?= $this->Url->build(['prefix' => 'admin', 'controller' => 'Delivery', 'action' => 'listDeliveries']); ?>"><i class="fa fa-list-alt"></i>List Deliveries</a></li>
                        <li><a href="<?= $this->Url->build(['prefix' => 'admin', 'controller' => 'Delivery', 'action' => 'addDelivery']); ?>"><i class="fa fa-plus"></i>Add New Delivery</a></li>
                    </ul>
                </li>
                <li class="<?php if ($paramController == 'Cms') { ?> active <?php } ?>">
                    <a href="javascript:;"><i class="fa fa-map"></i> <span> CMS </span> <span class="menu-arrow"></span></a>
                    <ul class="nav-second-level" aria-expanded="false">                   
                        <li class="<?php if ($paramAction == 'addCms') { ?> active <?php } ?>"><a href="<?= $this->Url->build(['prefix' => 'admin', 'controller' => 'Cms', 'action' => 'listCms']); ?>"><i class="fa fa-list-alt"></i>List Cms </a></li>  
                        <li class="<?php if ($paramAction == 'newPage') { ?> active <?php } ?>"><a href="<?= $this->Url->build(['prefix' => 'admin', 'controller' => 'Cms', 'action' => 'listPages']); ?>"><i class="fa fa-list-alt"></i>List Pages</a></li>                        
                        <li><a href="<?= $this->Url->build(['prefix' => 'admin', 'controller' => 'Cms', 'action' => 'homePageContent']); ?>"><i class="fa fa-home"></i>Home Page Content</a></li>                        
                    </ul>
                </li>
                <li class="<?php if ($paramController == 'Miscellaneous') { ?> active <?php } ?>">
                    <a href="javascript:;"><i class="fa fa-ils"></i> <span> Manage Misc</span> <span class="menu-arrow"></span></a>
                    <ul class="nav-second-level" aria-expanded="false">
                        <li><a href="<?= $this->Url->build(['prefix' => 'admin', 'controller' => 'Miscellaneous', 'action' => 'listHomeImages']); ?>"><i class="fa fa-picture-o"></i>Home Page Images</a></li>
                        <li class="<?php if ($paramAction == 'addNewFaq') { ?> active <?php } ?>"><a href="<?= $this->Url->build(['prefix' => 'admin', 'controller' => 'Miscellaneous', 'action' => 'listFaqs']); ?>"><i class="fa fa-question-circle"></i>List Faq's</a></li>
                        <li><a href="<?= $this->Url->build(['prefix' => 'admin', 'controller' => 'Miscellaneous', 'action' => 'countries']); ?>"><i class="fa fa-globe"></i>Countries</a></li>                                    
                        <li><a href="<?= $this->Url->build(['prefix' => 'admin', 'controller' => 'Miscellaneous', 'action' => 'company']); ?>"><i class="fa fa-building-o"></i>Company</a></li>                                    
                        <li><a href="<?= $this->Url->build(['prefix' => 'admin', 'controller' => 'Miscellaneous', 'action' => 'reviews']); ?>"><i class="fa fa-list-alt"></i>Reviews</a></li>                             
                    </ul>
                </li>
                <li>
                    <a href="javascript:;"><i class="fa fa-user-secret"></i> <span> Administrators </span> <span class="menu-arrow"></span></a>
                    <ul class="nav-second-level" aria-expanded="false">
                        <li><a href="<?= $this->Url->build(['prefix' => 'admin', 'controller' => 'Users', 'action' => 'listAdministrators']); ?>"><i class="fa fa-list-alt"></i>List Administrators</a></li>                        
                        <li><a href="<?= $this->Url->build(['prefix' => 'admin', 'controller' => 'Users', 'action' => 'addAdministrators']); ?>"><i class="fa fa-list-alt"></i>Add Administrator</a></li>                        
                    </ul>
                </li>

                <li> 
                    <a href="javascript:;"><i class="dripicons-gear"></i> <span> Settings </span> <span class="menu-arrow"></span></a>
                    <ul class="nav-second-level" aria-expanded="false">
                        <li><a href="<?= $this->Url->build(['prefix' => 'admin', 'controller' => 'Appadmins', 'action' => 'changePassword']); ?>"><i class="fa fa-edit"></i>Change Password</a></li>
                        <li><a href="<?= $this->Url->build(['prefix' => 'admin', 'controller' => 'Appadmins', 'action' => 'emailTemplates']); ?>"><i class="mdi mdi-email"></i>Email Templates</a></li>
                    </ul>
                </li>
                <li>
                    <a href="<?= $this->Url->build(['prefix' => false, 'controller' => 'Users', 'action' => 'logout']); ?>"><i class="mdi mdi-logout"></i> <span>Logout</span> </a>
                </li>                
            </ul>
        </div>
        <!-- Sidebar -->
        <div class="clearfix"></div>
    </div>
    <!-- Sidebar -left -->
</div>