<?php
$paramController = $this->request->getParam('controller');
$paramAction = $this->request->getParam('action');
?>
<header class="row user-page-header">		
    <div id="header-outer">
	<div id="header">
	<img src="images/admin-logo.jpg" alt="Wood Burning stoves at Saltfire Stoves" width="177" height="100" id="banner-logo"/>

	<p id="customerservice">Customer Service</p>

	<p id="customerservicetel">01929 555 211</p>
	<ul id="menu">
		<li class="<?php echo (strpos($_SERVER['PHP_SELF'], "index.php") !== false) ? 'selected' : ''; ?>"><a href="index.htm" title="Saltfire Stoves home page">Home</a></li>

		<li class="<?php echo (strpos($_SERVER['PHP_SELF'], "product-list.php") !== false) ? 'selected' : ''; ?>"><a href="product-list.htm" title="View our wood burning stoves">Products</a></li>

		<li class="<?php echo (strpos($_SERVER['PHP_SELF'], "brands.php") !== false) ? 'selected' : ''; ?>"><a href="brands.htm" title="Woodburing stove brands">Brands</a></li>

		<li class="<?php echo (strpos($_SERVER['PHP_SELF'], "faqs.php") !== false) ? 'selected' : ''; ?>"><a href="faqs.htm" title="FAQs regarding wood burning stoves">FAQs</a></li>

		<li class="<?php echo (strpos($_SERVER['PHP_SELF'], "contact-us.php") !== false) ? 'selected' : ''; ?>"><a href="contact-us.htm" title="Contact Saltfire Stoves">Contact Us</a></li>

		<li class="<?php echo (strpos($_SERVER['PHP_SELF'], "returns.php") !== false) ? 'selected' : ''; ?>"><a href="returns.htm" title="Saltfire Stoves returns policy">Returns Info</a></li>

		<li class="<?php echo (strpos($_SERVER['PHP_SELF'], "delivery.php") !== false) ? 'selected' : ''; ?>"><a href="delivery.htm" title="Saltfire Stoves delivery information">Delivery Info</a></li>
	</ul>
	
	<ul class="responsive_menu">
        <li class="menu"> <i class="line">&nbsp;</i>
          <ul>
            <li class="<?php echo (strpos($_SERVER['PHP_SELF'], "index.php") !== false) ? 'selected' : ''; ?>"><a href="index.htm" title="Saltfire Stoves home page">Home</a></li>

		<li class="<?php echo (strpos($_SERVER['PHP_SELF'], "product-list.php") !== false) ? 'selected' : ''; ?>"><a href="product-list.htm" title="View our wood burning stoves">Products</a></li>

		<li class="<?php echo (strpos($_SERVER['PHP_SELF'], "brands.php") !== false) ? 'selected' : ''; ?>"><a href="brands.htm" title="Woodburing stove brands">Brands</a></li>

		<li class="<?php echo (strpos($_SERVER['PHP_SELF'], "faqs.php") !== false) ? 'selected' : ''; ?>"><a href="faqs.htm" title="FAQs regarding wood burning stoves">FAQs</a></li>

		<li class="<?php echo (strpos($_SERVER['PHP_SELF'], "contact-us.php") !== false) ? 'selected' : ''; ?>"><a href="contact-us.htm" title="Contact Saltfire Stoves">Contact Us</a></li>

		<li class="<?php echo (strpos($_SERVER['PHP_SELF'], "returns.php") !== false) ? 'selected' : ''; ?>"><a href="returns.htm" title="Saltfire Stoves returns policy">Returns Info</a></li>

		<li class="<?php echo (strpos($_SERVER['PHP_SELF'], "delivery.php") !== false) ? 'selected' : ''; ?>"><a href="delivery.htm" title="Saltfire Stoves delivery information">Delivery Info</a></li>
          </ul>
        </li>
      </ul>
	</div>

</div>
</header>
