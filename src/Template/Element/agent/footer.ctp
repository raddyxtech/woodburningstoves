<?php
$paramController = $this->request->getParam('controller');
$paramAction = $this->request->getParam('action');
?>
<footer>
    <section class="footer-top">
        <div class="wrapper">
            <div class="footer-part">
                <h2>Site information</h2>
                <div class="footer-logo"><img src="images/logo.png" alt=""></div>
                <p>52-54 Allerton Road, Allerton, Liverpool, L18 1LN</p>
            </div>
            <div class="footer-part">
                <h2>Quick Link</h2>
                <ul>
                    <li><a href="javascript:;">Sell your home</a></li>
                    <li><a href="javascript:;">Free property valuation</a></li>
                    <li><a href="javascript:;">Landlords</a></li>
                    <li><a href="javascript:;">Mortgages & Finance</a></li>
                    <li><a href="javascript:;">Careers</a></li>
                </ul>						
            </div>
            <div class="footer-part">
                <h2>Sales</h2>
                <ul>
                    <li><a href="javascript:;">Allerton Sales (Liverpool)</a></li>
                    <li><a href="javascript:;">Heswall Sales (Wirral)</a></li>
                    <li><a href="javascript:;">Old Swan Sales (Liverpool)</a></li>
                    <li><a href="javascript:;">West Kirby Sales (Wirral)</a></li>
                </ul>						
            </div>
            <div class="footer-part">
                <h2>Lettings</h2>
                <ul>
                    <li><a href="javascript:;">Allerton Lettings (Liverpool)</a></li>
                    <li><a href="javascript:;">Heswall Lettings (Wirral)</a></li>
                </ul>						
            </div>
        </div>
    </section>
    <section class="footer-bottom">
        <div class="wrapper">
            <div class="copyrt">© Moverez 2018.All Rights Reserved.</div>
            <div class="footer-logos">
                <a href="javascript:;"><img src="images/footer-logo4.png" alt=""></a>
                <a href="javascript:;"><img src="images/footer-logo3.png" alt=""></a>
                <a href="javascript:;"><img src="images/footer-logo2.png" alt=""></a>
                <a href="javascript:;"><img src="images/footer-logo1.png" alt=""></a>
            </div>
        </div>
    </section>
</footer>