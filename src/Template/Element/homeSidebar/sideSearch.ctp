<p class="bold">Find your perfect stove</p>

<form name="frmSrch" method="get" action="search-results.htm">
    <p>
        <label for="searchbrand">By brand</label>
        <br />
        <select name="iBrandSearchId" id="searchbrand" class="large-select" onclick="frmSrch.searchcategory.selectedIndex = '';
                frmSrch.searchkeywords.value = '';" >
            <option value="">Select Brand -</option>
             <?php foreach ($sql as $value): ?>
                <option value="<?= $value['id'] ?>" ><?= $value['name']; ?></option>
            <?php endforeach; ?>
        </select>
    </p>
    <p>
        <label for="searchcategory">By category</label>
        <br />
        <select name="iCatSearchId" id="searchcategory" class="large-select" onclick="frmSrch.searchbrand.selectedIndex = '';
                frmSrch.searchkeywords.value = '';" >
            <option value="">Select Category -</option>
            <?php /*
            foreach ($oCategoriesSearch->getTopLevel() as $iCatSelectId => $aInfo) {
                $sCatSelect = $aInfo['name']; */
                ?>
                <option value="bbb" >bbb</option>
                <?php
            //}
            ?>
        </select>
    </p>
    <p>
        <label for="searchkeywords">By keyword</label>
        <br />
        <input name="sKeywordSearch" id="searchkeywords" class="large-input" onclick="frmSrch.searchcategory.selectedIndex = '';
                frmSrch.searchbrand.selectedIndex = '';" value=""  />
    </p>
    <p>
        <input name="sidesearch" type="submit" class="small-btn" value="Find"  />
    </p>
</form>
