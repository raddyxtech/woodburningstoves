<section class="login entire_width">
    <div class="container_12"> 
        <meta http-equiv="refresh" content="10;url=<?= HTTP_ROOT ?>login">
        <div id="content">
            <div class="login_cont">
                <h3>Congratulations!! <br/> <span style="font-size:18px;">Your account has been activated successfully!!<span></h3> 
                <p style="font-size:16px;">Please click <a href="<?= HTTP_ROOT ?>users/userLogin">here</a> to go the login page else it will be redirected after 10 secs automatically.</p>  
                            <br/><br/><br/>         
            </div>
        </div><!-- #content -->
    <div class="clear"></div>
    </div><!-- .container_12 -->
</section><!-- #main -->
<!--<script>
    setTimeout(function () {
        window.location = HTTP_ROOT."users/userLogin";
    }, 10000);
</script>-->
