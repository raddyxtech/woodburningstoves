<link href="css/bootstrap.min.css" rel="stylesheet">
<link href="css/theme.css" rel="stylesheet">
<section class="page-section">
    <div class="wrap container">
        <div class="row">
            <div class="col-md-12">
                <div class="information-title">Return Requests</div>
                <div class="details-wrap">                                    
                    <div class="details-box orders">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th class="thead">Image</th>
                                    <th class="thead">Qty</th>
                                    <th class="thead">Product Name</th>
                                    <th class="thead">Price</th>
                                    <th class="thead">Order ID</th>
                                    <th class="thead">Status</th>                                                    
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                if ($countRecords > 0) {
                                    foreach ($userInfos as $userInfo) :
                                        ?>
                                        <tr>
                                            <td class="image">
                                                <?php
                                                foreach ($userInfo['product']['images'] as $image) :
                                                    if ($image['main_image'] == "Y") {
                                                        ?>
                                                        <img style="height: 100px; width: 80px;" src="<?= HTTP_ROOT . PRODUCT_PIC . $image['filename']; ?>" />
                                                        <?php
                                                    }
                                                endforeach;
                                                ?>            
                                            </td>
                                            <td class="quantity"><?= $userInfo['qty']; ?></td>
                                            <td class="description">
                                                <?= $userInfo['name']; ?>
                                            </td>
                                            <td class="total">$<?= $userInfo['total']; ?></td>
                                            <td class="order-id" style="font-size: 13px; font-weight: 600;color: #232323;"><?= $userInfo['unique_id']; ?></td>
                                            <td class="status"> Return </td>                                                    
                                        </tr> 
                                    <?php
                                    endforeach;
                                } else {
                                    ?>
                                    <tr>
                                        <td colspan='6'> You have no return request.</td>
                                    </tr>
<?php } ?>
                            </tbody>
                        </table>

                        <div>
                            <a href="<?= HTTP_ROOT ?>users/account-information" class="btn btn-theme" style="height: 36px;padding-top: 8px;"> Back To Account </a>
                        </div>
                    </div>
                </div>                                
            </div>                       
        </div>                                
    </div>
    <!--end main contain of page-->
</section>

