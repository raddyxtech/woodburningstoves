<section class="login entire_width">
    <div class="container_12">      
       <div id="content">
		
		<div class="grid_12">
                    <h2>Reset Password</h2>
                       <?= $this->Form->create(NULL, ['id' => 'login-form']); ?>
                       <div class="password">
			<strong>Password:</strong><sup class="surely">*</sup><br/>
                       <?= $this->Form->control('password', ['placeholder' => 'New Password', 'div' => false, 'label' => false, 'required' => true]); ?>       
                       </div><br/>
                       <div class="password">
                        <strong>Confirm Password:</strong><sup class="surely">*</sup><br/>

                       <?= $this->Form->control('conf_password', ['type' => 'password', 'placeholder' => 'Confirm New Password', 'div' => false, 'label' => false, 'required' => true]); ?>       
                       </div><br/>
                           <p>  
                           <a href="<?= HTTP_ROOT . "users/userLogin" ?>">Back to Login?</a> |
                           <a href="<?= HTTP_ROOT . "users/register" ?>">Signup?</a>
                       </p>
                       <?= $this->Form->submit('Submit', ['class' => 'login-btn']); ?>
                       <?= $this->Form->end(); ?><br><br/>
                </div><!-- .grid_6 -->
       </div><!-- #content -->
       
      <div class="clear"></div>
    </div><!-- .container_12 -->
  </section><!-- #main -->
  <script>
    $("#login-form").validate({
        rules: {
            password: "required",
            conf_password: {
                required: true,
                equalTo: password
            }
        },
        messages: {
            password: "Please enter a new password",
            conf_password: {
                required: 'Retype password required',
                equalTo: 'Confirm password did not match'
            }
        },
        submitHandler: function (form) {
            form.submit();
        }
    });
</script>
<!--  <script type = "text/javascript">
   
      function validate() {
      
         if( document.myForm.password.value == "" ) {
            document.getElementById('password').innerHTML = "Please enter password!";
            return false;
         }
        else if( document.myForm.re_password.value == "" ) {
             document.getElementById('re_password').innerHTML = "Please enter confirm password!";
            return false;
         }
         else if( document.myForm.password.value != document.myForm.re_password.value ){
             document.getElementById('re_password').innerHTML = "\nPassword did not match: Please Try again!";
             return false;
         }
         return( true );
      }
   
</script>-->