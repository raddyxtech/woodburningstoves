<?php
$explode = explode(" ", $userInfo->username);
$firstName = $explode[0];
$lastName = $explode[1];
?>
<section class="page-section">
    <div class="wrap container">
        <div class="row">
            <?= $this->element('sidebar'); ?>
            <!--start main contain of page-->
            <div class="col-lg-9 col-md-9 col-sm-8">
                <div class="information-title">YOUR ACCOUNT INFORMATION</div>
                <div class="details-wrap">
                    <div class="details-box">
                        <?= $this->Form->create(NULL, ['class' => 'form-delivery', 'name' => 'registration']); ?>
                        <div class="row">
                            <div class="col-md-6 col-sm-6">
                                <label style="margin-bottom: -30px;">First Name</label>
                                <div class="form-group"><?= $this->Form->control('', ['value' => $firstName, 'type' => 'text', 'placeholder' => 'First Name', 'class' => 'form-control', 'name' => 'first_name', 'required' => TRUE]); ?></div>
                            </div>
                            <div class="col-md-6 col-sm-6">
                                <label style="margin-bottom: -30px;">Last Name</label>
                                <div class="form-group"><?= $this->Form->control('', ['value' => $lastName, 'type' => 'text', 'placeholder' => 'Last Name', 'class' => 'form-control', 'name' => 'last_name', 'required' => TRUE]); ?></div>
                            </div>                                                                                             
                            <div class="col-md-6 col-sm-6">
                                <label style="margin-bottom: -30px;">Email</label>
                                <div class="form-group"><?= $this->Form->control('', ['value' => $userInfo->email, 'type' => 'email', 'placeholder' => 'Email', 'class' => 'form-control', 'name' => 'email', 'required' => TRUE]); ?></div>
                            </div>
                            <div class="col-md-6 col-sm-6">
                                <label style="margin-bottom: -30px;">Phone Number</label>
                                <div class="form-group"><?= $this->Form->control('', ['value' => $userInfo->mobile, 'type' => 'number', 'placeholder' => 'Phone Number', 'class' => 'form-control', 'name' => 'mobile', 'required' => TRUE]); ?></div>
                            </div>  

                            <div class="col-md-12 col-sm-12">
                                <button class="btn btn-theme btn-theme-dark" type="submit" style="margin-top:0px;width:160px;"> Update </button>
                            </div>
                        </div>
                        <?= $this->Form->end(); ?>
                    </div>
                </div>                                
            </div>                                
        </div>
        <!--end main contain of page-->


    </div>

</div>
</section>
<script>
// Wait for the DOM to be ready
    $(function () {
        // Initialize form validation on the registration form.
        // It has the name attribute "registration"
        $("form[name='registration']").validate({
            // Specify validation rules
            rules: {
                // The key name on the left side is the name attribute
                // of an input field. Validation rules are defined
                // on the right side
                first_name: "required",
                last_name: "required",
                mobile: "required",
                email: {
                    required: true,
                    // Specify that email should be validated
                    // by the built-in "email" rule
                    email: true
                }
            },
            // Specify validation error messages
            messages: {
                firstname: "Please enter your firstname",
                lastname: "Please enter your lastname",
                mobile: "Please enter your phone number",
                email: "Please enter a valid email address"
            },
            // Make sure the form is submitted to the destination defined
            // in the "action" attribute of the form when valid
            submitHandler: function (form) {
                form.submit();
            }
        });
    });
</script>
