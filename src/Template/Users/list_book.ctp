<style>
    .details-wrap {
        border: 3px solid #e6e6e6;
    }
    .head {
        color: #1a1919;
        background-color: #f5f6f7;
        padding: 7px;
        font-size: 20px;
        margin: 0 0 5px 0;
        font-family: Roboto,Arial,sans-serif;
    }
    .editaddress {
        float: right;
        margin-right: 5px;
        color: #25909f;
    }
    .delete-user {
        color: red;
        float: right;
    }
    .pro-d-row {
        width: 100%;
        float: left;
        margin-bottom: 5px;
        padding-bottom: 20px;
        border-bottom: 1px solid #ccc;
    }
    .profile-data ul li .address-box:last-child .pro-d-row {border-bottom:0;}
    .pro-d-right {
        font-size: 17px;
        color: #212121;
        font-weight: 501;
        margin-left: 31px;
        font-family: Roboto,Arial,sans-serif;    
    }
    .pro-d-right-new {
        margin-left: 31px;
        color: #212121;
        font-size: 15px;
        line-height: 1.5;
        margin-top: 10px;
    }
    .address-box{ 
        font-size: 14px;
    }
    .action-btn {
        float: right;
        margin-right: 32px
    }
    .action-btn-list {
        box-shadow: rgba(179, 160, 160, 0.7) 0px 0px 3px;
        position: absolute;
        right: 21px;
        width: 77px;
        background: rgb(255, 255, 255) none repeat scroll 0% 0%;
        border-radius: 4px;
        border: 1px solid rgb(215, 224, 241);
        padding: 8px 10px;
    }
    .address{
        border: 3px solid #e6e6e6;
        padding: 13px;
        margin-top: 12px;
        font-weight: 501;
    }
    .addAddress {
        color: #1c1c1b;
        margin-left: 17px;
    }
    span.address {
        text-transform: uppercase;
        font-size: 11px;
        color: #595656;
        vertical-align: middle;
        padding: 4px 7px;
        border-radius: 2px;
        background-color: #f0f0f0;
        font-weight: 500;
        margin-right: 15px;
        margin-left: 31px
    }

</style>
<link rel="stylesheet" href="alert/dist/sweetalert.css">
<section class="page-section">
    <div class="wrap container">
        <div class="row">
            <?= $this->element('sidebar'); ?>
            <div class="col-lg-9 col-md-9 col-sm-8">
                <div class="information-title">Manage  Your  Address</div>
                <div class="details-wrap">

                    <div class="details-box">
                        <div class="profile-data">
                            <ul>
                                <li>
                                    <h3 class="head">Delivery Address</h3> 
                                    <?php foreach ($orderLists as $order) { ?>
                                        <div class="address-box">
                                            <div class="action-btn"><i class="fa fa-th" aria-hidden="true"></i>
                                                <ul class="action-btn-list" style="display: none;">

                                                    <li><a style="color:#1c1c1b;" href="<?= $this->Url->build(['controller' => 'Users', 'action' => 'addressBook', $order->order_id]) ?>"><i class="fa fa-angle-right" aria-hidden="true"></i>Edit</a></li>
                                                    <li><a style="color:#1c1c1b;"  href="<?= $this->Url->build(['controller' => 'Users', 'action' => 'deleteAddress', $order->order_id], ['class' => 'btn btn-danger']) ?>" onclick="return confirm('Are you sure you want to delete?')"><i class="fa fa-angle-right" aria-hidden="true"></i>Delete</a></li>
                                                </ul>
                                            </div>
                                            <div class="pro-d-row">
                                                <div style="margin-bottom: 5px;">
                                                    <span class="address"><?= $order->address_type; ?></span>
                                                </div>
                                                <div class="pro-d-right">
                                                    <?= $order->invoice_firstname; ?>
                                                    <?= $order->invoice_lastname; ?> 
                                                    <span>
                                                        <?= $order->invoice_tel1; ?></span>
                                                </div>
                                                <div class="pro-d-right-new">
                                                    <?= $order->invoice_address1; ?>,
                                                    <?= $order->invoice_address2; ?>,
                                                    <?= $order->invoice_countryid; ?>, 
                                                    <?= $order->invoice_city; ?> - 
                                                    <span style="font-weight: 501;"><?= $order->invoice_postcode; ?></span>                                                   
                                                    <div class="pro-d"><?= $order->invoice_email; ?></div>
                                                </div>
                                            </div>
                                        </div>
                                    <?php } ?>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="clear"></div>
                </div>
                <div class="clear"></div>
                <div class="address">
                    <i class="fa fa-plus" aria-hidden="true" style="color: black;"></i>
                    <a class="addAddress" href="<?= $this->Url->build(['controller' => 'Users', 'action' => 'addressBook']); ?>">ADD NEW ADDRESS</a>
                </div>
            </div>
        </div>
    </div>
</div>
</section>
<script src="plugins/sweet-alert2/sweetalert2.min.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script>
$(document).ready(function () {
    $(".action-btn").click(function () {
        if ($(this).find(".action-btn-list").is(':visible')) {
            $(this).find(".action-btn-list").fadeOut();
        } else {
            $(".action-btn-list").fadeOut();
            $(this).find(".action-btn-list").fadeIn();
        }
    });
    $('html').click(function (actionBtn) {
        if ($(actionBtn.target).closest('.action-btn, .action-btn-list').length === 0) {
            $(".action-btn-list").fadeOut();
        }
    });
    
  
});
//$(document).on('click', '.addAddress', function (e) {
////    alert(111);
//    var element = $(this);
//    var orderid = element.data('orderId');
//
//    $.ajax({
//        type: 'POST',
//        url: "<?= $this->Url->build(['controller' => 'Users', 'action' => 'addressBook']); ?>",
//        data: {'order_id': orderId},
//        dataType: 'json',
//        success: function (response) {
//            if (response.status == 'success') {
//                element.removeClass("addAddress").addClass("removeFavorite");
//            } else {
//                alert(response.msg);
//            }
//        }
//    });
//});
</script> 