<link href="css/bootstrap.min.css" rel="stylesheet">
<link href="css/theme.css" rel="stylesheet">
<section class="page-section">
    <div class="wrap container">
        <div class="row">
            <div class="col-md-12">
                <div class="information-title">YOUR REVIEWS AND RATINGS</div>
                <div class="details-wrap">                                    
                    <div class="details-box orders">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th class="thead">Image</th>
                                    <th class="thead">Product Name</th>
                                    <th class="thead">Quality</th>
                                    <th class="thead">Price</th>
                                    <th class="thead">Review</th>                                                    
                                    <th class="thead">Date</th>                                                    
                                    <th class="thead">Action</th>                                                    
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($reviewRatings as $reviewRating) : ?>
                                    <tr>
                                        <td class="image">
                                            <?php
                                            foreach ($reviewRating['product']['images'] as $image) :
                                                if ($image['main_image'] == "Y") {
                                                    ?>
                                                    <img style="height: 100px; width: 80px;" src="<?= HTTP_ROOT . PRODUCT_PIC . $image['filename']; ?>" />
                                                    <?php
                                                }
                                            endforeach;
                                            ?>        
                                        </td>
                                        <td class="description">
                                            <?= $reviewRating['product']['name']; ?>
                                        </td>
                                        <td class="quality_rating" >
                                            <?php if ($reviewRating['quality_rating'] == '1') { ?>
                                                <img src="<?= HTTP_ROOT ?>images/download.png" height="15px" width="15px">
                                                <img src="<?= HTTP_ROOT ?>images/download-blank.jpg" height="15px" width="15px">
                                                <img src="<?= HTTP_ROOT ?>images/download-blank.jpg" height="15px" width="15px">
                                                <img src="<?= HTTP_ROOT ?>images/download-blank.jpg" height="15px" width="15px">
                                                <img src="<?= HTTP_ROOT ?>images/download-blank.jpg" height="15px" width="15px">
                                            <?php } elseif ($reviewRating['quality_rating'] == '2') { ?>
                                                <img src="<?= HTTP_ROOT ?>images/download.png" height="15px" width="15px">
                                                <img src="<?= HTTP_ROOT ?>images/download.png" height="15px" width="15px">
                                                <img src="<?= HTTP_ROOT ?>images/download-blank.jpg" height="15px" width="15px">
                                                <img src="<?= HTTP_ROOT ?>images/download-blank.jpg" height="15px" width="15px">
                                                <img src="<?= HTTP_ROOT ?>images/download-blank.jpg" height="15px" width="15px">
                                            <?php } elseif ($reviewRating['quality_rating'] == '3') { ?>
                                                <img src="<?= HTTP_ROOT ?>images/download.png" height="15px" width="15px">
                                                <img src="<?= HTTP_ROOT ?>images/download.png" height="15px" width="15px">
                                                <img src="<?= HTTP_ROOT ?>images/download.png" height="15px" width="15px">
                                                <img src="<?= HTTP_ROOT ?>images/download-blank.jpg" height="15px" width="15px">
                                                <img src="<?= HTTP_ROOT ?>images/download-blank.jpg" height="15px" width="15px">
                                            <?php } elseif ($reviewRating['quality_rating'] == '4') { ?>
                                                <img src="<?= HTTP_ROOT ?>images/download.png" height="15px" width="15px">
                                                <img src="<?= HTTP_ROOT ?>images/download.png" height="15px" width="15px">
                                                <img src="<?= HTTP_ROOT ?>images/download.png" height="15px" width="15px">
                                                <img src="<?= HTTP_ROOT ?>images/download.png" height="15px" width="15px">
                                                <img src="<?= HTTP_ROOT ?>images/download-blank.jpg" height="15px" width="15px">
                                            <?php } elseif ($reviewRating['quality_rating'] == '5') { ?>
                                                <img src="<?= HTTP_ROOT ?>images/download.png" height="15px" width="15px">
                                                <img src="<?= HTTP_ROOT ?>images/download.png" height="15px" width="15px">
                                                <img src="<?= HTTP_ROOT ?>images/download.png" height="15px" width="15px">
                                                <img src="<?= HTTP_ROOT ?>images/download.png" height="15px" width="15px">
                                                <img src="<?= HTTP_ROOT ?>images/download.png" height="15px" width="15px">
                                            <?php } else { ?>
                                                <img src="<?= HTTP_ROOT ?>images/download-blank.jpg" height="15px" width="15px">
                                                <img src="<?= HTTP_ROOT ?>images/download-blank.jpg" height="15px" width="15px">
                                                <img src="<?= HTTP_ROOT ?>images/download-blank.jpg" height="15px" width="15px">
                                                <img src="<?= HTTP_ROOT ?>images/download-blank.jpg" height="15px" width="15px">
                                                <img src="<?= HTTP_ROOT ?>images/download-blank.jpg" height="15px" width="15px">
                                            <?php }
                                            ?>
                                        </td>
                                        <td class="price_rating">
                                            <?php if ($reviewRating['price_rating'] == '1') { ?>
                                                <img src="<?= HTTP_ROOT ?>images/download.png" height="15px" width="15px">
                                                <img src="<?= HTTP_ROOT ?>images/download-blank.jpg" height="15px" width="15px">
                                                <img src="<?= HTTP_ROOT ?>images/download-blank.jpg" height="15px" width="15px">
                                                <img src="<?= HTTP_ROOT ?>images/download-blank.jpg" height="15px" width="15px">
                                                <img src="<?= HTTP_ROOT ?>images/download-blank.jpg" height="15px" width="15px">
                                            <?php } elseif ($reviewRating['price_rating'] == '2') { ?>
                                                <img src="<?= HTTP_ROOT ?>images/download.png" height="15px" width="15px">
                                                <img src="<?= HTTP_ROOT ?>images/download.png" height="15px" width="15px">
                                                <img src="<?= HTTP_ROOT ?>images/download-blank.jpg" height="15px" width="15px">
                                                <img src="<?= HTTP_ROOT ?>images/download-blank.jpg" height="15px" width="15px">
                                                <img src="<?= HTTP_ROOT ?>images/download-blank.jpg" height="15px" width="15px">
                                            <?php } elseif ($reviewRating['price_rating'] == '3') { ?>
                                                <img src="<?= HTTP_ROOT ?>images/download.png" height="15px" width="15px">
                                                <img src="<?= HTTP_ROOT ?>images/download.png" height="15px" width="15px">
                                                <img src="<?= HTTP_ROOT ?>images/download.png" height="15px" width="15px">
                                                <img src="<?= HTTP_ROOT ?>images/download-blank.jpg" height="15px" width="15px">
                                                <img src="<?= HTTP_ROOT ?>images/download-blank.jpg" height="15px" width="15px">
                                            <?php } elseif ($reviewRating['price_rating'] == '4') { ?>
                                                <img src="<?= HTTP_ROOT ?>images/download.png" height="15px" width="15px">
                                                <img src="<?= HTTP_ROOT ?>images/download.png" height="15px" width="15px">
                                                <img src="<?= HTTP_ROOT ?>images/download.png" height="15px" width="15px">
                                                <img src="<?= HTTP_ROOT ?>images/download.png" height="15px" width="15px">
                                                <img src="<?= HTTP_ROOT ?>images/download-blank.jpg" height="15px" width="15px">
                                            <?php } elseif ($reviewRating['price_rating'] == '5') { ?>
                                                <img src="<?= HTTP_ROOT ?>images/download.png" height="15px" width="15px">
                                                <img src="<?= HTTP_ROOT ?>images/download.png" height="15px" width="15px">
                                                <img src="<?= HTTP_ROOT ?>images/download.png" height="15px" width="15px">
                                                <img src="<?= HTTP_ROOT ?>images/download.png" height="15px" width="15px">
                                                <img src="<?= HTTP_ROOT ?>images/download.png" height="15px" width="15px">
                                            <?php } else { ?>
                                                <img src="<?= HTTP_ROOT ?>images/download-blank.jpg" height="15px" width="15px">
                                                <img src="<?= HTTP_ROOT ?>images/download-blank.jpg" height="15px" width="15px">
                                                <img src="<?= HTTP_ROOT ?>images/download-blank.jpg" height="15px" width="15px">
                                                <img src="<?= HTTP_ROOT ?>images/download-blank.jpg" height="15px" width="15px">
                                                <img src="<?= HTTP_ROOT ?>images/download-blank.jpg" height="15px" width="15px">
                                            <?php }
                                            ?>
                                        </td>
                                        <td class="status"> <?= $reviewRating['review']; ?> </td>                                                    
                                        <td class="review-date"><?= $reviewRating['date']; ?> </td>                                                    
                                        <td class="delete"> 
                                            <a href="<?= HTTP_ROOT ?>users/deleteReview/<?= $reviewRating['id']; ?>" style="color:#e91e63c2;" class="btn btn-xs bg-pink text-white hint--left delete-user"> <i class ="fa fa-trash" ></i> </a> 
                                        </td>                                                    
                                    </tr> 
                                <?php endforeach; ?>
                            </tbody>
                        </table>

                        <div>
                            <a href="<?= HTTP_ROOT ?>users/account-information" class="btn btn-theme" style="height:36px;padding-top: 8px;"> Back To Account </a>
                        </div>
                    </div>
                </div>                                
            </div>                       
        </div>                                
    </div>
    <!--end main contain of page-->
</section>

