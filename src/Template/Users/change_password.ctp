<style>
    #crnt_password-error,
    #rePassword-error,
    #password-error{
        color:red;
    }   
</style>
<section class="page-section">
    <div class="wrap container">
        <div class="row">
            <?= $this->element('sidebar'); ?>
            <!--start main contain of page-->
            <div class="col-lg-9 col-md-9 col-sm-8">
                <div class="information-title">CHANGE YOUR PASSWORD</div>
                <div class="details-wrap">
                    <div class="details-box">
                        <?= $this->Form->create(NULL, ['class' => 'form-delivery', 'id' => 'myform']); ?>
                        <div class="row">
                            <div class="col-md-6 col-sm-6">
                                <div class="form-group">
                                    <?= $this->Form->control('', ['type' => 'password', 'placeholder' => 'Current Password', 'class' => 'form-control', 'name' => 'curr_password', 'id' => 'curr_password']); ?>
                                </div>
                            </div>
                            <div class="clear"></div>
                            <div class="col-md-6 col-sm-6">
                                <div class="form-group">
                                    <?= $this->Form->control('', ['type' => 'password', 'placeholder' => 'New Password', 'class' => 'form-control', 'name' => 'password', 'id' => 'password']); ?>
                                </div>
                            </div>
                            <div class="clear"></div>
                            <div class="col-md-6 col-sm-6">
                                <div class="form-group">
                                    <?= $this->Form->control('', ['type' => 'password', 'placeholder' => 'Confirm Password', 'class' => 'form-control', 'name' => 're_password', 'id' => 're_password']); ?>
                                </div>
                            </div>
                            <div class="clear"></div>
                            <div class="col-md-12 col-sm-12">
                                <button class="btn btn-theme btn-theme-dark"style="padding: 10px 10px;width:160px;margin-top: 6px;" type="submit" onclick="validatePassword()"> Update </button>
                            </div>
<!--                            <div class="col-md-6 col-sm-6">
                                <button class="btn btn-theme btn-theme-dark" type="submit" onclick="" style="margin-top: -50px;float: right;background-color: red;border-color: red;"> Cancel </button>
                            </div>-->
                        </div>
                        <?= $this->Form->end(); ?>
                    </div>
                </div>                                
            </div>                                
        </div>
        <!--end main contain of page-->


    </div>

</div>
</section>
<script src="<?= HTTP_ROOT ?>validation/dist/jquery.validate.min.js" language="javascript"></script>
<script>
    function validatePassword() {
        var validator = $('#myform').validate({
            rules: {
                curr_password: "required",
                password: "required",
                re_password: {
                    equalTo: "#password"
                }
            },
            messages: {
                curr_password: " Enter Current Password",
                password: " Enter Password",
                re_password: " Enter Confirm Password Same as Password"
            }
        });
    }
</script>

