<section class="login entire_width">
    <div class="container_12">      
        <div id="content">
            <div class="grid_12">
                <?= $this->Form->create(NULL, ['url' => '/users/forgotPassword', 'name' => 'myForm', 'class' => 'registed', 'method' => 'post', 'onsubmit' => 'return(validate());']); ?>
                <h2>Forgot Password</h2>
                <div class="email">
                    <strong>Email Adress:</strong><sup class="surely">*</sup><br/>
                    <input type="email" name="email" class="" value="" />
                    <span id="email" class="register_validate"></span>
                </div><!-- .email -->									
                <?= $this->Form->submit('submit'); ?>
                <?= $this->Form->end(); ?><br/><br/>
            </div><!-- .grid_6 -->
        </div><!-- #content -->
        <div class="clear"></div>
    </div><!-- .container_12 -->
</section><!-- #main -->
<script type = "text/javascript">

    function validate() {

        if (document.myForm.email.value == "") {
            document.getElementById('email').innerHTML = "Please provide your Email!";
            return false;
        }
        return(true);
    }

</script>