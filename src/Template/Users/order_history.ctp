<link href="css/bootstrap.min.css" rel="stylesheet">
<link href="css/theme.css" rel="stylesheet">
<section class="page-section" id="ordTblBody">
    <div class="wrap container">
        <div class="row">
            <div class="col-md-12">
                <div class="information-title">Your Order History</div>
                <div class="details-wrap">                                    
                    <div class="details-box orders">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th class="thead">Image</th>
                                    <th class="thead">Qty</th>
                                    <th class="thead">Product Name</th>
                                    <th class="thead">Price</th>
                                    <th class="thead">Order ID</th>
                                    <th class="thead">Delivered on</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                foreach ($userInfos as $userInfo):
                                    if ($userInfo['return_status'] != "A" && $userInfo['cancel_status'] != "A") {
                                        if ($countRecords > 0) {
                                            ?>
                                            <tr>
                                                <td class="image">
                                                    <?php
                                                    foreach ($userInfo['product']['images'] as $image) :
                                                        if ($image['main_image'] == "Y") {
                                                            ?>
                                                            <img style="height: 100px; width: 80px;" src="<?= HTTP_ROOT . PRODUCT_PIC . $image['filename']; ?>" />
                                                            <?php
                                                        }
                                                    endforeach;
                                                    ?>        
                                                </td>
                                                <td class="quantity"><?= $userInfo['qty']; ?></td>
                                                <td class="description">
                                                    <?= $userInfo['name']; ?>
                                                </td>
                                                <td class="total">$<?= $userInfo['total']; ?></td>
                                                <td class="order-id" style="font-size: 13px; font-weight: 600;color: #232323;"><?= $userInfo['unique_id']; ?></td>
                                                <td class="diliver-date"> 12th Dec'13 </td>
                                                <td class="order-status" >
                                                    <!--$userInfo['cancel_status'] == "C" ('C' for CANCEL ORDER),
                                                    $userInfo['return_status'] == "R" ('R' for RETURN ORDER),
                    $userInfo['return_status'] == "P" and $userInfo['cancel_status'] == "P" (STATUS CHANGE 'P' WHEN PAYMENT IS SUCCESS)
                    $userInfo['return_status'] == "G" and $userInfo['cancel_status'] == "G" (STATUS CHANGE 'G' WHEN PRODUCT DELIVER IS SUCCESS)-->
                                                    <?php if ($userInfo['return_status'] == ORDSTATUS_PAYMENT && $userInfo['cancel_status'] == ORDSTATUS_PAYMENT) { ?>
                                                        <button class="btn btn-theme btn-theme-dark" onclick="updateStatus('CANCEL',<?= $userInfo['id']; ?>)" style="">CANCEL ORDER</button>
                                                    <?php } else if ($userInfo['return_status'] == ORDSTATUS_DISPATCHED && $userInfo['cancel_status'] == ORDSTATUS_DISPATCHED) { ?>
                                                        <button onclick="updateStatus('RETURN', <?= $userInfo['id']; ?>)" class="btn btn-theme btn-theme-dark" style="">Return Order</button>
                                                    <?php } else if ($userInfo['cancel_status'] == ORDSTATUS_CANCEL && $userInfo['return_status'] == ORDSTATUS_PAYMENT) { ?>
                                                        <span class="return-request" id="span1"> You requested <br> this order for cancel </span>
                                                    <?php } else { ?>
                                                        <span class="return-request" id="span1"> You requested <br> this order for return </span>
                                                    <?php } ?>
                                                </td>        
                                            </tr>
                                        <?php } else { ?>
                                            <tr>
                                                <td colspan = '7'>Your Order History is empty.</td>
                                            </tr>
                                        <?php }
                                    } endforeach; ?>
                            </tbody>
                        </table>
                        <div>
                            <a href="<?= HTTP_ROOT ?>users/accountInformation" class="btn btn-theme" style="padding-top: 8px;height: 36px;"> Back To Account </a>
                        </div>
                    </div>
                </div>                                
            </div>                        
        </div>                                
    </div>
    <!--end main contain of page-->
</section>
<script>
            function updateStatus(type = null, id = null) {
            $.ajax({
            type: "POST", //or POST
                    url: "<?= $this->Url->build(['controller' => 'Users', 'action' => 'orderHistory']); ?>",
                    data: {status: type, id},
                    //can send multipledata like {data1:var1,data2:var2,data3:var3
                    //can use dataType:'text/html' or 'json' if response type expected 
                    success: function (responsedata) {
                    // process on data
//                alert("got response as " + "'" + responsedata + "'");
                    $('#ordTblBody').html(responsedata);
                    }
            })
            }
</script>
