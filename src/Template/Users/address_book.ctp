<style>
.radio_div radio {
    margin-right: 5px;
}
</style>
<section class="page-section">
    <div class="container">
        <div class="row">
            <?= $this->element('sidebar'); ?>
            <!--start main contain of page-->
            <div class="col-lg-9 col-md-9 col-sm-8">
                <div class="details-wrap">
                    <div class="information-title">Manage  Your  Address</div>
                    <div class="details-box">
                        <div class="row">  
                            <?= $this->Form->create(NULL, ['class' => 'form-address', 'name' => 'address']); ?>
                            <div class="col-md-6 col-sm-12">
                                <div class="form-group">
                                    <?= $this->Form->control('', ['placeholder' => 'First Name', 'class' => 'form-control', 'name' => 'invoice_firstname', 'value' => $order->invoice_firstname, 'required' => TRUE]); ?>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-12">
                                <div class="form-group">
                                    <?= $this->Form->control('', ['placeholder' => 'Last Name', 'class' => 'form-control', 'name' => 'invoice_lastname', 'value' => $order->invoice_lastname, 'required' => TRUE]); ?>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-12">
                                <div class="form-group">
                                    <?= $this->Form->textarea('', ['placeholder' => 'Address 1', 'class' => 'form-control', 'name' => 'invoice_address1', 'value' => $order->invoice_address1, 'required' => TRUE]); ?>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-12">
                                <div class="form-group">
                                    <?= $this->Form->textarea('', ['placeholder' => 'Address 2', 'class' => 'form-control', 'name' => 'invoice_address2', 'value' => $order->invoice_address2, 'required' => TRUE]); ?>
                                </div>
                            </div>
                            <div class="clear"></div>

                            <div class="col-md-3 col-sm-9">
                                <div class="form-group">
                                    <?= $this->Form->text('invoice_postcode', ['label' => false, 'type' => 'text', 'id' => 'postcode', 'value' => $order->invoice_postcode, 'placeholder' => 'Enter postcode']); ?> 
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-9">
                                <div class="form-group">
                                    <?= $this->Form->control('', ['label' => false, 'type' => 'email', 'placeholder' => 'Email', 'class' => 'form-control', 'name' => 'invoice_email', 'value' => $order->invoice_email, 'required' => TRUE]); ?>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-9">
                                <div class="form-group">
                                    <?= $this->Form->control('', ['label' => false, 'type' => 'number', 'placeholder' => 'Phone Number', 'class' => 'form-control', 'name' => 'invoice_tel1', 'value' => $order->invoice_tel1, 'required' => TRUE]); ?>
                                </div>
                            </div>
                            <div class="clear"></div>
                            <div class="col-md-3 col-sm-6">
                                <div class="form-group selectpicker-wrapper">
                                    <?=
                                    $this->Form->select('invoice_countryid', $countriesList, array('type' => 'option', 'label' => false,
                                        'value' => $order->invoice_countryid,
                                        'empty' => '-----select country-----',
                                        'class' => 'selectpicker input-price',
                                        'data-live-search' => 'true', 'data-width' => '100%',
                                        'data-toggle' => 'tooltip', 'title' => 'Select'));
                                    ?>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-6">
                                <div class="form-group">
                                    <?= $this->Form->text('invoice_city', ['class' => 'large-input', 'label' => false, 'type' => 'text', 'value' => $order->invoice_city, 'placeholder' => 'Enter City/town']); ?> 
                                </div>
                            </div>
                            <div class="col-md-3">
                                    <div class="radio_div">
                                        <p style="margin-bottom:9px;font-size: 18px;">Address Type</p>                                    
                                    <?= $this->Form->radio('address_type', [
                                        ['value' => 'Home', 'text' => 'Home','label' => ['class' => 'Home']],
                                        ['value' => 'Work', 'text' => 'Work','label' => ['class' => 'Work']],
                                     ]);
                                    ?>                                
                                </div>
                            </div>
                            <div class="col-md-12 col-sm-12">
                                <button type="submit" class="btn btn-theme btn-theme-dark"> Submit </button>
                                <a class="btn btn-danger" style="margin-bottom: 15px;" href="<?= $this->Url->build(['controller' => 'Users', 'action' => 'listBook']); ?>"> CANCEL </a>
                            </div>
                            <?= $this->Form->end(); ?>
                        </div>
                    </div>
                </div>                                
            </div>
        </div>
    </div>
