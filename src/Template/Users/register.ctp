<section class="login entire_width">
    <div class="container_12">      

        <div id="content">
            <div class="grid_12">
                <h1 class="page_title">Create an Account</h1>
            </div><!-- .grid_12 -->
            <div class="grid_12">
                <?= $this->Form->create(NULL, ['class' => 'registed', 'name' => 'myForm', 'onsubmit' => 'return(validate());']); ?>
                <h2>Customer Register</h2>

                <p>If you have an account with us, please log in.</p>
                <div class="grid_6 new_customers" style="margin-top: 0px;">		
                    <div class="name">
                        <strong>Name :</strong><sup class="surely">*</sup><br/>
                        <?= $this->Form->control('', ['type' => 'text', 'name' => 'username']); ?>
                        <span id="name" class="register_validate"></span>
                    </div><!-- .email -->
                    <div class="email">
                        <strong>Email Adress:</strong><sup class="surely">*</sup><br/>
                        <?= $this->Form->control('', ['type' => 'email', 'name' => 'email']); ?>
                        <span id="email" class="register_validate"></span>
                    </div><!-- .email -->
                    <div class="number">
                        <strong>Mobile Number:</strong><sup class="surely">*</sup><br/>
                        <?= $this->Form->control('', ['type' => 'number', 'name' => 'mobile']); ?>
                        <span id="mobile" class="register_validate"></span>
                    </div><!-- .email -->
                </div><!-- .email -->
                <div class="password">
                    <strong>Password:</strong><sup class="surely">*</sup><br/>
                    <?= $this->Form->control('password', ['type' => 'password', 'name' => 'password','label'=>false]); ?>
                    <span id="password" class="register_validate"></span>
                </div><!-- .password -->
                <div class="password">
                    <strong>Confirm Password:</strong><sup class="surely">*</sup><br/>
                    <?= $this->Form->control('', ['type' => 'password', 'name' => 're_password']); ?>
                    <span id="re_password" class="register_validate"></span>
                </div><!-- .password -->
                    <?= $this->Form->submit('Register', ['type' => 'submit']); ?>
                <?= $this->Form->end(); ?><br><br/>
            </div><!-- .grid_6 -->
        </div><!-- #content -->

        <div class="clear"></div>

    </div><!-- .container_12 -->
</section><!-- #main -->
<script type = "text/javascript">

    function validate() {

        if (document.myForm.username.value == "") {
            document.getElementById('name').innerHTML = "Please provide your name!";
            return false;
        }
        else if (document.myForm.email.value == "") {
            document.getElementById('email').innerHTML = "Please provide your Email!";
            return false;

        }
        else if (document.myForm.mobile.value == "") {
            document.getElementById('mobile').innerHTML = "Please provide your Phone Number!";
            return false;
        }
        else if (document.myForm.password.value == "") {
            document.getElementById('password').innerHTML = "Please enter password!";

            return false;
        }
        else if (document.myForm.re_password.value == "") {
            document.getElementById('re_password').innerHTML = "Please enter confirm password!";
            return false;
        }
        else if (document.myForm.cpassword.value != document.myForm.re_password.value) {
            document.getElementById('re_password').innerHTML = "\nPassword did not match: Please Try again!";
            return false;
        }

        return(true);
    }

</script>