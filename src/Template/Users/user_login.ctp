<section class="login entire_width">
    <div class="container_12">      
        <div id="content">
            <div class="grid_12">
                <h1 class="page_title">Login or Create an Account</h1>
            </div><!-- .grid_12 -->

            <div class="grid_6 new_customers">
                <h2>New Customers</h2>
                <p>By creating an account with our store, you will be able to move through the checkout process faster, store multiple shipping addresses, view and track your orders in your account and more.</p>
                <div class="clear"></div>
                <a href = "users/register"><button class="w3-button w3-green w3-large" style="width:100px;">Sign Up</button></a>

            </div><!-- .grid_6 -->

            <div class="grid_6">
                <?= $this->Form->create(NULL, ['url' => '/users/login', 'name' => 'myForm', 'class' => 'registed', 'method' => 'post', 'onsubmit' => 'return(validate());']); ?>
                <h2>Login Form</h2>

                <p>If you have an account with us, please log in.</p>

                <div class="email">
                    <strong>Email Adress:</strong><sup class="surely">*</sup><br/>
                    <input type="email" name="email" class="" value="" />
                    <span id="email" class="register_validate"></span>
                </div><!-- .email -->

                <div class="password">
                    <strong>Password:</strong><sup class="surely">*</sup><br/>
                    <input type="password" name="password" class="" value="" />
                    <span id="password" class="register_validate"></span>
                    <a class="forgot" href="<?= HTTP_ROOT ?>users/forgotPassword" >Forgot Your Password?</a>
                </div><!-- .password -->

                <div class="remember">
                    <?= $this->Form->control('', ['class' => 'niceCheck', 'type' => 'checkbox']); ?>
                    <span class="rem">Remember password</span>
                </div><!-- .remember -->

                <div class="submit">										
                    <?= $this->Form->submit('submit'); ?>

                </div><!-- .submit -->
                <?= $this->Form->end(); ?><br><br/>
            </div><!-- .grid_6 -->
        </div><!-- #content -->

        <div class="clear"></div>
    </div><!-- .container_12 -->
</section><!-- #main -->
<script type = "text/javascript">

    function validate() {

        if (document.myForm.email.value == "") {
            document.getElementById('email').innerHTML = "Please provide your Email!";
            return false;
        }
        else if (document.myForm.password.value == "") {
            document.getElementById('password').innerHTML = "Please enter password!";
            return false;
        }
        return(true);
    }

</script>