<section class="home">
    <div class="container_12">
        <h1><?= $newProducts['title']; ?></h1>
        <div id="inner">
            <?= $newProducts['content']; ?>
        </div>
    </div>
</section>
<div class="clearblock"></div>
