<style>
    div.gallery {
        margin: 9px;
        border: 2px solid #ccc;
        float: left;
        padding: 5px;
    }

</style>
<section class="home">
    <div class="container_12">
        <h1>Brands on Salt Fire Stoves</h1>
        <div>          
            <?php
            foreach ($brands as $brand):
                ?>
                <div class="gallery">
                    <a href="<?= $this->Url->build(['controller' => 'Products', 'action' => 'productsList/', 'brand_id' => $brand->id]); ?>   " target="_self" title="<?= $brand->name; ?>">
                        <img src="<?= HTTP_ROOT . BRAND_PIC . (!empty($brand->image) ?
                        $brand->image : 'no-photo.jpg'); ?>" alt="Image Not Present"  width="220" height="250"></a>                  
                </div>
            <?php endforeach; ?>
        </div>
    </div>
</section>
    <div class="clear"></div>


