<section class="home">
    <div class="container_12">
        <h1><?= $installationAdvice['title']; ?></h1>
        <div id="inner">
            <?= $installationAdvice['content']; ?>
        </div>
    </div>
</section>
<div class="clearblock"></div>
