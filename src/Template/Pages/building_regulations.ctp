<section class="home">
    <div class="container_12">
        <h1><?= $buildingRegulations['title']; ?></h1>

        <div id="inner">
            <?= $buildingRegulations['content']; ?>
        </div>

    </div>
</section>
<!-- InstanceEndEditable -->
<div class="clearblock"></div>
