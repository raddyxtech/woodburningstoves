<section class="home">
    <div class="container_12">
        <h1><?= $flexiLiner['title']; ?></h1>
        <div id="inner">
            <?= $flexiLiner['content']; ?>
        </div>
    </div>
</section>
<div class="clearblock"></div>

