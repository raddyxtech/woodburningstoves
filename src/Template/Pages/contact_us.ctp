<style>
    .login-box .form_div{position: relative;width: 100%;float: left;}
    .login-box .form_div label.error{position: absolute;left: 0;right: 0;bottom: 7px;color: #FF0000;}
    .login-box input[type="text"], .login-box input[type="Password"], .login-box input[type="email"],.login-box input[type="checkbox"] {padding-left: 15px;border: 2px solid #e4e4e4;width: 80%;margin-bottom: 25px;font-size: 13px;line-height: normal;font-weight: 400;color: #757575;text-align: left;}
    .login-box textarea {resize: vertical;border: 2px solid #e4e4e4;border-radius: 12px;height: 90px;width: 125%;padding-left: 15px;margin-bottom: 25px;font-size: 15px;line-height: normal;font-weight: 500;color: #757575;text-align: left;}
    .login-box .login-btn {cursor: pointer;background: #59b7c2;text-transform: uppercase;font-size: 15px;lline-height: 45px;font-weight: 400;color: #fff;padding: 0 30px;margin: 20px 0;}
    /*.g-recaptcha label.error{bottom: -15px !important}*/
    form.contacts  {margin: 22px 0 0 0;padding: 19px;}
    /*.error{color: red;}*/
</style>

<section class="home">
    <div class="container_12">
        <section class="entire_width">
            <div class="container_12">      
                <div id="content">
                    <div class="grid_12">
                        <h1 class="page_title">Contact Us</h1>
                    </div><!-- .grid_12 -->

                    <div class="grid_4 adress">
                        <h3>Address</h3>
                        <?= $companyInfo->company_name; ?><br/>
                        <?= $companyInfo->company_address_1; ?><br/>
                        <?= $companyInfo->company_address_2; ?><br/>
                        <?= $companyInfo->company_city; ?><br/>
                        <?= $companyInfo->company_county; ?><br/>
                        <?= $companyInfo->company_postcode; ?><br/>
                        <hr>

                        <h3>Phones</h3>
                        Tel: <?= $companyInfo->company_tel; ?>
                        <hr>
                        <h3>Email Addresses</h3>
                        Email: <?= $companyInfo->company_email_enquiries; ?>
                    </div><!-- .adress -->

                    <div class="grid_8 login-box form_div" >
                        <?= $this->Form->create(NULL, ['id' => 'contactform', 'class' => 'contacts']) ?>

                        <h2>Quick Contact</h2>
                        <div style="float:left;padding-left: 25px;">
                            <div class="form_div">
                                <strong>Name:</strong><sup class="surely">*</sup><br/>
                                <?= $this->Form->control('name', ['type' => 'text', 'placeholder' => 'Enter your name', 'div' => false, 'label' => false, 'class' => "xlarge-input"]); ?>
                            </div><!-- .name -->

                            <div class="form_div">
                                <strong>Email Adress: </strong><sup class="surely">*</sup><br/>
                                <?= $this->Form->control('email', ['type' => 'text', 'placeholder' => 'Email address', 'div' => false, 'label' => false, 'class' => "xlarge-input"]); ?>
                            </div><!-- .email -->
                        </div>
                        <div style="float:left;padding-left: 25px;">
                            <div class="form_div">
                                <strong>Telephone:</strong><br/>

                                <?= $this->Form->control('tel', ['type' => 'text', 'placeholder' => 'Enter your Tel no.', 'div' => false, 'label' => false, 'class' => "xlarge-input"]); ?>
                            </div><!-- .phone -->

                            <div class="form_div">
                                <strong>Enquiry:</strong><sup class="surely">*</sup><br/>
                                <?= $this->Form->textarea('enquiry', ['placeholder' => 'Enquiry', 'label' => false, 'class' => "xlarge-input"]) ?>
                            </div>
                        </div><!-- .comment -->
                        <div class="form_div" style="padding-left: 25px;"> 
                            <div class="captcha">
                                <div class="g-recaptcha" data-sitekey="<?= RECAPTCHA_SITEKEY ?>"></div><br/>
                            </div> 
                        </div>

                        <div class="form_div">  
                            <div class="check-box-style">
                                <div class="form_div" style="display: -webkit-box;padding-left: 25px;"> 
                                    <?= $this->Form->control('sub_newsletter', ['class' => 'niceCheck', 'type' => 'checkbox', 'div' => false, 'label' => false, 'id' => "squaredFour"]); ?> <label for="squaredFour"></label>
                                    <div> We comply with the GDPR regulations regarding your personal data, if you would like to receive communications from Saltfire on latest products and services via E-mail, please tick the box.
                                    </div>
                                </div>
                                <div class="form_div"> 
                                    <p style="margin-left: 15px;padding-left: 25px;">We’ll never sell on your details to third parties for them to market to you. You can opt out any time, by using the unsubscribe option in each email you receive, click here to view our <a href="<?= $this->Url->build(["controller" => "Pages", "action" => "privacyPolicy"]); ?>" style="color:#a73f16;" target="_blank">privacy policy</a>.</p>
                                </div>
                            </div>
                        </div>
                        <div class="form_div" style="text-align: end;">
                            <sup class="surely">*</sup><span>Required Field</span>
                        </div>
                        <div class="submit">
                            <?= $this->Form->submit('SUBMIT', ['class' => 'login-btn']); ?>
                        </div><!-- .submit -->
                        <?= $this->Form->end(); ?><br/><br/>
                    </div><!-- .grid_8 -->
                </div><!-- #content -->

                <div class="clear"></div>
            </div><!-- .container_12 -->
        </section><!-- #main -->
    </div>
</section>
<div class="clearblock"></div>


<script src="https://www.google.com/recaptcha/api.js"></script>
<script src="<?= HTTP_ROOT ?>validation/dist/jquery.validate.min.js" language="javascript"></script>
<script type="text/javascript">
    $("#contactform").validate({
        ignore: ':hidden:not([name="g-recaptcha-response"])',
        rules: {
            name: 'required',
            email: {
                required: true,
                email: true
            },
            tel: {
                required: true,
                number: true
            },
            enquiry: {
                required: true
            }, 'g-recaptcha-response': "required",
        },
        messages: {
            name: 'Name required.',
            email: {
                required: 'Email required.',
                email: 'Invalid email!!'
            },
            tel: {
                required: 'Phone number required.',
                number: 'Invalid phone number!!'
            },
            enquiry: {
                required: 'Enquiry required.'
            }, 'g-recaptcha-response': 'Please complete re-captcha'
        },
    });
</script>

