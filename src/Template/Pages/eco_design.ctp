<section class="home">
    <div class="container_12">
        <h1><?= $ecoDesign['title']; ?></h1>

        <div id="inner">
            <?= $ecoDesign['content']; ?>
        </div>

        <!-- InstanceEndEditable -->
        <div class="clearblock"></div>
    </div>
</section>