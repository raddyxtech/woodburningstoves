<section class="home">
    <div class="container_12">
        <h1><?= $stoveGlossary['title']; ?></h1>
        <div id="inner">
            <?= $stoveGlossary['content']; ?> 
        </div>
    </div>
</section>
<div class="clearblock"></div>
