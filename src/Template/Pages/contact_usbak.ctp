<section class="home">
    <div class="container_12">
        <h1>Contact Us</h1>
        <div id="inner">
            <p>Your can email us direct or if you wish you can fill in the form below and we shall get back to you as soon as possible.</p>

            <hr /><br />
            <div class="split5050 float-left">
                <address>
                    <?= $company->company_name; ?><br/>
                    <?= $company->company_address_1; ?><br/>
                    <?= $company->company_address_2; ?><br/>
                    <?= $company->company_city; ?><br/>
                    <?= $company->company_county; ?><br/>
                    <?= $company->company_postcode; ?>
                </address>
            </div>
            <div class="split5050 float-right">
                <?= print "Tel:    " . $company->company_tel; ?><br/>
                <?php if (!empty($company->company_fax)) { ?>
                    <?= print "Fax:    " . $company->company_fax; ?><br/><?php } ?>
                <?= print "<br />Email:  " . $company->company_email_enquiries; ?>
            </div>
            <div class="clearblock"></div>
            <hr />
            <p></p> 
            <script src='https://www.google.com/recaptcha/api.js'></script>

            <?= $this->form->create(NULL, ['id' => 'frmEnquiry']); ?>
            <div class="frmEnquiry-col-1">
                <p>
                    <label for="name">Name</label>
                    <?= $this->Form->control('sName', ['maxlength' => "50", 'class' => "xlarge-input", 'label' => false, 'required']) ?>
                </p>
                <p>
                    <label for="tel">Tel</label>
                    <?= $this->Form->control('sTel', ['maxlength' => "20", 'class' => "xlarge-input", 'label' => false, 'required']) ?>
                </p>
                <p>
                    <label for="email">Email</label>
                    <?= $this->Form->control('sEmail', ['maxlength' => "50", 'class' => "xlarge-input", 'label' => false, 'required']) ?>
                </p>
            </div>
            <div class="frmEnquiry-col-2">
                <p>
                    <label for="enquiry">Enquiry</label>
                    <?= $this->Form->textarea('sEnquiry', ['cols' => "50", 'rows' => "6", 'class' => "xlarge-input", 'label' => false, 'required']) ?>
                </p>
            </div>
            <div class="frmEnquiry-col-1">
                <p>
                    <label for="captcha">Captcha</label>
                <div style="margin-left: 140px; margin-top: 15px;" class="g-recaptcha" data-sitekey="6Lf7r18UAAAAALXds7suqRNxl6gedmtjNd7ULjal"></div>
                </p>
            </div></br>

            <div class="clear"></div>
            <div class="subscribe">  
                <div class="check-box-style">
                    <p>We comply with the GDPR regulations regarding your personal data, if you would like to receive communications from Saltfire on latest products and services via E-mail, please tick the box.
                        <span class="squaredFour">
                            <input type="checkbox" name="sub_newsletter" id="squaredFour" value="Yes" />
                            <label for="squaredFour"></label>
                        </span>
                    </p>

                    <p>We'll never sell on your details to third parties for them to market to you. You can opt out any time, by using the unsubscribe option in each email you receive, click here to view our <a href="<?= $this->Url->build(['controller' => 'Pages', 'action' => 'privacy']); ?>" style="color:#a73f16;" target="_blank">privacy policy</a>.

                    </p>
                </div>
            </div>
            <p>
                <input name="send" type="submit" id="send" class="medium-btn" value="Send Enquiry" />
            </p>
            </form>

        </div>
        <div class="clearblock">&nbsp;</div>
    </div>
</section>
<div class="clearblock">&nbsp;</div>

