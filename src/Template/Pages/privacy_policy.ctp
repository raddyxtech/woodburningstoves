<section class="home">
    <div class="container_12">
        <h1>Privacy policy</h1>

        <div id="inner">
            <b>PRIVACY STATEMENT</b>
            <p>Your privacy is important to us; this privacy statement explains what data we collect from you and how we use it. This information applies to all the services and products supplied by Saltfire Stoves ltd</p>
            <br /><br />
            <b>We give an assurance that:</b>

            <ul>
                <li>We will process your personal data according to the Data Protection Act 1998 and General Data Protection Regulation 2018.</li>
                <li>We will not sell your data.</li>
                <li>We will use your personal data for marketing by Saltfire and our Sister companies only.</li>
                <li>We will not keep your personal data for longer than necessary.</li>
                <li>We will keep your personal data secure.</li>
                <li>We will remove your personal data upon request.</li>
                <li>We will respect your right to find out what information we have about you.</li>
            </ul><br /><br />
            <b>Personal data we collect</b>

            <p>Saltfire Stoves Ltd collect data in order to operate efficiently and provide you with the best possible products and services. You provide us with some of your data directly, via our website contact form and order form. We additionally gather data from cookies and also from third party providers.</p>
            <br />
            <p>We do not sell your personal data such as name, address and E mail to anyone.</p>
            <br /><br />
            <b>How we use your personal data</b>

            <p>We use your personal data in order to comply with UK law and process any orders you place with us for goods and services and manage your account. We also store personal data such as E mail addresses and telephone numbers for follow-up marketing purposes by ourselves only.</p>
            <br /><br />
            <b>Reasons we may share your personal data</b>

            <p>Our authorised service providers are companies that perform certain services on our behalf. These services may include fulfilling orders, registering Guarantees, processing credit card payments with Sagepay and sales analyses that support the website functionality. These service providers may have access to your personal data to the extent necessary to perform their functions, but we do not allow them to share or use any of your personal information for any other purpose.</p>
            <br /><br />
            <b>How to access your personal data</b>

            <p>We provide you with the right to see a copy of the information we hold on you. By providing a written request with the necessary identification, we will within 30 days:</p>
            <ul>
                <li>Tell you if we are processing any personal data relating to them</li>
                <li>Give you a description of the personal data, the reason it is being used, and if it has been shared with a third party.</li>
                <li>Provide a copy of the data held and the source of the data, if known.</li>
            </ul><br />
            <p>Please send any written request Saltfire Stoves Ltd, Station Works, Johns road, Wareham, Dorset, BH20 4BG</p>
            <br /><br />
            <b>Your right to be forgotten</b>

            <p>Part of our GDPR compliance is requesting you accept our terms of business prior to sending us your personal data, this consent can be removed at any time by contacting us verbally or in writing. You also have the right to request us to erase all your personal data we hold, by writing to us, we will confirm this has been done within 30 days of your request.</p>
            <br /><br />
            <b>Cookie policy</b>

            <p>Cookies are small text files which store the IP address when you first visit a website page, the cookie will help the website, to recognise your device the next time you visit. Most cookies won�t collect information that identifies you personally, they will instead collect more general information such as how users arrive at our website, or a user�s general location.</p>
            <br /><br />
            <b>How do we use cookies?</b>

            <p>Our cookies are only those required by Google to provide analytics back to us, we track IP addresses for visitor page usage, time on each page etc. Google AdWords also require cookies to track the visitor information and provide correct information for click rates, most websites will insist you enable cookies or not visit the website. We use cookies on our website to improve their performance and enhance the user experience.</p>
            <p>&nbsp; </p>
            <div class="clearblock"></div>
        </div>
    </div>
</section>
<!-- InstanceEndEditable -->
<div class="clearblock"></div>
