
<section class="home">
    <div class="container_12">
        <h1>Reviews</h1>
        <input name="review" type="button" class="large-btn float-right"
               value="Submit a review" onclick="location.href = 'review-submit';"/>
        <div class="clearblock"></div>
        <div id="inner">
            <?php
            foreach ($reviews as $review):
                ?>
                <div class="testimonial-blk">
                    <p class="testimonial"><?= $review['testimonial']; ?></p>
                    <p><span class="testimonial-by"><?= $review['who']; ?></span><span class="testimonial-date"><?= $review['when_said']; ?></span></p>
                    <div class="clearblock"></div>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
</section>
<div class="clearblock"></div>