<section class="home">
    <div class="container_12">
        <h1><?= $twinWall['title']; ?></h1>
        <div id="inner">
            <?= $twinWall['content']; ?>
        </div>
    </div>
</section>
<div class="clearblock"></div>

