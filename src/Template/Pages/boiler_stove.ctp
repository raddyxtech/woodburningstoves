
<section class="home">
    <div class="container_12">
        <h1><?= $boilerStove['title']; ?></h1>

        <div id="inner">
            <?= $boilerStove['content']; ?>
        </div>
    </div>
</section>
<!-- InstanceEndEditable -->
<div class="clearblock"></div>
