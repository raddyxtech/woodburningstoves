<style>
    a{font-size: 20px;}
    </style>
<section class="home">
    <div class="container_12">
        <h1>FAQ</h1>
        <div class="card-header">

            <ul class="question">
                <?php foreach ($faqs as $faq) { ?>
                    <li>
                        <h2><hr/>                      
                            <a><?php echo $faq->question; ?></a>
                       </h2>
                        <div class="ans-box">
                            <p><?php echo nl2br($faq->answer); ?></p>
                        </div>
                    </li>
                <?php } ?>
            </ul>
        </div>
    </div>
</section>
<div class="clearblock"></div>

<script>
$(document).ready(function() {
$(".ans-box").hide();
    $(".question li h2").click(function () {
        if (!$(this).siblings(".ans-box").is(':visible')) {
           $(this).siblings(".ans-box").slideDown();         
        }
         else {
            $(this).siblings(".ans-box").slideUp();
        }
    });
});

</script>

