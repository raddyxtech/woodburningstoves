<section class="home">
    <div class="container_12">
        <h1><?= $peanutStove['title']; ?></h1>
        <div id="inner">
            <?= $peanutStove['content']; ?>
        </div>
    </div>
</section>
<div class="clearblock"></div>
