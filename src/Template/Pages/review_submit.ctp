<section class="home">
    <div class="container_12">
        <h1>Submit your review</h1>

        <div id="inner">
            <p>To submit a review, all you need to do is complete the simple form below.  Once submitted, it will be reviewed and then approved.</p>

            <?= $this->Form->create('', ['type' => 'file']); ?>
            <p>
                <label for="name">Name</label>
                <?= $this->Form->control('who', ['class' => 'xlarge-input', 'label' => FALSE, 'required' => TRUE]); ?>
            </p>
            <p>
                <label for="review">Review</label>
                <?= $this->Form->control('testimonial', ['cols' => '60', 'rows' => '5', 'label' => FALSE, 'required' => TRUE]); ?>
            </p>              
            <p>
                <label for="send">&nbsp;</label>
                <?= $this->Form->submit('submit', ['class' => 'medium-btn', 'label' => FALSE, 'value' => 'Submit Review']); ?>
            </p>
            <?= $this->Form->end(); ?>
        </div>
    </div>
</section>
<!-- InstanceEndEditable -->
<div class="clearblock"></div>
