<style>
    /*    .product {width: 226px!important;height: 336px;overflow: hidden;border: 1px solid #e0e0e0;border-radius: 2px;-moz-border-radius: 2px;-khtml-border-radius: 2px;behavior: url(PIE.htc);}
        .submit{border: 0; background: 0;background-color: #f5f7f9;}*/
    .removeFavorite { color: #d01818; }
</style>
<div class="container_12">
    <div class="grid_12">
        <div class="slidprev"><span>Prev</span></div>
        <div class="slidnext"><span>Next</span></div>
        <div id="slider">
            <?php foreach ($bannerImgs as $bannerImg) { ?>
                <div class="slide">
                    <?php if (!empty($bannerImg)) { ?>
                        <img src="files/banners/<?= $bannerImg->image_name; ?>" alt="" title="" />
                    <?php } else { ?>
                        <img src= "<?= HTTP_ROOT ?>/images/no_image_available.jpg " />
                    <?php } ?>
                    <div class="slid_text">
                        <h3 class="slid_title"><span><?= $bannerImg->title; ?> </span></h3>
                        <p><span><?= $bannerImg->description; ?></span></p>
                    </div>
                </div> 
            <?php } ?>
        </div><!-- .slider -->
        <div class="clear"></div>
        <div id="myController">
            <div class="control"><span>1</span></div>
            <div class="control"><span>2</span></div>
            <div class="control"><span>3</span></div>
        </div>
    </div><!-- .grid_12 -->
</div>
<!-- .container_12 -->
<div class="clear"></div><br/>
<section class="home">
    <div class="container_12">
        <!--        <div id="top_button">
        <?php foreach ($CategorieImgs as $CategorieImg) { ?>
                        <div class="grid_4" style="width: 175px;">
                             <a href="<?= $this->Url->build(['controller' => 'Products', 'action' => 'productsList/' . $CategorieImg->category_id]); ?>"  target="_self" title="<?= $CategorieImg->name; ?>"><h3 style="text-align: center;padding-bottom: 15px;padding-top: 10px;font: normal 13px/18px Bitter, Myriad Pro, Verdana, serif;box-shadow: 0 0 5px rgba(0, 0, 0, 0.3);"class="title"><?= $CategorieImg->name; ?></h3></a>  
                             <h3 style="text-align: center;padding-bottom: 15px;padding-top: 10px;font: normal 13px/18px Bitter, Myriad Pro, Verdana, serif;box-shadow: 0 0 5px rgba(0, 0, 0, 0.3);"class="title"><?= $CategorieImg->name; ?></h3>
                            
                            <a href="#" class="button_block best_price">
                                <img src="images/banner1.png" alt="Banner 1"/>
                            </a> .best_price 
                        </div> .grid_4 
        <?php } ?>
                </div> #top_button -->
        <div class="clear"></div>

        <div class="carousel">
            <div class="c_header">
                <div class="grid_10">
                    <h2>Best Brands</h2>
                </div> 
                <div class="grid_2">
                    <a id="next_c1" class="next arows" href="#"><span>Next</span></a>
                    <a id="prev_c1" class="prev arows" href="#"><span>Prev</span></a>
                </div>                <!--.grid_2--> 
            </div> 

            <div class="list_carousel">
                <ul id="list_product" class="list_product">
                    <?php foreach ($brandImgs as $brandImg) { ?>
                        <li class="">
                            <div class="grid_3 product"style="height:260px;">
                                <div class="prev">
                                    <?php if (!empty($brandImg)) { ?>
                                        <a href="<?= $this->Url->build(['controller' => 'Products', 'action' => 'productsList/', 'brand_id' => $brandImg->id]); ?>"  target="_self" title="<?= $brandImg->name; ?>"><img   src="<?= HTTP_ROOT . BRAND_PIC . $brandImg->image; ?>" /></a>   
                                    <?php } else { ?>
                                        <a href="<?= $this->Url->build(['controller' => 'Products', 'action' => 'productsList/' . $brandImg->id]); ?>"  target="_self" title="<?= $brandImg->name; ?>"><img   src= "<?= HTTP_ROOT ?>/images/no_image_available.jpg " /></a> 
                                    <?php } ?>
                                </div><!-- .prev -->
                                <h3  style="text-align: center;height: 43px;padding: 3px 8px 0;margin: 0px;font: normal 13px/18px Bitter, Myriad Pro, Verdana, serif;color: #777;"><?= $brandImg->name; ?></h3>
                            </div><!-- .grid_3 -->
                        </li>
                    <?php } ?>
                </ul><!-- #list_product -->
            </div><!-- .list_carousel -->
        </div><!-- .carousel -->

        <div class="carousel">
            <div class="c_header">
                <div class="grid_10">
                    <h2>Featured Products</h2>
                </div><!-- .grid_10 -->
                <div class="grid_2">
                    <a id="next_c2" class="next arows" href="#"><span>Next</span></a>
                    <a id="prev_c2" class="prev arows" href="#"><span>Prev</span></a>
                </div><!-- .grid_2 -->
            </div><!-- .c_header -->

            <div class="list_carousel">
                <ul id="list_product2" class="list_product">
                    <?php foreach ($products as $product) { ?>
                        <li class="">
                            <div class="grid_3 product">
                                <div class="prev">
                                    <?php if ($product['images'][0]['main_image'] == 'Y') { ?>
                                        <a href="<?= $this->Url->build(['controller' => 'Products', 'action' => 'productsDetails/' . $product->product_id]); ?>"  target="_self" title="<?= $product->name; ?>"><img src="<?= HTTP_ROOT . PRODUCT_PIC . $product['images'][0]['filename']; ?>" /></a>   
                                    <?php } else { ?>
                                        <a href="<?= $this->Url->build(['controller' => 'Products', 'action' => 'productsDetails/' . $product->product_id]); ?>"  target="_self" title="<?= $product->name; ?>"><img src= "<?= HTTP_ROOT ?>/images/no_image_available.jpg " /></a> 
                                    <?php } ?>
                                </div><!-- .prev -->
                                <h3 class="title" style="text-align:center;"><?= $product->name; ?></h3>

                                <?php foreach ($product['product_variations'] as $ordrproduct)  ?>
                                    <div class="cart">
                                        <div class="price">
                                            <div class="vert">
                                                <div class="price_new">&pound;<?= number_format($ordrproduct['retail_price'], 2, ".", " "); ?></div>
                                                <!--                                            <div class="price_old">$725.00</div>-->
                                            </div>
                                        </div>
                                      <?php if (!$this->request->getSession()->read('Auth.User.id')) { ?>                                      
  <a style="width: 63px;" href="<?= HTTP_ROOT . "users/userLogin" ?>"  data-productid="<?= $product['product_id'] ; ?>"target="_self" title="Add to favorite"><i class="icon fa fa-heart like"style="margin-top:10px;"></i></a>
                                        <?php } else { ?>
                                   <a style="width: 63px;cursor: pointer;" class="<?= in_array($product->product_id, $favProductList) ? 'removeFavorite' : 'addToFavorite' ?>" data-productid="<?=$product['product_id'] ; ?>" target="_self" title="Add to favorite"><i class="icon fa fa-heart like" style="margin-top:10px;"></i></a> 
                                        <?php } ?>  

                                      <?= $this->Form->create(null, ['url' => '/products/basket', 'class' => 'no-margin-padding']) ?>
                                    <input name="product_id" type="hidden" value="<?= $product->product_id; ?>"/>
                                    <input name="variation_id" type="hidden" value="<?= $ordrproduct->variation_id; ?>"/>
                                    <input name="code" type="hidden" value="<?= $ordrproduct->code; ?>"/>
                                    <input name="name" type="hidden" value="<?= $product->name; ?>"/>
                                    <input name="weight" type="hidden" value="<?= $ordrproduct->weight; ?>"/>
                                    <input name="zero_vat" type="hidden" value="<?= $ordrproduct->zero_vat; ?>"/>
                                    <input name="brand" type="hidden" value="<?= $product->brand_id; ?>"/>
                                    <input name="variations" type="hidden" value="<?= $ordrproduct->var_data_1; ?>"/>
                                    <input name="unit_price" type="hidden" value="<?= $ordrproduct->retail_price; ?>"/>
                                    <input name="vat_rate" type="hidden" value= "<?= DEFAULT_VAT_RATE; ?>"/>
                                    <?php //  foreach ($products->orders_products as $order) ?>
                                    <input name="qty" type="hidden" class="numeric-input" value="1"/>
<!--                                   <a style="width: 63px;" href=" <?= $this->Url->build(['controller' => 'Products', 'action' => 'basket/']); ?>" class="bay"><input type="submit"> </a> -->
                                    <?php if (!$this->request->getSession()->read('Auth.User.id')) { ?>                                      
                                    <a  class="bay"style="width:63px;"href="<?= HTTP_ROOT . "login" ?>" target="_self" title="Add to cart"><i class="fas fa-shopping-cart" style="margin-top:10px;"></i></a>
                                    <?php } else { ?>
                                    <button style="background-color:#fff;" type="submit" class="bay"><i class="fas fa-shopping-cart" style="margin-top:10px;color:#78c4cd;"></i></button>
                                    <?php } ?>
                                    <?= $this->Form->end() ?> 

                                    </div>
                                <?php // } ?>
                            </div><!-- .grid_3 -->
                        </li>
                    <?php } ?>

                </ul>
                <div class="clear"></div>
            </div>

            </ul><!-- #list_product2 -->
        </div><!-- .list_carousel -->
    </div><!-- .carousel -->

    <!--        <div id="content_bottom">
                <div class="grid_4">
                    <div class="bottom_block about_as">
                        <h3>About Us</h3>
                        <p>A block of text is a stack of line boxes. In the case of 'left', 'right' and 'center', this property specifies how the inline-level boxes within each line box align with respect to the line box's </p>
                        <p>Alignment is not with respect to the viewport. In the case of 'justify', this property specifies that the inline-level boxes are to be made flush with both sides of the line box if possible.</p>
                        <p>by expanding or contracting the contents of inline boxes, else aligned as for the initial value.</p>
                    </div> .about_as 
                </div> .grid_4 
    
                <div class="grid_4">
                    <div class="bottom_block news">
                        <h3>News</h3>
                        <ul>
                            <li>
                                <time datetime="2012-03-03">3 january 2012</time>
                                <a href="#">A block of text is a stack of line boxes. In the case of 'left', 'right' and 'center', this property specifies</a>
                            </li>
    
                            <li>
                                <time datetime="2012-02-03">2 january 2012</time>
                                <a href="#">A block of text is a stack of line boxes. In the case of 'left', 'right' and 'center', this property specifies</a>
                            </li>
    
                            <li>
                                <time datetime="2012-01-03">1 january 2012</time>
                                <a href="#">A block of text is a stack of line boxes. In the case of 'left', 'right' and 'center', this property specifies how the inline-level boxes within each line</a>
                            </li>
                        </ul>
                    </div> .news 
                </div> .grid_4 
    
                <div class="grid_4">
                    <div class="bottom_block newsletter">
                        <h3>Newsletter</h3>
                        <p>Cursus in dapibus ultrices velit fusce. Felis lacus erat. Fermentum parturient lacus tristique habitant nullam morbi et odio nibh mus dictum tellus erat.</p>
                        <form class="letter_form">
                            <input type="email" name="newsletter" class="l_form" value="" placeholder="Enter your email address..."/>
                            <input type="submit" id="submit" value="" />
                        </form>
                        <div class="lettel_description">
                            Vel lobortis gravida. Cursus in dapibus ultrices velit fusce. Felis lacus erat.
                        </div> .lettel_description 
                    </div> .newsletter 
                </div> .grid_4 
    
                <div class="clear"></div>
            </div> #content_bottom -->
    <div class="clear"></div>

</div><!-- .container_12 -->
</section>

<div class="clearblock"></div>
<!--</div>-->
<script>
    $(document).on('click', '.addToFavorite', function (e) {
        var element = $(this);
        var productid = element.data('productid');

        $.ajax({
            type: 'POST',
            url: siteUrl + 'products/addFavourite',
            data: {'product_id': productid},
            dataType: 'json',
            success: function (response) {
                if (response.status == 'success') {
                    element.removeClass("addToFavorite").addClass("removeFavorite");
                } else {
                    alert(response.msg);
                }
            }
        });
    });

    $(document).on('click', '.removeFavorite', function (e) {
        var element = $(this);
        var productid = element.data('productid');
        $.ajax({
            type: 'POST',
            url: siteUrl + 'products/deleteFavourite',
            data: {'product_id': productid},
            dataType: 'json',
            success: function (response) {
                if (response.status == 'success') {
                    element.removeClass("removeFavorite").addClass("addToFavorite");
                }
                else {
                    alert(response.msg);
                }
            }
        });
    });
</script>
