<section class="home">
    <div class="container_12">
        <h1><?= $visitShowroom['title']; ?></h1>
        <div id="inner">
            <?= $visitShowroom['content']; ?>
        </div>
    </div>
</section>
<div class="clearblock"></div>