<p class="bold">Your basket</p>

<p><span class="float-left">No. of Items:</span><span class="float-right">0</span></p>
<p class="clearblock bold red"><span class="float-left">TOTAL:</span><span class="float-right">&pound;0.00</span></p>

<hr class="clearblock"/>

<p><input name="go2basket" type="button" class="go2basket-btn" value="Checkout" onclick="location.href='basket.htm';"/></p>

<hr/>
<div id="socialmedia">
	<a href="http://www.facebook.com/#!/pages/Saltfire-Stoves-Ltd/211161172230666" target="_blank" title="Visit Woodburning Stoves Direct on Facebook" class="facebook"><img src="images/facebook-36x36.jpg" width="36" height="36" alt="Our Facebook page"/></a>
	<a href="http://twitter.com/SaltfireStoves" target="_blank" title="Follow us on Twitter"><img src="images/twitter-36x36.jpg" width="36" height="36" alt="Our Twitter feed"/></a>
</div>