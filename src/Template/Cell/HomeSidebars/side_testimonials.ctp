<div id="reviews">
    <p class="bold">Reviews</p>
    <?php foreach($reviews as $review) { ?>
        <div class="sidetestimonial-blk">
            <img src="images/quotes.png" width="19" height="18" class="left-quote" alt="" />
            <p class="sidetestimonial"><?= substr($review->testimonial, 0, 100); ?>&rdquo;</p>
            <p><span class="sidetestimonial-by"><?= str_replace(',',',<br>', $review->who);?></span></p>
        </div>
    <?php } ?>
</div>