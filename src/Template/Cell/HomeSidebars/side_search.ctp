<p class="bold">Find your perfect stove</p>

    <?= $this->Form->create(NULL,['method' => 'get']); ?>
    <p>
        <label for="searchbrand">By brand</label>
        <br />
        <select name="iBrandSearchId" id="searchbrand" class="large-select" onclick="frmSrch.searchcategory.selectedIndex = '';
                frmSrch.searchkeywords.value = '';" >
            <option value="">Select Brand -</option>
            <?php foreach ($brands as $brand): ?>
                <option value="<?= $brand['id']; ?>" ><?= $brand['name']; ?></option>
            <?php endforeach; ?>
        </select>
    </p>
    <p>
        <label for="searchcategory">By category</label>
        <br />
        <select name="iCatSearchId" id="searchcategory" class="large-select" onclick="frmSrch.searchbrand.selectedIndex = '';
                frmSrch.searchkeywords.value = '';" >
            <option value="">Select Category -</option>
            <?php foreach ($categories as $category): ?>
                <option value="<?= $category['category_id']; ?>" ><?= $category['name']; ?></option>
            <?php endforeach; ?>
        </select>
    </p>
    <p>
        <label for="searchkeywords">By keyword</label>
        <br />
        <input name="sKeywordSearch" id="searchkeywords" class="large-input" onclick="frmSrch.searchcategory.selectedIndex = '';
                frmSrch.searchbrand.selectedIndex = '';" value=""  />
    </p>
    <p>
        <input name="submit" type="submit" class="small-btn" value="Find"  />
    </p>
<?= $this->Form->end(); ?>
