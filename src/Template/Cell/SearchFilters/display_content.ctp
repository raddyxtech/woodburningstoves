<?= $this->Html->css('jquery-ui'); ?>
<?= $this->Html->css('hint'); ?>
<?= $this->Html->css('/plugins/jquery-toastr/jquery.toast.min'); ?>
<div class="content-section" id="property-search">
    <div class="search-loader"></div>
    <div class="banner property-search-page">
        <img src="images/inner-banner3.jpg" alt="Moverez banner" />
        <div class="banner-content">
            <div class="property-search">
                <div class="wrapper">				
                    <h2>Property Search</h2>
                    <?= $this->Form->create(NULL, [ 'valueSources' => ['query', 'context'], 'id' => 'propertSearchForm', 'url' => ['action' => 'propertyList'], 'type' => 'get']); ?>
                    <div class="check-buttons">
                        <label class="custom-check-box">FOR SALE
                            <?= $this->Form->radio('category',['sell'=>'sell'],['label'=>  false,'hiddenField'=>false, 'checked' => 'checked']); ?>
                            
                            <!--<input type="radio" name="category" value="sell" <?= ( $this->request->getQuery('property_category') != 'let' ) ? 'checked="checked"' : ''; ?> >-->
                            <span class="checkmark2"></span>
                        </label>
                        <label class="custom-check-box2">TO LET
                            <?= $this->Form->radio('category',['let'=>'let'],['label'=>  false,'hiddenField'=>false]); ?>
                            <!--<input type="radio" name="category" value="let" <?= ( $this->request->getQuery('property_category') == 'let') ? 'checked="checked"' : ''; ?> >-->
                            <span class="checkmark3"></span>
                        </label>
                    </div>	
                    <div class="form-section">
                        <div class="frm-group">
                            <label>Location</label>
                            <div class="form-box">							
                                <!--<input type="text" class="text1" name="location" placeholder="Enter postcode, street, town or city" id="postcode">-->
                                <?= $this->Form->control('loc', ['placeholder' => 'Enter postcode, locality, town or city', 'id' => 'address', 'label' => false]) ?>
                                <div class="hidden-fields" style="display: none;">
                                    <?= $this->Form->hidden('address_type'); ?>
                                    <?= $this->Form->hidden('goecode[lat]'); ?>
                                    <?= $this->Form->hidden('goecode[lng]'); ?>
                                </div>
                            </div>
                            <label>Search Radius</label>
                            <div class="form-box">	
                                <?= $this->Form->select('radius',[1=>'1-Mile',2=>'2-Miles',3=>'2-Miles',5=>'2-Miles',10=>'10-Miles',20=>'20-Miles',30=>'30-Miles'], ['class' => 'select1', 'div' => false, 'label' => false, 'empty' => 'Search Radius']) ?>
                            </div>
                        </div>
                        
                        <div class="frm-group">
                            <label>Price Range</label>
                            <div class="form-box  from-2-col">
                                <?= $this->Form->select('min_price', $priceOptions, ['escape'=>false, 'class' => 'select1', 'div' => false, 'label' => false,  'empty' => 'No min']) ?> 
                            </div>
                            <div class="form-box  from-2-col">							
                                <?= $this->Form->select('max_price', $priceOptions, [ 'escape'=>false,'class' => 'select1', 'div' => false, 'label' => false, 'empty' => 'No max']) ?>
                            </div>
                            <label>Bedrooms</label>
                            <div class="form-box  from-2-col">
                                <?= $this->Form->select('min_bed', ['0' => 'Studio'] + range(1, 10), ['class' => 'select1', 'div' => false, 'label' => false, 'empty' => 'No min']); ?> 
                            </div>
                            <div class="form-box  from-2-col">
                                <?= $this->Form->select('max_bed', ['0' => 'Studio'] + range(1, 10), ['class' => 'select1', 'div' => false, 'label' => false, 'empty' => 'No min']); ?> 
                            </div>
                        </div>
                        <div class="frm-group">
                            <label>Property type</label>

                            <div class="form-box">
                                <?= $this->Form->select('property_type', $propertyTypes, ['class' => 'select1', 'div' => false, 'label' => false, 'empty' => 'Property Type']) ?> 
                            </div>
                            <label>Include Sold?</label>
                            <div class="form-box  from-2-col">
                                <label class="custom-check-box">YES
                                    <input type="radio" name="sold" value="1" <?= ( $this->request->getQuery('property_category') == '1' ) ? 'checked="checked"' : ''; ?> >
                                    <span class="checkmark2"></span>
                                </label>
                            </div> 
                            <div class="form-box  from-2-col">	
                                <label class="custom-check-box2">NO
                                    <input type="radio" name="sold" value="0" <?= ( $this->request->getQuery('property_category') != '1') ? 'checked="checked"' : ''; ?> >
                                    <span class="checkmark3"></span> 
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="form-button-section">
                        <?= $this->Form->button('<i class="fab fa-searchengin"></i> SEARCH Properties', ['escape' => false, 'class' => 'button-1', 'id' => 'search-btn', 'type' => 'submit']); ?>
                        <?php if($this->request->getSession()->read('Auth.User.id')){ ?> 
                            <?= $this->Form->button('<i class="far fa-lg fa-save"></i>', ['escape' => false, 'class' => 'button-1 hint--top', 'id' => 'save-preference','data-hint'=>'Save Preference', 'type' => 'button']); ?>
                        <?php }?>
                    </div>
                    <?= $this->Form->end() ?>
                </div>
            </div>
        </div>
    </div>

</div>
<?= $this->Html->script('jquery-ui'); ?>

<?= $this->Html->script('/plugins/jquery-toastr/jquery.toast.min'); ?>
<script>

    $(function(){
        /*=============== Form submit and list =========================*/
        $('#propertSearchForm').submit(function(e){
            $('.search-loader').show();
        });   
        $('#address').autocomplete({
                source: siteUrl + 'pages/ajax-get-location',
                minLength: 2,
                select: function (event, ui) {
                    var element = event.target;
                    if (typeof ui.item.address_type !== 'undefined') {
                        $('.hidden-fields input[name="address_type"]').val(ui.item.address_type);
                        $('.hidden-fields input[name="goecode[lat]"]').val(ui.item.lat);
                        $('.hidden-fields input[name="goecode[lng]"]').val(ui.item.lng);
                    } else {
                        $('.hidden-fields input').val('');
                    }
                }
         });
         $('#save-preference').click(function(){
             
             var data = $('#propertSearchForm').serialize();        
              $.ajax({
                url: siteUrl + 'customers/update-user-preference',
                type:'post',
                data: data,
                dataType: 'json',
                success:function(response){
                    if(response.status == 'success'){
                        $.toast.success({text:response.message});
                        $('#propertSearchForm').submit();
                    } else {
                        $.toast.error({text:response.message});
                    }
                },
                error:function(){
                    $.toast.error({text:'Some error occurred!!'});
                },
                complete:function(){
                    $.modal.close();
                }
               
              });
         });
        
    });


</script>
