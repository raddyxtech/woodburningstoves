<?php if (!empty($products)) { ?>
        <?php foreach ($products as $product) { ?>
            <div class="grid_3 product">
                <div class="prev" style="padding-top:20px;">
                    <?php if (@$product['images'][0]['main_image'] == 'Y') { ?>
                        <a href="<?= $this->Url->build(['controller' => 'Products', 'action' => 'productsDetails/' . $product->product_id]); ?>"  target="_self" title="<?= $product->name; ?>"><img src="<?= HTTP_ROOT . PRODUCT_PIC . $product['images'][0]['filename']; ?>" /></a>   
                    <?php } else { ?>
                        <a href="<?= $this->Url->build(['controller' => 'Products', 'action' => 'productsDetails/' . $product->product_id]); ?>"  target="_self" title="<?= $product->name; ?>"><img src= "<?= HTTP_ROOT ?>/images/no_image_available.jpg " /></a> 
                    <?php } ?>
                </div><!-- .prev -->
                <h3 class="title" style="text-align:center;"><?= $product->name; ?></h3>
                <?php foreach ($product['product_variations'] as $ordrproduct)
                     ?>
                <div class="cart">
                    <div class="price">
                        <div class="vert">
                            <div class="price_new">&pound;<?= number_format($ordrproduct['retail_price'], 2, ".", " "); ?></div>
                        </div>
                    </div>
                    <?php if (!$this->request->getSession()->read('Auth.User.id')) { ?>                                      
                        <a style="width: 63px;" href="<?= HTTP_ROOT . "login" ?>"  data-productid="<?= $product['product_id']; ?>"target="_self" title="Add to favorite"><i class="icon fa fa-heart like"style="margin-top:10px;"></i></a>
                    <?php } else { ?>
                       <a style="width: 63px;cursor: pointer;" class="<?= in_array($product->product_id, $favProductList) ? 'removeFavorite' : 'addToFavorite' ?>" data-productid="<?= $product['product_id']; ?>" target="_self" title="Add to favorite"><i class="icon fa fa-heart like" style="margin-top:10px;"></i></a> 
                    <?php } ?>  

        <?= $this->Form->create(null, ['url' => '/products/basket', 'class' => 'no-margin-padding']) ?>
                    <input name="product_id" type="hidden" value="<?= $product->product_id; ?>"/>
                    <input name="variation_id" type="hidden" value="<?= $ordrproduct->variation_id; ?>"/>
                    <input name="code" type="hidden" value="<?= $ordrproduct->code; ?>"/>
                    <input name="name" type="hidden" value="<?= $product->name; ?>"/>
                    <input name="weight" type="hidden" value="<?= $ordrproduct->weight; ?>"/>
                    <input name="zero_vat" type="hidden" value="<?= $ordrproduct->zero_vat; ?>"/>
                    <input name="brand" type="hidden" value="<?= $product->brand_id; ?>"/>
                    <input name="variations" type="hidden" value="<?= $ordrproduct->var_data_1; ?>"/>
                    <input name="unit_price" type="hidden" value="<?= $ordrproduct->retail_price; ?>"/>
                    <input name="vat_rate" type="hidden" value= "<?= DEFAULT_VAT_RATE; ?>"/>
        <?php //  foreach ($products->orders_products as $order)  ?>
                    <input name="qty" type="hidden" class="numeric-input" value="1"/>
                    <?php if (!$this->request->getSession()->read('Auth.User.id')) { ?>                                      
                        <a  class="bay"style="width:63px;"href="<?= HTTP_ROOT . "login" ?>" target="_self" title="Add to cart"><i class="fas fa-shopping-cart" style="margin-top:10px;"></i></a>
                    <?php } else { ?>
                        <button style="background-color:#fff;" type="submit" class="bay"><i class="fas fa-shopping-cart" style="margin-top:10px;color:#78c4cd;"></i></button>
                    <?php } ?>
                <?= $this->Form->end() ?> 
                </div>
            <?php // }  ?>
            </div><!-- .grid_3 -->
        <?php } ?>
<?php } ?>
</div><!-- .grid_product -->

<div class="clear"></div>
