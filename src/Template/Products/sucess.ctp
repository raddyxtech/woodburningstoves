
<style>
    .dash_head_row3 h3{
        margin-bottom: 20px;
    }
    .sub_details_box{
        max-width: 400px;
        width: 100%;
        margin: auto;
        border: solid 1px #dedede;
        display: inline-block;
    }
    .sub_details_box div{
        width: 100%;
        float: left;
        text-align: left;
        border-bottom: solid 1px #dedede;
    }
    .sub_details_box div:last-child{
        border-bottom: none;
    }
    .sub_details_box label{
        width: 50%;
        float: left;
        margin-bottom: 0px;
        line-height: 24px;
        padding: 6px 10px;
        border-right: solid 1px #dedede;
        color: #176aae;
        font-weight: 600;
    }
    .sub_details_box p{
        width: 30%;
        float: left;
        margin-bottom: 0px;
        padding: 6px 10px;
    }
    .dash_head_btn{margin-top: 0;text-align: center !important;}
    .dash_head_btn a{
        display: inline-block;
        background: #176aae;
        color: #fff;
        padding: 8px 0;
        border-radius: 3px;
    }
    .dash_head_btn a:hover{
        text-decoration: none;
        background: #0d5895;
    }
</style>
<div id="wrapper">  
    <div id="main">
        <div id="right-col">
            <div class="dash_head_row3" style="text-align: center;">
                <div class="wrapper">
                    <h3> Congratulations!! <br/> <span style="font-size:18px;">You have pay successfully!!</span></h3>
                    <h3> Your Payment details </h3> 
                   <div class="sub_details_box">
                        <?php foreach ($order->orders_products as $orderdetail) { ?>
                    <div>
                        <label> Product Name : &nbsp;</label>
                        <p><?= $orderdetail->name; ?></p> 
                    </div>
                                        <div>
                                            <label> Price : &nbsp;</label>
                                            <p><?= $orderdetail->total ?></p> 
                                        </div>
                                        <div>
                                            <label> code : &nbsp;</label>
                                            <p><?= $orderdetail->code ?></p> 
                                        </div>
                                        <div>
                                            <label> orderID : &nbsp;</label>
                                            <p><?= $orderdetail->order_id ?></p> 
                                        </div>
                    <?php }  ?>
                    </div>
                </div>
            </div>

            <div class="clearblock"></div>
        </div>
    </div> 
</div> 


<script>
setTimeout(function () {
    window.location = "<?= HTTP_ROOT ?>products/payment";
}, 10000);
</script>






