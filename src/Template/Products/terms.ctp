<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
        <title>Woodburning Stoves, Multifuel Stoves, Log Burners, Cast Iron Stoves, Wood Burners, Chimney Flue Pipe, UK + Europe Delivery</title>
        <meta name="description" content="At Salt Fire Woodburning Stoves Ltd. we sale only the best Wood burning stoves and multi fuel stoves"/>
        <meta name="keywords" content="wood burning stoves, stoves, wood burning, wood, stove, multi fuel stoves, wood burners, burning, woodburning stoves, log stoves"/>

        <base href="https://www.woodburningstovesdirect.com/"/>
        <link href="sfs.css" rel="stylesheet" type="text/css"/>
        <script type="text/javascript">//<![CDATA[
            var tlJsHost = ((window.location.protocol == "https:") ? "https://secure.comodo.com/" : "http://www.trustlogo.com/");
            document.write(unescape("%3Cscript src='" + tlJsHost + "trustlogo/javascript/trustlogo.js' type='text/javascript'%3E%3C/script%3E"));
//]]>
        </script>
    </head>
    <body>
        <div id="wrapper">
            <div id="header">
                <img src="images/banner-logo.jpg" alt="Wood Burning stoves at Salt Fire Stoves" width="177" height="100" id="banner-logo"/>
                <p id="customerservice">Customer Service</p>
                <p id="customerservicetel">01929 555 211</p>
            </div>
            <div id="tc-whole">
                <h1>Terms and conditions</h1>
                <div id="inner">
                    <h2>Warranty</h2>
                    <p>All new products are covered by our full 12 months parts and  labour warranty. </p>
                    <p>Items not covered by the Warranty are consumables including  ashpans and rope seals around stove doors.</p>
                    <p>The Warranty is only valid for items and systems installed  in accordance with current Building Regulations and must only be operated as  specified in the instruction manual.</p>
                    <p>We will refund or replace any faulty items accompanied by proof of  purchase within this 12 month period.</p>
                    <p>All faulty items must be returned to:</p>
                    <address>
                        Saltfire Stoves Ltd<br />Station Works<br />Johns Road<br />Wareham<br />Dorset<br />BH20 4BG<br /><br />	
                        <p>Contact number 01929 555211</p>
                    </address>
                    <h2>Returns outside of Warranty terms</h2>
                    <p>We offer a no quibbles refund policy if you are  not entirely happy with your purchase for any reason. We must however be  notified in advance of your intention to return the item/s and they must then  be returned in their original state with original packaging to the above  address within 14 days of receipt of receiving the goods.</p>
                    <h2>Availability</h2>
                    <p>All items ordered online should normally be in  stock. If however your items are not available immediately, we will inform you  straight away, and give the option of a full refund.</p>
                    <h2>Delivery</h2>
                    <p>We endeavour to ensure your purchase, if in  stock, is delivered within 5 working days of cleared payment being received,  however on rare occasions there may be a delay in delivery times due to third  party couriers.</p>
                    <p>We will require your full contact details  including phone number in order for the courier to deliver on time.</p>
                    <h2>Insurance</h2>
                    <p>Your items will be fully insured against loss or  damage in transit until the point of delivery. Once goods are signed for as  received in good condition, the insurance ceases. &ndash; The courier will allow time  for you to check your items for any damage upon delivery.</p>                </div>
            </div>
            <div id="footer">
                <script type="text/javascript" src="includes/copyright_year.js"></script>
                <p class="copyright grey text-center big-top-pad">Copyright
                    <script type="text/javascript">js_copyright_year();</script> &copy; Salt Fire Stoves Ltd.
                </p>
            </div>
            <p class="float-left">Registered Office: Station Works, Johns Road, Wareham, Near Poole / Bournemouth, Dorset, BH20 4BG, UK</p>
            <p class="float-right"><a href="http://www.harbour-design.co.uk" title="Website by Harbour Design - www.harbour-design.co.uk">site by harbour design</a></p>
        </div>
    </body>
</html>
