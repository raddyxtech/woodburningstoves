<link rel="stylesheet" href="/woodburningstoves/css/checkout.css"/> 
<link rel="stylesheet" href="/woodburningstoves/css/address.css"/> 
<section id="main" class="entire_width">
    <div class="container_12">      
        <div id="content">
            <div class="grid_12">
                <h1 class="page_title">Checkout</h1>
            </div> 
            <div class="grid_3" id="sidebar_right">
                <aside id="checkout_progress">
                    <h3>Your Checkout Progress</h3>
                    <ul>
                        <li>Billing Address<a title="Edit" href="javascript:;">Edit</a></li>
                        <li>Shipping Address<a title="Edit" href="javascript:;">Edit</a></li>
                        <li>Shipping Method<a title="Edit" href="javascript:;">Edit</a></li>
                        <li>Payment Method<a title="Edit" href="javascript:;">Edit</a></li>
                    </ul>
                </aside>
            </div><!-- #sidebar_right -->

            <div class="grid_9" id="checkout_info">
                <?php if ($user_type == 2) { ?>
                    <ul class="checkout_list">
                        <li class="active">
                            <div class="list_header">Checkout Method<div class="number"></div>

                            </div>
                            <div class="list_body">
                                <!--Click to below radio button to continue your checkout process.-->
                                <div class="clear"></div>
                            </div>
                        </li>
                        <li>
                        <div class="_7XMNLT" id="checkaddress" style="display:none;">
                            <div class="_1GRhLX _38vNoF">
                                <div class="_1QbRjw" >
                                    <h3 class="_2RMAtd">
                                        <span class="_1Tmvyj">1</span></h3>
                                    <div class="_3FNwOm"><div class="_1_m52b">Delivery Address</div>
                                        <div class="_2jDL7w">
                                            <span><span class="dNZmcB"><?= $defaultOrder->delivery_firstname; ?> <?= $defaultOrder->delivery_lastname; ?>. </span> <span><?= $defaultOrder->delivery_address1; ?>  - <span class="_3n0HwW"><?= $defaultOrder->delivery_postcode; ?></span></span></span>
                                        </div>
                                    </div>
                                    <button class="_3Jk8fm" id="change">Change</button>
                                </div>
                            </div>
                        </div>

                    <div class="_7XMNLT" id="delivery">
                            <div class="_1GRhLX _38vNoF">
                                <h3 class="_1fM65H _2RMAtd"><span class="_1Tmvyj">2</span><span class="_1_m52b">Delivery Address</span></h3>
                                <div class="_3K1hJZ">
                                    <div>
                                        <div>
                                            <?php foreach ($orderaddresses as $orderaddress) { ?>
                                                <label for="<?= $orderaddress->order_id ?>" class="_8J-bZE _1tkDFt _1YWq5E _2i24Q8 _1Icwrf">
                                                    <div class="check">
                                                        <input type="radio"  class="_2haq-9" data-orderid="<?= $orderaddress['order_id']; ?>"name="choose_delivery" value= <?= $orderaddress->choose_delivery ?> <?php if ($orderaddress['choose_delivery'] == "Y") { echo "checked"; } ?>>
                                                        <div class="_2o59RR">
                                                            <div class="A1v2GV">
                                                                <div id="CNTCT35F50B11D374458F9711627D1" class="_1i74Oi _2Y3Dxm">
                                                                    <p class="_22O2Xt"><span class="_3n0HwW"><?= $orderaddress->delivery_firstname; ?> <?= $orderaddress->delivery_lastname; ?> .</span><span class="_rmbzw"> <?= @$orderaddress->address_type; ?></span><span class="_2kSC_X _3n0HwW"><?= @$orderaddress->delivery_tel1; ?>, <?= @$orderaddress->delivery_tel2; ?></span></p>
                                                <span class="_22O2Xt GeUS8P">
                                                    <?php if (!empty($orderaddress->delivery_address1) || !empty($orderaddress->delivery_address2)) { ?>
                                                        <?= $orderaddress->delivery_address1; ?>,   <?= $orderaddress->delivery_address2; ?>
                                                    <?php } ?>, 
                                                    <span class="_3n0HwW"><?= $orderaddress->delivery_postcode; ?></span>
                                                </span>
                                                                    <?php if ($orderaddress['choose_delivery'] == "Y") { ?>
                                                                        <div class="show" style="display:block;float: left;padding: 0px;border-left: 1px solid #ffff;margin: 0px;">
                                                                            <button class="_2AkmmA _I6-pD _7UHT_c">Deliver Here</button>
                                                                        </div>
                                                                        <div class="_3Ojtt3" style="display:block;">
                                                                            <button type="button"class="FoDyGO"id="edit" Onclick= "viewAddress('<?= $orderaddress['order_id'] ?>')">
                                                                                EDIT
                                                                            </button>
                                                                        </div>
                                                                    <?php } else { ?>
                                                                        <div class="show" style="display:none;float: left;padding: 0px;border-left: 1px solid #ffff;margin: 0px;">
                                                                            <button class="_2AkmmA _I6-pD _7UHT_c">Deliver Here</button>
                                                                        </div>
                                                                        <div class="_3Ojtt3" style="display:none;">
                                                                            <button type="button"class="FoDyGO"id="edit" Onclick= "viewAddress('<?= $orderaddress['order_id'] ?>')">
                                                                                EDIT
                                                                            </button>
                                                                        </div>
                                                                    <?php } ?>           
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </label>
                                            <?php } ?>
                                        </div>
                                    </div>
                                    <div>
                                        <div class="-pqoEC"></div>
                                        <div class="_2Y8lQ1" id="addAddress">
                                         <i class="fa fa-plus _2hT5Bw" aria-hidden="true" style="color: #2874f;"></i> Add a new address</div>
                                    </div>
                                </div>
                            </div>
                        </div>

                            <!--//ADD NEW ADDRESS-->

                            <div class="_7XMNLT" id="addform" style="display:none;">
                                <!--<div class="_7XMNLT" id="addform">-->
                                <div class="_1GRhLX _38vNoF">
                                    <div class="_3K1hJZ"><div>
                                            <div>
                                                <div> 
                                                    <label for="newAddressForm" class="_8J-bZE _2s38SO _29FFjj _3lyfHx _1Icwrf">
                    <!--                                  <input type="radio" class="_2haq-9" name="address" readonly="" id="newAddressForm" value="on">
                                                      <div class="_6ATDKp"> </div>-->
                                                        <div class="_2o59RR">
                                                            <div>
                                                                <?= $this->Form->create(null, ['id' => 'addressform', 'name' => 'frmCheckout', 'type' => 'file']) ?>
                                                                <span class="aGTn1j">ADD A NEW ADDRESS</span>

                                                                <div class="_3llGqN">
                                                                    <div class="uK6xOa">
                                                                        <div class="_3fgAI3 Th26Zc">
                                                                            <?= $this->Form->text('delivery_firstname', ['class' => '_16qL6K _366U7Q', 'label' => false, 'type' => 'text', 'id' => 'firstname', 'placeholder' => 'First Name', 'tabindex' => "1"]); ?>
                                                                            <!--<label for="name" class="_20i8vs"></label>-->
                                                                        </div>
                                                                        <div class="_3fgAI3 Th26Zc">
                            <!--                                                                     <input type="text" class="_16qL6K _366U7Q" name="phone" required="" maxlength="10"  tabindex="2" value="">-->
                                                                            <?= $this->Form->text('delivery_lastname', ['class' => '_16qL6K _366U7Q', 'label' => false, 'type' => 'text', 'id' => 'lastname', 'placeholder' => 'Last Name', 'tabindex' => "2"]); ?>
                                                                        </div>
                                                                    </div>
                                                                    <div class="uK6xOa">
                                                                        <div class="_3fgAI3 Th26Zc">
                                                                            <?= $this->Form->text('delivery_postcode', ['class' => '_16qL6K _366U7Q', 'label' => false, 'type' => 'text', 'id' => 'pincode', 'placeholder' => 'Pincode', 'tabindex' => "3"]); ?>
                                                                        </div>
                                                                        <div class="_3fgAI3 Th26Zc">
                                                                            <?= $this->Form->text('delivery_address2', ['class' => '_16qL6K _366U7Q', 'label' => false, 'type' => 'text', 'id' => 'pincode', 'placeholder' => 'Locality', 'tabindex' => "4"]); ?>
                                                                        </div>
                                                                    </div>
                                                                    <div class="uK6xOa _3g_m0J">
                                                                        <div class="_1X5WZZ _1QG_7g">
                                                                            <div class="_2mJu7M Th26Zc">

                                                                                <?= $this->Form->textarea('delivery_address1', ['class' => 'giyiJa _16qL6K _21icXK', 'label' => false, 'type' => 'text', 'placeholder' => 'Address (Area and Street)', "rows" => "4", "cols" => "10", 'tabindex' => "5"]); ?>
                                                                            </div>
                                                                            <div class="_39_KcN _3-mqeJ"></div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="uK6xOa">
                                                                        <div class="_1X5WZZ _3fgAI3 _3g_m0J">
                                                                            <div class="_2mJu7M Th26Zc">
                                                                                <?= $this->Form->text('delivery_city', ['class' => '_16qL6K _366U7Q', 'label' => false, 'type' => 'text', 'id' => 'delivery_city', 'placeholder' => 'City/District/Town', 'tabindex' => "6"]); ?>
                                    <!--                                            <input type="text" class="_16qL6K _366U7Q" name="city" required="" autocomplete="city" tabindex="6" value="">
                                                                                <label for="city" class="_20i8vs">City/District/Town</label>-->
                                                                            </div>
                                                                            <div class="_39_KcN _3-mqeJ"></div>

                                                                        </div>
                                                                        <div>
                                                                            <div class="M2VvVb">
                                                                                <div class="_2uWdcR _3fgAI3 N25bMB">
                                    <!--                                                <select class="_3092M2 _3fgAI3 N25bMB QoXnA9" name="state" required="" tabindex="7">
                                                                                        <option value="" disabled="">--Select State--</option>
                                                                                        <option value="West Bengal">West Bengal</option>
                                                                                    </select>-->
                                                                                    <?= $this->Form->select('delivery_countryid', $countriesList, ['type' => 'select', 'id' => "country", 'label' => false, 'class' => '_3092M2 _3fgAI3 N25bMB QoXnA9', 'empty' => '--select country--']); ?>
                                                                                    <span class="_1LBnEa _2KY_MK"></span>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="uK6xOa">
                                                                        <div class="_3fgAI3 Th26Zc">
                                                                            <?= $this->Form->text('delivery_tel1', ['class' => '_16qL6K _366U7Q', 'label' => false, 'id' => 'delivery_tel1', 'placeholder' => '10-digit mobile number', 'tabindex' => "8"]); ?>
                                                                        </div>
                                                                        <div class="_3fgAI3 Th26Zc">
                                                                            <?= $this->Form->text('delivery_tel2', ['class' => '_16qL6K _366U7Q', 'label' => false, 'id' => 'delivery_tel2', 'placeholder' => 'Alternate Phone', 'tabindex' => "9"]); ?>
                                                                        </div>

                                                                    </div>
                                                                    <div class="_3XXwRR">
                                                                        <p class="_2dwzAy">Address Type</p>
                                                                        <div class="_3qg3HS"><div>
                                                                                <label for="HOME" class="_8J-bZE _2tcMoY">
                                                                                    <input type="radio" class="_2haq" name="address_type" readonly="" id="HOME" value="HOME">
                                                                                    <!--<div class="_6ATDKp"></div>-->
                                                                                    <div class="_2o59RR"><span>Home (All day delivery)</span></div>
                                                                                </label>
                                                                                <label for="WORK" class="_8J-bZE _2tcMoY">
                                                                                    <input type="radio" class="_2haq" name="address_type" readonly="" id="WORK" value="WORK">
                                                                                    <!--<div class="_6ATDKp"></div>-->
                                                                                    <div class="_2o59RR"><span>Work (Delivery between 10 AM - 5 PM)</span></div>
                                                                                </label>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="_1qbqu2 uK6xOa">
                                                                        <button class="_2AkmmA EqjTfe _7UHT_c" type="submit" tabindex="10">Save and Deliver Here</button>
                                                                        <button class="_2AkmmA _237M5J" type="button" tabindex="11" id="cancel">Cancel</button>
                                                                    </div>
                                                                </div>
                                                                <?= $this->Form->end() ?>
                                                            </div>
                                                        </div>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!--//Edit address-->
                            <div class="_7XMNLT" id="editForm" style="display:none;">
                                <div class="_1GRhLX _38vNoF">
                                    <div class="_3K1hJZ"><div>
                                            <div>
                                                <div> 
                                                    <label for="newAddressForm" class="_8J-bZE _2s38SO _29FFjj _3lyfHx _1Icwrf">
                    <!--                                  <input type="radio" class="_2haq-9" name="address" readonly="" id="newAddressForm" value="on">
                                                      <div class="_6ATDKp"> </div>-->
                                                        <div class="_2o59RR">
                                                            <div>
                                                                <!--<= $this->Form->create(NULL, ['id' => 'fromedit', 'name' => 'frmCheckout', 'type' => 'file']) ?>-->
                                                                <?= $this->Form->create(null, ['id' => 'editaddressform', 'name' => 'frmCheckout', 'type' => 'file']) ?>
                                                                <span class="aGTn1j">EDIT ADDRESS</span>

                                                                <div class="_3llGqN">
                                                                    <div class="uK6xOa">
                                                                        <div class="_3fgAI3 Th26Zc">
                                                                            <?= $this->Form->text('delivery_firstname', ['class' => '_16qL6K _366U7Q', 'label' => false, 'type' => 'text', 'id' => 'delivery_firstname', 'placeholder' => 'First Name', 'tabindex' => "1"]); ?>
                                                                            <?= $this->Form->text('order_id', ['id' => 'order_id', 'type' => 'hidden']); ?>
                                                                            <!--<label for="name" class="_20i8vs"></label>-->
                                                                        </div>
                                                                        <div class="_3fgAI3 Th26Zc">
                                                                            <?= $this->Form->text('delivery_lastname', ['class' => '_16qL6K _366U7Q', 'label' => false, 'type' => 'text', 'id' => 'delivery_lastname', 'placeholder' => 'Last Name', 'tabindex' => "2"]); ?>
                                                                        </div>
                                                                    </div>
                                                                    <div class="uK6xOa">
                                                                        <div class="_3fgAI3 Th26Zc">
                                                                            <?= $this->Form->text('delivery_postcode', ['class' => '_16qL6K _366U7Q', 'label' => false, 'type' => 'text', 'id' => 'delivery_postcode', 'placeholder' => 'Pincode', 'tabindex' => "3"]); ?>
                                                                        </div>
                                                                        <div class="_3fgAI3 Th26Zc">
                                                                            <?= $this->Form->text('delivery_address2', ['class' => '_16qL6K _366U7Q', 'label' => false, 'type' => 'text', 'id' => 'delivery_address2', 'placeholder' => 'Locality', 'tabindex' => "4"]); ?>
                                                                        </div>
                                                                    </div>
                                                                    <div class="uK6xOa _3g_m0J">
                                                                        <div class="_1X5WZZ _1QG_7g">
                                                                            <div class="_2mJu7M Th26Zc">
                                                                                <?= $this->Form->textarea('delivery_address1', ['class' => 'giyiJa _16qL6K _21icXK', 'label' => false, 'type' => 'text', 'id' => 'delivery_address1', 'placeholder' => 'Address (Area and Street)', "rows" => "4", "cols" => "10", 'tabindex' => "5"]); ?>
                                                                            </div>
                                                                            <div class="_39_KcN _3-mqeJ"></div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="uK6xOa">
                                                                        <div class="_1X5WZZ _3fgAI3 _3g_m0J">
                                                                            <div class="_2mJu7M Th26Zc">
                                                                                <?= $this->Form->text('delivery_city', ['class' => '_16qL6K _366U7Q', 'label' => false, 'type' => 'text', 'id' => 'delivery_city', 'placeholder' => 'City/District/Town', 'tabindex' => "6"]); ?>
                                                                            </div>
                                                                            <div class="_39_KcN _3-mqeJ"></div>
                                                                        </div>
                                                                        <div>
                                                                            <div class="M2VvVb">
                                                                                <div class="_2uWdcR _3fgAI3 N25bMB">
                                                                                    <?= $this->Form->select('delivery_countryid', $countriesList, ['type' => 'select', 'id' => "delivery_countryid", 'label' => false, 'class' => '_3092M2 _3fgAI3 N25bMB QoXnA9', 'empty' => '--select country--']); ?>
                                                                                    <span class="_1LBnEa _2KY_MK"></span>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="uK6xOa">
                                                                        <div class="_3fgAI3 Th26Zc">
                                                                            <?= $this->Form->text('delivery_tel1', ['class' => '_16qL6K _366U7Q', 'label' => false, 'id' => 'delivery_tel1', 'placeholder' => '10-digit mobile number', 'tabindex' => "8"]); ?>
                                                                        </div>
                                                                        <div class="_3fgAI3 Th26Zc">
                                                                            <?= $this->Form->text('delivery_tel2', ['class' => '_16qL6K _366U7Q', 'label' => false, 'id' => 'delivery_tel2', 'placeholder' => 'Alternate Phone', 'tabindex' => "9"]); ?>
                                                                        </div>
                                                                    </div>
                                                                    <div class="_3XXwRR">
                                                                        <p class="_2dwzAy">Address Type</p>
                                                                        <div class="_3qg3HS"><div>
                                                                                <label for="HOME" class="_8J-bZE _2tcMoY">
    <!-- <input type="radio"  class="_2haq" data-orderid="<?= $orderaddress['order_id']; ?>"name="choose_delivery" value= 'HOME' 
                                                                                    <?php
                                                                                    if ($orderaddress['address_type'] == "HOME") {
                                                                                        echo "checked";
                                                                                    }
                                                                                    ?>>-->
                                                                                    <input type="radio" class="_2haq" name="address_type" readonly="" id="address_type" value="HOME">
                                                                                    <!--<div class="_6ATDKp"></div>-->
                                                                                    <div class="_2o59RR"><span>Home (All day delivery)</span></div>
                                                                                </label>
                                                                                <label for="WORK" class="_8J-bZE _2tcMoY">
    <!--                                                                                    <input type="radio"  class="_2haq" data-orderid="<?= $orderaddress['order_id']; ?>"name="choose_delivery" value= 'WORK' <?php
                                                                                    if ($orderaddress['address_type'] == "WORK") {
                                                                                        echo "checked";
                                                                                    }
                                                                                    ?>>-->
                                                                                    <input type="radio" class="_2haq" name="address_type" id="address_type" value="WORK">
                                                                                    <!--<div class="_6ATDKp"></div>-->
                                                                                    <div class="_2o59RR"><span>Work (Delivery between 10 AM - 5 PM)</span></div>
                                                                                </label>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="_1qbqu2 uK6xOa">

                                                                        <button class="_2AkmmA EqjTfe _7UHT_c" type="submit" tabindex="10">Save and Deliver Here</button>
                                                                        <button class="_2AkmmA _237M5J" type="button" id="canceledit"tabindex="11">Cancel</button>
                                                                    </div>
                                                                </div>
    <?= $this->Form->end() ?>
                                                            </div>
                                                        </div>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>

                        <li>
                            <div class="_7XMNLT" id="Summary">
                                <div class="_1GRhLX _38vNoF">
                                    <h3 class="_2RMAtd"><span class="_1Tmvyj">2</span><span class="_1_m52b">Order Summary</span></h3>
                                    <div class="_3K1hJZ"></div>
                                </div>
                            </div>
                            <div class="_7XMNLT" id="ordersummary" style="display:none;">
                                <div class="_1GRhLX _38vNoF">
                                    <h3 class="_1fM65H _2RMAtd">
                                        <span class="_1Tmvyj">3</span>
                                        <span class="_1_m52b">Order Summary</span></h3>
                                    <div class="_3K1hJZ">
                                        <div class="_2eTL2v">
                                            <div class="_20egpM">
                                                <div class="_3ycxrs">
    <?php foreach ($orderlists as $orderlist) { ?>
                                                                <?php if ($orderlist->qty > 0) { ?>
                                                            <div class="PaJLWc">
                                                                <div class="_2u3tZM">
                                                                    <?php if ($orderlist->product['images'][0]['main_image'] == 'Y') { ?>
                                                                        <img src="<?= HTTP_ROOT . PRODUCT_PIC . $orderlist->product['images'][0]['filename']; ?>" width="100" height="42" />  
                                                                    <?php } else { ?>
                                                                        <img src= "<?= HTTP_ROOT ?>/images/no_image_available.jpg " />
            <?php } ?>
                                                                </div>
                                                                <div class="_3vIvU_">
                                                                    <div class="_1Ox9a7">
                                                                        <div class="_325-ji"><?= $orderlist->name; ?></div>
                                                                    </div>
                                                                    <div class="v7-Wbf"> &pound;<?= number_format($orderlist->total, 2, '.', ''); ?>
                                                                    </div
                                                                </div>
                                                            </div>                                                    
                                                        </div>
                                                        <?php
                                                    }
                                                }
                                                ?>
                                            </div>
                                            <div class="_3cCusG">
                                                <span style="flex: 1 1 auto;">Order confirmation email will be sent to <form class="BuHaJm _1pSXw6" role="form"><input type="email" value="<?= $usermail->email; ?>" class="_35lzyU" disabled="" placeholder="Enter your email ID"></form></span>
                                                <span id="to-payment">
                                                    <button class="_2AkmmA _2Q4i61 _7UHT_c" id="continue" onclick="location.href = '<?= HTTP_ROOT ?>Products/payment';" target="_sel">CONTINUE</button>

                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
<?php } else { ?>
            <ul class="checkout_list">
                <li class="active">
                    <div class="list_header">Checkout Method<div class="number"></div></div>
                    <div class="list_body">
                        <div class="clear"></div>
                    </div>
                </li>
                <li>
                    <a href="#" class="list_header">Delivery Address<div class="number">1</div></a>
                </li>
                <li>
                    <div class="list_header">Order Summary<div class="number">2</div></div>
                </li>
            </ul>
<?php } ?>
    </div><!-- .grid_9 
</div><!-- #content -->
    <div class="clear"></div>
</div><!-- .container_12 -->
</section><!-- #main -->
<div class="clear"></div>

<script>
    $(document).ready(function () {
        $("#change").click(function () {
            $("#delivery").show();
            $("#checkaddress").hide();
            $("#Summary").show();
            $("#ordersummary").hide();
        });
        $(".check").click(function () {
            $(this).siblings(".show").hide();
            $(this).find(".show").show();
            $(this).find("._3Ojtt3").show();
//            window.location.reload();
        });

        $(".FoDyGO").click(function () {
            $("#editForm").show();
            $(".delivery").hide();
            $("#addAddress").hide();
            $("#addform").hide();
        });
        $(".show").click(function () {
            $("#ordersummary").show();
            $("#checkaddress").show();
            $("#delivery").hide();
            $("#Summary").hide();
        });
        $("#continue").click(function () {
            $("#ordersummary").show();
            $("#checkaddress").show();
            $("#delivery").hide();
            $("#Summary").hide();
        });
        $("#addAddress").click(function () {
            $("#addform").show();
        });
        $("#cancel").click(function () {
            $("#addform").hide();
        });
        $("#canceledit").click(function () {
            $("#editForm").hide();
        });
        $('._2haq-9').click(function () {

            $(this).find(".show").show();
            $(this).find("._3Ojtt3").show();

            var element = $(this);
            var orderid = element.data('orderid');
            $.ajax({
                type: 'POST',
                url: siteUrl + 'products/chooseDelivery',
                data: {'order_id': orderid},
                dataType: 'json',
                success: function (response) {
//                    if (response.status == 'success') {
////                        element.removeClass("removeFavorite").addClass("addToFavorite"); 
//                        alert(response.msg);
//                    }
//                    else {
                    alert(response.msg);
//                    }
                }
            });
        });

        $(document).on('submit', '#addressform', function (e) {
            e.preventDefault();
            addAddress('');
        });
        $(document).on('submit', '#editaddressform', function (e) {
            e.preventDefault();
            editAddress('');
        });
    });
    function addAddress() {
        var form = $('#addressform').get(0);
        $.ajax({
            type: 'POST',
            url: siteUrl + 'products/ajaxAddaddress',
            data: new FormData(form),
            dataType: 'json',
            cache: false,
            contentType: false,
            processData: false,
            success: function (response) {
                if (response.status == 'success') {
                    alert(response.msg);
                    ("#ordersummary").show();
                } else {
                    alert(response.msg);
                }
            }
        });
    }
    function viewAddress(id) {
        $.ajax({
            type: 'POST',
            url: siteUrl + 'products/ajaxView',
            data: {id: id},
            dataType: 'json',
            success: function (response) {

                if (response.status == 'sucess') {
//                     alert(response.data);
                    $('#order_id').val(response.data.order_id);
                    $('#delivery_firstname').val(response.data.delivery_firstname);
                    $('#delivery_lastname').val(response.data.delivery_lastname);
                    $('#delivery_postcode').val(response.data.delivery_postcode);
                    $('#delivery_address2').val(response.data.delivery_address2);
                    $('#delivery_address1').val(response.data.delivery_address1);
                    $('#delivery_city').val(response.data.delivery_city);
                    $('#delivery_countryid').val(response.data.delivery_countryid);
                    $('#delivery_tel1').val(response.data.delivery_tel1);
                    $('#delivery_tel2').val(response.data.delivery_tel2);
                    $('#address_type').val(response.data.address_type);
                } else {
                    alert(response.msg);
                }
            }
        });
        $("#display").children().remove();
    }
    function editAddress() {
        var form = $('#editaddressform').get(0);
//        alert(form);
        $.ajax({
            type: 'POST',
            url: siteUrl + 'products/addressEdit',
//            data: $('#editForm').serialize(),
            data: new FormData(form),
            cache: false,
            contentType: false,
            processData: false,
            dataType: 'json',
            success: function (response) {
                if (response.status == 'success') {
//                    $('#editModal').modal('hide');
                    alert(response.msg);

                } else {
                    alert(response.msg);
                }
            }

        });
    }
</script>