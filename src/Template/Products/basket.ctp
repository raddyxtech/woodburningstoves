<section id="main" class="entire_width">
    <div class="container_12">
        <div class="grid_12">
            <h3>Shopping Cart</h3>
 <?php if (!empty($orders)) { ?>
                <p>Details of your current basket are shown below.</p>
              <?= $this->Form->create(null) ?>
            <table class="cart_product">
                <tr>
                    <th class="images">Image</th>
                    <th class="bg name">Product Name</th>
                    <th class=" price">Unit Price</th>
                    <th class="bg qty">Qty</th>
                    <th class="subtotal">Subtotal</th>
                    <th class="close"> </th>
                </tr>
                  <?php foreach ($orders as $order) { ?>
                       <?php if($order->qty > 0) { ?>
                        <tr id="order<?= $order->id ?>">
                            <td class="images">
                                    <?php if ($order->product['images'][0]['main_image'] == 'Y') { ?>
                                        <img src="<?= HTTP_ROOT . PRODUCT_PIC . $order->product['images'][0]['filename']; ?>" width="110px" height="110px"/>  
                                    <?php } else { ?>
                                        <img src= "<?= HTTP_ROOT ?>/images/no_image_available.jpg " />
                                    <?php } ?>
                            </td>
                            <td class="bg name">
                                <?= $order->name; ?>
                            </td>
                        <input type="hidden" value="<?= $order->product_id; ?>" name="product_id[]">
                            <td class=" price">&pound;<?= number_format($order->unit_price, 2, '.', ''); ?></td>
                            <td class=" bg qty"><input maxlength="5"type="text" name="iQty[]" value="<?= $order->qty; ?>"/></td>
                            <td class="subtotal">&pound;<?= number_format($order->unit_price * $order->qty, 2, '.', ''); ?></td>
                            <td class="close"><a  Onclick="deleteProduct('<?= $order['id'] ?>')" title="close" class="close"  href="javascript:;"></a></td>
                        </tr>
                 <?php } } ?>
                <td colspan="7" class="cart_but">
                     
                    <input name="proceed" type="button" class="continue" value="&pound;<?= number_format($total, 2, '.', ''); ?>" style="padding: 0 20px;height: 29px;background: #ffff;border: 0 none;color: #444;float:right;cursor: pointer;"/> 
                     <input name="continue" type="button" class="continue" value="Total Basket" style="padding: 0 20px;height: 29px;background: #ffff;border: 0 none;color: #444;float: right;pointer:cursher;" onclick="location.href = '<?= HTTP_ROOT ?>Products/products-details/<?= @$order->product_id; ?>';" target="_self" />   
                     <input name="submit" class="update" style="hover:#59b7c2;padding: 0 20px;float: right;margin-right: 80px;display: inline;height: 29px;background: #f1f3f5;border: 0 none;color: #444;" type="submit" value="update"/>
                     <p style="margin-top:9px;">*set quantity to zero and press update button to remove a product from your cart</p>
                     
                    </td>  
                <tr>
                    <?php foreach($orders as $order) ?>
                    <td colspan="7" class="cart_but">
                        <input name="continue" type="button" class="continue" value="&lt;&lt;&nbsp;Continue Shopping" style="padding: 0 20px;height: 29px;background: #f1f3f5;border: 0 none;color: #444;float:left;cursor: pointer;hover:#59b7c2" onclick="location.href = '<?= HTTP_ROOT ?>Products/products-details/<?= @$order->product_id; ?>';" target="_self" />        
                         
                   
                        <input name="proceed" type="button" class="continue" value="proceed>>" style="hover:#59b7c2;padding: 0 20px;height: 29px;background: #f1f3f5;border: 0 none;color: #444;float:right;cursor: pointer;" onclick="location.href = '<?= HTTP_ROOT ?>Products/check-out'" target="_self" /> 
                    </td>
                </tr>
            </table>
                 <?php } else { ?>
                    <p>Your order basket is empty</p>
                <?php } ?>
                <?= $this->Form->end() ?>
        </div><!-- .grid_12 

        <div class="clear"></div>

        <div id="content_bottom" class="shopping_box">
            <div class="grid_4">
                <div class="bottom_block estimate">
                    <h3>Estimate Shipping and Tax</h3>
                    <p>Enter your destination to get a shipping estimate.</p>
                    <form>
                        <p>
                            <strong>Country:</strong><sup class="surely">*</sup><br/>
                            <select>
                                <option>United States</option>
                                <option>United States</option>
                            </select>
                        </p>
                        <p>
                            <strong>State/Province:</strong><br/>
                            <select>
                                <option>Please select region, state or province</option>
                                <option>Please select region, state or province</option>
                            </select>
                        </p>
                        <p>
                            <strong>Zip/Postal Code</strong><br/>
                            <input type="text" name="" value="" />
                        </p>
                        <input type="submit" id="get_estimate" value="Get a Quote" />
                    </form>

                </div><!-- .estimate 
            </div><!-- .grid_4 -->

<!--            <div class="grid_4">
                <div class="bottom_block discount">
                    <h3>Discount Codes</h3>
                    <p>Enter your coupon code if you have one.</p>
                    <form>
                        <p>
                            <input type="text" name="" value="" placeholder="United States"/>
                        </p>
                        <input type="submit" id="apply_coupon" value="Apply Coupon" />
                    </form>
                </div> .discount 
            </div> .grid_4 

            <div class="grid_4">
                <div class="bottom_block total">
                    <table class="subtotal">
                        <tr>
                            <td>Subtotal</td><td class="price">$1, 500.00</td>
                        </tr>
                        <tr class="grand_total">
                            <td>Grand Total</td><td class="price">$1, 500.00</td>
                        </tr>
                    </table>
                    <button class="checkout">PROCEED TO CHECKOUT</button>
                    <a href="#">Checkout with Multiple Addresses</a>
                </div> .total 
            </div> .grid_4 -->

            <div class="clear"></div>
        </div><!-- #content_bottom -->
        <div class="clear"></div>
    </div><!-- .container_12 -->
</section><!-- #main -->

<div class="clear"></div>
<script>
      function deleteProduct(id) {
//    alert("#product-" + id);

        if (confirm("Are you sure want to delete?")) {
            $.ajax({
                type: 'POST',
                url: siteUrl + 'products/ajaxDelete',
                data: {'id': id},
                dataType: 'json',
                success: function (response) {
                    if (response.status == 'success') {
                        $("#order" + id).remove();
                         window.location.reload();
                    } else {
                        alert(response.msg);
                    }
                }
            });
        }
    }
        </script>