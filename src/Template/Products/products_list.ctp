<?php
$this->Paginator->setTemplates([
    'prevDisabled' => '<li class="page-item page-link disabled">{{text}}</li>',
    'nextDisabled' => '<li class="page-item page-link disabled">{{text}}</li>',
    'number' => '<li class="page-item"><a class="page-link" href="{{url}}">{{text}}</a></li>',
    'prevActive' => '<li class="page-item"><a class="page-link" href="{{url}}" aria-label="Previous">{{text}}</a></li>',
    'nextActive' => '<li class="page-item"><a class="page-link" href="{{url}}" aria-label="Next">{{text}}</a></li>',
    'current' => '<li class="page-item active"><a class="page-link" href="{{url}}">{{text}}</a></li>'
]);
?>
  <link rel="stylesheet" href="https://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<style>
    .product {width: 226px!important;height: 336px;overflow: hidden;border: 1px solid #e0e0e0;border-radius: 2px;-moz-border-radius: 2px;-khtml-border-radius: 2px;behavior: url(PIE.htc);}
    .submit{border: 0; background: 0;background-color: #f5f7f9;}
    .removeFavorite { color: #d01818; }
    .pagination > .active > a, .pagination > .active > span, .pagination > .active > a:hover, .pagination > .active > span:hover, .pagination > .active > a:focus, .pagination > .active > span:focus, .page-item.active .page-link {
    background-color: #8592;
    /*border-color: #64c5b1;*/
}
</style>

<section>
    <div class="container_12">      
        <div id="content" class="grid_9">
            <h1 class="page_title">Product List</h1>
            <div class="grid_product row filter_data">
            </div>
            <div class="clear"></div>
            <div class="paginationRow">
                <div class="paginationLeft">
                    <p><?= $this->Paginator->counter('Showing {{start}}-{{end}} Orders out of {{count}}'); ?></p>
                </div>
                <div class="paginationRight">
                    <?php if ($this->Paginator->numbers()): ?>
                        <ul class="pagination pagination-split">
                            <?= $this->Paginator->prev('<span aria-hidden="true"> << </span><span class="sr-only">Previous</span>', ['escape' => false]) ?> 
                            <?= $this->Paginator->numbers() ?>
                            <?= $this->Paginator->next('<span aria-hidden="true"> >> </span><span class="sr-only">Next</span>', ['escape' => false]) ?>
                        </ul>
                    <?php endif; ?>
                </div>
            </div>
        </div><!-- #content -->
       

        <div id="sidebar" class="grid_3">
            <aside id="shop_by">
                  <div class="list-group">
                        <h3>Keyword</h3>
                        <input type="text" name="keyword" placeholder="Search by Product Name" id="keyword" onkeyup="filter_data()">
               </div><br/><br/>	
               <div class="list-group">
                        <h3>Price</h3>
                        <input type="hidden" id="hidden_minimum_price" value="100.00" />
                        <input type="hidden" id="hidden_maximum_price" value="1990.00" />
                        <p id="price_show">10.00 - 1990.00</p>
                        <div id="price_range"></div><br/><br/>  <div class="clear"></div>
               </div>	
               
               <div class="clear"></div>
                <div id="specials" class="specials">
                    <h3>Brands</h3>
                    <div style="height: 180px; overflow-y: auto; overflow-x: hidden;">
                        <div class="list-group-item checkbox">
                            <?php foreach ($brands as $brand){ ?>
                              <label><input type="checkbox" class="common_selector brand_id" value="<?= $brand->id ?>"> <?= $brand->name ?></label><br/>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </aside>
         <aside id="categories_nav">
                <h3>Categories</h3>
                <nav class="left_menu">
                    <ul>
                        <?php foreach ($categories as $categorie) { ?>
    <!--                            <li><a href="<?= $this->Url->build(['controller' => 'Products', 'action' => 'productsList', $categorie->category_id]); ?>" ><?= $categorie->name; ?> <span>(<?= $categorie->count; ?>)</span></a></li>-->
                            <label>
                            <input type="checkbox" class="common_selector category_id" value="<?= $categorie->category_id ?>"> <?= $categorie->name ?><span>(<?= $categorie->count; ?>)</span>
                            </label><br/>
                        <?php } ?> 
                    </ul>
                </nav><!-- .left_menu -->
            </aside><!-- #categories_nav -->
<!---->            </aside><!-- #categories_nav -->
        </div><!-- .sidebar -->
        <div class="clear"></div>
    </div><!-- .container_12 -->
</section><!-- #main -->
<div class="clear"></div>

<script>
     $(document).on('click', '.addToFavorite', function(e) {  
            var element = $(this);
            var productid = element.data('productid');  
            
            $.ajax({
                type: 'POST',
                url: siteUrl + 'products/addFavourite',
                data: {'product_id': productid},
                dataType: 'json',
                success: function (response) {
                    if(response.status == 'success') {
                        element.removeClass("addToFavorite").addClass("removeFavorite"); 
                    } else {
                        alert(response.msg);                    }                   
                }                
            });                         
        });
        
        $(document).on('click', '.removeFavorite', function(e) {  
            var element = $(this);
           var productid = element.data('productid');          
            $.ajax({
                type: 'POST',
                 url: siteUrl + 'products/deleteFavourite',
                data: {'product_id': productid},
                dataType: 'json',
                success: function (response) {
                    if(response.status == 'success') {
                        element.removeClass("removeFavorite").addClass("addToFavorite"); 
                    } 
                    else {
                       alert(response.msg);
                    }   
                }         
            });
        }); 


            $(document).ready(function () {

                filter_data();

                function filter_data()
                {
                    $('.filter_data').html('<div id="loading" style="" ></div>');
//                    var action = 'fetch_data';
                    var minimum_price = $('#hidden_minimum_price').val();
                    var maximum_price = $('#hidden_maximum_price').val();
                    var keyword = $('#keyword').val();
//                    var brand_id = $('.niceCheck brand_id').val();
//                    var category_id = $('.niceCheck category_id').val();
                    var brand_id = get_filter('brand_id');
                    var category_id = get_filter('category_id');
//                    var ram = get_filter('ram');
//                    var storage = get_filter('storage');
                    $.ajax({
                        url: siteUrl + 'products/search',
                        method: "POST",
                        data: {minimum_price: minimum_price, maximum_price: maximum_price, brand_id: brand_id,keyword: keyword,category_id:category_id},
                        success: function (data) {
                            $('.filter_data').html(data);
                        }
                    });
                }

                function get_filter(class_name){
                    var filter = [];
                    $('.' + class_name + ':checked').each(function () {
                        filter.push($(this).val());
                    });
                    return filter;
                }

                $('.common_selector').click(function () {
                    filter_data();
                });
                $('#keyword').click(function () {
                    filter_data();
                });

                $('#price_range').slider({
                    range: true,
                    min: 10.00,
                    max: 1990.00,
                    values: [10.00, 1990.00],
                    step: 500,
                    stop: function (event, ui)
                    {
                        $('#price_show').html(ui.values[0] + ' - ' + ui.values[1]);
                        $('#hidden_minimum_price').val(ui.values[0]);
                        $('#hidden_maximum_price').val(ui.values[1]);
                        filter_data();
                    }
                });

            });
        </script>

