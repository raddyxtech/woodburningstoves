<html>
    <head>

        <title>WOOD BURNING STOVES</title>
        <link rel="stylesheet" href="<?= HTTP_ROOT; ?>css/font-awesome.min.css"> 
        <link rel="shortcut icon" type="image/x-icon" href="<?= HTTP_ROOT ?>images/favicon.ico">
        <base href="<?= HTTP_ROOT ?>" />
        <style>
            .invoice-main{border:1px solid #ddd;font-family: arial;padding: 2px;margin: 0 auto;}
            .invoice-top{border-bottom:1px solid #ddd;}
            .invoice-top td{font-size: 20px;color: #000;text-align: center;padding: 10px}
            .invoice-top td:first-child{text-align: left;}
            .invoice-header1{padding: 10px;}    
            .invoice-header1 td{text-align: left;padding:10px 0;font-size: 14px;text-transform: capitalize;line-height: 22px;}    
            .invoice-item-container{border:1px solid #ddd;border-radius: 4px;margin: 2%;}
            .invoice-item-container tr th.invoice-item-header{border-bottom:1px solid #ddd; background: #f5f5f5;font-size: 18px;color: #000;text-align: left;padding:7px 10px;}
            .invoice-item-data tr th{font-size: 14px;color: #000;text-align: left;padding:8px 10px;}
            .invoice-item-data tr td{font-size: 14px;color: #000;text-align: left;padding:8px 10px;}
            .invoice-item-data tr td:last-child{text-align: right;}
            .invoice-item-data tr th:last-child{text-align: right;}    
            .invoice-item-data tr td.f-total{font-size: 18px;color: #000;}
            .invoice-button-section{text-align: center; width:100%;padding: 15px;font-family:'Poppins', sans-serif;}
            .invoice-button-section a{cursor: pointer;display: inline-block;font-size: 14px;line-height: 32px;font-weight: 600;color: #fff;background: #176aae;padding: 0 10px;border-radius: 3px;text-decoration: none; }
            .invoice-button-section a:hover{background: #ec971f;}
            .invoice-button-section .cancel-btn{ background: #c9302c;}
            @media print {
                .invoice-button-section {
                    display:none
                }
            }

        </style>
    </head>
    <body>
        <table class="invoice-main" width="800px" cellspacing="0" cellpadding="0" align="center;">
            <tr>
                <td>
                    <table class="invoice-top" width="100%" cellspacing="0" cellpadding="0">
                        <tr>                   
                            <td  align="center" valign="middle" colspan="2" style="font-size:20px;text-align: center;background: #B8B8B8;padding:7px 0;color: #fff">INVOICE</td>                    
                        </tr>
                        <tr>
                            <td valign="middle" align="left"><img src="<?= HTTP_ROOT ?>images/logo.png" height="80"></td>                   
                            <td valign="middle" align="right" style="text-align:right;">woodburningstoves</td>
                        </tr>
                    </table>
                    <table class="invoice-header1" width="100%" cellspacing="0" cellpadding="0">

                    </table>
                    <table class="invoice-item-container" width="96%" cellspacing="0" cellpadding="0">
                        <tr>
                            <th class="invoice-item-header">Purchase Summary</th> 
                        </tr>
                        <tr>
                            <td>
                                <table class="invoice-item-data" width="100%" cellspacing="0" cellpadding="0">
                                    <thead>
                                        <tr>
                                            <th style="border-bottom:1px solid #ddd;"> Membership   </th>
                                            <th style="border-bottom:1px solid #ddd;"> Price   </th>
                                            <th style="border-bottom:1px solid #ddd;" align="right">  Total  </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td> <b><?= $invoiceDetail->subscription_plan->name ?></b>   </td>
                                            <td> <?= $this->Custom->priceFormat($invoiceDetail->invoice_price); ?>  </td>
                                            <td align="right"><?= $this->Custom->priceFormat($invoiceDetail->invoice_price); ?>   </td>
                                        </tr>
                                    </tbody>


                                    <tfoot>
                                        <tr>
                                            <td style="border-top:1px solid #ddd;"> &nbsp; </td>
                                            <td style="border-top:1px solid #ddd;" class="f-total"> <b>Total </b> </td>
                                            <td style="border-top:1px solid #ddd;" class="f-total" align="right"> <?= $this->Custom->priceFormat($invoiceDetail->invoice_price); ?> </td>
                                        </tr>
                                        <tr>
                                            <td style="border-top:1px solid #ddd;"> &nbsp; </td>
                                            <td style="border-top:1px solid #ddd;" class="f-total"> <b>Received </b> </td>
                                            <td style="border-top:1px solid #ddd;" class="f-total" align="right"> <?= $this->Custom->priceFormat($invoiceDetail->received_price); ?> </td>
                                        </tr>
                                        <tr>
                                            <td style="border-top:1px solid #ddd;"> &nbsp; </td>
                                            <td style="border-top:1px solid #ddd;" class="f-total"> <b>Balance </b> </td>
                                            <td style="border-top:1px solid #ddd;" class="f-total" align="right"> <?= $this->Custom->priceFormat($invoiceDetail->invoice_price - $invoiceDetail->received_price); ?> </td>
                                        </tr>
                                    </tfoot>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <?php if (!$this->request->getQuery('download')) { ?>
            <div class="invoice-button-section">
                <a href="<?= HTTP_ROOT . "invoice-preview/$invoiceDetail->order_id"; ?>?download=true" target="_top" class="download-btn"><i class="fa fa-download"></i> DOWNLOAD</a>
                <?php if ($user_type == 2) { ?> 
                    <a href="<?= HTTP_ROOT ?>payment-history" class="cancel-btn"><i class="fa fa-undo"></i> CANCEL</a>
                <?php } ?>
            </div>
        <?php } ?>
    </body>
</html>