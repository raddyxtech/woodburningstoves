<div id="wrapper">  
    <div id="main">
        <div id="left-col"> 
            <?= $this->cell('HomeSidebars::sideSearch'); ?>
            <?= $this->cell('HomeSidebars::mini_cart'); ?>            
            <?= $this->cell('HomeSidebars::side_testimonials'); ?>
        </div>  
        <div id="right-col">    
            <h1><?= @$Parentcategories->name; ?></h1>
            <div id="sub-categories">
                <?php foreach ($categories as $categorie) { ?>
                    <a href="<?= $this->Url->build(['controller' => 'Products', 'action' => 'productsInf/' . $categorie->category_id . $categorie->name]); ?>"   title="<?= $categorie->name; ?>"><span><?= $categorie->name; ?></span></a>
                <?php } ?> 
            </div>
            <p class="text-center red bold"></p>
            <?php foreach ($products as $product) { ?>
                <?php foreach ($product as $productInf) { ?>
                    <div class="small-prod-blk float-left">  
                        <p class="text-center list-prod-title"><?= $productInf->name; ?> </p>   
                        <p class="underline">&nbsp;</p>     
                        <?php foreach ($productInf->images as $image); ?>
                             <?php if ($image['images'][0]['main_image'] == 'Y') { ?>
                                    <a href="<?= $this->Url->build(['controller' => 'Products', 'action' => 'productsDetails/' . $productInf->product_id]); ?>"  target="_self" title="<?= $image->name; ?>"><img class="float-left" width="104" height="131" src="<?= HTTP_ROOT . PRODUCT_PIC . $image['images'][0]['filename']; ?>" /></a>   
                                   <?php } else { ?>
                                        <a href="<?= $this->Url->build(['controller' => 'Products', 'action' => 'productsDetails/' . $productInf->product_id]); ?>"  target="_self" title="<?= $image->name; ?>"><img class="float-left" width="104" height="131" src= "<?= HTTP_ROOT ?>/images/no_image_available.jpg " /></a> 
                                     <?php } ?>
                                    <?php foreach ($productInf->product_variations as $order) ?>
                                    <p class="big-price text-center top-pad">&pound;<?= $order->retail_price; ?></p> 
                                    <p class="text-center more-info clearright"><a href="<?= $this->Url->build(['controller' => 'Products', 'action' => 'productsDetails/' . $productInf->product_id]); ?>" target="_self" title="<?= $productInf->name; ?>">More info</a></p>
                    </div>    
                <?php } ?>
           <?php } ?>
            <div class="clearblock"></div>
        </div>    
    </div>
</div>
<div class="clearblock"></div>   
