 <link rel="stylesheet" href="/woodburningstoves/css/style.css"/> 
<section class="home">
    <div class="container_12">
        <h1>Delivery Details</h1>
        <div id="inner" class="login-box">
            <p>Please enter your details which will become your Delivery details. If you are going to pay by credit card, these details must match the billing details for your credit card.</p>
            <?= $this->Form->create($order, ['id' => 'frmCheckout', 'name' => 'frmCheckout', 'type' => 'file']) ?>
            <fieldset>
                <legend>Contact Details</legend>
                <div class="split5050 text-right float-left">
                    <p>
                        <label for="title">Title</label>
                        <?= $this->Form->select('delivery_title', [ 'Mr' => 'Mr.', 'Miss' => 'Miss.', 'Ms' => 'Ms.', 'Mrs' => 'Mrs.', 'Dr' => 'Dr.'], ['empty' => '--Select Title--', 'div' => false, 'class' => 'large-select', 'id' => 'title', 'title' => 'Select title']); ?>
                    </p>
                    <p>
                        <label for="firstname">First Name</label>
                        <?= $this->Form->text('delivery_firstname', ['class' => 'large-input', 'label' => false, 'type' => 'text', 'id' => 'firstname', 'placeholder' => ' Enter First Name ']); ?>
                    </p>
                    <p>
                        <label for="lastname">Last Name</label>
                        <?= $this->Form->text('delivery_lastname', ['class' => 'large-input', 'label' => false, 'type' => 'text', 'id' => 'lastname', 'placeholder' => ' Enter Last Name ']); ?>
                    </p>
                    <p>
                        <label for="email">Email</label>
                        <?= $this->Form->text('delivery_email', ['class' => 'large-input', 'label' => false, 'type' => 'email', 'id' => 'email', 'placeholder' => 'Enter Email']); ?>
                    </p>
                </div>

                <div class="split5050 text-right float-right">
                    <p>
                        <label for="landline">Telephone 1</label>
                        <?= $this->Form->text('delivery_tel1', ['class' => 'large-input', 'label' => false, 'type' => 'number', 'id' => 'landline', 'placeholder' => 'Enter Telephone no.']); ?>
                    </p>
                    <p>
                        <label for="mobile">Telephone 2</label>
                        <?= $this->Form->text('delivery_tel2', ['class' => 'large-input', 'label' => false, 'type' => 'number', 'id' => 'mobile', 'placeholder' => 'Enter Telephone no.']); ?>
                    </p>
                    <p>
                        <label for="tel3">Telephone 3</label>
                        <?= $this->Form->text('delivery_tel3', ['class' => 'large-input', 'label' => false, 'type' => 'number', 'id' => 'tel3', 'placeholder' => 'Enter Telephone no.']); ?>
                    </p>
                    <p>
                        <label for="emailconfirm">Confirm Email</label>
                        <?= $this->Form->text('sEmail2', ['class' => 'large-input', 'label' => false, 'type' => 'email', 'id' => 'emailconfirm', 'placeholder' => 'Enter Email']); ?>

                    </p>
                </div>
            </fieldset>

            <fieldset>
                <legend>Address Details</legend>
                <div class="split5050 text-right float-left">
                    <p>
                        <label for="company">Company</label>
                        <?= $this->Form->text('delivery_company', ['class' => 'large-input', 'label' => false, 'type' => 'text', 'id' => 'tel3', 'placeholder' => 'Enter Company']); ?>
                    </p>
                    <p>
                        <label for="address1">Address</label>
                        <?= $this->Form->text('delivery_address1', ['class' => 'large-input', 'label' => false, 'type' => 'text', 'id' => 'address1', 'placeholder' => 'Enter Address']); ?>
                    </p>
                    <p>
                        <label for="address2">&nbsp;</label>
                        <?= $this->Form->text('invoice_address2', ['class' => 'large-input', 'label' => false, 'type' => 'text', 'id' => 'address2', 'placeholder' => 'Enter Address']); ?>
                    </p>
                    <p>
                        <label for="city">City or town</label>
                        <?= $this->Form->text('delivery_address2', ['class' => 'large-input', 'label' => false, 'type' => 'text', 'id' => 'address2', 'placeholder' => 'Enter City/town']); ?>
                    </p>
                </div>
                <div class="split5050 text-right float-right">
                    <p>
                        <label for="county">County</label>
                        <?= $this->Form->text('delivery_county', ['class' => 'large-input', 'label' => false, 'type' => 'text', 'id' => 'county', 'placeholder' => 'Enter county']); ?>
                    </p>
                    <p>
                        <label for="county">City</label>
                        <?= $this->Form->text('delivery_city', ['class' => 'large-input', 'label' => false, 'type' => 'text', 'id' => 'county', 'placeholder' => 'Enter county']); ?>
                    </p>
                    <p>
                        <label for="postcode">Postcode</label>
                        <?= $this->Form->text('delivery_postcode', ['class' => 'large-input', 'label' => false, 'type' => 'text', 'id' => 'postcode', 'placeholder' => 'Enter postcode']); ?>
                    </p>
                    <p>
                        <label for="country">Country</label>
                        <?= $this->Form->select('delivery_countryid', $countriesList, ['type' => 'select', 'id' => "country", 'label' => false, 'class' => 'large-select', 'empty' => '--select country--']); ?>
                    </p>
                </div>
            </fieldset>
<!--                <p class="float-right">
                Please check this box if delivery details are the same as invoice details.
                <input type="checkbox" name="cSameAddr" class="check-radio-box"title="Delivery address the same" />
            </p>
            <div class="clearblock"></div>
            <p class="float-right">I agree and accept to abide by the <a href="<?= HTTP_ROOT ?>products/check-out"title="Read Salt Fire Stoves terms and conditions"onclick="window.open('<?= HTTP_ROOT ?>products/terms', 'termsconditions', 'width=830, height=500, scrollbars=yes')">terms and conditions</a>
                <input type="checkbox" name="cTC" value="Y" class="check-radio-box"title="Agree to our terms and conditions"/>
            </p>-->

            <div class="clearblock"></div>
<!--                <input name="continue" type="button" class="large-btn float-left"value="&lt;&lt;&nbsp;Continue Shopping" onclick="location.href = 'products/basket';"/>-->
            <button type="submit"name="proceed" class="medium-btn float-right" id="pic-update-btn">Submit</button>

            <?= $this->Form->end() ?>

        </div>
        <div class="clearblock">&nbsp;</div>
    </div>
</section>
<!-- InstanceEndEditable -->
<div class="clearblock"></div>

<script src="<?= HTTP_ROOT ?>validation/dist/jquery.validate.min.js" language="javascript"></script>

<script type="text/javascript">
    $("#frmCheckout").validate({
        rules: {
            invoice_title: 'required',
            invoice_firstname: 'required',
            invoice_lastname: 'required',
            invoice_email: {
                required: true,
                email: true
            },
            sEmail2: {
                required: true,
                email: true
//                                equalTo: invoice_email
            },
            invoice_tel1: {
                required: true,
                number: true
            },
            invoice_address1: 'required',
            invoice_address2: 'required',
            invoice_city: 'required',
            invoice_county: 'required',
            invoice_postcode: 'required',
            invoice_countryid: 'required',
//                            cTC:'required',
        },
        messages: {
            invoice_title: 'Title required.',
            invoice_firstname: 'First Name required.',
            invoice_lastname: 'Last Name required.',
            invoice_email: {
                required: 'Email required.',
                email: 'Invalid email!!'
            },
            sEmail2: {
                required: 'Email required.',
                email: 'Invalid email!!'
            },
            invoice_tel1: {
                required: 'Phone number required.',
                number: 'Invalid phone number!!'
            },
            invoice_address1: 'Address required',
            invoice_address2: 'Address required',
            invoice_city: 'City required',
            invoice_county: 'Country required',
            invoice_postcode: 'PostCode required',
            invoice_countryid: 'Country required',
//                            cTC: 'Choose  CTC'
        },
    });
</script>