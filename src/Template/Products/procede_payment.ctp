<?= $this->Html->script('jquery-1.10.2.min'); ?>
<?php
$this->Form->setTemplates([
    'inputContainer' => '{{content}}',
    'submitContainer' => '{{content}}'
]);
?>

<form method="post" id="paypalform" action="<?= $this->Custom->readConfig('Paypal.PAYPAL_PAYMENT_URL'); ?>" style="display: none;">
    <input type="hidden" name="cmd" value="_xclick" />
    <input type="hidden" name="currency_code" value="<?= CURRENCY_NAME ?>" />
    <input type="hidden" name="business" value="<?= $this->Custom->readConfig('Paypal.PAYPAL_MERCHANT_EMAIL'); ?>">

    <input type="hidden" name="item_number" value="<?= $totalqty ; ?>">
    <input type="hidden" name="amount" value=" <?= number_format(@$total + @$vats, 2, '.', ''); ?>">                     

    <input type="hidden" name="return" value="<?= $paypayUrl['success']; ?>" /> 
    <input type="hidden" name="cancel_return" value="<?= $paypayUrl['cancel']; ?>" />
    <input type="hidden" name="notify_url" value="<?= $paypayUrl['notify']; ?>" />
</form>

<script>
$(document).ready(function(){
    $('#paypalform').submit();
});
</script>

