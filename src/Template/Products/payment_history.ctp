<div class="vendor_dashboard">
    <div class="wrapper">
        <div class="dash_right">
            <div class="vendor_detail">
                <h3>Payment History</h3>
            </div>
            <div class="profile_sec_right" style="padding: 0px; display: block;">
                <div class="review_sec"> 
                    <table class="form_table" style="max-width: 100%;">
                        <thead>
                            <tr>
                                <th style="text-align:center;">Date</th>
                                <th style="text-align:center;">Transaction Id</th>
                                <th style="text-align:center;">Membership</th>
                                <th style="text-align:center;">Invoice Price </th> 
                                <!--<th style="text-align:center;">Status </th>-->                               
                                <th style="text-align:center;">Action </th>                               

                            </tr>
                        </thead>
                        <tbody>  
                            <?php if ($transactions->isEmpty()) { ?>
                            <td colspan="7" style="color:#F00;text-align: center;">No Payment History Found </td>
                            <?php
                        } else {
                            foreach ($transactions as $transaction) {
                                ?>
                                <tr>
                                    <td style="text-align:center;"><?= $this->Custom->dateDisplay($transaction->created) ?></td>
                                    <td style="text-align:center;"><?= $transaction->txn_id ?></td>
                                    <td style="text-align:center;"><a href="javascript:;"> <?= $this->Custom->getMembershipIcon($transaction->subscription_plan_id, $transaction->subscription_plan->name); ?></a></td>
                                    <td style="text-align:center;"><?= $this->Custom->priceFormat($transaction->invoice_price); ?></td>     
        <!--                                <td style="text-align:center;"><= $this->Custom->getTransactionStaus($transaction->status); ?></td>-->
                                    <td style="text-align:center;">
                                        <a target="_blank" data-hint="View Invoice" class="btn-xs btn-info hint--top"  href="<?= HTTP_ROOT . 'invoice-preview/' . $transaction->unique_id ?>"><i class="fa fa-eye" ></i></a> 
                                        <a target="_top" data-hint="Download Invoice" class="btn-xs  btn-success hint--left"  href="<?= HTTP_ROOT . 'invoice-preview/' . $transaction->unique_id ?>?download=true" ><i class="fa fa-download"></i></a> 
                                    </td>
                                </tr>
                                <?php
                            }
                        }
                        ?>
                        </tbody>
                    </table>
                </div>
            </div>
            <br/>
            <br/>
        </div>
    </div>        
</div>





