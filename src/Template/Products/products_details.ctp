<style>
/*    .product {width: 226px!important;height: 336px;overflow: hidden;border: 1px solid #e0e0e0;border-radius: 2px;-moz-border-radius: 2px;-khtml-border-radius: 2px;behavior: url(PIE.htc);}
    .submit{border: 0; background: 0;background-color: #f5f7f9;}*/
    .removeFavorite { color: #d01818; }
   #addcart{float: right;display: block;width: 120px;height: 35px;color: #fefefe;text-align: center;text-decoration: none;font: bold 13px/35px Segoeui-Bold, Arial, Verdana, serif;background: #59b7c2;border-radius: 2px;-moz-border-radius: 2px;-khtml-border-radius: 2px;behavior: url(PIE.htc);position: relative;}
   .input text{display: inline-block;}
</style>
<section>
    <div class="container_12">
        <div id="content" class="grid_9">
            <h3 style="border-bottom: 1px solid #e0e0e0;"><?= $products->name ?></h3>
            <div class="product_page">
                <div class="grid_4 img_slid" id="products">
                    <!--<img class="sale" src="images/sale.png" alt="Sale"/>-->
                    <div class="preview slides_container">
                        <div class="prev_bg">
                            <a class="jqzoom" rel="gal1" href="<?= HTTP_ROOT . PRODUCT_PIC . @$products->images[0]->filename ?>">
                                <img src="<?= HTTP_ROOT . PRODUCT_PIC . @$products->images[0]->filename ?>" width="279" height="280"/>
                            </a>
                        </div>
                    </div><!-- .prev -->
                    
                    <ul class="pagination clearfix" id="thumblist">  
                        <li><a class="zoomThumbActive" href="javascript:;"  rel="{gallery: 'gal1', smallimage: '<?= HTTP_ROOT . PRODUCT_PIC . $products->images[0]->filename ?>',largeimage: '<?= HTTP_ROOT . PRODUCT_PIC . $products->images[0]->filename ?>'}" target="_self" title="<?= $products->name; ?>"><img src="<?= HTTP_ROOT . PRODUCT_PIC . $products->images[0]->filename ?>" alt=""></a></li>
                            <?php foreach ($products->images as $image) { ?>
                            <li><a href="javascript:;" rel="{gallery: 'gal1', smallimage: '<?= HTTP_ROOT . PRODUCT_PIC . $image->filename ?>',largeimage: '<?= HTTP_ROOT . PRODUCT_PIC . $image->filename ?>'}" target="_self" title="<?= $products->name; ?>"><img src="<?= HTTP_ROOT . PRODUCT_PIC . $image->filename ?>" alt=""></a></li>
                        <?php } ?> 
                    </ul>
                    <div class="next_prev">
                        <a id="img_prev" class="arows" href="#"><span>Prev</span></a>
                        <a id="img_next" class="arows" href="#"><span>Next</span></a>
                    </div><!-- . -->
                </div><!-- .grid_4 -->
                <div class="grid_5">                   
                    <div class="entry_content">
<!--                        <div class="review">
                            <a class="plus" href="#"></a>
                            <a class="plus" href="#"></a>
                            <a class="plus" href="#"></a>
                            <a href="#"></a>
                            <a href="#"></a>
                            <span>1 REVIEW(S)</span>
                            <a class="add_review" href="#">ADD YOUR REVIEW</a>
                        </div>-->
                        <?= $this->Form->create(null, ['url' => '/products/basket', 'class' => 'no-margin-padding']) ?>
                        <input name="product_id" type="hidden" value="<?= $products->product_id; ?>"/>
                        <input name="variation_id" type="hidden" value="<?= $products->product_variations[0]->variation_id; ?>"/>
                        <input name="code" type="hidden" value="<?= $products->product_variations[0]->code; ?>"/>
                        <input name="name" type="hidden" value="<?= $products->name; ?>"/>
                        <input name="weight" type="hidden" value="<?= $products->product_variations[0]->weight; ?>"/>
                        <input name="zero_vat" type="hidden" value="<?= $products->product_variations[0]->zero_vat; ?>"/>
                        <input name="brand" type="hidden" value="<?= $products->brand_id; ?>"/>
                        <input name="variations" type="hidden" value="<?= $products->product_variations[0]->var_data_1; ?>"/>
                        
                        <?php if (!empty($products->product_variations[0]->var_name_1) || !empty($products->product_variations[0]->var_name_2)) { ?>
                            <!--<input name="unit_price"class="product_price" id="price" type="hidden"/>-->
                            <input name="unit_price"class="product_price" id="price" type="hidden" value="<?= $products->product_variations[0]->retail_price; ?>/>
                        <?php } else { ?>
                            <input name="unit_price" id="price" type="hidden" value="<?= $products->product_variations[0]->retail_price; ?>"/>
                        <?php } ?>
                        
                        <input name="vat_rate" type="hidden" value= "<?= DEFAULT_VAT_RATE; ?>"/>
                        <?php //  foreach ($products->orders_products as $order) ?>
                        <input name="qty" type="hidden" class="numeric-input" value="1"/>
<!--                        <p class="red bold">Description</p>-->
                        <p> <?= substr(strip_tags($products->desc_full), 0, 250) ?>...</p>
                        <?php
                        if (!empty($products) && $products->product_variations[0]->out_of_stock == 'N') {
                            $fPound = number_format($products->product_variations[0]->retail_price, 2);
                            ?>
                            <div class="ava_price">
                                <div class="availability_sku">
                                    <div class="availability">
                                        Availability: <span>In stock</span>
                                    </div>
                                </div><!-- .availability_sku -->
                                <div class="price">
                                        <input id="price" name="productprice" type="hidden" value="<?php echo number_format($products->product_variations[0]->retail_price, 2, '.', ''); ?>"/> 
                             <?php if (!empty($products->product_variations[0]->var_name_1) || !empty($products->product_variations[0]->var_name_2)) { ?>
                                     <div class="price_new"><?= $this->Form->control('price', ['class' => 'product_price', 'label' => false,'style'=>'display:block;text-align: center;font: normal 18px/20px Bitter, Myriad Pro, Verdana, serif;color: #2e8f9a;','value' => $fPound,'readonly'=>true]); ?></div>
                             <?php } else { ?>
                                     <div class="price_new" id="product_price" style="dispaly:block;">&pound;<?= $fPound; ?></div>
                             <?php } ?>

                                    </div><!-- .price -->
                            </div><!-- .ava_price -->
                        <?php } else if (@$products->product_variation->out_of_stock == 'Y') { ?>
                            <div class="ava_price">
                                <div class="availability_sku">
                                    <div class="availability">
                                        Availability: 
                                        <span><?php // echo red($sFld1 . ' ' . $sFld2 . ' ' . $sFld3);        ?> currently out of stock.</span>
                                    </div>
                                </div><!-- .availability_sku -->
                                <div class="price">
                                    <input name="productprice" type="hidden" value="0"/>
                                </div><!-- .price -->
                            </div><!-- .ava_price -->
                        <?php } else if ($products) { ?>
                            <div class="ava_price">
                                <div class="availability_sku">
                                    <div class="availability">
                                        Availability: <span style="color:red;"><?php // echo red($sFld1 . ' ' . $sFld2 . ' ' . $sFld3);       ?> not available</span>
                                    </div>
                                </div><!-- .availability_sku -->
                                <div class="price">
                                    <input name="productprice" type="hidden" value="0"/>
                                </div><!-- .price -->
                            </div><!-- .ava_price --> 
                        <?php } ?>
                        <div class="block_cart">
                            <div class="cart">
                                <?php if (!$this->request->getSession()->read('Auth.User.id')) { ?>                                      
                                    <a href="<?= HTTP_ROOT . "login" ?>" class="bay">
                                        <button  class="submit" name="submit" value="Add to Cart" type="submit">Add to Cart</button>
                                    </a>
                                <?php } else { ?>
                                        <button  class="submit" name="submit" value="Add to Cart" type="submit">Add to Cart</button>
                                <?php } ?>
                            </div>
                            <div class="clear"></div>
                        </div><!-- .block_cart -->
                        <?php if(!empty($products->product_variations[0]->var_name_1) || !empty($products->product_variations[0]->var_name_2) ){ ?>
                        <h3 style="font-size:18px;">Variations</h3>
                        <div style="display: inline-block;text-align: center;"><?= $products->product_variations[0]->var_name_1; ?></div>
                        <div style="display: inline-block;text-align: center;margin-left: 72px;"><?= $products->product_variations[0]->var_name_2; ?></div>
                       <div>
                           <?php if(!empty($products->product_variations[0]->var_name_1)){ ?>
                               <?= $this->Form->select('var_data_1', $varationList, ['empty' => 'Select Leg Height', 'id' => 'cat', 'label' => false, 'class' => 'common_selector var_data_1','onChange' => 'varation1(this.value)']); ?>
                           <?php } ?>
                            <?php if(!empty($products->product_variations[0]->var_name_2)){ ?>
                               <?= $this->Form->select('var_data_2', $varationsList, ['empty' => 'Select Door Type', 'id' => 'cat', 'label' => false, 'class' => 'common_selector var_data_2 ','onChange' => 'varation2(this.value)']); ?>
                            <?php } ?>
                       </div>
                            <!--<label><input type="checkbox" class="common_selector varation" value="<?= $varationsList; ?>"><?= $varationsList; ?></label><br/>-->
                        <?php } ?>
                        <?= $this->Form->end() ?>  
                    </div><!-- .entry_content -->
                </div><!-- .grid_5 -->
                <div class="clear"></div>
                <div class="grid_9" >
                    
                    <div id="wrapper_tab" class="tab1">
                        <a href="#" class="tab1 tab_link">Description</a>
                        <a href="#" class="tab2 tab_link">Reviews</a>
                        <div class="clear"></div>
                        <div class="tab1 tab_body">
                            <!--<h4>Tables Style</h4>-->
                            <p> <?= $products->desc_full; ?></p>
                            <div class="clear"></div>
                        </div><!-- .tab1 .tab_body -->

                        <div class="tab2 tab_body">
                            <h4>Customer reviews</h4>
                            <ul class="comments">
                                <?php  foreach($reviews as $review){ ?>
                                <li>
                                   <div class="autor"><?= $review->name;?></div>, <time datetime="<?= $review->created;?>"><?= $review->created;?></time>
                                    <div class="evaluation">
                                        <div class="quality">
                                            <strong>Quality</strong>
                                            <a class="plus"><?php $this->Custom->getRating($review->quality_rating) ?></a><br/>
                                        </div>
                                        <div class="price">
                                            <strong>Price</strong>
                                             <a><?php $this->Custom->getRating($review->price_rating) ?></a><br/>
                                        </div>
                                        <div class="clear"></div>
                                    </div><!-- .evaluation -->
                                    <p><?= $review->review ;?></p>
                                </li>
                                <?php } ?>
                            </ul><!-- .comments -->
                        <!--//<= $this->Form->create(NULL, ['url' => '/products/addreview','id' => 'addreview','class'=>'add_comments']); ?>-->
                        <?= $this->Form->create(NULL, ['id' => 'addreview','class'=>'add_comments']); ?>
                           
                                <h4>Write Your Own Review</h4>
                                <div class="evaluation">
                                    <div class="quality">
                                        <strong>Quality</strong><sup class="surely">*</sup>
                                        <input class="niceRadio" type="radio" name="quality_rating" value="1" /><span class="eva_num">1</span>
                                        <input class="niceRadio" type="radio" name="quality_rating" value="2" /><span class="eva_num">2</span>
                                        <input class="niceRadio" type="radio" name="quality_rating" value="3" /><span class="eva_num">3</span>
                                        <input class="niceRadio" type="radio" name="quality_rating" value="4" /><span class="eva_num">4</span>
                                        <input class="niceRadio" type="radio" name="quality_rating" value="5" /><span class="eva_num">5</span>
                                    </div>
                                    <div class="price">
                                        <strong>Price</strong><sup class="surely">*</sup>
                                        <input class="niceRadio" type="radio" name="price_rating" value="1" /><span class="eva_num">1</span>
                                        <input class="niceRadio" type="radio" name="price_rating" value="2" /><span class="eva_num">2</span>
                                        <input class="niceRadio" type="radio" name="price_rating" value="3" /><span class="eva_num">3</span>
                                        <input class="niceRadio" type="radio" name="price_rating" value="4" /><span class="eva_num">4</span>
                                        <input class="niceRadio" type="radio" name="price_rating" value="5" /><span class="eva_num">5</span>
                                    </div>
                                    <div class="clear"></div>
                                </div><!-- .evaluation -->
                                <div class="nickname">
                                    <strong>Nickname</strong><sup class="surely">*</sup><br/>
                                    <input type="text" name="name" class="" value="" />
                                </div><!-- .nickname -->
                                <div class="your_review">
                                    <strong>Email</strong><sup class="surely">*</sup><br/>
                                    <input type="email" name="email" class="" value="" />
                                </div><!-- .your_review -->
                                <input type="hidden" name="product_id" class="" value="<?= $products->product_id; ?>" />

                                <div class="clear"></div>
                                <div class="text_review">
                                    <strong>Review</strong><sup class="surely">*</sup><br/>
                                    <textarea name="review"></textarea>
                                    <!--<i>Note: HTML is not translated!</i>-->
                                </div><!-- .text_review -->
                                <!--<input type="submit" value="Submit Review" />-->
                                <?= $this->Form->button('Submit', ['type' => 'submit', 'class' => 'submit']); ?>
                           <?= $this->Form->end(); ?><!-- .add_comments -->
                            <div class="clear"></div>
                        </div><!-- .tab2 .tab_body -->
                        <div class="clear"></div>
                    </div><!-- #wrapper_tab -->
                    <div class="clear"></div>
                </div><!-- .grid_9 -->
                <div class="clear"></div>

                <div class="related">
                    <div class="c_header">
                        <div class="grid_7">
                             <?php if (!empty($releatedproducts)) { ?>
                            <h2>Related Products</h2>
                             <?php } ?>
                        </div><!-- .grid_7 -->

                        <div class="grid_2">
                            <a id="next_c1" class="next arows" href="#"><span>Next</span></a>
                            <a id="prev_c1" class="prev arows" href="#"><span>Prev</span></a>
                        </div><!-- .grid_2 -->
                    </div><!-- .c_header -->

            <div class="list_carousel">
                <ul id="list_product" class="list_product">                                                      
                    <?php if (!empty($releatedproducts)) { ?>
                        <?php foreach ($releatedproducts as $releatedproduct) { ?>
                        <a href="<?= $this->Url->build(['controller' => 'Products', 'action' => 'productsDetails/' . $releatedproduct->product_id]); ?>" target="_self" title="<?= $releatedproduct->name; ?>"></a>
                        <li class="">
                            <div class="grid_3 product">
                                    <div class="prev">
                                         <a href="<?= $this->Url->build(['controller' => 'Products', 'action' => 'productsDetails/' . $releatedproduct->product_id]); ?>"  target="_self" title="<?= $releatedproduct->name; ?>"><img src="<?= HTTP_ROOT . PRODUCT_PIC . $releatedproduct['images'][0]['filename']; ?>" /></a>                         
                                    </div><!-- .prev -->
                                    <h3 class="title"><?= $releatedproduct->name; ?></h3>
                                  <?php foreach ($releatedproduct->product_variations as $productvarations)  ?>
                                  <div class="cart">
                                        <div class="price">
                                            <div class="vert">
                                                <div class="price_new">£<?= $releatedproduct->product_variations[0]->retail_price; ?></div>
                                            </div>
                                        </div>
                                              <?php if (!$this->request->getSession()->read('Auth.User.id')) { ?>                                      
                                                  <a style="width: 63px;" href="<?= HTTP_ROOT . "login" ?>"  data-productid="<?= $releatedproduct['product_id']; ?>"target="_self" title="Add to favorite"><i class="icon fa fa-heart like"style="margin-top:10px;"></i></a>
                                              <?php } else { ?>
                                                  <a style="width: 63px;cursor: pointer;" class="<?= in_array($releatedproduct->product_id, $favProductList) ? 'removeFavorite' : 'addToFavorite' ?>" data-productid="<?= $releatedproduct['product_id']; ?>" target="_self" title="Add to favorite"><i class="icon fa fa-heart like" style="margin-top:10px;"></i></a> 
                                              <?php } ?>  

                    <?= $this->Form->create(null, ['url' => '/products/basket', 'class' => 'no-margin-padding']) ?>
                    <input name="product_id" type="hidden" value="<?= $releatedproduct->product_id; ?>"/>
                    <input name="variation_id" type="hidden" value="<?= $releatedproduct->product_variations[0]->variation_id; ?>"/>
                    <input name="code" type="hidden" value="<?= $releatedproduct->product_variations[0]->code; ?>"/>
                    <input name="name" type="hidden" value="<?= $releatedproduct->name; ?>"/>
                    <input name="weight" type="hidden" value="<?= $releatedproduct->product_variations[0]->weight; ?>"/>
                    <input name="zero_vat" type="hidden" value="<?= $releatedproduct->product_variations[0]->zero_vat; ?>"/>
                    <input name="brand" type="hidden" value="<?= $releatedproduct->brand_id; ?>"/>
                    <input name="variations" type="hidden" value="<?= $releatedproduct->product_variations[0]->var_data_1; ?>"/>
                    <input name="unit_price" type="hidden" value="<?= $releatedproduct->product_variations[0]->retail_price; ?>"/>
                    <input name="vat_rate" type="hidden" value= "<?= DEFAULT_VAT_RATE; ?>"/>
                    <input name="qty" type="hidden" class="numeric-input" value="1"/>
                    <?php if (!$this->request->getSession()->read('Auth.User.id')) { ?>                                      
                        <a  class="bay"style="width:63px;"href="<?= HTTP_ROOT . "login" ?>" target="_self" title="Add to cart"><i class="fas fa-shopping-cart" style="margin-top:10px;"></i></a>
                    <?php } else { ?>
                        <button style="background-color:#fff;" type="submit" class="bay"><i class="fas fa-shopping-cart" style="margin-top:10px;color:#78c4cd;"></i></button>
                    <?php } ?>
                <?= $this->Form->end() ?> 
                            </div><!-- .grid_3 -->
                        </li></a>
                    <?php } }?>

                </ul><!-- #list_product -->
                  </div><!-- .list_carousel -->
                </div><!-- .carousel -->
            </div><!-- .product_page -->
            <div class="clear"></div>

        </div><!-- #content -->

        <div id="sidebar" class="grid_3">
            <aside id="categories_nav">
                <h3>Categories</h3>
                <nav class="left_menu">
                    <ul>
                        <?php foreach ($categories as $categorie) { ?>
                            <?php foreach ($categorie->categories_products as $productvalue) ?>
                            <li><a href="<?= $this->Url->build(['controller' => 'Products', 'action' => ' products-details', @$productvalue->product_id]); ?>"><?= $categorie->name; ?> <span>(<?= $categorie->count ?>)</span></a></li>
                        <?php } ?> 
                    </ul>
                </nav><!-- .left_menu -->
            </aside><!-- #categories_nav -->

            <!--            <aside id="specials" class="specials">
                            <h3>Specials</h3>
            
                            <ul>
                                <li>
                                    <div class="prev">
                                        <a href="#"><img src="images/special1.png" alt="" title="" /></a>
                                    </div>
            
                                    <div class="cont">
                                        <a href="#">Honeysuckle Flameless Luminary Refill</a>
                                        <div class="prise"><span class="old">$177.00</span>$75.00</div>
                                    </div>
                                </li>
            
                                <li>
                                    <div class="prev">
                                        <a href="#"><img src="images/special2.png" alt="" title="" /></a>
                                    </div>
            
                                    <div class="cont">
                                        <a href="#">Honeysuckle Flameless Luminary Refill</a>
                                        <div class="prise"><span class="old">$177.00</span>$75.00</div>
                                    </div>
                                </li>
                            </ul>
                        </aside> #specials 
            
                        <aside id="newsletter_signup">
                            <h3>Newsletter Signup</h3>
                            <p>Phasellus vel ultricies felis. Duis
                                rhoncus risus eu urna pretium.</p>
            
                            <form class="newsletter">
                                <input type="email" name="newsletter" class="your_email" value="" placeholder="Enter your email address..."/>
                                <input type="submit" id="submit" value="Subscribe" />
                            </form>
                        </aside> #newsletter_signup 
            
                        <aside id="banners">
                            <a id="ban_next" class="next arows" href="#"><span>Next</span></a>
                            <a id="ban_prev" class="prev arows" href="#"><span>Prev</span></a>
            
                            <h3>Banners</h3>
            
                            <div class="list_carousel">
                                <ul id="list_banners">
                                    <li class="banner"><a href="#">
                                            <div class="prev">
                                                <img src="images/banner.png" alt="" title="" />
                                            </div> .prev 
            
                                            <h2>New smells</h2>
            
                                            <p>in the next series</p>
                                        </a></li>
            
                                    <li class="banner"><a href="#">
                                            <div class="prev">
                                                <img src="images/banner.png" alt="" title="" />
                                            </div> .prev 
            
                                            <h2>New smells</h2>
            
                                            <p>in the next series</p>
                                        </a></li>
            
                                    <li class="banner"><a href="#">
                                            <div class="prev">
                                                <img src="images/banner.png" alt="" title="" />
                                            </div> .prev 
            
                                            <h2>New smells</h2>
            
                                            <p>in the next series</p>
                                        </a></li>
            
                                </ul>
                            </div> .list_carousel 
                        </aside> #banners 
            
                        <aside id="tags">
                            <h3>Tags</h3>
                            <a class="t1" href="">california</a>
                            <a class="t2" href="">canada</a>
                            <a class="t3" href="">canon</a>
                            <a class="t4" href="">cat</a>
                            <a class="t5" href="">chicago</a>
                            <a class="t6" href="">christmas</a>
                            <a class="t7" href="">mars</a>
                            <a class="t8" href="">church</a>
                            <a class="t9" href="">city</a>
                            <a class="t10" href="">clouds</a>
                            <a class="t11" href="">color</a>
                            <a class="t12" href="">concert</a>
                            <a class="t13" href="">dance</a>
                            <a class="t14" href="">day</a>
                            <a class="t15" href="">dog</a>
                            <a class="t16" href="">england</a>
                            <a class="t17" href="">europe</a>
                        </aside> #community_poll -->
        </div><!-- .sidebar -->

        <div class="clear"></div>

    </div><!-- .container_12 -->
</section><!-- #main -->
<script>

     $(document).on('click', '.addToFavorite', function(e) {  
            var element = $(this);
            var productid = element.data('productid');  
            
            $.ajax({
                type: 'POST',
                url: siteUrl + 'products/addFavourite',
                data: {'product_id': productid},
                dataType: 'json',
                success: function (response) {
                    if(response.status == 'success') {
                        element.removeClass("addToFavorite").addClass("removeFavorite"); 
                    } else {
                        alert(response.msg);                    }                   
                }                
            });                         
        });

        $(document).on('click', '.removeFavorite', function(e) {  
            var element = $(this);
           var productid = element.data('productid');          
            $.ajax({
                type: 'POST',
                 url: siteUrl + 'products/deleteFavourite',
                data: {'product_id': productid},
                dataType: 'json',
                success: function (response) {
                    if(response.status == 'success') {
                        element.removeClass("removeFavorite").addClass("addToFavorite"); 
                    } 
                    else {
                       alert(response.msg);
                    }   
                }         
            });
        }); 
</script>
<script>
      var productID = '<?= $products->product_id ?>';
    $(document).ready(function () {
        $(document).on('submit', '#addreview', function (e) {
            e.preventDefault();
            addProduct();
        });
    });       

//         $("#addreview").validate({
//            rules: {
//                name: 'required',
//                email: {
//                    required: true,
//                    email: true
//                },
//                price: 'required',
//                quality: 'required',
//                reviews: {
//                    required: true
//                },
//            },
//            messages: {
//                name: 'Name required!!',
//                email: {
//                    required: 'Email required!!',
//                    email: 'Invalid email!!'
//                },
//                price: 'Price required!!',
//                quality: 'Quality required!!',
//                reviewS: 'Review required!!'
//            },
//            submitHandler: function (form) {
//                var data = $(form).serialize();
//                var submitBtn = $(form).find('button[type="submit"],input[type="submit"]').first();
////                submitBtn.addClass('loader-submit');
//                var url = siteUrl + 'products/product_Review/' + productID;
//                $.ajax({
//                    type: "POST",
//                    url: url,
//                    data: data,
//                    headers: {'Content-Type': 'application/x-www-form-urlencoded'},
//                    dataType: "json",
//                    success: function (response) {
//                        if (response.status == 'success') {
//                           
//                            $.notify({message: response.message}, {type: 'success'});
//                        } else {
//                            $.notify({message: response.message}, {type: 'danger'});
//                        }
//
//                    },
//                    error: function () {    
//                        $.notify({message: 'Some error ocurred while submiting you reviews'}, {type: 'danger'});
//                    },
//                    complete: function () {
//                        submitBtn.removeClass('loader-submit');
//                    }
//                });
//                return false;
//            }
//        });
        function addProduct() {
        var form = $('#addreview').get(0);
        $.ajax({
            type: 'POST',
            url: siteUrl + 'products/addreview',
            data: new FormData(form),
            dataType: 'json',
            cache: false,
            contentType: false,
            processData: false,
            success: function (response) {
                if (response.status == 'success') {
                    alert(response.msg);
                        window.location.reload();               
                    } else {
                    alert(response.msg);
                }
            }
        });
    }
    function varation1(varationId) {
        $.ajax({
            type: 'post',
            url: "<?= $this->Url->build(['controller' => 'Products', 'action' => 'varation']); ?>",
            data: {"var_data_1": varationId},
             success: function (response) {
                var data = JSON.parse(response);
                console.log(data);
                 $.each(data, function (index, value) {
//                        varations.push(response.img[index].varation);
//                        var path = '';
//                        path = data[index].retail_price;
//                        alert(path);
                        $('.product_price').val(data[index].retail_price);
                    });
            },
            error: function (e) {
                alert("An error occurred: ");
                console.log(e);
            }
        });
    }
    function varation2(varationId) {
        $.ajax({
            type: 'post',
            url: "<?= $this->Url->build(['controller' => 'Products', 'action' => 'varation']); ?>",
            data: {"var_data_1": varationId},
             success: function (response) {
                var data = JSON.parse(response);
                console.log(data);
                 $.each(data, function (index, value) {
//                        varations.push(response.img[index].varation);
//                        var path = '';
//                        path = data[index].retail_price;
//                        alert(path);
                        $('.product_price').val(data[index].retail_price);
                    });
                
            },
            error: function (e) {
                alert("An error occurred: ");
                console.log(e);
            }
        });
    }
    </script>
