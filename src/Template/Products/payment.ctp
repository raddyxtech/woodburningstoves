 <link rel="stylesheet" href="/woodburningstoves/css/style.css"/> 

<section class="home">
    <div class="container_12">
        <h3>Confirm Order</h3>
        <div id="inner">
            <p>Please check all details below, then click on the 'Proceed to Payment' button.</p>
<!--            <div class="split5050 float-left">
                <fieldset>
                    <legend>Invoice Details</legend><br/><br/>
                    <?= $order->invoice_title; ?> <?= $order->invoice_firstname; ?>  <?= $order->invoice_lastname; ?> <br>
                    <?= $order->invoice_company; ?><br>
                    <?= $order->invoice_address1; ?> , <?= @$order->invoice_address2; ?><br>
                    <?= $order->invoice_city; ?><br>
                    <?= $order->invoice_county; ?><br>
                    <?= $order->invoice_postcode; ?><br>
                    <?= $order->invoice_tel1; ?><br>
                    <?= @$order->invoice_tel2; ?><br>
                    <?= @$order->invoice_tel3; ?><br>
                    <?= $order->invoice_email; ?><br>

                </fieldset>
            </div>-->
            <div class="split5050">
                <fieldset>
                    <legend>Delivery Details</legend><br/><br/>
                    <?= $order->delivery_title; ?> <?= $order->delivery_firstname; ?>  <?= $order->delivery_lastname; ?> <br>
                    <?= $order->delivery_company; ?><br>
                    <?= $order->delivery_address1; ?> , <?= @$order->delivery_address2; ?><br>
                    <?= $order->delivery_city; ?><br>
                    <?= $order->delivery_county; ?><br>
                    <?= $order->delivery_postcode; ?><br>
                    <?= $order->delivery_tel1; ?><br>
                    <?= @$order->delivery_tel2; ?><br>
                    <?= @$order->delivery_tel3; ?><br>
                    <?= $order->delivery_email; ?><br>
                </fieldset>
            </div>
            <div class="clearblock"></div>
            <table summary="Your order basket">
                <thead>
                    <tr>
                        <th id="basket-product">Name</th>
                        <th id="basket-quantity">Quantity</th>
                        <th id="basket-total">Total</th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td class="text-right">Sub Total</td>                              
                        <td headers="basket-total"class="text-right">&pound;<?= number_format($total, 2, '.', ''); ?></td>

                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td class="text-right">VAT</td>
                        <td headers="basket-total" class="text-right">&pound; <?= number_format(@$vats, 2, '.', ''); ?></td></tr>

                    <tr>
                        <td>&nbsp;</td>
                        <td class="text-right">Total</td>
                        <td headers="basket-total"class="text-right">&pound; <?= number_format(@$total + @$vats, 2, '.', ''); ?></td>

                    </tr>
                </tfoot>
                <tbody>
                    </tfoot>
                    <tbody>
                        <?php foreach ($order->orders_products as $orderProduct) { ?>
                            <tr>
                                <td headers="basket-product"><?= $orderProduct->name; ?></td>
                                <td headers="basket-quantity" class="text-right"><?= $orderProduct->qty; ?></td>
                                <td headers="basket-total"class="text-right">&pound; <?= number_format($orderProduct->total, 2, '.', ''); ?></td>
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>
              <?= $this->Form->create(NULL, ['url' => ['controller' => 'Products', 'action' => 'procedePayment']]) ?>
                                <input type="hidden"   name=order_id" value="<?= $order->order_id ?>"  />                                   
                                <!--<button type="button" class="cancel" style="margin-bottom: 5px;" data-dismiss="modal" >Cancel</button>-->

                                <button style="float: right;width: 170px;font: bold 14px/35px Segoeui-Bold, Arial, Verdana, serif;" type="submit" class="submit" class="active" > Proceed</button> 
                                

                                <?= $this->Form->end(); ?>
            
            
<!--
                <form method="post" id="paypalform" action="<?= $this->Custom->readConfig('Paypal.PAYPAL_PAYMENT_URL'); ?>">
                    <input type="hidden" name="cmd" value="_xclick" />
                    <input type="hidden" name="currency_code" value="<?= CURRENCY_NAME ?>" />
                    <input type="hidden" name="business" value="<?= $this->Custom->readConfig('Paypal.PAYPAL_MERCHANT_EMAIL'); ?>">
                    
                    <input type="hidden" name="item_number" value="<?= $totalqty ; ?>">
                    <input type="hidden" name="amount" value=" <?= number_format(@$total + @$vats, 2, '.', ''); ?>">
                    <input type='hidden' name='cancel_return' value='<?= HTTP_ROOT . 'products/cancel' ?>'>
                    <input type='hidden' name='return' value='<?= HTTP_ROOT . 'products/sucess' ?>'>
                    <input type='hidden' name='notify_url' value='<?= HTTP_ROOT . 'products/notify' ?>'>
                    <input type="submit" style="float: right;width: 170px;font: bold 14px/35px Segoeui-Bold, Arial, Verdana, serif;" title="Pay online"value="Proceed to Payment&nbsp;&gt;&gt;" />
                </form>-->
                <!--
                                <input name="next" type="button" id="protx" class="large-btn float-right" title="Pay online"value="Proceed to Payment&nbsp;&gt;&gt;" onclick="location.href = '<?= HTTP_ROOT . 'products/pay' ?>';"/>-->



                <hr/>
                <br/>
            </div>

            <div class="clearblock">&nbsp;</div>

        </div>

        <div class="clearblock">&nbsp;</div>
    </div>
</section>
<!-- InstanceEndEditable -->
<div class="clearblock"></div>










