<?php

namespace App\View\Cell;

use Cake\View\Cell;

class SearchFiltersCell extends Cell {

    public function displayContent() {
        $this->loadModel('PropertyTypes');
        $this->loadModel('Categories');
        $priceArr = range(50000, 5000000, 25000);
        $priceArr = array_combine($priceArr, $priceArr);
        $priceOptions = array_map(function($option) {
            return CURRENCY_SYMBOL . $option;
        }, $priceArr);

        $this->loadModel('UserPreferences');

        /* // buyer preference autoload part removed from search page
          $buyerId = $this->request->getSession()->read('Auth.User.id');
          $fields = ['max_price','min_price'=>'price_from','min_bed'=>'min_beds','category'=>'CASE WHEN `property_category` = 0 THEN "sell" ELSE "let" END',
          'property_type'=>'property_type_id','loc'=>'preference_postcode','radius'=>'search_radius'];

          $buyerPreference = $this->UserPreferences->find()->select($fields)->where(['user_id'=>$buyerId])->first();
          if(empty($buyerPreference)){
          $buyerPreference = $this->UserPreferences->newEntity();
          }
         */
        $mapIcons = [
            'property' =>
            ['icon' => 'properties-new-location.png', 'hover' => 'properties-location-hov.png'],
        ];

        $propertyTypes = $this->PropertyTypes->find('list');

        $this->set(compact(['properties', 'propertyTypes', 'params', 'mapIcons', 'sortByOptions', 'priceOptions']));
    }


}
