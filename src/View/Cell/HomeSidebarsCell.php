<?php

namespace App\View\Cell;

use Cake\View\Cell;

class HomeSidebarsCell extends Cell {

    public function sideSearch() {
        $this->loadModel('Brands');
        $this->loadModel('Categories');
        $this->loadModel('Products');
        $this->loadModel('ProductVariations');
        $this->loadModel('Images');
        $this->loadModel('CategoriesProducts');
        $brands = $this->Brands->find();
        $categories = $this->Categories->find()->where(['parent_id' => 0]);
        $this->set(compact(['brands', 'categories']));
        if ($this->request->is(['get'])) {
            $brand_id = $this->request->query('iBrandSearchId');
            $category_id = $this->request->query('iCatSearchId');
            if (!empty($brand_id) && !empty($category_id) && isset($brand_id) && isset($category_id)) {
                return $this->redirect(['controller' => 'Products', 'action' => 'productsList']);
            } elseif (!empty($brand_id)) {
                return $this->redirect(['controller' => 'Products', 'action' => 'productsList']);
            } elseif (!empty($category_id)) {
                return $this->redirect(['controller' => 'Products', 'action' => 'productsList']);
            }
        }
    }

    public function mini_cart() {
        
    }

    public function side_testimonials() {
        $this->loadModel('Testimonials');
        $reviews = $this->Testimonials->find()->where(['approved' => 'Y'])->order('rand()')->limit(3);
        $this->set(compact(['reviews']));
    }

}
