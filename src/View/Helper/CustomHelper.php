<?php

namespace App\View\Helper;
use DateTime;
use Cake\View\Helper;
use Cake\Datasource\ConnectionManager;
use Cake\I18n\FrozenTime;
use Cake\Core\Configure;
class CustomHelper extends Helper {

    public function month() {
        $months = [
            '01' => 'January',
            '02' => 'February',
            '03' => 'March',
            '04' => 'April',
            '05' => 'May',
            '06' => 'June',
            '07' => 'July',
            '08' => 'August',
            '09' => 'September',
            '10' => 'October',
            '11' => 'November',
            '12' => 'December',
        ];
        return $months;
    }

    public function monthBak() {
        for ($x = 0; $x < 12; $x++) {
            $time = strtotime('-' . $x . ' months', strtotime(date('Y-M' . '-01')));
            $key = date('m', $time);
            $name = date('F', $time);
            $months[$key] = $name;
        }
        return $months;
    }

    public function year() {
        $years = range(date('Y'), 2017);
        return $years;
    }

    public function monthYear($noOfMonths = 6) {
        $months = [];
        for ($i = 0; $i < $noOfMonths; $i++) {
            $months[] = date("F Y", strtotime(date('Y-m-01') . " -$i months"));
        }
        return $months;
    }

    public function convert_number_to_words($number) {

        $hyphen = '-';
        $conjunction = ' and ';
        $separator = ', ';
        $negative = 'negative ';
        $decimal = ' point ';
        $dictionary = array(
            0 => 'zero',
            1 => 'one',
            2 => 'two',
            3 => 'three',
            4 => 'four',
            5 => 'five',
            6 => 'six',
            7 => 'seven',
            8 => 'eight',
            9 => 'nine',
            10 => 'ten',
            11 => 'eleven',
            12 => 'twelve',
            13 => 'thirteen',
            14 => 'fourteen',
            15 => 'fifteen',
            16 => 'sixteen',
            17 => 'seventeen',
            18 => 'eighteen',
            19 => 'nineteen',
            20 => 'twenty',
            30 => 'thirty',
            40 => 'fourty',
            50 => 'fifty',
            60 => 'sixty',
            70 => 'seventy',
            80 => 'eighty',
            90 => 'ninety',
            100 => 'hundred',
            1000 => 'thousand',
            1000000 => 'million',
            1000000000 => 'billion',
            1000000000000 => 'trillion',
            1000000000000000 => 'quadrillion',
            1000000000000000000 => 'quintillion'
        );

        if (!is_numeric($number)) {
            return false;
        }

        if (($number >= 0 && (int) $number < 0) || (int) $number < 0 - PHP_INT_MAX) {
            // overflow
            trigger_error(
                    'convert_number_to_words only accepts numbers between -' . PHP_INT_MAX . ' and ' . PHP_INT_MAX, E_USER_WARNING
            );
            return false;
        }

        if ($number < 0) {
            return $negative . $this->convert_number_to_words(abs($number));
        }

        $string = $fraction = null;

        if (strpos($number, '.') !== false) {
            list($number, $fraction) = explode('.', $number);
        }

        switch (true) {
            case $number < 21:
                $string = $dictionary[$number];
                break;
            case $number < 100:
                $tens = ((int) ($number / 10)) * 10;
                $units = $number % 10;
                $string = $dictionary[$tens];
                if ($units) {
                    $string .= $hyphen . $dictionary[$units];
                }
                break;
            case $number < 1000:
                $hundreds = $number / 100;
                $remainder = $number % 100;
                $string = $dictionary[$hundreds] . ' ' . $dictionary[100];
                if ($remainder) {
                    $string .= $conjunction . $this->convert_number_to_words($remainder);
                }
                break;
            default:
                $baseUnit = pow(1000, floor(log($number, 1000)));
                $numBaseUnits = (int) ($number / $baseUnit);
                $remainder = $number % $baseUnit;
                $string = $this->convert_number_to_words($numBaseUnits) . ' ' . $dictionary[$baseUnit];
                if ($remainder) {
                    $string .= $remainder < 100 ? $conjunction : $separator;
                    $string .= $this->convert_number_to_words($remainder);
                }
                break;
        }

        if (null !== $fraction && is_numeric($fraction)) {
            $string .= $decimal;
            $words = array();
            foreach (str_split((string) $fraction) as $number) {
                $words[] = $dictionary[$number];
            }
            $string .= implode(' ', $words);
        }

        return ucfirst($string);
    }

    public function formatPrice($price) {
        return 'Rs. ' . number_format($price, 2);
    }

    public function encrypt($id) {
        return urlencode(base64_encode($id));
    }

    public function decrypt($id) {
        return base64_decode(urldecode($id));
    }

    function dateDisplay($datetime = NULL) {
        if (!empty($datetime) && $datetime != "" && $datetime != "0000-00-00 00:00:00") {
            return date("M d, Y", strtotime($datetime));
        } else {
            return false;
        }
    }
    function dateBasic($date = NULL) {
        if (!empty($date) && $date != "0000-00-00") {
            return date("Y-m-d", strtotime($date));
        } else {
            return false;
        }
    }

    function dateDisplayTime($datetime) {
        if (!empty($datetime) && $datetime != "NULL" && $datetime != "0000-00-00 00:00:00") {
            return date("M d, Y H:i:s", strtotime($datetime));
        } else {
            return false;
        }
    }

    public function shortLength($x, $length) {
        if (strlen($x) <= $length) {
            return $x;
        } else {
            $y = substr($x, 0, $length) . '...';
            return $y;
        }
    }

    public function bubbleSort(array $arr) {
        $n = sizeof($arr);
        $totalWidth = 0;
        for ($i = 0; $i < $n; $i++) {
            list($width) = getimagesize(POST_IMAGES . $arr[$i]['image']);
            $totalWidth = $totalWidth + $width;
        }
        for ($i = 1; $i < $n; $i++) {
            for ($j = $n - 1; $j >= $i; $j--) {
                list($width1) = getimagesize(POST_IMAGES . $arr[$j - 1]['image']);
                list($width2) = getimagesize(POST_IMAGES . $arr[$j]['image']);
                if ($width1 < $width2) {
                    $tmp = $arr[$j - 1];
                    $arr[$j - 1] = $arr[$j];
                    $arr[$j] = $tmp;
                }
            }
        }
        return ['arr' => $arr, 'total_width' => $totalWidth, 'count' => $n];
    }

    public static function makeSeoUrl($url) {
        if ($url) {
            $url = trim($url);
            $value = preg_replace("![^a-z0-9]+!i", "-", $url);
            $value = trim($value, "-");
            return strtolower($value);
        }
    }

    public static function getPostType($userType = null) {
        if ($userType == 4) { //For kid
            return [0 => 'Public', 1 => 'Private'];
        } else { //For School
            return [0 => 'Public', 1 => 'Private', 2 => 'Followers'];
        }
    }

    public static function getCommentTime($datetime) {
        if (!empty($datetime) && $datetime != "" && $datetime != "0000-00-00 00:00:00") {
            $time = date("M d Y", strtotime($datetime)) . " at " . date("H:i \H\\r ", strtotime($datetime));
        } else {
            return false;
        }
        
        return $time;
    }

    public static function timeAgo($time_ago) {
        if (!is_numeric($time_ago)) {
            $time_ago = strtotime($time_ago);
        }
        $cur_time = time();
        $time_elapsed = $cur_time - $time_ago;
        $seconds = $time_elapsed;
        $minutes = round($time_elapsed / 60);
        $hours = round($time_elapsed / 3600);
        $days = round($time_elapsed / 86400);
        $weeks = round($time_elapsed / 604800);
        $months = round($time_elapsed / 2600640);
        $years = round($time_elapsed / 31207680);

        if ($seconds <= 60) {  // Seconds
            echo "$seconds seconds ago";
        } else if ($minutes <= 60) { //Minutes
            if ($minutes == 1) {
                echo "one minute ago";
            } else {
                echo "$minutes minutes ago";
            }
        } else if ($hours <= 24) { //Hours
            if ($hours == 1) {
                echo "an hour";
            } else {
                echo "$hours hours ago";
            }
        } else if ($days <= 7) { //Days
            if ($days == 1) {
                echo "yesterday";
            } else {
                echo "$days days ago";
            }
        } else if ($weeks <= 4.3) { //Weeks
            if ($weeks == 1) {
                echo "a week ago";
            } else {
                echo "$weeks weeks ago";
            }
        } else if ($months <= 12) { //Months
            if ($months == 1) {
                echo "a month ";
            } else {
                echo "$months months ago";
            }
        } else { //Years
            if ($years == 1) {
                echo "one year ago";
            } else {
                echo "$years years ago";
            }
        }
    }

    function defaultImage($image) {

        return $image;
    }

    function getStatus($status) {
        $status = "Enabled";
        if ($status == 0) {
            $status = "Disabled";
        }
        return $status;
    }

/////////////////////////////ENCRYPTION AND DECRYPTION CODE/////////////////////////////////////
    public function encryptor($action, $string) {
        $output = false;
        $encrypt_method = "AES-256-CBC";
        //pls set your unique hashing key
        $secret_key = 'chinu';
        $secret_iv = 'uk';

        // hash
        $key = hash('sha256', $secret_key);

        // iv - encrypt method AES-256-CBC expects 16 bytes - else you will get a warning
        $iv = substr(hash('sha256', $secret_iv), 0, 16);

        //do the encyption given text/string/number
        if ($action == 'encrypt') {
            $output = openssl_encrypt($string, $encrypt_method, $key, 0, $iv);
            $output = base64_encode($output);
        } else if ($action == 'decrypt') {
            //decrypt the given text/string/number
            $output = openssl_decrypt(base64_decode($string), $encrypt_method, $key, 0, $iv);
        }

        return $output;
    }

    function dateDisplaysaprate($datetime) {

        if ($datetime != "" && $datetime != "NULL" && $datetime != "0000-00-00 00:00:00") {

            return date("M-d-Y, H:i:s", strtotime($datetime));
        } else {

            return false;
        }
    }

    function expDate($orderCreatedDate, $editId, $invoiceNumber, $orderEditExparationHour) {
        $time = new FrozenTime($orderCreatedDate);
        // date_default_timezone_set('Asia/Kolkata');
        $CurrentTime = strtotime(new FrozenTime(date('Y-m-d H:i:s')));
        //$exptime =$CurrentTime.','.$time. ','.$time->modify('+1 hours');
        $exptime = strtotime($time->modify('+' . $orderEditExparationHour . ' hours'));
        if ($CurrentTime < $exptime) {
            return '<a style="padding: 0 7px!important;" class="btn btn-info hint--left  hint" data-hint="Edit order having invoice number-' . $invoiceNumber . '" data-placement="top" href="' . HTTP_ROOT . 'appadmins/edit-order/' . $editId . '"><i class="fa fa-edit"></i></a>';
        } else {
            return '<a style="padding: 0 7px!important;cursor:not-allowed" class="btn btn-info disabled hint--left  hint" data-hint="Edit order having invoice number-' . $invoiceNumber . '" data-placement="top" href="' . HTTP_ROOT . 'appadmins/edit-order/' . $editId . '"><i class="fa fa-edit"></i></a>';
        }
    }

    function showExpDate($orderCreatedDate, $orderEditExparationHour) {
        $time = new FrozenTime($orderCreatedDate);
        $exptime = $time->modify('+' . $orderEditExparationHour . 'hours');
        return date("M d, Y H:i:s", strtotime($exptime));
    }

    function getGraphYearBack($month) {
        // return $month;
        $checkedMonth = array("January", "February", "March", "April", "May");
        if (in_array($month, $checkedMonth)) {
            return date("Y") - 1;
        } else {
            return date("Y");
        }
    }

    function getGraphYearForward($month) {
        // return $month;
        $checkedMonth = array("August", "September", "October", "November", "December");
        if (in_array($month, $checkedMonth)) {
            return date("Y") + 1;
        } else {
            return date("Y");
        }
    }

    function backGroundColor($dueDate, $paidStatus) {
        $time = new FrozenTime(date('Y-m-d'));
        // date_default_timezone_set('Asia/Kolkata');
        $CurrentTime = strtotime(new FrozenTime(date('Y-m-d')));
        //$exptime =$CurrentTime.','.$time. ','.$time->modify('+1 hours');
        $threedays = strtotime($time->modify('+ 3 days'));
        $sevendays = strtotime($time->modify('+ 7 days'));
        //   $sevendays = strtotime($time->modify('+ 8 days'));
        $duedays = strtotime(new FrozenTime($dueDate));
        if ($duedays > $sevendays && $paidStatus == 0 && $duedays >= $CurrentTime) {
            //return 'style=background-color:#ffbdb1;';
            return 'style="border-left:5px solid #fff"';
        }

        if ($duedays <= $threedays && $paidStatus == 0 && $duedays >= $CurrentTime) {
            //return 'style=background-color:#ffbdb1;';
            return 'style="border-left:5px solid #ffbdb1"';
        } if ($duedays <= $sevendays && $paidStatus == 0 && $duedays >= $CurrentTime) {

            return 'style="border-left:5px solid #ffc755"';
        }
        if ($duedays < $CurrentTime && $paidStatus == 0) {

            return 'style="border-left:5px solid #e04444"';
            //return 'style=background-color:#e04444;color:';
        }
        if ($paidStatus == 1) {
            return 'style="border-left:5px solid #83ffc7"';
            // return 'style=background-color:#83ffc7';
        }
    }

//     function backGroundColor($dueDate, $paidStatus) {
//        $time = new FrozenTime(date('Y-m-d'));
//        // date_default_timezone_set('Asia/Kolkata');
//        $CurrentTime = strtotime(new FrozenTime(date('Y-m-d')));
//        //$exptime =$CurrentTime.','.$time. ','.$time->modify('+1 hours');
//        $threedays = strtotime($time->modify('+ 3 days'));
//        $sevendays = strtotime($time->modify('+ 7 days'));
//     //   $sevendays = strtotime($time->modify('+ 8 days'));
//        $duedays = strtotime(new FrozenTime($dueDate));
//        return $duedays.'---'.$CurrentTime;
//     }

    function paginate_function($item_per_page, $current_page, $total_records, $total_pages) {

        //  echo "hii";exit;
        $pagination = '';
        if ($total_pages > 0 && $total_pages != 1 && $current_page <= $total_pages) { //verify total pages and current page number
            $pagination .= '<ul class="pagination">';

            $right_links = $current_page + 3;
            $previous = $current_page - 3; //previous link 
            $next = $current_page + 1; //next link
            $first_link = true; //boolean var to decide our first link

            if ($current_page > 1) {
                $previous_link = ($previous == 0) ? 1 : $previous;
                $pagination .= '<li class="first"><a href="javscript:;" data-page="1" title="First">&laquo;</a></li>'; //first link
                $pagination .= '<li><a href="javscript:;" data-page="' . ($current_page - 1) . '" title="Previous">&lt;</a></li>'; //previous link
                for ($i = ($current_page - 2); $i < $current_page; $i++) { //Create left-hand side links
                    if ($i > 0) {
                        $pagination .= '<li><a href="#" data-page="' . $i . '" title="Page' . $i . '">' . $i . '</a></li>';
                    }
                }
                $first_link = false; //set first link to false
            }

            if ($first_link) { //if current active page is first link
                $pagination .= '<li class="first active">' . $current_page . '</li>';
            } elseif ($current_page == $total_pages) { //if it's the last active link
                $pagination .= '<li class="last active">' . $current_page . '</li>';
            } else { //regular current link
                $pagination .= '<li class="active">' . $current_page . '</li>';
            }

            for ($i = $current_page + 1; $i < $right_links; $i++) { //create right-hand side links
                if ($i <= $total_pages) {
                    $pagination .= '<li><a href="javscript:;" data-page="' . $i . '" title="Page ' . $i . '">' . $i . '</a></li>';
                }
            }
            if ($current_page < $total_pages) {
                $next_link = ($i > $total_pages) ? $total_pages : $i;
                $pagination .= '<li><a href="javscript:;" data-page="' . ($current_page + 1) . '" title="Next">&gt;</a></li>'; //next link
                $pagination .= '<li class="last"><a href="javscript:;" data-page="' . $total_pages . '" title="Last">&raquo;</a></li>'; //last link
            }

            $pagination .= '</ul>';
        }
        return $pagination; //return pagination links
    }

    public function getRating($rating) {
        $rt = round($rating);
        $i = 1;
        while ($i <= $rt) {
            echo '<div class="star"><i class="fas fa-star"></i></div>';
            $i = $i + 1;
        }
        while ($i <= 5) {
            echo '<div class="star"><i class="far fa-star"></i></div>';

            $i = $i + 1;
        }
    }

    public function getDateDiff($inputDate) {
        $curr_date = date_create();
        $date = date_create($inputDate);
        $diff = date_diff($curr_date, $date);
        $format = NULL;
        if ($diff->d > 0) {
            $format = "%a Day" . ($diff->d > 1 ? 's' : '');
        }
        if ($diff->h > 0) {
            $format .= ($format ? ", %h Hour" : "%h Hour") . ($diff->d > 1 ? 's Ago' : ' Ago');
        }
        if (!$format) {
            $format = 'Few Minutes Ago..';
        }

        $diffText = $diff->format($format);
        return $diffText;
    }
    public function getMembershipIcon($membershipID = NULL ,$name) {
        $color = '';
        $membershipID = (int) $membershipID;
        switch ($membershipID){
            case 2:
                $color = '#ce6e39';
                break;
            case 3:
                $color = '#cccccc';
                break;
            case 4:
                $color = '#f1c40f';
                break;
            default :
                return 'FREE';
        }
        $icon = "<i class=\"fa  fa-empire\" style=\"color:$color;font-size:20px;font-family: fontawesome;text-transform: uppercase;\" aria-hidden=\"true\"> $name </i>";
        return $icon;
    }
    
    public function priceFormat($inputNumber) {
        $decimal_format = CURRENCY_SYMBOL .' '. number_format($inputNumber, 2);
        return $decimal_format;
    }
    
    function timeElapsed($datetime, $full = false) {
        $now = new DateTime;
        $ago = new DateTime($datetime);
        $diff = $now->diff($ago);

        $diff->w = floor($diff->d / 7);
        $diff->d -= $diff->w * 7;

        $string = array(
            'y' => 'year',
            'm' => 'month',
            'w' => 'week',
            'd' => 'day',
            'h' => 'hour',
            'i' => 'minute',
            's' => 'second',
        );
        foreach ($string as $k => &$v) {
            if ($diff->$k) {
                $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
            } else {
                unset($string[$k]);
            }
        }

        if (!$full)
            $string = array_slice($string, 0, 1);
        return $string ? implode(', ', $string) . ' ago' : 'just now';
    }
   
    
    public function hideChars($param) {
        if(!empty($param)){
            return str_repeat('*', strlen($param) - 4) . substr($param, -4);
        }
        return '';
    }
    function maskEmail($email) {
        $em   = explode("@",$email);
        $name = implode(array_slice($em, 0, count($em)-1), '@');
        $len  = floor(strlen($name)/2);
        return substr($name,0, $len) . str_repeat('*', $len) . "@" . end($em);   
    }

    function optionsRadio($options){
        return $formattedOptions = array_map(function($option){
            return ['text'=>"<span class=\"checkmark7\">$option</span>$option"];
        }, $options);
    }
    function optionsRadio3($options){
        return $formattedOptions = array_map(function($option){
            return ['text'=>"<span class=\"float-l csm-check\">$option</span>$option"];
        }, $options);
    }
    public function radioCustom($name, array $options = ['class' => NULL, 'value' => NULL]) {
        
        $title = '<label class="c-checkbox-7">';
        $title .= $options['value'];
        $title .= '<input type="radio" checked="checked" name="'.$name.'">';
        $title .= '<span class="checkmark7">';
        $title .= $options['value'];
        $title .= '</span>';
        $title .= '</label>';
        
        return $title;
    }

    
    function optionsRadio2($options){
        return $formattedOptions = array_map(function($option){
            return ['text'=>"<span class=\"checkmark2\">$option</span>$option"];
        }, $options);
    }
     public static function scheduledDateTime($datetime) {
        if (!empty($datetime) && $datetime != "" && $datetime != "0000-00-00 00:00:00") {
            $time = date("d-M-Y", strtotime($datetime)) . " at " . date("H:i \H\\r ", strtotime($datetime));
        } else {
            return false;
        }
        
        return $time;
    }
      public function readConfig($config) {
       return Configure::read($config);
    }
    public function getTransactionStaus($status_code) {
        $all_payment_status = Configure::read('Paypal.payment_status');
        $status_text = array_search($status_code, $all_payment_status);
        return $status_text;
    }
}
