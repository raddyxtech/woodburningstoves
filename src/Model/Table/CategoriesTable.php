<?php

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

class CategoriesTable extends Table {

    public function initialize(array $config) {
        parent::initialize($config);

        $this->setTable('categories');
        $this->setDisplayField('name');
        $this->setPrimaryKey('category_id');

        $this->belongsTo('Deliveries', [
            'foreignKey' => 'delivery_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('ParentCategories', [
            'className' => 'Categories',
            'foreignKey' => 'parent_id'
        ]);
        $this->hasMany('ChildCategories', [
            'className' => 'Categories',
            'foreignKey' => 'parent_id'
        ]);

        $this->hasMany('CategoriesProducts', [
            'foreignKey' => 'category_id',
            'joinType' => 'INNER'
//            'targetForeignKey' => 'category_id',
//            'joinTable' => 'categories_products'
        ]);
        $this->belongsToMany('Products', [
            'foreignKey' => 'category_id',
            'targetForeignKey' => 'product_id',
            'joinTable' => 'categories_products'
        ]);
    }

    public function validationDefault(Validator $validator) {

        $validator
                ->scalar('name')
                ->maxLength('name', 75)
                ->requirePresence('name', 'create')
                ->notEmpty('name');

        $validator
                ->scalar('information')
                ->maxLength('information', 250)
//                ->requirePresence('information', 'create')
                ->notEmpty('information');

        $validator
                ->scalar('baby_cat')
//                ->requirePresence('baby_cat', 'create')
                ->notEmpty('baby_cat');


        return $validator;
    }

}
