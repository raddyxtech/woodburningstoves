<?php

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * DeliveryRegions Model
 *
 * @property \App\Model\Table\UniquesTable|\Cake\ORM\Association\BelongsTo $Uniques
 * @property \App\Model\Table\CountriesTable|\Cake\ORM\Association\BelongsTo $Countries
 * @property \App\Model\Table\DeliveriesTable|\Cake\ORM\Association\HasMany $Deliveries
 *
 * @method \App\Model\Entity\DeliveryRegion get($primaryKey, $options = [])
 * @method \App\Model\Entity\DeliveryRegion newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\DeliveryRegion[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\DeliveryRegion|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\DeliveryRegion|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\DeliveryRegion patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\DeliveryRegion[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\DeliveryRegion findOrCreate($search, callable $callback = null, $options = [])
 */
class DeliveryRegionsTable extends Table {

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config) {
        parent::initialize($config);

        $this->setTable('delivery_regions');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->belongsTo('Countries', [
            'foreignKey' => 'countries_id',
            'joinType' => 'INNER'
        ]);
        $this->hasMany('Deliveries', [
            'foreignKey' => 'delivery_region_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator) {
        $validator
                ->integer('id')
                ->allowEmpty('id', 'create');

        $validator
                ->scalar('name')
                ->maxLength('name', 100)
                ->requirePresence('name', 'create')
                ->notEmpty('name');

        $validator
                ->scalar('postcodes')
                ->requirePresence('postcodes', 'create')
                ->notEmpty('postcodes');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules) {
        $rules->add($rules->existsIn(['countries_id'], 'Countries'));

        return $rules;
    }

}
