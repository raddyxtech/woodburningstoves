<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Countries Model
 *
 * @method \App\Model\Entity\Country get($primaryKey, $options = [])
 * @method \App\Model\Entity\Country newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Country[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Country|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Country|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Country patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Country[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Country findOrCreate($search, callable $callback = null, $options = [])
 */
class CountriesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('countries');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');
        
//        $this->belongsTo('Orders', [
//            'foreignKey' => 'invoice_countryid',
//            'joinType' => 'INNER'
//        ]);
    }

    public function validationDefault(Validator $validator)
    {
        $validator
            ->nonNegativeInteger('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('code_abv')
            ->maxLength('code_abv', 50)
            ->requirePresence('code_abv', 'create')
            ->notEmpty('code_abv');

        $validator
            ->scalar('iso_3166')
            ->maxLength('iso_3166', 50)
            ->requirePresence('iso_3166', 'create')
            ->notEmpty('iso_3166');

        $validator
            ->scalar('name')
            ->maxLength('name', 50)
            ->requirePresence('name', 'create')
            ->notEmpty('name');

        $validator
            ->scalar('code_numeric')
            ->maxLength('code_numeric', 50)
            ->requirePresence('code_numeric', 'create')
            ->notEmpty('code_numeric');

        $validator
            ->scalar('flag')
            ->maxLength('flag', 30)
            ->requirePresence('flag', 'create')
            ->notEmpty('flag');

        $validator
            ->scalar('currency')
            ->maxLength('currency', 50)
            ->requirePresence('currency', 'create')
            ->notEmpty('currency');

        $validator
            ->numeric('tax_rate')
            ->requirePresence('tax_rate', 'create')
            ->notEmpty('tax_rate');

        $validator
            ->integer('is_active')
            ->requirePresence('is_active', 'create')
            ->notEmpty('is_active');

        return $validator;
    }
}
