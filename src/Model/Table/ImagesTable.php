<?php

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

class ImagesTable extends Table {

    public function initialize(array $config) {

        parent::initialize($config);

        $this->setTable('images');
        $this->setDisplayField('title');
        $this->setPrimaryKey('image_id');

        $this->belongsTo('Products', [
            'foreignKey' => FALSE,
            'conditions' => ['Images.item_id = Products.product_id'],
            'joinType' => 'LEFT'
        ]);
        $this->belongsToMany('Brands', [
            'foreignKey' => 'image_id',
            'targetForeignKey' => 'brand_id',
            'joinTable' => 'brands_images'
        ]);
    }

    public function validationDefault(Validator $validator) {
        $validator
                ->nonNegativeInteger('image_id')
                ->allowEmpty('image_id', 'create');

        $validator
                ->scalar('filename')
                ->maxLength('filename', 50)
                ->requirePresence('filename', 'create')
                ->notEmpty('filename');
        $validator
                ->scalar('title')
                ->maxLength('title', 20)
                ->requirePresence('title', 'create')
                ->notEmpty('title');

        $validator
                ->scalar('main_image')
                ->requirePresence('main_image', 'create')
                ->notEmpty('main_image');

        $validator
                ->scalar('hidden')
                ->requirePresence('hidden', 'create')
                ->notEmpty('hidden')
                ->notEmpty('title');

        return $validator;
    }

}
