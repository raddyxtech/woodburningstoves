<?php

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

class ProductsTable extends Table {

    public function initialize(array $config) {
        parent::initialize($config);

        $this->setTable('products');
        $this->setDisplayField('name');
        $this->setPrimaryKey('product_id');
        
        $this->hasOne('ProductVariations', [
            'foreignKey' => 'product_id',
            'joinType' => 'INNER'
        ]);

               
        $this->belongsTo('Groups', [
            'foreignKey' => 'group_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Suppliers', [
            'foreignKey' => 'supplier_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Brands', [
            'foreignKey' => 'brand_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsToMany('Categories', [
            'foreignKey' => 'product_id',
            'targetForeignKey' => 'category_id',
            'joinTable' => 'categories_products'
        ]);
        $this->belongsToMany('Orders', [
            'foreignKey' => 'product_id',
            'targetForeignKey' => 'order_id',
            'joinTable' => 'orders_products'
        ]);
        $this->hasMany('Images', [
            'foreignKey' => 'item_id',
//            'conditions' => ['Products.product_id = Images.item_id'],
            'joinType' => 'INNER'
        ]);
        $this->hasMany('ProductVariations', [
            'foreignKey' => 'product_id', 
            'joinType' => 'INNER'
        ]);
        $this->hasMany('CategoriesProducts', [
            'foreignKey' => 'product_id',
            'joinType' => 'INNER'
        ]);
        $this->hasMany('CategoriesProducts', [
            'foreignKey' => 'product_id',
            'joinType' => 'INNER'
        ]);
        $this->hasMany('OrdersProducts', [
            'foreignKey' => 'product_id',
            'joinType' => 'INNER'
        ]);
    }

    public function validationDefault(Validator $validator) {
        $validator
                ->nonNegativeInteger('product_id');

        $validator
                ->maxLength('name', 30)
                ->requirePresence('name', 'create', "Name Required.")
                ->allowEmpty('name', false);

        $validator
                ->maxLength('name', 30)
                ->requirePresence('name', 'create');

        $validator
                ->scalar('desc_full')
                ->requirePresence('desc_full', 'create')
                ->notEmpty('desc_full');
        ;
        $validator
                ->scalar('keywords')
                ->requirePresence('keywords', 'create')
                ->notEmpty('keywords');

        $validator
                ->scalar('featured')
                ->requirePresence('featured', 'create')
                ->allowEmpty('keywords', false);

        $validator
                ->scalar('desc_full')
                ->requirePresence('desc_full', 'create')
                ->allowEmpty('desc_full', false);
        $validator
                ->scalar('keywords')
                ->requirePresence('keywords', 'create')
                ->allowEmpty('keywords', false);
        $validator
                ->scalar('featured')
                ->notEmpty('featured');

        $validator
                ->scalar('status_new')
                ->requirePresence('status_new', 'create')
                ->notEmpty('status_new');

        $validator
                ->scalar('status_sale')
                ->requirePresence('status_sale', 'create');

        $validator
                ->scalar('status_clearance')
                ->requirePresence('status_clearance', 'create')
                ->notEmpty('status_clearance');

        $validator
                ->scalar('hidden')
                ->requirePresence('hidden', 'create')
                ->notEmpty('hidden');


        $validator
                ->scalar('spec_heatoutput')
                ->maxLength('spec_heatoutput', 20)
                ->requirePresence('spec_heatoutput', 'create');

        $validator
                ->scalar('spec_height')
                ->maxLength('spec_height', 20)
                ->requirePresence('spec_height', 'create');

        $validator
                ->scalar('status_new')
                ->requirePresence('status_new', 'create')
                ->allowEmpty('status_new', false);
        $validator
                ->scalar('status_sale')
                ->requirePresence('status_sale', 'create')
                ->allowEmpty('status_sale', false);
        $validator
                ->scalar('status_clearance')
                ->requirePresence('status_clearance', 'create')
                ->allowEmpty('status_clearance', false);
        $validator
                ->dateTime('date_updated');
//            ->notEmptyDateTime('date_updated');
        $validator
                ->scalar('spec_heatoutput')
                ->maxLength('spec_heatoutput', 20)
                ->requirePresence('spec_heatoutput', 'create')
                ->allowEmpty('spec_heatoutput');
        $validator
                ->scalar('spec_height')
                ->maxLength('spec_height', 20)
                ->requirePresence('spec_height', 'create')
                ->allowEmpty('spec_height');

        $validator
                ->scalar('spec_width')
                ->maxLength('spec_width', 20)
                ->requirePresence('spec_width', 'create');

        $validator
                ->scalar('spec_depth')
                ->maxLength('spec_depth', 20);


        $validator
                ->scalar('spec_fluediameter')
                ->maxLength('spec_fluediameter', 20)
                ->requirePresence('spec_fluediameter', 'create');

        $validator
                ->scalar('spec_misc')
                ->requirePresence('spec_misc', 'create')
                ->notEmpty('spec_misc');

     
        return $validator;
    }

}
