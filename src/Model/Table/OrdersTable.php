<?php

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\ORM\TableRegistry;
use App\Controller\Component\SendEmailComponent;

class OrdersTable extends Table {

    public function initialize(array $config) {
        parent::initialize($config);

        $this->setTable('orders');
        $this->setDisplayField('order_id');
        $this->setPrimaryKey('order_id');

        $this->belongsTo('Customers', [
            'foreignKey' => 'customer_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Trades', [
            'foreignKey' => 'trade_id'
        ]);
        $this->belongsTo('Deliveries', [
            'foreignKey' => 'delivery_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('PaymentMethods', [
            'foreignKey' => 'payment_method_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Responses', [
            'foreignKey' => 'response_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER'
        ]);
        $this->hasMany('OrdersProducts', [
            'foreignKey' => 'order_id',
            'joinType' => 'INNER'
        ]);
//        $this->hasOne('Countries', [
//            'foreignKey' => 'country_id',
//            'joinType' => 'INNER'
//        ]);

//        $this->belongsTo('Products', [
//            'foreignKey' => 'auth_id',
//            'joinTable' => 'orders_products'
//        ]);
//        $this->hasMany('OrdersProducts', [
//            'foreignKey' => 'auth_id',
//            'joinType' => 'INNER'
//        ]);
        $this->hasMany('OrdersProducts', [
            'foreignKey' => 'order_id',
            'joinType' => 'INNER'
        ]);
    }

    public function validationDefault(Validator $validator) {
        $validator
                ->nonNegativeInteger('order_id');

        $validator
                ->scalar('cart_ref')
                ->maxLength('cart_ref', 20);

        $validator
                ->scalar('invoice_title')
                ->maxLength('invoice_title', 4);

        $validator
                ->scalar('invoice_firstname')
                ->maxLength('invoice_firstname', 20);

        $validator
                ->scalar('invoice_middlename')
                ->maxLength('invoice_middlename', 20);

        $validator
                ->scalar('invoice_lastname')
                ->maxLength('invoice_lastname', 20);

        $validator
                ->scalar('invoice_company')
                ->maxLength('invoice_company', 30);
        $validator
                ->scalar('invoice_address2')
                ->maxLength('invoice_address2', 30);

        $validator
                ->scalar('invoice_city')
                ->maxLength('invoice_city', 25);

        $validator
                ->scalar('invoice_county')
                ->maxLength('invoice_county', 25);

        $validator
                ->scalar('invoice_postcode')
                ->maxLength('invoice_postcode', 10);

        $validator
                ->scalar('invoice_tel1')
                ->maxLength('invoice_tel1', 20);

        $validator
                ->scalar('invoice_tel2')
                ->maxLength('invoice_tel2', 20);
        $validator
                ->scalar('delivery_title')
                ->maxLength('delivery_title', 4);

        $validator
                ->scalar('delivery_firstname')
                ->maxLength('delivery_firstname', 20);

        $validator
                ->scalar('delivery_middlename')
                ->maxLength('delivery_middlename', 20);
        $validator
                ->scalar('delivery_company')
                ->maxLength('delivery_company', 30);

        $validator
                ->scalar('delivery_address1')
                ->maxLength('delivery_address1', 30);

        $validator
                ->scalar('delivery_address2')
                ->maxLength('delivery_address2', 30);

        $validator
                ->scalar('delivery_county')
                ->maxLength('delivery_county', 25);

        $validator
                ->scalar('delivery_postcode')
                ->maxLength('delivery_postcode', 10);

        $validator
                ->notEmpty('delivery_countryid');

        $validator
                ->scalar('delivery_tel1')
                ->maxLength('delivery_tel1', 20)
                ->notEmpty('delivery_tel1');
//                ->notEmptyString('delivery_tel1');

        $validator
                ->scalar('delivery_tel2')
                ->maxLength('delivery_tel2', 20)
//                ->requirePresence('delivery_tel2', 'create')
                ->notEmpty('delivery_tel2');
        $validator
                ->scalar('delivery_instructions')
                ->maxLength('delivery_instructions', 100)
                ->notEmpty('delivery_instructions');

        $validator
                ->scalar('priority_dispatch')
                ->notEmpty('priority_dispatch');

        $validator
                ->scalar('priority_dispatch');

        $validator
                ->scalar('ip_address')
                ->maxLength('ip_address', 15)
                ->notEmpty('ip_address');

        $validator
                ->scalar('order_status')
                ->maxLength('order_status', 1)
                ->notEmpty('order_status');

        $validator
//                ->scalar('auth_code')
                ->maxLength('auth_code', 10)
                ->notEmpty('auth_code');

        $validator
                ->scalar('response_code')
//                ->maxLength('response_code', 10)
                ->notEmpty('response_code');

        $validator
                ->scalar('response_msg')
                ->maxLength('response_msg', 255)
                ->notEmpty('response_msg');

        $validator
                ->scalar('response_status')
                ->maxLength('response_status', 50)
                ->notEmpty('response_status');

        $validator
                ->numeric('delivery_cost')
                ->notEmpty('delivery_cost');

        $validator
                ->numeric('priority_dispatch_cost')
                ->notEmpty('priority_dispatch_cost');

        $validator
                ->numeric('surcharge')
                ->notEmpty('surcharge');

        $validator
                ->numeric('total')
                ->notEmpty('total');

        $validator
                ->numeric('delivery_cost')
                ->notEmpty('delivery_cost');

        $validator
                ->numeric('priority_dispatch_cost');


        $validator
                ->dateTime('date_created')
                ->notEmpty('date_created');

        $validator
                ->dateTime('date_placed')
                ->notEmpty('date_placed');

        $validator
                ->dateTime('date_adjusted')
                ->notEmpty('date_adjusted');

        $validator
                ->dateTime('date_dispatched')
                ->notEmpty('date_dispatched');

        $validator
                ->scalar('comments')
//                ->requirePresence('comments', 'create')
                ->notEmpty('comments');

        $validator
                ->numeric('vat_rate')
                ->notEmpty('vat_rate');

        $validator
                ->numeric('vat_rate')
                ->notEmpty('vat_rate');

        return $validator;
    }
     public function sendEmail($order_id, $templateName, $data = []) {
        $status = false;
        try {
            $this->Settings = TableRegistry::getTableLocator()->get('Settings');
            $this->Users = TableRegistry::getTableLocator()->get('Users');
            $this->OrdersProducts = TableRegistry::getTableLocator()->get('OrdersProducts');
            $this->Products = TableRegistry::getTableLocator()->get('Products');
            $this->SendEmail = new SendEmailComponent();
            $this->Custom = new CustomComponent();
            $query = $this->find()->contain(['OrdersProducts.Products'])->where(['Orders.order_id' => $order_id]);
            $adminConfig = $this->Settings->getTemplate('ADMIN_EMAIL');
           
            if (!$query->isEmpty()) {
                
                $transaction = $query->first();
                $template = $this->Settings->getTemplate($templateName);
                
                switch ($template->name) {
                    case 'PAYMENT_EMAIL':
                        
                        $subject = $template->display;
                        $msg = $template->value;
                        
                        $msg = str_replace(array("[NAME]"), $transaction->user->name, $msg);
                        $msg = str_replace(array("[SITE_NAME_TEXT]"), SITE_NAME_TEXT, $msg);
                        foreach($transaction->orders_products as $product){
                        $msg = str_replace(array("[CURR_MEMBERSHIP]"), $product->name, $msg);
                        }
                        $msg = str_replace(array("[PAYMENT_HISTORY_LINK]"), HTTP_ROOT . "payment-history", $msg);
                        $msg = str_replace(array("[SITE_NAME]"), SITE_NAME, $msg);
//                        $msg = str_replace(array("[EXP_DATE]"),$transaction->date_created , $msg);
                        $msg = str_replace(array("[SITE_EMAIL]"), $adminConfig->value, $msg);
                        $msg = str_replace(array("[SITE_LINK]"), HTTP_ROOT, $msg);   
                        
//                        $attachments = file_exists(INVOICES . $transaction->invoice_number . '.pdf') ? [INVOICES . $transaction->invoice_number . '.pdf'] : NULL;
                       
                        if ($this->SendEmail->sendEmail($transaction->user->email, $subject, $msg,$attachments)) {
                            $status = true;
                        }
                        break;
                        
                    case 'PAYMENT_STATUS_EMAIL':
                        
                        $subject = $template->display;
                        $msg = $template->value;
                        $all_payment_status = Configure::read('Paypal.payment_status');
                        $status = array_search($transaction->status, $all_payment_status);
                        $statusText = '<span style="font-weight: 600;background:#fbce0d;">'.$status.'</span>';
                        $msg = str_replace(array("[NAME]"), $transaction->user->vendor_detail->name, $msg);
                        $msg = str_replace(array("[SITE_NAME_TEXT]"), SITE_NAME_TEXT, $msg);
                        $msg = str_replace(array("[TDATE]"), $transaction->date_created->format('M d,Y H:ia'), $msg);
//                        $msg = str_replace(array("[INVOICE_PRICE]"), $this->Custom->priceFormat($transaction->invoice_price), $msg);
                        $msg = str_replace(array("[PAYMENT_STATUS]"), $statusText, $msg);
//                        $msg = str_replace(array("[MEMBERSHIP]"), $transaction->subscription_plan->name, $msg);
                        $msg = str_replace(array("[PAYMENT_HISTORY_LINK]"), HTTP_ROOT . "products/payment-history", $msg);
                        $msg = str_replace(array("[SITE_NAME]"), SITE_NAME, $msg);
                        
                        $msg = str_replace(array("[SITE_EMAIL]"), $adminConfig->value, $msg);
                        $msg = str_replace(array("[SITE_LINK]"), HTTP_ROOT, $msg);   
                        
                        if ($this->SendEmail->sendEmail($transaction->user->email, $subject,$msg)) {
                            $status = true;
                        }
                        break;
      
                    default :
                        break;
                }
            }
        } catch (\Exception $ex) {
           
        }
        return $status;
    }

}
