<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;


class CategoriesProductsTable extends Table
{

 
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('categories_products');
        $this->setDisplayField('cat_prod_id');
        $this->setPrimaryKey('cat_prod_id');

        $this->belongsTo('Categories', [
            'foreignKey' => 'category_id',
            'joinType' => 'LEFT'
        ]);
        $this->belongsTo('Products', [
            'foreignKey' => 'product_id',
            'joinType' => 'INNER'
        ]);
    }

 
    public function validationDefault(Validator $validator)
    {
        $validator
            ->nonNegativeInteger('cat_prod_id')
            ->allowEmpty('cat_prod_id', 'create');

        $validator
            ->nonNegativeInteger('list_sequence')
            ->requirePresence('list_sequence', 'create')
            ->notEmpty('list_sequence');

        return $validator;
    }

    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['category_id'], 'Categories'));
        $rules->add($rules->existsIn(['product_id'], 'Products'));

        return $rules;
    }
}
