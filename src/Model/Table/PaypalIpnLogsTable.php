<?php

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

class PaypalIpnLogsTable extends Table {

    public function initialize(array $config) {
        parent::initialize($config);

        $this->setTable('paypal_ipn_logs');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');
    }

    public function validationDefault(Validator $validator) {
        $validator
                ->integer('id')
                ->allowEmpty('id', 'create');

        $validator
                ->scalar('ipn_data')
                ->requirePresence('ipn_data', 'create')
                ->notEmpty('ipn_data');

        return $validator;
    }

}
