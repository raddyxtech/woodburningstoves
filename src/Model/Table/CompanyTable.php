<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Company Model
 *
 * @method \App\Model\Entity\Company get($primaryKey, $options = [])
 * @method \App\Model\Entity\Company newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Company[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Company|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Company|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Company patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Company[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Company findOrCreate($search, callable $callback = null, $options = [])
 */
class CompanyTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('company');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('company_name')
            ->maxLength('company_name', 30)
            ->requirePresence('company_name', 'create')
            ->notEmpty('company_name');

        $validator
            ->scalar('company_address_1')
            ->maxLength('company_address_1', 30)
            ->requirePresence('company_address_1', 'create')
            ->notEmpty('company_address_1');

//        $validator
//            ->scalar('company_address_2')
//            ->maxLength('company_address_2', 30)
//            ->requirePresence('company_address_2', 'create')
//            ->notEmpty('company_address_2');

        $validator
            ->scalar('company_city')
            ->maxLength('company_city', 25)
            ->requirePresence('company_city', 'create')
            ->notEmpty('company_city');

        $validator
            ->scalar('company_county')
            ->maxLength('company_county', 25)
            ->requirePresence('company_county', 'create')
            ->notEmpty('company_county');

        $validator
            ->scalar('company_postcode')
            ->maxLength('company_postcode', 10)
            ->requirePresence('company_postcode', 'create')
            ->notEmpty('company_postcode');

        $validator
            ->scalar('company_country')
            ->maxLength('company_country', 30)
            ->requirePresence('company_country', 'create')
            ->notEmpty('company_country');

        $validator
            ->scalar('company_tel')
            ->maxLength('company_tel', 20)
            ->requirePresence('company_tel', 'create')
            ->notEmpty('company_tel');

//        $validator
//            ->scalar('company_fax')
//            ->maxLength('company_fax', 20)
//            ->requirePresence('company_fax', 'create')
//            ->notEmpty('company_fax');

        $validator
            ->scalar('company_efax')
            ->maxLength('company_efax', 20)
            ->requirePresence('company_efax', 'create')
            ->notEmpty('company_efax');

//        $validator
//            ->scalar('company_email_sales')
//            ->maxLength('company_email_sales', 50)
//            ->requirePresence('company_email_sales', 'create')
//            ->notEmpty('company_email_sales');

        $validator
            ->scalar('company_email_enquiries')
            ->maxLength('company_email_enquiries', 50)
            ->requirePresence('company_email_enquiries', 'create')
            ->notEmpty('company_email_enquiries');

//        $validator
//            ->scalar('company_email_support')
//            ->maxLength('company_email_support', 50)
//            ->requirePresence('company_email_support', 'create')
//            ->notEmpty('company_email_support');

        $validator
            ->scalar('company_email_misc_1')
            ->maxLength('company_email_misc_1', 50)
            ->requirePresence('company_email_misc_1', 'create')
            ->notEmpty('company_email_misc_1');

        $validator
            ->scalar('company_email_misc_2')
            ->maxLength('company_email_misc_2', 50)
            ->requirePresence('company_email_misc_2', 'create')
            ->notEmpty('company_email_misc_2');

        $validator
            ->scalar('company_number')
            ->maxLength('company_number', 20)
            ->requirePresence('company_number', 'create')
            ->notEmpty('company_number');

        $validator
            ->scalar('company_vat_no')
            ->maxLength('company_vat_no', 20)
            ->requirePresence('company_vat_no', 'create')
            ->notEmpty('company_vat_no');

        return $validator;
    }
}
