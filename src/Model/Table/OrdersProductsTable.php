<?php

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\ORM\TableRegistry;
use App\Controller\Component\SendEmailComponent;

class OrdersProductsTable extends Table {

    public function initialize(array $config) {
        parent::initialize($config);

        $this->setPrimaryKey('id');
        $this->setTable('orders_products');
        $this->setDisplayField('name');

//        $this->belongsTo('Orders', [
//            'foreignKey' => 'auth_id',
//            'joinType' => 'INNER'
//        ]);
        $this->belongsTo('Orders', [
            'foreignKey' => 'order_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Variations', [
            'foreignKey' => 'variation_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Products', [
            'foreignKey' => 'product_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER'
        ]);
    }

    public function validationDefault(Validator $validator) {
        $validator
                ->scalar('code')
                ->maxLength('code', 20)
                ->notEmpty('code');

        $validator
                ->scalar('brand')
                ->maxLength('brand', 20)
                ->notEmpty('brand');

        $validator
                ->scalar('name')
                ->maxLength('name', 30)
                ->notEmpty('name');

//        $validator
//            ->scalar('variations')
//            ->maxLength('variations', 100);
////            ->notEmpty('variations');

        $validator
                ->nonNegativeInteger('qty')
                ->notEmpty('qty');

        $validator
//            ->nonNegativeInteger('qty_dispatched')
                ->notEmpty('qty_dispatched');

        $validator
                ->numeric('unit_price')
                ->notEmpty('unit_price');

        $validator
                ->nonNegativeInteger('weight')
                ->notEmpty('weight');

        $validator
                ->numeric('vat_rate')
                ->notEmpty('vat_rate');

        $validator
                ->scalar('zero_vat')
                ->notEmpty('zero_vat');

        return $validator;
    }

    public function sendEmail($user, $templateName, $userDetails) {
        $this->Settings = TableRegistry::get('Settings');
        $this->SendEmail = new SendEmailComponent();
        $query = $this->find()->contain(['Products.Images'])->where(['OrdersProducts.id' => $user])->first();
        $fileName = '';
        foreach ($query->product->images as $image) :
            $fileName = $image['filename'];
        endforeach;
        if (isset($query) && !empty($query)) {
            $template = $this->Settings->getTemplate($templateName);
            $adminDetails = $this->Users->find()->where(['id' => 1])->first();
            $subject = $template->display;
            $msg = $template->value;
            switch (@$templateName) {
                case 'CANCEL':

                    $msg = str_replace(array("[NAME]"), $userDetails->username, $msg);
                    $msg = str_replace(array("[ORDER_ID]"), $query->order_id, $msg);
                    $msg = str_replace(array("[PRODUCT_NAME]"), $query->name, $msg);
                    $msg = str_replace(array("[QUANTITY]"), $query->qty, $msg);
                    $msg = str_replace(array("[IMAGE]"), HTTP_ROOT . PRODUCT_PIC . $fileName, $msg);
                    $msg = str_replace(array("[PRICE]"), $query->total, $msg);
                    $this->SendEmail->sendEmail($userDetails->email, $subject, $msg);
//                    $this->SendEmail->sendEmail($adminDetails->email, $subject, $msg);
                    break;

                case 'RETURN':

                    $msg = str_replace(array("[NAME]"), $userDetails->username, $msg);
                    $msg = str_replace(array("[ORDER_ID]"), $query->order_id, $msg);
                    $msg = str_replace(array("[PRODUCT_NAME]"), $query->name, $msg);
                    $msg = str_replace(array("[QUANTITY]"), $query->qty, $msg);
                    $msg = str_replace(array("[IMAGE]"), HTTP_ROOT . PRODUCT_PIC . $fileName, $msg);
                    $msg = str_replace(array("[PRICE]"), $query->total, $msg);
                    $this->SendEmail->sendEmail($userDetails->email, $subject, $msg);
//                    $this->SendEmail->sendEmail($adminDetails->email, $subject, $msg);
                    break;

                default :
            }
        }
    }

    public function buildRules(RulesChecker $rules) {
        $rules->add($rules->existsIn(['order_id'], 'Orders'));
        $rules->add($rules->existsIn(['product_id'], 'Products'));
//        $rules->add($rules->existsIn(['variation_id'], 'Variations'));

        return $rules;
    }

}
