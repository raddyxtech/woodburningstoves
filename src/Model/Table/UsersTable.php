<?php

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\ORM\TableRegistry;
use App\Controller\Component\SendEmailComponent;

class UsersTable extends Table {

    
    public function initialize(array $config) {
        parent::initialize($config);

        $this->setTable('users');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

//        $this->belongsTo('Uniques', [
//            'foreignKey' => 'unique_id',
//            'joinType' => 'INNER'
//        ]);
        $this->hasMany('UserLogins', [
            'foreignKey' => 'user_id'
        ]);
        $this->hasMany('Orders', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER'
        ]);
        $this->hasMany('OrdersProducts', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER'
        ]);
    }

    public function validationDefault(Validator $validator) {
        $validator
                ->integer('id')
                ->allowEmpty('id', 'create');

        $validator
                ->email('email')
                ->requirePresence('email', 'create','Email is required')
                ->notEmpty('email','Email should not be empty' );

        $validator
//                ->phone('mobile')
                ->requirePresence('mobile', 'create', 'Phone Number is required')
                ->notEmpty('mobile', 'Mobile number should not be empty');
        
        $validator
                ->scalar('password')
                ->maxLength('password', 250)
                ->requirePresence('password', 'create','Password is required')
                ->notEmpty('password','Password should not be Empty');

        $validator
                ->requirePresence('user_type', 'create')
                ->notEmpty('user_type');

//        $validator
//                ->requirePresence('is_active', 'create')
//                ->notEmpty('is_active');

        $validator
                ->scalar('username')
                ->maxLength('username', 100)
                ->requirePresence('username', 'create')
                ->notEmpty('username');

        $validator
                ->scalar('qstr')
                ->maxLength('qstr', 100)
                ->requirePresence('qstr', 'create')
                ->notEmpty('qstr');
//        $validator
//                ->scalar('g-recaptcha-response')
//                ->requirePresence('g-recaptcha-response', 'create', 'Recaptcha required!!')
//                ->notEmpty('g-recaptcha-response', 'Recaptcha required!!')
//                ->add('g-recaptcha-response', 'validateRecaptcha', [
//                    'on' => 'create',
//                    'rule' => 'validateRecaptcha',
//                    'provider' => 'table',
//                    'message' => 'Invalid recaptcha. Please try again!!'
//        ]);

        return $validator;
    }

    
    public function buildRules(RulesChecker $rules) {
        $rules->add($rules->isUnique(['email']));
        $rules->add($rules->isUnique(['username']));
//        $rules->add($rules->existsIn(['unique_id'], 'Uniques'));

        return $rules;
    }
    public function validateRecaptcha($value, $context = []) {
        $url = 'https://www.google.com/recaptcha/api/siteverify?secret=' . RECAPTCHA_SECRETKEY . '&response=' . $value;
        $verifyResponse = file_get_contents($url);
        $responseData = json_decode($verifyResponse);
        return ($responseData->success);
    }
     public function validationChangePassword(Validator $validator) {
        $validator
                ->integer('id')
                ->requirePresence('id', true, 'Profile not found!!')
                ->notEmpty('id', true, 'Profile not found!!');

        $validator
                ->scalar('curr_password')
                ->requirePresence('curr_password', true, 'Current password required!!')
                ->add('curr_password', 'validatePassword', [
                    'on' => 'update',
                    'rule' => 'validatePassword',
                    'provider' => 'table',
                    'message' => 'Your current password is incorrect!!'
        ]);
        $validator
                ->scalar('password')
                ->requirePresence('password', true, 'New password required')
                ->notEmpty('password', 'New password should not be empty')
                ->maxLength('password', 50, 'New password length should not exceed 50!!');
        $validator
                ->scalar('re_password')
                ->requirePresence('re_password', true, 'Repeat password not found!!')
                ->notEmpty('re_password', 'Please retype password!!')
                ->equalToField('re_password', 'password', 'Retype password did not match!!');

        return $validator;
    }
     public function validatePassword($value, $context) {
        $userID = $context['data']['id'];
        $query = $this->find()->select(['id', 'password'])->where(['id' => $userID]);
        if (!$query->isEmpty()) {
            $oldPassword = $query->first()->password;
            return $this->check($value, $oldPassword);
        }
        return FALSE;
    }
    public function check($password, $hashedPassword) {
        return password_verify($password, $hashedPassword);
    }

public function sendEmail($user, $templateName, $data = []) {
        $this->Settings = TableRegistry::get('Settings');
        $this->SendEmail = new SendEmailComponent();
//        pj($user);
//        pj($templateName);exit;
//        
//        $contain = [];
        $query = $this->find()->where(['Users.id' => $user->id]);

        if (!$query->isEmpty()) {

            $userDetails = $query->first();

            $template = $this->Settings->getTemplate($templateName);
            $subject = $template->display;
            $msg = $template->value;

            switch (@$templateName) {

                case 'WELCOME_EMAIL':

                    $loginLinkBtn = "g<a href='" . HTTP_ROOT . "login" . "' style='background:#F15D22;padding:5px 10px;color:#FFFFFF;text-decoration:none;border-radius:5px 5px 5px 5px;-moz-border-radius:5px 5px 5px 5px;-webkit-border-radius:5px 5px 5px 5px;font-size:14px;font-weight:bold;border:1px solid #F15D22;margin-left:37%;'>Click here to login</a>";
                    $activationLink = HTTP_ROOT . "activate-account/" . $userDetails->unique_id . '/' . $userDetails->qstr;
                    $activationLinkBtn = "<a href='" . $activationLink . "' style='background:#F15D22;padding:5px 10px;color:#FFFFFF;text-decoration:none;border-radius:5px 5px 5px 5px;-moz-border-radius:5px 5px 5px 5px;-webkit-border-radius:5px 5px 5px 5px;font-size:14px;font-weight:bold;border:1px solid #F15D22;margin-left:37%;'>Activate Your Account</a>";

                    $msg = str_replace(array("[NAME]"), $userDetails->name, $msg);
                    $msg = str_replace(array("[SITE_LINK]"), HTTP_ROOT, $msg);
                    $msg = str_replace(array("[SITE_NAME_TEXT]"), SITE_NAME_TEXT, $msg);
                    $msg = str_replace(array("[EMAIL]"), $userDetails->email, $msg);
                    $msg = str_replace(array("[PASSWORD]"), $data['password'], $msg);
                    $msg = str_replace(array("[LINK]"), $loginLinkBtn, $msg);
                    $msg = str_replace(array("[ACTIVATION_LINK]"), $activationLink, $msg);
                    $msg = str_replace(array("[ACTIVATION_LINK_BTN]"), $activationLinkBtn, $msg);

                    $this->SendEmail->sendEmail($userDetails->email, $subject, $msg);
                    break;

                case 'FORGOT_PASSWORD':
                    $link = HTTP_ROOT . "users/resetPassword/{$user->unique_id}/{$user->qstr}";
                    $linkBtn = "<a href='" . $link . "' style='background:#F15D22;padding:5px 10px;color:#FFFFFF;text-decoration:none;border-radius:5px 5px 5px 5px;-moz-border-radius:5px 5px 5px 5px;-webkit-border-radius:5px 5px 5px 5px;font-size:14px;font-weight:bold;border:1px solid #F15D22;margin-left:37%;'>Reset Password</a>";

                    $msg = str_replace(array("[NAME]"), $user->name, $msg);
                    $msg = str_replace(array("[EMAIL]"), $user->email, $msg);

                    $msg = str_replace(array("[LINK]"), $link, $msg);
                    $msg = str_replace(array("[BTN_LINK]"), $linkBtn, $msg);

                    $msg = str_replace(array("[SITE_NAME]"), SITE_NAME, $msg);

                    $this->SendEmail->sendEmail($user->email, $subject, $msg);
                    break;
                
                case 'PASSWORD_RESET':
                    $siteName = "<a href='" . HTTP_ROOT . "'> " . SITE_NAME . " </a>";
                    $link = "<a href='" . HTTP_ROOT . "' style='background:#9e1f63;padding:5px 10px;color:#FFFFFF;text-decoration:none;border-radius:5px 5px 5px 5px;-moz-border-radius:5px 5px 5px 5px;-webkit-border-radius:5px 5px 5px 5px;font-size:14px;font-weight:bold;border:1px solid #9e1f63;margin-left:37%;'>Click here to login</a>";

                    $msg = str_replace(array("[NAME]"), $userDetails->name, $msg);
                    $msg = str_replace(array("[PASSWORD]"), $data['password'], $msg);
                    $msg = str_replace(array("[LINK]"), $link, $msg);
                    $msg = str_replace(array("[SITENAME]", "[SITE_NAME]"), $siteName, $msg);

                    $this->SendEmail->sendEmail($userDetails->email, $subject, $msg);
                    break;

                default :
            }
        }
    }
}
