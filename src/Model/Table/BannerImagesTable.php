<?php

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

class BannerImagesTable extends Table {

    public function initialize(array $config) {
        parent::initialize($config);

        $this->setTable('banner_images');
        $this->setDisplayField('title');
        $this->setPrimaryKey('id');
    }

    public function validationDefault(Validator $validator) {
        $validator
                ->integer('id')
                ->allowEmpty('id', 'create');

        $validator
                ->scalar('title')
                ->requirePresence('title', 'create')
                ->maxLength('title', 200);


        $validator
                ->scalar('description')
                ->requirePresence('description', 'create')
                ->maxLength('description', 500);



        return $validator;
    }

}
