<?php

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

class ProductReviewsTable extends Table {

    public function initialize(array $config) {
        parent::initialize($config);

        $this->setTable('product_reviews');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Products', [
            'foreignKey' => 'product_id',
            'joinType' => 'INNER'
        ]);
    }

    public function validationDefault(Validator $validator) {
        $validator
                ->integer('id')
                ->allowEmpty('id', 'create');

        $validator
                ->scalar('name')
                ->maxLength('name', 60)
                ->requirePresence('name', 'create')
                ->notEmpty('name');

        $validator
                ->email('email')
                ->requirePresence('email', 'create')
                ->notEmpty('email');

        $validator
                ->requirePresence('price_rating', 'create')
                ->notEmpty('price_rating');
        
        $validator
                ->requirePresence('date')
                ->notEmpty('date');

        $validator
                ->allowEmpty('quality_rating');

        $validator
                ->scalar('review')
                ->requirePresence('review', 'create')
                ->notEmpty('review');

        $validator
//            ->requirePresence('is_published', 'create')
                ->notEmpty('is_published');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
//    public function buildRules(RulesChecker $rules)
//    {
//        $rules->add($rules->existsIn(['product_id'], 'Products'));
//
//        return $rules;
//    }
}
