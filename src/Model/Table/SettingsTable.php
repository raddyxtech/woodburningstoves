<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

class SettingsTable extends Table {


    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('settings');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');
    }

    public function validationDefault(Validator $validator){
        $validator
            ->scalar('value')
            ->notEmpty('value', 'Enter Some Template Content');
        
        return $validator;
    }
    
    public function getTemplate($templateName = NULL) {
        try {
            $template = $this->find()->where(['name' => $templateName])->first();
        } catch (Exception $ex) {
            
        }
        return $template;
    }

}
