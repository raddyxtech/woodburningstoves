<?php

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Deliveries Model
 *
 * @property \App\Model\Table\UniquesTable|\Cake\ORM\Association\BelongsTo $Uniques
 * @property \App\Model\Table\DeliveryRegionsTable|\Cake\ORM\Association\BelongsTo $DeliveryRegions
 *
 * @method \App\Model\Entity\Delivery get($primaryKey, $options = [])
 * @method \App\Model\Entity\Delivery newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Delivery[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Delivery|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Delivery|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Delivery patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Delivery[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Delivery findOrCreate($search, callable $callback = null, $options = [])
 */
class DeliveriesTable extends Table {

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config) {
        parent::initialize($config);

        $this->setTable('deliveries');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->belongsTo('Uniques', [
            'foreignKey' => 'unique_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('DeliveryRegions', [
            'foreignKey' => 'delivery_region_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator) {
        $validator
                ->integer('id')
                ->allowEmpty('id', 'create');

        $validator
                ->scalar('courier')
                ->maxLength('courier', 100)
                ->requirePresence('courier', 'create')
                ->notEmpty('courier');

        $validator
                ->scalar('name')
                ->maxLength('name', 100)
                ->requirePresence('name', 'create')
                ->notEmpty('name');

        $validator
                ->integer('min_weight')
                ->requirePresence('min_weight', 'create')
                ->notEmpty('min_weight');

        $validator
                ->integer('max_weight')
                ->requirePresence('max_weight', 'create')
                ->notEmpty('max_weight');

        $validator
                ->numeric('min_value')
                ->requirePresence('min_value', 'create')
                ->notEmpty('min_value');

        $validator
                ->numeric('max_value')
                ->requirePresence('max_value', 'create')
                ->notEmpty('max_value');

        $validator
                ->numeric('cost')
                ->requirePresence('cost', 'create')
                ->notEmpty('cost');

        $validator
                ->allowEmpty('delivery_free');

        $validator
                ->requirePresence('is_active', 'create')
                ->notEmpty('is_active');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules) {
        $rules->add($rules->existsIn(['delivery_region_id'], 'DeliveryRegions'));

        return $rules;
    }

}
