<?php

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

class ProductVariationsTable extends Table {

    public function initialize(array $config) {
        parent::initialize($config);

        $this->setTable('product_variations');
        $this->setDisplayField('variation_id');
        $this->setPrimaryKey('variation_id');
//        $this->belongsTo('Products', [
//            'foreignKey' => 'product_id',
//            'joinType' => 'INNER',
//            'associated' => 'product'
//        ]);
         $this->belongsTo('Products', [
            'className' => 'Products',
            'foreignKey' => 'product_id',
            'joinType' => 'INNER'
        ]);
    }
    
    public function validationDefault(Validator $validator) {
        $validator
                ->nonNegativeInteger('variation_id')
                ->allowEmpty('variation_id', 'create');

        $validator
                ->scalar('code')
                ->maxLength('code', 20)
                ->allowEmpty('code');

        $validator
                ->nonNegativeInteger('qty_in_stock')
                ->allowEmpty('qty_in_stock');

        $validator
                ->nonNegativeInteger('low_stock_level')
                ->allowEmpty('low_stock_level');

        $validator
                ->nonNegativeInteger('weight')
                ->allowEmpty('weight');

        $validator
                ->scalar('zero_vat')
                ->allowEmpty('zero_vat');

        $validator
                ->scalar('out_of_stock')
                ->allowEmpty('out_of_stock');

        $validator
                ->numeric('retail_price')
                ->allowEmpty('retail_price');

        $validator
                ->scalar('hidden')
                ->allowEmpty('hidden');

        $validator
                ->scalar('var_name_1')
                ->maxLength('var_name_1', 20)
                ->allowEmpty('var_name_1');

        $validator
                ->scalar('var_data_1')
                ->maxLength('var_data_1', 20)
                ->allowEmpty('var_data_1');

        $validator
                ->scalar('var_name_2')
                ->maxLength('var_name_2', 20)
                ->allowEmpty('var_name_2');

        $validator
                ->scalar('var_data_2')
                ->maxLength('var_data_2', 20)
                ->allowEmpty('var_data_2');

        $validator
                ->scalar('var_name_3')
                ->maxLength('var_name_3', 20)
                ->allowEmpty('var_name_3');

        $validator
                ->scalar('var_data_3')
                ->maxLength('var_data_3', 20)
                ->allowEmpty('var_data_3');


        return $validator;
    }

}
