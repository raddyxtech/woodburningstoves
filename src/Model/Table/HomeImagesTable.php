<?php

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * HomeImages Model
 *
 * @property \App\Model\Table\UniquesTable|\Cake\ORM\Association\BelongsTo $Uniques
 * @property \App\Model\Table\ItemsTable|\Cake\ORM\Association\BelongsTo $Items
 *
 * @method \App\Model\Entity\HomeImage get($primaryKey, $options = [])
 * @method \App\Model\Entity\HomeImage newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\HomeImage[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\HomeImage|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\HomeImage|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\HomeImage patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\HomeImage[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\HomeImage findOrCreate($search, callable $callback = null, $options = [])
 */
class HomeImagesTable extends Table {

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config) {
        parent::initialize($config);

        $this->setTable('home_images');
        $this->setDisplayField('title');
        $this->setPrimaryKey('id');

        $this->belongsTo('Items', [
            'foreignKey' => 'item_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator) {
        $validator
                ->nonNegativeInteger('id')
                ->allowEmpty('id', 'create');

        $validator
                ->scalar('title')
                ->maxLength('title', 20)
                ->requirePresence('title', 'create')
                ->notEmpty('title');

        return $validator;
    }

}
