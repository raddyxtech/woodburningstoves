<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * DeliveryRegion Entity
 *
 * @property int $id
 * @property string $unique_id
 * @property string $name
 * @property int $countries_id
 * @property string $postcodes
 *
 * @property \App\Model\Entity\Unique $unique
 * @property \App\Model\Entity\Country $country
 * @property \App\Model\Entity\Delivery[] $deliveries
 */
class DeliveryRegion extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'unique_id' => true,
        'name' => true,
        'countries_id' => true,
        'postcodes' => true,
        'unique' => true,
        'country' => true,
        'deliveries' => true
    ];
}
