<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Delivery Entity
 *
 * @property int $id
 * @property string $unique_id
 * @property int $delivery_region_id
 * @property string $courier
 * @property string $name
 * @property int $min_weight
 * @property int $max_weight
 * @property float $min_value
 * @property float $max_value
 * @property float $cost
 * @property int|null $delivery_free
 * @property int $is_active
 *
 * @property \App\Model\Entity\Unique $unique
 * @property \App\Model\Entity\DeliveryRegion $delivery_region
 */
class Delivery extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'unique_id' => true,
        'delivery_region_id' => true,
        'courier' => true,
        'name' => true,
        'min_weight' => true,
        'max_weight' => true,
        'min_value' => true,
        'max_value' => true,
        'cost' => true,
        'delivery_free' => true,
        'is_active' => true,
        'unique' => true,
        'delivery_regions' => true
    ];
}
