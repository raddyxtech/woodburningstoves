<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Company Entity
 *
 * @property int $id
 * @property string $company_name
 * @property string $company_address_1
 * @property string $company_address_2
 * @property string $company_city
 * @property string $company_county
 * @property string $company_postcode
 * @property string $company_country
 * @property string $company_tel
 * @property string $company_fax
 * @property string $company_efax
 * @property string $company_email_sales
 * @property string $company_email_enquiries
 * @property string $company_email_support
 * @property string $company_email_misc_1
 * @property string $company_email_misc_2
 * @property string $company_number
 * @property string $company_vat_no
 */
class Company extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'company_name' => true,
        'company_address_1' => true,
        'company_address_2' => true,
        'company_city' => true,
        'company_county' => true,
        'company_postcode' => true,
        'company_country' => true,
        'company_tel' => true,
        'company_fax' => true,
        'company_efax' => true,
        'company_email_sales' => true,
        'company_email_enquiries' => true,
        'company_email_support' => true,
        'company_email_misc_1' => true,
        'company_email_misc_2' => true,
        'company_number' => true,
        'company_vat_no' => true
    ];
}
