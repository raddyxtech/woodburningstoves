<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * ProductVariation Entity
 *
 * @property int $variation_id
 * @property int $product_id
 * @property string $code
 * @property int $qty_in_stock
 * @property int $low_stock_level
 * @property int $weight
 * @property string $zero_vat
 * @property string $out_of_stock
 * @property float $retail_price
 * @property string $hidden
 * @property string $var_name_1
 * @property string $var_data_1
 * @property string $var_name_2
 * @property string $var_data_2
 * @property string $var_name_3
 * @property string $var_data_3
 *
 * @property \App\Model\Entity\Product $product
 */
class ProductVariation extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'product_id' => true,
        'code' => true,
        'qty_in_stock' => true,
        'low_stock_level' => true,
        'weight' => true,
        'zero_vat' => true,
        'out_of_stock' => true,
        'retail_price' => true,
        'hidden' => true,
        'var_name_1' => true,
        'var_data_1' => true,
        'var_name_2' => true,
        'var_data_2' => true,
        'var_name_3' => true,
        'var_data_3' => true,
        'product' => true
    ];
}
