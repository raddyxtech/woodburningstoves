<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * PaypalIpnLog Entity
 *
 * @property int $id
 * @property string $ipn_data
 * @property \Cake\I18n\FrozenTime $created
 */
class PaypalIpnLog extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'ipn_data' => true,
        'created' => true
    ];
}
