<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;


class OrdersProduct extends Entity
{
    
    protected $_accessible = [
        'id' => true,
        'auth_id' => true,
        'product_id' => true,
        'variation_id' => true,
        'code' => true,
        'brand' => true,
        'name' => true,
        'variations' => true,
        'qty' => true,
        'total' => true,
        'qty_dispatched' => true,
        'unit_price' => true,
        'weight' => true,
        'vat_rate' => true,
        'zero_vat' => true,
//        'order' => true,
//        'product' => true,
//        'variation' => true
    ];
}
