<?php

namespace App\Model\Entity;

use Cake\ORM\Entity;

class Product extends Entity {

    protected $_accessible = [
        'group_id' => true,
        'supplier_id' => true,
        'brand_id' => true,
        'name' => true,
        'desc_brief' => true,
        'desc_full' => true,
        'key_features' => true,
        'keywords' => true,
        'featured' => true,
        'hazardous' => true,
        'status_new' => true,
        'status_sale' => true,
        'status_clearance' => true,
        'hidden' => true,
        'date_updated' => true,
        'spec_heatoutput' => true,
        'spec_height' => true,
        'spec_width' => true,
        'spec_depth' => true,
        'spec_approxweight' => true,
        'spec_fluediameter' => true,
        'spec_misc' => true,
        'addons' => true,
        'brand' => true,
        'product_variation' => true,
        'images' => true,
        'group' => true,
        'supplier' => true,
        'brand' => true,
        'created' => true
    ];

}
