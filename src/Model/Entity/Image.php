<?php

namespace App\Model\Entity;

use Cake\ORM\Entity;

class Image extends Entity {

    protected $_accessible = [
        'item_id' => true,
        'filename' => true,
        'title' => true,
        'main_image' => true,
        'hidden' => true,
        'item' => true
    ];

}
