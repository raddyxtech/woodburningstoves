<?php

namespace App\Model\Entity;

use Cake\ORM\Entity;
use Cake\Auth\DefaultPasswordHasher;

class User extends Entity {

    protected $_accessible = [
        'unique_id' => true,
        'email' => true,
        'password' => true,
        'user_type' => true,
        'is_active' => true,
        'username' => true,
        'mobile' => true,
        'qstr' => true,
        'created' => true,
        'modified' => true,
        'unique' => true,
        'user_logins' => true
    ];

    protected function _setPassword($password) {
        return (new DefaultPasswordHasher)->hash($password);
    }

}
