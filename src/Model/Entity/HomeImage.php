<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * HomeImage Entity
 *
 * @property int $id
 * @property string $unique_id
 * @property int $item_id
 * @property string $filename
 * @property string $title
 * @property string $main_image
 * @property int $is_active
 *
 * @property \App\Model\Entity\Unique $unique
 * @property \App\Model\Entity\Item $item
 */
class HomeImage extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'unique_id' => true,
        'item_id' => true,
        'filename' => true,
        'title' => true,
        'main_image' => true,
        'is_active' => true,
        'unique' => true,
        'item' => true
    ];
}
