<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Country Entity
 *
 * @property int $id
 * @property string $code_abv
 * @property string $iso_3166
 * @property string $name
 * @property string $code_numeric
 * @property string $flag
 * @property string $currency
 * @property float $tax_rate
 * @property int $is_active
 */
class Country extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'code_abv' => true,
        'iso_3166' => true,
        'name' => true,
        'code_numeric' => true,
        'flag' => true,
        'currency' => true,
        'tax_rate' => true,
        'is_active' => true
    ];
}
