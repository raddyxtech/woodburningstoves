<?php

namespace App\Model\Entity;

use Cake\ORM\Entity;

class Category extends Entity {

    protected $_accessible = [

        'category_id' => true,
        'group_id' => true,
        'delivery_id' => true,
        'parent_id' => true,
        'name' => true,
        'information' => true,
        'baby_cat' => true,
        'group' => true,
        'delivery' => true,
        'parent_category' => true,
        'child_categories' => true,
        'baby_cat' => true,
        'products' => true
    ];

}
