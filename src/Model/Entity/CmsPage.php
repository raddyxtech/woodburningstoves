<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * CmsPage Entity
 *
 * @property int $id
 * @property string $unique_id
 * @property string $title
 * @property string $tag
 * @property string $meta_desc
 * @property string $header_tag
 * @property string $content
 * @property string $display_name
 * @property string|null $url
 * @property \Cake\I18n\FrozenTime $created
 */
class CmsPage extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'unique_id' => true,
        'title' => true,
        'tag' => true,
        'meta_desc' => true,
        'header_tag' => true,
        'content' => true,
        'display_name' => true,
        'url' => true,
        'created' => true
    ];
}
