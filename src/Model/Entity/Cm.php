<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Cm Entity
 *
 * @property int $id
 * @property string $name
 * @property string $value1
 * @property string|null $value2
 * @property string|null $type
 * @property \Cake\I18n\FrozenTime $created
 */
class Cm extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'name' => true,
        'value1' => true,
        'value2' => true,
        'type' => true,
        'created' => true
    ];
}
