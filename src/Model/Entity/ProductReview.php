<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * ProductReview Entity
 *
 * @property int $id
 * @property int $product_id
 * @property string $name
 * @property string $email
 * @property int $price_rating
 * @property int|null $quality_rating
 * @property string $review
 * @property int $is_published
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \App\Model\Entity\Product $product
 */
class ProductReview extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'user_id' => true,
        'product_id' => true,
        'name' => true,
        'email' => true,
        'price_rating' => true,
        'quality_rating' => true,
        'review' => true,
        'is_published' => true,
        'date' => true,
        'created' => true,
        'modified' => true,
        'product' => true
    ];
}
