<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Brand Entity
 *
 * @property int $id
 * @property string $unique_id
 * @property string $name
 * @property string $image
 * @property int $is_active
 *
 * @property \App\Model\Entity\Unique $unique
 */
class Brand extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'unique_id' => true,
        'name' => true,
        'image' => true,
        'is_active' => true,
        'unique' => true
    ];
}
